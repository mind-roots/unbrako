package com.unbrako.specializedCoating

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class HotGalvSpecialActivity : AppCompatActivity() {

    private lateinit var toolbar10: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hot_galv_special)
        toolbar10 = findViewById(R.id.toolbar_edit10)
        setSupportActionBar(toolbar10)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
      //  title = "Hot Dip Galvanizing"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
