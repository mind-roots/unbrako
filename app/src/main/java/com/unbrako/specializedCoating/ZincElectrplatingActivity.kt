package com.unbrako.specializedCoating

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class ZincElectrplatingActivity : AppCompatActivity() {
    private lateinit var toolbar5: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zinc_electrplating)
        toolbar5 = findViewById(R.id.toolbar_edit5)
        setSupportActionBar(toolbar5)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
      //  title = "Zinc-Electroplating"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
