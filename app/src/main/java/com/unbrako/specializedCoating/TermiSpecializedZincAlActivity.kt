package com.unbrako.specializedCoating

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class TermiSpecializedZincAlActivity : AppCompatActivity() {
    private lateinit var toolbar6: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_termi_specialized_zinc_al)
        toolbar6 = findViewById(R.id.toolbar_edit6)
        setSupportActionBar(toolbar6)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
       // title = "Zinc-AI Flake Coating"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
