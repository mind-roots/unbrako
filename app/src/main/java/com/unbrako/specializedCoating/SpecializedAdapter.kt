package com.unbrako.specializedCoating

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.unbrako.R
import kotlinx.android.synthetic.main.fastener_recycler_card.view.*

class SpecializedAdapter(private val mContext: Context, val list: ArrayList<SpecializedModel>) :
    androidx.recyclerview.widget.RecyclerView.Adapter<SpecializedAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, itemView: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.specialized_recycler_card, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.textview_card_title_fastener.text = model.imageName
        holder.itemView.imageview_card_fastener.setImageResource(model.image)


//        Picasso.with(mContext).load(R.drawable.card_img)
//            .resize(150, 150)
//            .centerCrop()
//            .into(holder.itemView.imageview_card)

        holder.itemView.setOnClickListener {
            when (position) {

                0 -> {
                    val intent = Intent(mContext, HotGalvSpecialActivity::class.java)
                    mContext.startActivity(intent)
                }
                1 -> {
                    val intent = Intent(mContext, ZincElectrplatingActivity::class.java)
                    mContext.startActivity(intent)
                }
                2 -> {
                    val intent = Intent(mContext, MechanicalGalvanizedActivity::class.java)
                    mContext.startActivity(intent)
                }
                3 -> {
                    val intent = Intent(mContext, TermiSpecializedZincAlActivity::class.java)
                    mContext.startActivity(intent)
                }
            }

        }
    }

    class MyViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

}
