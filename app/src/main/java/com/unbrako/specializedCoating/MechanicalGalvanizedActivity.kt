package com.unbrako.specializedCoating

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class MechanicalGalvanizedActivity : AppCompatActivity() {
    private lateinit var toolbar16: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mechanical_galvanized)
        toolbar16 = findViewById(R.id.toolbar_edit16)
        setSupportActionBar(toolbar16)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
       // title = "Mechanical Galvanized"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
