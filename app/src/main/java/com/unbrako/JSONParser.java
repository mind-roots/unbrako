package com.unbrako;

import android.content.Context;
import android.net.Uri;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

public class JSONParser {
    // constructor
    public JSONParser() {

    }

/*             key value pairs
            builder = new Uri.Builder()
                    .appendQueryParameter("auth_code", auth_code);*/


    public String getJSONFromUrl(String urlAddress, Uri.Builder builder, Context context) {
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
       try {
           URL url = new URL(urlAddress);
           HttpURLConnection con = (HttpURLConnection) url
                   .openConnection();

            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("accept", "application/json");
//            con.setRequestProperty("X-Api-Key", "f246820b2daf20e6bb6a1aec341a9414");
//            con.setRequestProperty("X-Auth-Token", "df432af2632933bdd68073d7511cff68");
            con.setRequestProperty("X-Api-Key", "86524a9d14d203c840b7c044d790d989");
            con.setRequestProperty("X-Auth-Token", "8090595ff536a780a4cc0306bf2acb23");
            con.setReadTimeout(15000);
            con.setConnectTimeout(15000);
            con.setRequestMethod("POST");
            con.setDoInput(true);
            con.setDoOutput(true);

            String query = builder.build().getEncodedQuery();
           con.connect();
            //adds key value pair to your request
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            in = con.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    in, "iso-8859-1"), 8);
            String line;
            while ((line = reader.readLine()) != null) {
                tempData.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return tempData.toString();
    }

 public String getJSONFromUrl1(String urlAddress, Uri.Builder builder) {
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
       try {
           URL url = new URL(urlAddress);
           HttpURLConnection con = (HttpURLConnection) url
                   .openConnection();

            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            con.setRequestProperty("Content-Type", "application/json");
            con.setReadTimeout(60000);
            con.setConnectTimeout(60000);
            con.setRequestMethod("POST");
            con.setDoInput(true);
            con.setDoOutput(true);

            String query = builder.build().getEncodedQuery();
           con.connect();
            //adds key value pair to your request
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            in = con.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    in, "iso-8859-1"), 8);
            String line;
            while ((line = reader.readLine()) != null) {
                tempData.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return tempData.toString();
    }


    /*             key value pairs
                builder = new Uri.Builder()
                        .appendQueryParameter("auth_code", auth_code);*/
    public String getresFromUrl(String urlAddress, Uri.Builder builder) {
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;

        try {
            URL url = new URL(urlAddress);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            con.setRequestProperty("Content-Type", "application/json");
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            String query = builder.build().getEncodedQuery();

            //adds key value pair to your request
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();


            int responseCode = connection.getResponseCode();
            if(responseCode == 200) {
                // response code is OK
                in = connection.getInputStream();
            }else{
                // response code is not OK
            }

            // in = url.openStream(); remove this line
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(in,"iso-8859-1"),8);
            String line;
            while ((line = reader.readLine()) != null) {
                tempData.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return tempData.toString();
    }
}