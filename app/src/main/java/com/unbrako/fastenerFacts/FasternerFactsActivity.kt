package com.unbrako.fastenerFacts

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class FasternerFactsActivity : AppCompatActivity() {
    private lateinit var toolbar21: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_facts_fastener)
        toolbar21 = findViewById(R.id.toolbar_edit21)
        setSupportActionBar(toolbar21)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
      //  title = "Fastener Facts"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}

