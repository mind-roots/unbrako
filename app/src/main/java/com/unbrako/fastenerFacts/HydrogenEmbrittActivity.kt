package com.unbrako.fastenerFacts

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class HydrogenEmbrittActivity : AppCompatActivity() {
    private lateinit var toolbar22: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hydrogen_embritt)
        toolbar22 = findViewById(R.id.toolbar_edit22)
        setSupportActionBar(toolbar22)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //title = "Hydrogen Embrittlement"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)

    }
}
