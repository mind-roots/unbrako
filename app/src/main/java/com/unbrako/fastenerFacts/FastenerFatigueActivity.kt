package com.unbrako.fastenerFacts

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class FastenerFatigueActivity : AppCompatActivity() {
    private lateinit var toolbar20: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fastener_fatigue)
        toolbar20 = findViewById(R.id.toolbar_edit20)
        setSupportActionBar(toolbar20)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //title = "Fastener Fatigue"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}

