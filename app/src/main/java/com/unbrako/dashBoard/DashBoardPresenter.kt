package com.unbrako.dashBoard

import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.dataBase.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashBoardPresenter(var view: DashBoardContract.View) {
    fun getAllValues(date_time: String?, s: String) {

        if (s=="0") {

            view.showLoader()
        }
        val retrofit = Constants.getWebClient()
        val service = retrofit!!.create(DashBoardContract.Service::class.java)
        val call: Call<ResponseBody> = service.getAllData(date_time)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val `object` = JSONObject(res)
                        val date_time = `object`.optString("date_time")
                        val success = `object`.optString("success")


                        if (success.equals("true", ignoreCase = true)) {
                            val result = `object`.optJSONObject("result")
                            saveResultToDataBase(result)
                            view.showDateTime(date_time)
                            } else {

                               }

                    } catch (e: Exception) {
                        view.hideLoader()
                        e.printStackTrace()
                    } catch (e: JSONException) {
                        view.hideLoader()
                        e.printStackTrace()
                    }

                }else{
                    view.hideLoader()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
               view.hideLoader()
                view.showNetworkError(R.string.network_error)

            }
        })
    }

    private fun saveResultToDataBase(result: JSONObject?){
        val products=result!!.getJSONArray("Products")
        val productMetric= result.getJSONArray("ProductMetric")
        val productInch= result.getJSONArray("ProductInch")
        val metricBox= result.getJSONArray("MetricBox")
        val inchBox= result.getJSONArray("InchBox")


        val productsArray = ArrayList<Products>()
        val productMetricArray = ArrayList<ProductMetric>()
        val productInchArray = ArrayList<ProductInch>()
        val metricBoxArray = ArrayList<MetricBox>()
        val inchBoxArray = ArrayList<InchBox>()
        for (i in 0 until products.length()){
            val productObject=products.getJSONObject(i)
            val productModel=Products()
            productModel.ProductID=productObject.optString("ProductID")
            productModel.name=productObject.optString("name")
            productModel.prod_image=productObject.optString("prod_image")
            productsArray.add(productModel)

        }



   for (i in 0 until productMetric.length()){
            var productObject=productMetric.getJSONObject(i)
            var productModel= ProductMetric()
            productModel.pro_det_id=productObject.optString("pro_det_id").trim()
            productModel.ProductID=productObject.optString("ProductID").trim()
//       if (productModel.ProductID=="33"){
//           productModel.Diameter = productObject.optString("Diameter").trim()
//           productModel.AFNom = (productObject.optString("AFNom"))
//           productModel.HeadODMax = (productObject.optString("HeadODMax"))
//           productModel.HeadOdMin = (productObject.optString("HeadOdMin"))
//           productModel.HeadHeightMax = (productObject.optString("HeadHeightMax"))
//           productModel.ThreadLengthRef = (productObject.optString("ThreadLengthRef"))
//           productModel.Grade = (productObject.optString("Grade"))
//           productModel.Thread = (productObject.optString("Thread"))
//           productModel.Pitch =(productObject.optString("Pitch"))
//           productModel.SocketDepth = (productObject.optString("SocketDepth"))
//           productModel.TensileStressArea =(productObject.optString("TensileStressArea"))
//           productModel.NominalShankArea = (productObject.optString("NominalShankArea"))
//           productModel.SeatingTorqueNm = (productObject.optString("SeatingTorqueNm"))
//           productModel.SeatingTorqueInlbs = (productObject.optString("SeatingTorqueInlbs"))
//           productModel.TensileStrengthkN = (productObject.optString("TensileStrengthkN"))
//           productModel.TensileStrengthlbs = (productObject.optString("TensileStrengthlbs"))
//           productModel.CtrBore = (productObject.optString("CtrBore"))
//           productModel.BodyDrill =(productObject.optString("BodyDrill"))
//           productModel.TapDrill = (productObject.optString("TapDrill"))
//           productModel.BodyODMax = (productObject.optString("BodyODMax"))
//           productModel.BodyODMin = (productObject.optString("BodyODMin"))
//           productModel.ShearStrengthOfBody = (productObject.optString("ShearStrengthOfBody"))
//           productModel.ScrewLengthMin = (productObject.optString("ScrewLengthMin"))
//           productModel.Teeth = (productObject.optString("teeth"))
//           productModel.D1MIN = (productObject.optString("D1MIN"))
//           productModel.D2MAX = (productObject.optString("D2MAX"))
//           productModel.HMAX = (productObject.optString("HMAX"))
//           productModel.HMIN = (productObject.optString("HMIN"))
//           productModel.hardness = (productObject.optString("hardness"))
//           productModel.last_modified_time = productObject.optString("last_modified_time")
//           productModel.webStatus = (productObject.optString("status"))
//           productModel.status = "false"
//       }else {
           productModel.Diameter = productObject.optString("Diameter").trim()
           productModel.AFNom = Constants.formatValues(productObject.optString("AFNom"))
           productModel.HeadODMax = Constants.formatValues(productObject.optString("HeadODMax"))
           productModel.HeadOdMin = Constants.formatValues(productObject.optString("HeadOdMin"))
           productModel.HeadHeightMax = Constants.formatValues(productObject.optString("HeadHeightMax"))
           productModel.ThreadLengthRef = Constants.formatValues(productObject.optString("ThreadLengthRef"))
           productModel.Grade = Constants.formatValuesForGrades(productObject.optString("Grade"))
           productModel.Thread = Constants.formatValues(productObject.optString("Thread"))
           productModel.Pitch = Constants.formatValues(productObject.optString("Pitch"))
           productModel.SocketDepth = Constants.formatValues(productObject.optString("SocketDepth"))
           productModel.TensileStressArea = Constants.formatValues(productObject.optString("TensileStressArea"))
           productModel.NominalShankArea = Constants.formatValues(productObject.optString("NominalShankArea"))
           productModel.SeatingTorqueNm = Constants.formatValues(productObject.optString("SeatingTorqueNm"))
           productModel.SeatingTorqueInlbs = Constants.formatValues(productObject.optString("SeatingTorqueInlbs"))
           productModel.TensileStrengthkN = Constants.formatValues(productObject.optString("TensileStrengthkN"))
           productModel.TensileStrengthlbs = Constants.formatValues(productObject.optString("TensileStrengthlbs"))
           productModel.CtrBore = Constants.formatValues(productObject.optString("CtrBore"))
           productModel.BodyDrill = Constants.formatValues(productObject.optString("BodyDrill"))
           productModel.TapDrill = Constants.formatValues(productObject.optString("TapDrill"))
           productModel.BodyODMax = Constants.formatValues(productObject.optString("BodyODMax"))
           productModel.BodyODMin = Constants.formatValues(productObject.optString("BodyODMin"))
           productModel.ShearStrengthOfBody = Constants.formatValues(productObject.optString("ShearStrengthOfBody"))
           productModel.ScrewLengthMin = Constants.formatValues(productObject.optString("ScrewLengthMin"))
           productModel.Teeth = Constants.formatValues(productObject.optString("teeth"))
           productModel.D1MIN = Constants.formatValues(productObject.optString("D1MIN"))
           productModel.D2MAX = Constants.formatValues(productObject.optString("D2MAX"))
           productModel.HMAX = Constants.formatValues(productObject.optString("HMAX"))
           productModel.HMIN = Constants.formatValues(productObject.optString("HMIN"))
           productModel.hardness = (productObject.optString("hardness"))
           productModel.last_modified_time = productObject.optString("last_modified_time")
           productModel.webStatus = (productObject.optString("status"))
           productModel.status = "false"
//       }
       productMetricArray.add(productModel)

        }

   for (i in 0 until productInch.length()){
            var productObject=productInch.getJSONObject(i)
            var productModel=ProductInch()
       productModel.inch_det_id=productObject.optString("inch_det_id").trim()
       productModel.ProductID=productObject.optString("ProductID").trim()
       productModel.Diameter=productObject.optString("Diameter").trim()
       productModel.DiamDecimal=Constants.formatValues(productObject.optString("DiamDecimal"))
       productModel.DiamMM=Constants.formatValues(productObject.optString("DiamMM"))
       productModel.AFNom=Constants.formatValues(productObject.optString("AFNom"))
       productModel.HeadODMax=Constants.formatValues(productObject.optString("HeadODMax"))
       productModel.HeadHeightMax=Constants.formatValues(productObject.optString("HeadHeightMax"))
       productModel.ThreadLengthRef=Constants.formatValues(productObject.optString("ThreadLengthRef"))
       productModel.Grade=Constants.formatValuesForGrades(productObject.optString("Grade"))
       productModel.Thread=Constants.formatValues(productObject.optString("Thread"))
       productModel.PitchUNC=Constants.formatValues(productObject.optString("PitchUNC"))
       productModel.PitchUNF=Constants.formatValues(productObject.optString("PitchUNF"))
       productModel.TensileStressAreaUNC=Constants.formatValues(productObject.optString("TensileStressAreaUNC"))
       productModel.TensileStressAreaUNF=Constants.formatValues(productObject.optString("TensileStressAreaUNF"))
       productModel.NominalShankAreaUNC=Constants.formatValues(productObject.optString("NominalShankAreaUNC"))
       productModel.NominalShankAreaUNF=Constants.formatValues(productObject.optString("NominalShankAreaUNF"))
       productModel.SeatingTorqueInlbsUNC=Constants.formatValues(productObject.optString("SeatingTorqueInlbsUNC"))
       productModel.SeatingTorqueInlbsUNF=Constants.formatValues(productObject.optString("SeatingTorqueInlbsUNF"))
       productModel.TensileStrengthlbsUNC=Constants.formatValues(productObject.optString("TensileStrengthlbsUNC"))
       productModel.TensileStrengthlbsUNF=Constants.formatValues(productObject.optString("TensileStrengthlbsUNF"))
       productModel.CtrBore=Constants.formatValues(productObject.optString("CtrBore"))
       productModel.BodyDrill=Constants.formatValues(productObject.optString("BodyDrill"))
       productModel.TapDrillUNC=Constants.formatValues(productObject.optString("TapDrillUNC"))
       productModel.TapDrillUNF=Constants.formatValues(productObject.optString("TapDrillUNF"))
       productModel.BodyODMax=Constants.formatValues(productObject.optString("BodyODMax"))
       productModel.BodyODMin=Constants.formatValues(productObject.optString("BodyODMin"))
       productModel.ShearStrengthOfBody=Constants.formatValues(productObject.optString("ShearStrengthOfBody"))
       productModel.ScrewLengthMin=Constants.formatValues(productObject.optString("ScrewLengthMin"))
       productModel.last_modified_time=(productObject.optString("last_modified_time"))
       productModel.webStatus=productObject.optString("status")
       productModel.hardness=productObject.optString("hardness")
       productModel.SocketDepth=Constants.formatValues(productObject.optString("SocketDepth"))
       productModel.Pitch_TPI=Constants.formatValues(productObject.optString("Pitch_TPI"))
       productModel.status="false"
//       productModel.DiamDecimal=(productObject.optString("DiamDecimal"))
//       productModel.DiamMM=(productObject.optString("DiamMM"))
//       productModel.AFNom=(productObject.optString("AFNom"))
//       productModel.HeadODMax=(productObject.optString("HeadODMax"))
//       productModel.HeadHeightMax=(productObject.optString("HeadHeightMax"))
//       productModel.ThreadLengthRef=(productObject.optString("ThreadLengthRef"))
//       productModel.Grade=(productObject.optString("Grade"))
//       productModel.Thread=(productObject.optString("Thread"))
//       productModel.PitchUNC=(productObject.optString("PitchUNC"))
//       productModel.PitchUNF=(productObject.optString("PitchUNF"))
//       productModel.TensileStressAreaUNC=(productObject.optString("TensileStressAreaUNC"))
//       productModel.TensileStressAreaUNF=(productObject.optString("TensileStressAreaUNF"))
//       productModel.NominalShankAreaUNC=(productObject.optString("NominalShankAreaUNC"))
//       productModel.NominalShankAreaUNF=(productObject.optString("NominalShankAreaUNF"))
//       productModel.SeatingTorqueInlbsUNC=(productObject.optString("SeatingTorqueInlbsUNC"))
//       productModel.SeatingTorqueInlbsUNF=(productObject.optString("SeatingTorqueInlbsUNF"))
//       productModel.TensileStrengthlbsUNC=(productObject.optString("TensileStrengthlbsUNC"))
//       productModel.TensileStrengthlbsUNF=(productObject.optString("TensileStrengthlbsUNF"))
//       productModel.CtrBore=(productObject.optString("CtrBore"))
//       productModel.BodyDrill=(productObject.optString("BodyDrill"))
//       productModel.TapDrillUNC=(productObject.optString("TapDrillUNC"))
//       productModel.TapDrillUNF=(productObject.optString("TapDrillUNF"))
//       productModel.BodyODMax=(productObject.optString("BodyODMax"))
//       productModel.BodyODMin=(productObject.optString("BodyODMin"))
//       productModel.ShearStrengthOfBody=(productObject.optString("ShearStrengthOfBody"))
//       productModel.ScrewLengthMin=(productObject.optString("ScrewLengthMin"))
//       productModel.last_modified_time=(productObject.optString("last_modified_time"))
//       productModel.webStatus=productObject.optString("status")
//       productModel.hardness=productObject.optString("hardness")
//       productModel.SocketDepth=(productObject.optString("SocketDepth"))
//       productModel.Pitch_TPI=(productObject.optString("Pitch_TPI"))
//       productModel.status="false"
       productInchArray.add(productModel)

        }

   for (i in 0 until metricBox.length()){
            var productObject=metricBox.getJSONObject(i)
            var productModel=MetricBox()
            productModel.ProductID=productObject.optString("ProductID").trim()
            productModel.met_box_id=productObject.optString("met_box_id").trim()
            productModel.ShanonPartNumber=productObject.optString("ShanonPartNumber").trim()
            productModel.PartNumber=productObject.optString("PartNumber").trim()
            productModel.DescriptionOne=productObject.optString("DescriptionOne").trim()
            productModel.DescriptionTwo=productObject.optString("DescriptionTwo").trim()

            productModel.BoxQuantity=Constants.formatValues(productObject.optString("BoxQuantity").trim())
            productModel.Diameter=Constants.formatValues(productObject.optString("Diameter").trim())
            productModel.Length=Constants.formatValues(productObject.optString("Length").trim())
            productModel.countmetricbox=Constants.formatValues(productObject.optString("countmetricbox").trim())
            productModel.last_modified_time=Constants.formatValues(productObject.optString("last_modified_time").trim())
            productModel.bookmark_desc=Constants.formatValues(productObject.optString("bookmark_desc").trim())
       productModel.webStatus=Constants.formatValues(productObject.optString("status"))
//       productModel.BoxQuantity=(productObject.optString("BoxQuantity").trim())
//            productModel.Diameter=(productObject.optString("Diameter").trim())
//            productModel.Length=(productObject.optString("Length").trim())
//            productModel.countmetricbox=(productObject.optString("countmetricbox").trim())
//            productModel.last_modified_time=(productObject.optString("last_modified_time").trim())
//            productModel.bookmark_desc=(productObject.optString("bookmark_desc").trim())
//       productModel.webStatus=(productObject.optString("status"))
       metricBoxArray.add(productModel)

        }
        for (i in 0 until inchBox.length()){
            var productObject=inchBox.getJSONObject(i)
            var productModel=InchBox()

            productModel.inch_box_id=productObject.optString("inch_box_id").trim()
            productModel.ShanonPartNumber=productObject.optString("ShanonPartNumber").trim()
            productModel.PartNumber=productObject.optString("PartNumber").trim()
            productModel.ProductID=productObject.optString("ProductID").trim()
            productModel.DescriptionOne=productObject.optString("DescriptionOne").trim()
            productModel.DescriptionTwo=productObject.optString("DescriptionTwo").trim()
            productModel.BoxQuantity=Constants.formatValues(productObject.optString("BoxQuantity").trim())
            productModel.Length=Constants.formatValues(productObject.optString("Length").trim())
            productModel.Diameter=Constants.formatValues(productObject.optString("Diameter").trim())
            productModel.countinchbox=Constants.formatValues(productObject.optString("countinchbox").trim())
            productModel.last_modified_time=Constants.formatValues(productObject.optString("last_modified_time").trim())
            productModel.bookmark_desc=Constants.formatValues(productObject.optString("bookmark_desc").trim())
            productModel.Thread_type=Constants.formatValues(productObject.optString("Thread_type").trim())
            productModel.webStatus=Constants.formatValues(productObject.optString("status"))
//           productModel.BoxQuantity=(productObject.optString("BoxQuantity").trim())
//            productModel.Length=(productObject.optString("Length").trim())
//            productModel.Diameter=(productObject.optString("Diameter").trim())
//            productModel.countinchbox=(productObject.optString("countinchbox").trim())
//            productModel.last_modified_time=(productObject.optString("last_modified_time").trim())
//            productModel.bookmark_desc=(productObject.optString("bookmark_desc").trim())
//            productModel.Thread_type=(productObject.optString("Thread_type").trim())
//            productModel.webStatus=(productObject.optString("status"))
           inchBoxArray.add(productModel)


        }


        view.saveToProductsTable(productsArray)
        view.saveToproductMetric(productMetricArray)
        view.saveToProductInch(productInchArray)
        view.saveToMetricBox(metricBoxArray)
        view.saveToInchBox(inchBoxArray)
        view.saveToDataBase()
    }

}