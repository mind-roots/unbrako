package com.unbrako.dashBoard

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.unbrako.Constants
import com.unbrako.bookmarks.BookmarkFragment
import com.unbrako.catalog.CatalogFragment
import com.unbrako.dataBase.*
import com.unbrako.resource.TerminologyFragment
import com.unbrako.search.SearchActivity
import java.lang.Exception

import android.app.ProgressDialog
import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import com.unbrako.R
import com.unbrako.metric.MetricActivity



class DashboardActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener,
    DashBoardContract.View {


    var productsArray = ArrayList<Products>()
    var productMetricArray = ArrayList<ProductMetric>()
    var productInchArray = ArrayList<ProductInch>()
    var metricBoxArray = ArrayList<MetricBox>()
    var inchBoxArray = ArrayList<InchBox>()
    private lateinit var fragmentManager: androidx.fragment.app.FragmentManager
    private lateinit var fragmentTransaction: androidx.fragment.app.FragmentTransaction
    private lateinit var mBottomNav: BottomNavigationView

    lateinit var flReload : FrameLayout
    lateinit var reload : ImageView
    lateinit var metricActivity: MetricActivity
    lateinit var ll: LinearLayout


//    private lateinit var progress: LinearLayout
    private lateinit var dialog: ProgressDialog

    var arrayList: java.util.ArrayList<ProductMetric> = arrayListOf()


    lateinit var fragment: androidx.fragment.app.Fragment
    var position = 0
    var search = false
    lateinit var aarray: ArrayList<MetricBox>
    private lateinit var mPresenter: DashBoardPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        mPresenter = DashBoardPresenter(this)
        mBottomNav = findViewById(R.id.bottom_navigation)
        flReload = findViewById(R.id.flReload)
        reload = findViewById(R.id.reload)
        ll = findViewById(R.id.ll)
        dialog = ProgressDialog(this)


        mBottomNav.setOnNavigationItemSelectedListener(this)
        mBottomNav.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_LABELED
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container, CatalogFragment())
        fragmentTransaction.commit()
        metricActivity = MetricActivity()
//        ll.background.alpha = 100


    }
    fun BottomNavigationView.disableShiftMode() {
        val menuView = getChildAt(0) as BottomNavigationMenuView

        menuView.javaClass.getDeclaredField("mShiftingMode").apply {
            isAccessible = true
            setBoolean(menuView, false)
            isAccessible = false
        }

        @SuppressLint("RestrictedApi")
        for (i in 0 until menuView.childCount) {
            (menuView.getChildAt(i) as BottomNavigationItemView).apply {
                (menuView.getChildAt(i) as BottomNavigationItemView).setShifting(false)
                //setChecked(false)
            }
        }
    }
    @SuppressLint("RestrictedApi")
    fun removeNavigationShiftMode(view: BottomNavigationView) {
        val menuView = view.getChildAt(0) as BottomNavigationMenuView
        menuView.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_LABELED
        menuView.buildMenuView()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            R.id.catalog -> {
                fragment = CatalogFragment()
                loadFragment(fragment)
                position = 0
                search = false
                return true
            }

            R.id.resource -> {
                fragment = TerminologyFragment()
                loadFragment(fragment)
                position = 1
                search = false
                return true
            }

            R.id.bookmarks -> {
                fragment = BookmarkFragment()
                loadFragment(fragment)
                position = 2
                search = false
                return true
            }

            R.id.search -> {
                search = true
                startActivity(Intent(this, SearchActivity::class.java))
                return true
            }
        }
        return false
    }

    private fun loadFragment(fragment: androidx.fragment.app.Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    override fun onBackPressed() {
        val app = Constants.getSharedPrefs(this).getString("App","")
       if (app.equals("main")){
        if (position != 0) {
            position = 0
            loadFragment(CatalogFragment())
            mBottomNav.menu.getItem(0).isChecked = true
        } else {
            var value = Constants.getSharedPrefs(this).getString("child", "yes")
            if (value == "yes") {
                Constants.getSharedPrefs(this).edit().putString("child","no").apply()
                position = 0
                loadFragment(CatalogFragment())
                mBottomNav.menu.getItem(0).isChecked = true
            } else {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Exit")
                builder.setMessage("Are you sure you want to Exit?")
                builder.setPositiveButton("YES")
                { dialog, which ->
                    finish()
                }
                builder.setNegativeButton("No") { dialog, which ->
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
        }}
    }

    override fun onResume() {
        super.onResume()
       getProduct2()
        if (search) {

            if (position == 0) {
                fragment = CatalogFragment()
                loadFragment(fragment)
                position = 0
                search = false

            }

            if (position == 1) {
                fragment = TerminologyFragment()
                loadFragment(fragment)
                position = 1
                search = false

            }

            if (position == 2) {
                fragment = BookmarkFragment()
                loadFragment(fragment)
                position = 2
                search = false
            }
            mBottomNav.menu.getItem(position).isChecked = true
        }
    }

    override fun saveToProductsTable(productModel: ArrayList<Products>) {
        productsArray = productModel
    }

    override fun saveToInchBox(productModel: ArrayList<InchBox>) {
        inchBoxArray = productModel
    }

    override fun saveToMetricBox(productModel: ArrayList<MetricBox>) {

        metricBoxArray = productModel
    }

    override fun saveToProductInch(productModel: ArrayList<ProductInch>) {

        productInchArray = productModel
    }

    override fun saveToproductMetric(productModel: ArrayList<ProductMetric>) {
        productMetricArray = productModel
    }

    override fun showDateTime(date_time: String?) {
        Constants.getSharedPrefs(applicationContext).edit().putString("date_time", date_time).apply()
    }

    override fun saveToDataBase() {
        getProduct()
    }

    private fun getProduct() {
        class GetProduct : AsyncTask<Void, Void, Void>() {
            lateinit var productIdArr: ArrayList<ProductInch>
            override fun onPreExecute() {
                super.onPreExecute()
//                if (inchBoxArray.size==0) {
//                    progress.visibility = View.VISIBLE
//                }
            }

            override fun doInBackground(vararg params: Void?): Void? {

                val db = Constants.getDataBase(applicationContext)
                db!!.productDao().deleteProductsTable()
                for (i in 0 until productsArray.size) {

                    db.productDao().insertProducts(productsArray[i])
                }
                for (i in 0 until productMetricArray.size) {
                    //var arr1 = db.productDao().CheckIfExistProductMetric(productMetricArray[i].Diameter, productMetricArray[i].ProductID)
                    try{
                        db.productDao().insertProductMetric(productMetricArray[i])
                    }catch (e:Exception){
                        db.productDao().updateProductMetric(productMetricArray[i])
                    }
                }

                for (i in 0 until productInchArray.size) {
//                    var arr1 = db.productDao()
//                        .CheckIfExistProductInch(productInchArray[i].Diameter, productInchArray[i].ProductID)
//                    if (arr1.isNotEmpty()) {
//
//                    } else {
//
//                    }
                    try{
                        db.productDao().insertProductInch(productInchArray[i])
                    }catch (e:Exception){
                        db.productDao().updateProductInch(productInchArray[i])
                    }

                }
                for (i in 0 until metricBoxArray.size) {
//                    var arr1 =
//                        db.productDao().CheckIfExistMetricBox(metricBoxArray[i].PartNumber, metricBoxArray[i].Diameter)
//                    if (arr1.isNotEmpty()) {
//
//                    } else {
//
//                    }
                    try{
                        db.productDao().insertMetricBox(metricBoxArray[i])
                    }catch (e:Exception){
                        db.productDao().updateMetricBox(metricBoxArray[i])
                    }

                }
                for (i in 0 until inchBoxArray.size) {
                    try{
                        db.productDao().insertInchBox(inchBoxArray[i])
                    }catch (e:Exception){
                        db.productDao().updateInchBox(inchBoxArray[i])
                    }
//                    var arr1 =
//                        db.productDao().CheckIfExistInchBoxArray(inchBoxArray[i].PartNumber, inchBoxArray[i].Diameter)
//                    if (arr1.isNotEmpty()) {
//
//                    } else {
//
//                    }
                }
                return null
            }
            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)
                dialog.dismiss()
                if (dialog!!.isShowing) {
                    dialog.dismiss()
                }
//                progress.visibility = View.GONE
            }
        }
        GetProduct().execute()
    }

    private fun getProduct2() {
        class GetProduct : AsyncTask<Void, Void, Void>() {
            lateinit var productIdArr: ArrayList<ProductInch>
            override fun onPreExecute() {
                super.onPreExecute()

//                if (inchBoxArray.size==0) {
//                    progress.visibility = View.VISIBLE
//                }
            }

            override fun doInBackground(vararg params: Void?): Void? {

                val db = Constants.getDataBase(applicationContext)
                aarray = db!!.productDao().getAllMetricBoxArray() as ArrayList<MetricBox>

                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)

                if (aarray.size > 0) {
                    mPresenter.getAllValues(
                        Constants.getSharedPrefs(applicationContext).getString(
                            "date_time",
                            "2019-01-10 14:37:33"
                        ), "1"
                    )
                } else {
                    mPresenter.getAllValues(
                        Constants.getSharedPrefs(applicationContext).getString(
                            "date_time",
                            "2019-01-10 14:37:33"
                        ), "0"
                    )
                }
            }
        }
        GetProduct().execute()
    }

    override fun hideLoader() {
        dialog.dismiss()
//        progress.visibility = View.GONE
    }

    override fun showLoader() {
        dialog.setMessage("Updating products from server. Please wait...")
        dialog.show()
        dialog.setCancelable(false)

//        if (inchBoxArray.size == 0) {
////            progress.visibility = View.VISIBLE
//        }
    }

    override fun showNetworkError(network_error: Int) {
        getValues(this)
    }

    private fun getValues(applicationContext: Context) {

        var arrayList: java.util.ArrayList<ProductMetric> = arrayListOf()
        var arrayList2: java.util.ArrayList<ProductMetric> = arrayListOf()
        var arrayListMetrix: java.util.ArrayList<MetricBox> = arrayListOf()

        class GetProduct : AsyncTask<Void, Void, Void>() {

            override fun doInBackground(vararg params: Void?): Void? {

                var db = Constants.getDataBase(this@DashboardActivity)

                arrayList = db!!.productDao().getAllMetricProductsDb() as java.util.ArrayList<ProductMetric>

                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)

                if (arrayList.size == 0) {
                    flReload.visibility = View.VISIBLE
//                    Constants.getBitmapFromView()
//                    blurBehindLayout.viewBehind = ll
//                    BlurKit.getInstance().blur(R.layout.activity_dashboard,  0);//                    BlurKit.init(this@DashboardActivity)
                    reload.setOnClickListener {
                        flReload.visibility = View.GONE
                        mPresenter.getAllValues(
                            Constants.getSharedPrefs(applicationContext).getString(
                                "date_time",
                                "2019-01-10 14:37:33"
                            ), "0"
                        )
                    }
                } else {
                    flReload.visibility = View.GONE

                }
            }
        }
        GetProduct().execute()
    }

}