package com.unbrako.dashBoard

import com.unbrako.dataBase.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query

class DashBoardContract {

    interface View {
        fun saveToProductsTable(productModel: ArrayList<Products>)
        fun saveToInchBox(productModel: ArrayList<InchBox>)
        fun saveToMetricBox(productModel: ArrayList<MetricBox>)
        fun saveToProductInch(productModel: ArrayList<ProductInch>)
        fun saveToproductMetric(productModel: ArrayList<ProductMetric>)
        fun showDateTime(date_time: String?)
        fun saveToDataBase()
        fun hideLoader()
        fun showLoader()
        fun showNetworkError(network_error: Int)
    }

    interface Service {
        @POST("HomeInfo")
        fun getAllData(@Query("date_time") date_time: String?)
                : Call<ResponseBody>
    }
}