package com.unbrako.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.ViewModelProviders
import com.unbrako.Constants
import com.unbrako.IntroActivity
import com.unbrako.R
import com.unbrako.dashBoard.DashboardActivity
import com.unbrako.e_Commerce.DashboardEcommerceActivity
import com.unbrako.intro.SelectEcommViewModel

class SplashActivity : AppCompatActivity() {

    private var mDelayHandler: Handler? = null
    private val mSplashDelay: Long = 3000
    lateinit var loginViewModel: SelectEcommViewModel
    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            val intent = Intent(applicationContext, IntroActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        loginViewModel = ViewModelProviders.of(this)[SelectEcommViewModel::class.java]
        loginViewModel.getCountryCode()
        mDelayHandler = Handler()
        mDelayHandler!!.postDelayed(mRunnable, mSplashDelay)

        val prefs = Constants.getSharedPrefs(this).edit()
        prefs.putString("Selected", "1")
        prefs.apply()

//        if (Constants.getLoggedStatus(this)){
//            val intent = Intent(this, DashboardEcommerceActivity::class.java)
//            startActivity(intent)
//            finish()
//        }else{
//            val intent = Intent(this, DashboardActivity::class.java)
//            startActivity(intent)
//            finish()
//        }
    }

        public override fun onDestroy() {

            if (mDelayHandler != null) {
                mDelayHandler!!.removeCallbacks(mRunnable)
            }

            super.onDestroy()
        }
    }

