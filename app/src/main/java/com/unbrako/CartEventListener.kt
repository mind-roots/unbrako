package com.unbrako

import android.content.Context
import com.unbrako.e_Commerce.dataBase.CartModel

/**
 * Created by Manisha Thakur on 29/4/19.
 */
interface CartEventListener {
//    fun payNow(model: CartModel, position : Int)
    fun payNow()
    fun quotationRequest()

}