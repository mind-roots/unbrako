package com.unbrako.resource

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.resource.aboutUs.AboutUsActivity
import kotlinx.android.synthetic.main.about_item.view.*

class AboutAdapter(var context: Context, var listAbout: List<Int>) : RecyclerView.Adapter<AboutAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.about_item, parent, false))
    }

    override fun getItemCount(): Int {
        return listAbout.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.imageview_card.setImageResource(listAbout[position])

        holder.itemView.setOnClickListener {
            val intent = Intent(context, AboutUsActivity::class.java)
            intent.putExtra("data", position)
            context.startActivity(intent)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
