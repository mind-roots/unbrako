package com.unbrako.resource

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.unbrako.R
import kotlinx.android.synthetic.main.fastener_recycler_card.view.*

class TerminologyAdapter(val mContext: Context, val list: ArrayList<TerminologyModel>) :
    androidx.recyclerview.widget.RecyclerView.Adapter<TerminologyAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.fastener_recycler_card, parent, false)
        return TerminologyAdapter.MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.textview_card_title_fastener.text = model.imageName
        holder.itemView.imageview_card_fastener.setImageResource(model.image)

        holder.itemView.setOnClickListener {
            when (position) {

                0 -> {
                    val intent = Intent(mContext, ProductTerminologyActivity::class.java)
                    mContext.startActivity(intent)
                }
                1 -> {
                    val intent = Intent(mContext, ThreadTerminologyActivity::class.java)
                    mContext.startActivity(intent)
                }
                2 -> {
                    val intent = Intent(mContext, MechanicalTerminology::class.java)
                    mContext.startActivity(intent)
                }
                3 -> {
                    val intent = Intent(mContext, MaterialTerminologyActivity::class.java)
                    mContext.startActivity(intent)
                }
                4 -> {
                    val intent = Intent(mContext, GuideLineActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
        }
    }

    class MyViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

}
