package com.unbrako.resource

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.unbrako.R
import com.unbrako.fastenerFacts.FastnerAdapter
import com.unbrako.fastenerFacts.FastnerModel
import com.unbrako.specializedCoating.SpecializedAdapter
import com.unbrako.specializedCoating.SpecializedModel
import java.util.*

/**
 * Created by Loveleen on 11/1/19.
 */
class TerminologyFragment : androidx.fragment.app.Fragment(), View.OnClickListener {

    lateinit var recyclerViewTerminology: RecyclerView
    lateinit var recyclerViewSpecialized: RecyclerView
    lateinit var recyclerViewFastner:RecyclerView
    lateinit var recyclerViewAbout:RecyclerView
    lateinit var privacyPolicy:ImageView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(
            R.layout.fragment_terminology,
            container, false
        )

        recyclerViewTerminology = view.findViewById(R.id.rv_first)
        recyclerViewSpecialized = view.findViewById(R.id.rv_second)
        recyclerViewFastner = view.findViewById(R.id.rv_thrid)
        recyclerViewAbout = view.findViewById(R.id.rv_first_zero)
        privacyPolicy = view.findViewById(R.id.privacyPolicy)
        privacyPolicy.setOnClickListener(this)
        setLayout()
        setAdapter()
        return view

    }

    @SuppressLint("WrongConstant")
    fun setLayout() {

        recyclerViewTerminology.layoutManager = GridLayoutManager(activity, 3)
        recyclerViewTerminology.itemAnimator = DefaultItemAnimator()
        recyclerViewTerminology.isNestedScrollingEnabled = false

        recyclerViewSpecialized.layoutManager = GridLayoutManager(activity, 3)
        recyclerViewSpecialized.itemAnimator = DefaultItemAnimator()
        recyclerViewSpecialized.isNestedScrollingEnabled = false

        recyclerViewFastner.layoutManager = GridLayoutManager(activity, 3)
        recyclerViewFastner.itemAnimator = DefaultItemAnimator()
        recyclerViewFastner.isNestedScrollingEnabled = false

        recyclerViewAbout.layoutManager = GridLayoutManager(activity, 3)
        recyclerViewAbout.itemAnimator = DefaultItemAnimator()
        recyclerViewAbout.isNestedScrollingEnabled = false

    }

    private fun setAdapter() {
        recyclerViewTerminology.adapter = TerminologyAdapter(this.activity!!, getList())
        recyclerViewSpecialized.adapter = SpecializedAdapter(this.activity!!, getListSpecialized())
        recyclerViewFastner.adapter = FastnerAdapter(this.activity!!, getListFastner())
        recyclerViewAbout.adapter = AboutAdapter(this.activity!!, getListAbout())

    }

    private fun getListAbout(): List<Int> {
        val arrayList: ArrayList<Int> = arrayListOf()
        arrayList.add(R.mipmap.ic_about_us)
        arrayList.add(R.mipmap.ic_certification)
        arrayList.add(R.mipmap.ic_why_unbrako)
        arrayList.add(R.mipmap.ic_contact_us)
        return arrayList
    }

    private fun getList(): ArrayList<TerminologyModel> {


        val arrayList: java.util.ArrayList<TerminologyModel> = arrayListOf()

        val recyclerModel1: TerminologyModel = TerminologyModel()
        recyclerModel1.image = R.mipmap.product_thumbnail
        recyclerModel1.imageName = "Product"
        arrayList.add(recyclerModel1)


        val recyclerModel2: TerminologyModel = TerminologyModel()
        recyclerModel2.image = R.mipmap.thread
        recyclerModel2.imageName = "Thread"
        arrayList.add(recyclerModel2)

        val recyclerModel3: TerminologyModel = TerminologyModel()
        recyclerModel3.image = R.mipmap.mechanical
        recyclerModel3.imageName = "Mechanical"
        arrayList.add(recyclerModel3)

        val recyclerModel4: TerminologyModel = TerminologyModel()
        recyclerModel4.image = R.mipmap.material
        recyclerModel4.imageName = "Material"
        arrayList.add(recyclerModel4)

        val recyclerModel5: TerminologyModel = TerminologyModel()
        recyclerModel5.image = R.mipmap.guidelines
        recyclerModel5.imageName = "Guideline"
        arrayList.add(recyclerModel5)

        return arrayList

    }

    private fun getListSpecialized(): ArrayList<SpecializedModel> {


        val arrayList: java.util.ArrayList<SpecializedModel> = arrayListOf()

        var recyclerModel1: SpecializedModel =
            SpecializedModel()
        recyclerModel1.image = R.mipmap.hot_dip
        recyclerModel1.imageName = "Hot Dip Galvanized"
        arrayList.add(recyclerModel1)

        var recyclerModel2: SpecializedModel =
            SpecializedModel()
        recyclerModel2.image = R.mipmap.zinc_electroplating
        recyclerModel2.imageName = "Zinc Electro - Plating"
        arrayList.add(recyclerModel2)

        var recyclerModel3: SpecializedModel =
            SpecializedModel()
        recyclerModel3.image = R.mipmap.mechanical_galvanized
        recyclerModel3.imageName = "Mechanical Galvanized"
        arrayList.add(recyclerModel3)

        var recyclerModel4: SpecializedModel =
            SpecializedModel()
        recyclerModel4.image = R.mipmap.zinc_al_flake
        recyclerModel4.imageName = "Zinc AI-Flake"
        arrayList.add(recyclerModel4)

        return arrayList

    }

    private fun getListFastner(): ArrayList<FastnerModel> {


        val arrayList: java.util.ArrayList<FastnerModel> = arrayListOf()

        var recyclerModel1: FastnerModel = FastnerModel()
        recyclerModel1.image = R.mipmap.fastener_facts
        recyclerModel1.imageName = "Fastener Facts"
        arrayList.add(recyclerModel1)

        var recyclerModel2: FastnerModel = FastnerModel()
        recyclerModel2.image = R.mipmap.fastener_fatigue_clear
        recyclerModel2.imageName = "Fastener Fatigue"
        arrayList.add(recyclerModel2)

        var recyclerModel3: FastnerModel = FastnerModel()
        recyclerModel3.image = R.mipmap.hydrogen_embrittlement_clear
        recyclerModel3.imageName = "Hydrogen Embrittlement"
        arrayList.add(recyclerModel3)

        return arrayList
    }

    override fun onClick(v: View?) {
        val url = "http://www.unbrako.com/privacy-policy"
        var intent: Intent = Intent()
        intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

}