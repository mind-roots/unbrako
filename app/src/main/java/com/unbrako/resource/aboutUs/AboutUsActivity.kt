package com.unbrako.resource.aboutUs

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.unbrako.R

class AboutUsActivity : AppCompatActivity() {

    lateinit var title: String
    lateinit var data: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.about_us)
        val image = findViewById<ImageView>(R.id.image)
        when (intent.getIntExtra("data", -1)) {
            0 -> {
                title = "About Us"
                data = getString(R.string.about_us)
                image.visibility = View.GONE
            }
            1 -> {
                title = "Certification"
                data = getString(R.string.certification)
                image.visibility = View.VISIBLE
            }
            2 -> {
                title = "Why Unbrako"
                data = getString(R.string.why_unbrako)
                image.visibility = View.GONE
            }
            3 -> {
                title = "Contact Us"
                try {
                    val data1 = assets.open("contact_us.html").bufferedReader().use { it.readText() }
                    data = Html.fromHtml(data1).toString()
                }catch (e:Exception){
                    e.printStackTrace()
                }
                //data = getString(R.string.contact_us)
                image.visibility = View.GONE
            }
        }

        init()
    }

    @SuppressLint("SetTextI18n")
    fun init() {
        val mToolbar = findViewById<Toolbar>(R.id.toolbarWhy)
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val mTittle = findViewById<TextView>(R.id.whyTittle)
        val detail = findViewById<TextView>(R.id.detail)
        mTittle.text = title
        detail.text = data
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
