package com.unbrako.resource

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class MaterialTerminologyActivity : AppCompatActivity() {
    private lateinit var toolbar11: Toolbar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_material_terminology)
        toolbar11 = findViewById(R.id.toolbar_edit11)
        setSupportActionBar(toolbar11)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
     //  title = "Material"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
