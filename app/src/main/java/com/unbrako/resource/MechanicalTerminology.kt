package com.unbrako.resource

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class MechanicalTerminology : AppCompatActivity() {
    private lateinit var toolbar15: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mechanical_terminology)
        toolbar15 = findViewById(R.id.toolbar_edit15)
        setSupportActionBar(toolbar15)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //title = "Mechanical Terminology"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}

