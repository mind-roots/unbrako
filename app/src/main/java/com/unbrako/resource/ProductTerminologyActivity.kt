package com.unbrako.resource

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.unbrako.R

class ProductTerminologyActivity : AppCompatActivity() {
    private lateinit var toolbar14: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terminology_product)
        toolbar14 = findViewById(R.id.toolbar_edit14)
        setSupportActionBar(toolbar14)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
       // title = "Terminology"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
