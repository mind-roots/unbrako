package com.unbrako.resource

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.widget.TextView
import com.unbrako.R
import com.unbrako.e_Commerce.catalog.sortFilter.CatalogSortFilterActivity

class GuideLineActivity : AppCompatActivity() {

    private lateinit var toolbar11: Toolbar
    private lateinit var textV1: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide_line)
        toolbar11 = findViewById(R.id.toolbar_edit11)
        textV1 = findViewById(R.id.textV1)
        setSupportActionBar(toolbar11)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
       // title = "Guideline"

        textV1.setOnClickListener {
//            val intent = Intent(this, WhyUnbrakoActivity::class.java)
//            startActivity(intent)

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    }
