package com.unbrako.e_Commerce.login

import android.app.Application
import android.content.Intent
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.e_Commerce.DashboardEcommerceActivity
import com.unbrako.e_Commerce.signup.ModelStatus
import com.unbrako.e_Commerce.signup.SignUpModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit

class LoginRepo(var application: Application) {

    val mData = MutableLiveData<SignUpModel>()
    val mDataS = MutableLiveData<ModelStatus>()

    fun getItems(model: ModelLogin, progressBar: ProgressBar) {
        getProducts(model, progressBar)
    }

    fun getmData() : LiveData<SignUpModel>{

        return mData

    }

    private fun getProducts(model: ModelLogin, progressBar: ProgressBar) {

        progressBar.visibility = View.VISIBLE

        progressBar.visibility = View.VISIBLE
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(LoginListener.LoginService::class.java)
        val call: Call<ResponseBody> = service.login(model.email, model.password, model.pushToken, "android")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                if (response.isSuccessful) {
                    val mm=ModelStatus()

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val token = json.optString("token")
                        val success = json.optString("success")
                        val loginModel = SignUpModel()
                        val editor = Constants.getPrefs(application).edit()
                        editor.putString(Constants.token, token)
                        editor.apply()

                        if (success == "true") {

                            mm.msg=""
                            mm.status=1
                            val signUpObj = json.getJSONObject("customer")

                            val editorJson = Constants.getPrefs(application).edit()
                            editorJson.putString("signUpObj", signUpObj.toString())
                            editorJson.apply()


                            loginModel.customer_id = signUpObj.optString("customer_id")
                            loginModel.customer_group_id = signUpObj.optString("customer_group_id")
                            loginModel.store_id = signUpObj.optString("store_id")
                            loginModel.language_id = signUpObj.optString("language_id")
                            loginModel.firstname = signUpObj.optString("firstname")
                            loginModel.lastname = signUpObj.optString("lastname")
                            loginModel.email = signUpObj.optString("email")
                            loginModel.telephone = signUpObj.optString("telephone")
                            loginModel.fax = signUpObj.optString("fax")
                            loginModel.password = signUpObj.optString("password")
                            loginModel.salt = signUpObj.optString("salt")
                            loginModel.cart = signUpObj.optString("cart")
                            loginModel.wishlist = signUpObj.optString("wishlist")
                            loginModel.newsletter = signUpObj.optString("newsletter")
                            loginModel.address_id = signUpObj.optString("address_id")
                            loginModel.custom_field = signUpObj.optString("custom_field")
                            loginModel.ip = signUpObj.optString("ip")
                            loginModel.status = signUpObj.optString("status")
                            loginModel.safe = signUpObj.optString("safe")
                            loginModel.token = signUpObj.optString("token")
                            loginModel.code = signUpObj.optString("code")
                            loginModel.date_added = signUpObj.optString("date_added")

                            val profilegst=JSONObject(loginModel.custom_field)
                            loginModel.gst = profilegst.optString("2")
                            loginModel.company_name = profilegst.optString("1")
                            loginModel.landline = signUpObj.optString("landline")
                            loginModel.customer_type = signUpObj.optString("customer_type")
                            loginModel.credit = signUpObj.optString("credit")
                            loginModel.discount = signUpObj.optString("discount")
                            Constants.setLoggedIn(application, true)
                            val gson = Gson()
                            val json = gson.toJson(loginModel)
                            Constants.getPrefs(application).edit().putString("profile", json).apply()
                            val json1= Constants.getPrefs(application).getString("profile", "")
                            val profileData : SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
                            val intent = Intent(application, DashboardEcommerceActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            application.startActivity(intent)

                        }else{

                            mm.msg=json.optString("error")
                            mm.status=0

                        }
                        mData.value = loginModel
                        mDataS.value=mm
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }else{
                    Toast.makeText(application, "Credentials are not Valid.",
                        Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.GONE

                Toast.makeText(application, "No Response", Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun getSt(): LiveData<ModelStatus> {

        return mDataS
    }
}
