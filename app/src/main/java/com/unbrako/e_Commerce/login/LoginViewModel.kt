package com.unbrako.e_Commerce.login

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.unbrako.e_Commerce.signup.ModelStatus
import com.unbrako.e_Commerce.signup.SignUpModel

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    var refVar:LoginRepo = LoginRepo(application)

    fun getLoginData(model : ModelLogin,  progressBar: ProgressBar){

        return refVar.getItems(model, progressBar)

    }

    fun getLoginMData() : LiveData<SignUpModel>{
        return  refVar.getmData()
    }

    fun getStatus(): LiveData<ModelStatus> {
return refVar.getSt()
    }

}