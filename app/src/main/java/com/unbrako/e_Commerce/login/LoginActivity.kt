package com.unbrako.e_Commerce.login

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.unbrako.Constants
import com.unbrako.intro.SelectApp
import com.unbrako.R
import com.unbrako.databinding.ActivityLoginBinding
import com.unbrako.e_Commerce.DashboardEcommerceActivity
import com.unbrako.e_Commerce.forgotPassword.ForgotPassword
import com.unbrako.e_Commerce.signup.ModelStatus
import com.unbrako.e_Commerce.signup.SignUpActivity




@Suppress("DEPRECATION")
class LoginActivity : AppCompatActivity(), View.OnClickListener, LoginEventListener {

    lateinit var mBinding: ActivityLoginBinding
    lateinit var loginViewModel: LoginViewModel
    var shown=0
    var shown2=0
    var shown3=0
    var modelS=ModelStatus()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        loginViewModel = ViewModelProviders.of(this)[LoginViewModel::class.java]
        mBinding.login = this
        mBinding.done.setOnClickListener(this)
        onClick()
        setSpanText()


        if (Constants.getLoggedStatus(this)){
            val intent = Intent(this, DashboardEcommerceActivity::class.java)
            startActivity(intent)
            finish()

        }else{
            mBinding.loginLayout.visibility = View.VISIBLE
        }
        mBinding.back.setOnClickListener{
//            val prefs = Constants.getSharedPrefs(this)
//            var  dd=prefs.getString("App", "0")
//            if (prefs.getString("from", "0").equals("main")) {
//                val intent = Intent(this, DashboardActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                startActivity(intent)
//                finish()
//            }else {
                val intent = Intent(this, SelectApp::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
                finish()
//            }
        }

    }

    private fun onClick() {

        mBinding.forgotTV.setOnClickListener(this)
        mBinding.signUpTV.setOnClickListener(this)
    }

    private fun setSpanText() {
        val wordSpan =
            SpannableString("Don't have an account? Sign Up")

        wordSpan.setSpan(
            ForegroundColorSpan(Color.parseColor("#F37836")),
            23,
            30,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        mBinding.signUpTV.text = wordSpan
    }


    override fun onClick(v: View?) {

        when (v!!.id) {

            R.id.forgotTV -> {
                startActivity(Intent(this, ForgotPassword::class.java))
            }
            R.id.signUpTV -> {
                startActivity(Intent(this, SignUpActivity::class.java))
            }
        }
    }


    override fun loginEventListener(email: String, password: String) {

        shown=0
        shown2=0
        val model = ModelLogin()
        model.email = email
        model.password = password
        model.pushToken = "45788"


        loginViewModel.getLoginData(model, mBinding.progressBarLogin)


        loginViewModel.getLoginMData().observe(this, Observer {

            if (it.address_id.isNotEmpty()) {
                if (it.status == "1") {
                    if (shown3==0) {
                        shown3=1
                        val intent = Intent(this, DashboardEcommerceActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                } else {
                    if (shown==0){
                        shown=1
                        Constants.showLoginDialog(this)
                    }
                }
            }
        })
   loginViewModel.getStatus().observe(this, Observer {
       modelS=it
            if (modelS.status!=-1) {
                if (modelS.status == 0) {
                    modelS.status=-1
                    if (shown2==0){
                        shown2=1
                        Constants.showLoginDialogError(this,modelS.msg)
                    }
                }
            }
        })
    }
}
