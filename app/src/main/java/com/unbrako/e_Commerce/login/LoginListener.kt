package com.unbrako.e_Commerce.login

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

class LoginListener {


    interface LoginService {
        @FormUrlEncoded
        @POST("?route=mobileapi/customer/login")
        fun login(
            @Field("email") email: String,
            @Field("password") password: String,
            @Field("push_token") push_token: String,
            @Field("platform") platform: String)
                : Call<ResponseBody>
    }
}