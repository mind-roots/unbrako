package com.unbrako.e_Commerce

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.AsyncTask
import android.widget.ProgressBar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.e_Commerce.account.logout.LogoutModel
import com.unbrako.e_Commerce.account.logout.LogoutRepository
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.login.LoginActivity
import com.unbrako.e_Commerce.signup.SignUpModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RepoDashBoardEcommerce(var application: Application) {


    var array = ArrayList<CartModel>()
    var mData = MutableLiveData<ArrayList<CartModel>>()
    var array1 = ArrayList<BasketModel>()
    var mData1 = MutableLiveData<ArrayList<BasketModel>>()
    val forceLogout = MutableLiveData<String>()

    fun getCartSize(): MutableLiveData<ArrayList<CartModel>> {

        GetProductsOfCart().execute()
        mData.value=array
        return mData
    }

    fun getBasketSize(): MutableLiveData<ArrayList<BasketModel>> {
        GetProductsOfBasket().execute()
        mData1.value=array1
        return mData1

    }

    fun getForceLogout(): LiveData<String> {
        return forceLogout
    }

    fun getProfileData(dashboardEcommerceActivity: DashboardEcommerceActivity) {
        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.getProfile(auth!!)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()

                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
//
                        if (success) {
                            val signUpObj = json.getJSONObject("profile")
                            val editorJson = Constants.getPrefs(application).edit()
                            editorJson.putString("signUpObj", signUpObj.toString())
                            editorJson.apply()
                            val loginModel = SignUpModel()
                            loginModel.customer_id = signUpObj.optString("customer_id")

                            loginModel.customer_group_id = signUpObj.optString("customer_group_id")
                            loginModel.store_id = signUpObj.optString("store_id")
                            loginModel.language_id = signUpObj.optString("language_id")
                            loginModel.firstname = signUpObj.optString("firstname")
                            loginModel.lastname = signUpObj.optString("lastname")
                            loginModel.email = signUpObj.optString("email")
                            loginModel.telephone = signUpObj.optString("telephone")
                            loginModel.fax = signUpObj.optString("fax")
                            loginModel.password = signUpObj.optString("password")
                            loginModel.salt = signUpObj.optString("salt")
                            loginModel.cart = signUpObj.optString("cart")
                            loginModel.wishlist = signUpObj.optString("wishlist")
                            loginModel.newsletter = signUpObj.optString("newsletter")
                            loginModel.address_id = signUpObj.optString("address_id")
                            loginModel.custom_field = signUpObj.optString("custom_field")
                            loginModel.ip = signUpObj.optString("ip")
                            loginModel.status = signUpObj.optString("status")
                            loginModel.safe = signUpObj.optString("safe")
                            loginModel.token = signUpObj.optString("token")
                            loginModel.code = signUpObj.optString("code")
                            loginModel.date_added = signUpObj.optString("date_added")
                            loginModel.company_name = signUpObj.optString("company_name")
                            loginModel.gst = signUpObj.optString("gst")
                            loginModel.landline = signUpObj.optString("landline")
                            loginModel.customer_type = signUpObj.optString("customer_type")
                            loginModel.discount = signUpObj.optString("discount")
                            loginModel.credit = signUpObj.optString("credit")
                            val gson = Gson()
                            val json1 = gson.toJson(loginModel)
                            Constants.getPrefs(application).edit().putString("profile", json1).apply()
                           if(loginModel.status == "0"){
                               forceLogout.value = "true"
                           }
                        }
                    } catch (e: Exception) {
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })

    }
    @SuppressLint("StaticFieldLeak")
    inner class GetProductsOfCart : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(application)

            array =  db!!.productDao().getAllCart() as ArrayList<CartModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mData.value = array
        }
    }

 @SuppressLint("StaticFieldLeak")
 inner class GetProductsOfBasket : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(application)

            array1 =  db!!.productDao().getAllBasketModelArray() as ArrayList<BasketModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mData1.value = array1
        }
    }


}