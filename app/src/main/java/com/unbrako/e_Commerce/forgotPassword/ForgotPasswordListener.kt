package com.unbrako.e_Commerce.forgotPassword

interface ForgotPasswordListener {

    fun forgotPassword(email: String)

}