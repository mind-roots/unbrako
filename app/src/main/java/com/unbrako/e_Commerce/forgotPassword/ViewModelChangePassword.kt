package com.unbrako.e_Commerce.forgotPassword

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ViewModelChangePassword (application: Application) : AndroidViewModel(application)  {

    var changePassword : RepoChangePassword = RepoChangePassword(application)


    fun getCatageoryMData() : LiveData<ModelForgotPassword> {

        return changePassword.changeMPassword()
    }


    fun setChangePassword(progressBar: ProgressBar, currentPassword: String, newPassword: String) {
        changePassword.changePasswordRepo(progressBar, currentPassword, newPassword)
    }

  fun setForgotPassword(progressBar: ProgressBar, edtEmail: String) {
        changePassword.forgotPasswordResponse(progressBar, edtEmail)
    }


    fun setForgotMPassword() : LiveData<ModelForgotPassword>{
        return changePassword.forgotMPassword()
    }

}