package com.unbrako.e_Commerce.forgotPassword

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityChangePasswordBinding

class ChangePasswordActivity : AppCompatActivity(), ChangePasswordListener {

    lateinit var mBinding: ActivityChangePasswordBinding
    lateinit var viewModel: ViewModelChangePassword

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password)
        viewModel = ViewModelProviders.of(this)[ViewModelChangePassword::class.java]
        setToolbar()
        mBinding.changePasswords = this

        viewModel.getCatageoryMData().observe(this, Observer {

            if (it.success){
               showAlert(it.error)
            }else{
                showAlertFalse(it.error)
            }

        })

    }
    private fun showAlertFalse(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

//        val builder = AlertDialog.Builder(this)
//
//        // Set the alert dialog title
//        builder.setTitle("Alert!")
//
//        // Display a message on alert dialog
//        builder.setMessage(msg)
//
//        // Set a positive button and its click listener on alert dialog
//        builder.setPositiveButton("OK") { dialog, which ->
//
//
//        }
//
//        // Finally, make the alert dialog using builder
//        val dialog: AlertDialog = builder.create()
//
//        // Display the alert dialog on app interface
//        dialog.show()
    }
     private fun showAlert(msg: String) {
        val builder = AlertDialog.Builder(this)

        // Set the alert dialog title
        builder.setTitle("Alert!")

        // Display a message on alert dialog
        builder.setMessage(msg)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, which ->

            finish()
        }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }
    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.includeProfile.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.includeProfile.toolbarTitle.text = "Change Password"

        mBinding.done.setOnClickListener {
          //  finish()
        }

    }

    override fun changePassword(currentPassword: String, newPassword: String, confirmPassword: String) {

        when {
            newPassword.isEmpty() -> {
                Toast.makeText(this, getString(R.string.password_error), Toast.LENGTH_SHORT).show()
                return
            }
            confirmPassword.isEmpty() -> {
                Toast.makeText(this, getString(R.string.password_error), Toast.LENGTH_SHORT).show()
                return
            }
        }
        if (newPassword != confirmPassword) {
            Toast.makeText(this, getText(R.string.cPassword_error), Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.setChangePassword(mBinding.progressBar, currentPassword, newPassword)

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }




}
