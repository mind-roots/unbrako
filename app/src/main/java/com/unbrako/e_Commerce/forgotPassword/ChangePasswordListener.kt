package com.unbrako.e_Commerce.forgotPassword

interface ChangePasswordListener {

    fun changePassword(currentPassword : String, newPassword : String, confirmPassword : String )

}