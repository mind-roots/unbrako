package com.unbrako.e_Commerce.forgotPassword

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class RepoChangePassword(var application: Application) {


    val mData = MutableLiveData<ModelForgotPassword>()
    val mDataForgot = MutableLiveData<ModelForgotPassword>()

    fun changeMPassword(): LiveData<ModelForgotPassword> {

        return mData
    }

    fun forgotMPassword(): LiveData<ModelForgotPassword> {
        return mDataForgot
    }

    fun changePasswordRepo(progressBar: ProgressBar, currentPassword: String, newPassword: String) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.changePassword(auth!!, currentPassword, newPassword)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE


                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
                        if (success) {
                            val model = ModelForgotPassword()
                            model.success = success
                            model.error = json.optString("msg")
                            mData.value = model

                        }else{
                            val model = ModelForgotPassword()
                            model.success = success
                            model.error = json.optString("error")
                            mData.value = model
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.VISIBLE
            }


        })
    }

    fun forgotPasswordResponse(progressBar: ProgressBar, edtEmail: String) {
        getForgotPasswordResponse(progressBar, edtEmail)
    }

    private fun getForgotPasswordResponse(progressBar: ProgressBar, edtEmail: String) {


        progressBar.visibility = View.VISIBLE
        val retrofit = Constants.getWebClient3()
        val service = retrofit!!.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.forgotPassword(edtEmail)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                val modelForgotPassword = ModelForgotPassword()


                if (response.isSuccessful) {
                    try {

                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
                        if (success) {

                            modelForgotPassword.success = json.optBoolean("success")
                            modelForgotPassword.error = json.optString("msg")
                            mDataForgot.value = modelForgotPassword
                        } else {
                            modelForgotPassword.success = json.optBoolean("success")
                            modelForgotPassword.error = json.optString("error")
                            mDataForgot.value = modelForgotPassword
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.GONE
            }


        })

    }


}