package com.unbrako.e_Commerce.forgotPassword

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityForgotPasswordBinding

class ForgotPassword : AppCompatActivity(), ForgotPasswordListener {


    lateinit var mBinding: ActivityForgotPasswordBinding
    lateinit var viewModel: ViewModelChangePassword

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        viewModel = ViewModelProviders.of(this)[ViewModelChangePassword::class.java]

        setToolbar()
        mBinding.forgot = this


        viewModel.setForgotMPassword().observe(this, Observer {
            if (it.error.isNotEmpty()){
                showForgotDialog(this,it)
            }
        })
    }
    fun showForgotDialog(
        context: Context,
        it: ModelForgotPassword
    ) {
        android.app.AlertDialog.Builder(context)
            .setMessage(it.error)
            .setPositiveButton("Ok") { dialog, which ->
                dialog.dismiss()
                finish()
            }

            .show()
    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.includeForgot.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.includeForgot.toolbarTitle.text = "Forgot Password?"


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun forgotPassword(email: String) {

       viewModel.setForgotPassword(mBinding.progressBar, mBinding.edtEmail.text.toString())

    }


}
