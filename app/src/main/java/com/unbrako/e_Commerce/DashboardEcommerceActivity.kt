package com.unbrako.e_Commerce

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.os.Bundle
import android.text.format.Formatter.formatIpAddress
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessaging
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityDashboardEcommerceBinding
import com.unbrako.e_Commerce.account.AccountECommerceFragment
import com.unbrako.e_Commerce.account.logout.LogoutViewModel
import com.unbrako.e_Commerce.cart.CartEcoFragment
import com.unbrako.e_Commerce.catalog.CatalogECommerceFragment
import com.unbrako.e_Commerce.login.LoginActivity
import com.unbrako.e_Commerce.search.SearchECommerceFragment
import com.unbrako.e_Commerce.wishlist.BasketECommerceFragment
import kotlinx.android.synthetic.main.activity_dashboard_ecommerce.*
import java.net.NetworkInterface
import java.net.SocketException

class DashboardEcommerceActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener,
    updateCart, UpdateingCart, UpdatindCartFromSearch,
    UpdatingCartFromWishlist, UpdateBasket , CartEcoFragment.AppliedCoupon {

    var coupon_applied = 0
    var coupon_amount =0.0
    var typea=""
    var codeD=""
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var fragment: Fragment
    lateinit var mBinding: ActivityDashboardEcommerceBinding
    var position = 0
    var size = 0
    lateinit var tvUnreadChats: TextView
    lateinit var ggg: RelativeLayout
    lateinit var tvUnreadChats1: TextView
    lateinit var ggg1: RelativeLayout
    lateinit var itemView: BottomNavigationItemView
    lateinit var itemView2: BottomNavigationItemView
    lateinit var chatBadge: View
    lateinit var basketBadge: View
    var search = false
    lateinit var viewModel: DashBoardEModel
    private lateinit var logoutViewModel: LogoutViewModel

    @SuppressLint("StringFormatInvalid")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard_ecommerce)
        viewModel = ViewModelProviders.of(this)[DashBoardEModel::class.java]
        logoutViewModel = ViewModelProviders.of(this)[LogoutViewModel::class.java]
        init()
        var add = getLocalIpAddress()

        fragmentManager()



        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("token", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result
                // Log and toast
                val msg = getString(R.string.msg_token_fmt, token)
                Log.d("token", msg)
//                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
            })

        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)

    }

    private fun fragmentManager() {

        mBinding.bottomNavigation.setOnNavigationItemSelectedListener(this)
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.ecommerce_container, CatalogECommerceFragment())
        fragmentTransaction.commit()

        val mbottomNavigationMenuView = mBinding.bottomNavigation.getChildAt(0) as BottomNavigationMenuView
        chatBadge = LayoutInflater.from(this).inflate(R.layout.notification_badge, mbottomNavigationMenuView, false)
        basketBadge = LayoutInflater.from(this).inflate(R.layout.basket_badge_view, mbottomNavigationMenuView, false)
        itemView = mbottomNavigationMenuView.getChildAt(2) as BottomNavigationItemView
        itemView2 = mbottomNavigationMenuView.getChildAt(3) as BottomNavigationItemView

        tvUnreadChats = chatBadge.findViewById(R.id.badge_text_view)
        ggg = chatBadge.findViewById(R.id.badge_frame_layout)
        tvUnreadChats1 = basketBadge.findViewById(R.id.badge_text_viewb)
        ggg1 = basketBadge.findViewById(R.id.badge_basket_frame_layout)

    }

    @SuppressLint("SetTextI18n")
    private fun init() {

        setSupportActionBar(mBinding.dashToolbar.MainToolbar)
        title = ""
        mBinding.dashToolbar.title.text = "Unbrako"
        supportActionBar!!.elevation = 0f
    }

    @SuppressLint("SetTextI18n")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {


        when (item.itemId) {

            R.id.e_catalog -> {
                viewModel.getProfile(this)
                fragment = CatalogECommerceFragment()
                loadFragment(fragment)
                mBinding.dashToolbar.title.text = "Unbrako"
                position = 0
                search = false
                return true
            }

            R.id.e_serach -> {
                fragment = SearchECommerceFragment()
                loadFragment(fragment)
                mBinding.dashToolbar.title.text = "Search"
                position = 1
                search = false
                return true
            }

            R.id.e_cart -> {
                fragment = CartEcoFragment()
                loadFragment(fragment)
                mBinding.dashToolbar.title.text = "Cart"
                position = 2
                search = false
                return true
            }

            R.id.e_wishlist -> {
                fragment = BasketECommerceFragment()
                loadFragment(fragment)
                mBinding.dashToolbar.title.text = "Enquiry"

                position = 3
                search = false
                return true
            }

            R.id.e_account -> {
                fragment = AccountECommerceFragment()
                loadFragment(fragment)
                mBinding.dashToolbar.title.text = "My Account"
                position = 4
                search = false
                return true
            }
        }
        return false
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.ecommerce_container, fragment)
        //  transaction.addToBackStack(null)
        transaction.commit()
    }


    override fun onBackPressed() {
        //super.onBackPressed()

        if (position != 0) {
            fragment = CatalogECommerceFragment()
            loadFragment(fragment)
            mBinding.dashToolbar.title.text = "Catalog"
            position = 0
            search = false
            mBinding.bottomNavigation.selectedItemId = R.id.e_catalog
        } else {
            finishAffinity()
        }
    }

    fun getLocalIpAddress(): String? {
        try {
            val en = NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val intf = en.nextElement()
                val enumIpAddr = intf.inetAddresses
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress) {
                        return inetAddress.hostAddress.toString()
                    }
                }
            }
        } catch (ex: SocketException) {
            Log.e("", ex.toString())
        }

        return null
    }

    private fun getWifiIPAddress(): String {
        val wifiMgr = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

        return formatIpAddress(wifiMgr.connectionInfo.ipAddress)
    }

    override fun onResume() {
        super.onResume()
        val home = Constants.getPrefs(application).getString("showHome", "no")
        if (home == "yes") {
             coupon_applied = 0
             coupon_amount =0.0
             typea=""
             codeD=""
            Constants.getPrefs(this).edit().remove("showHome").apply()
            fragment = CatalogECommerceFragment()
            loadFragment(fragment)
            mBinding.dashToolbar.title.text = "Catalog"

            position = 0
            search = false
            mBinding.bottomNavigation.selectedItemId = R.id.e_catalog

        }
        var vv = Constants.getPrefs(this).getString("gotoCart", "");
        if (vv!!.isNotBlank()) {
            Constants.getPrefs(this).edit().putString("gotoCart", "").apply()
            fragment = CartEcoFragment()
            loadFragment(fragment)
            mBinding.dashToolbar.title.text = "My Cart"
            position = 2
            search = false
            mBinding.bottomNavigation.selectedItemId = R.id.e_cart
        }
        var vv1 = Constants.getPrefs(this).getString("gotoEnquiry", "");
        if (vv1!!.isNotBlank()) {
            Constants.getPrefs(this).edit().putString("gotoEnquiry", "").apply()
            fragment = BasketECommerceFragment()
            loadFragment(fragment)
            mBinding.dashToolbar.title.text = "Enquiry"
            position = 3
            search = false
            mBinding.bottomNavigation.selectedItemId = R.id.e_wishlist
        }

        val cart = Constants.getPrefs(this).getString("showCart", "no")
        if (cart == "yes") {
            Constants.getPrefs(this).edit().remove("showCart").apply()
            fragment = CartEcoFragment()
            loadFragment(fragment)
            mBinding.dashToolbar.title.text = "My Cart"
            position = 2
            search = false
            mBinding.bottomNavigation.selectedItemId = R.id.e_cart
        }
        viewModel.getCartSize().observe(this, Observer {
            showBadge(it.size)
        })
        viewModel.getBasketSize().observe(this, Observer {
            showBadge2(it.size)
        })
        Constants.getProductsOfCart(this).execute()
        Constants.getProductsOfBasket(this).execute()
        showBadge(Constants.cartSize)
        showBadge2(Constants.basketSize)
        viewModel.getProfile(this)

        viewModel.getForceLogout().observe(this,Observer{
          if(it == "true"){
              val auth = Constants.getPrefs(this).getString(Constants.token, "")
              logoutViewModel.getLogoutData(auth!!, progressBar)
          }
        })

        logoutViewModel.getmLogoutData().observe(this, Observer {
            Constants.deleteCart(this).execute()
            Constants.deleteBasket(this).execute()
            val i = Intent(this, LoginActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
            (this as Activity).overridePendingTransition(0, 0)
            this.finish()
        })
    }


    override fun updatingCart(size: Int) {
        viewModel.getCartSize().observe(this, Observer {

            showBadge(it.size)
        })

    }

    override fun updateCarrtCat(value: Int) {
        viewModel.getCartSize().observe(this, Observer {
            showBadge(it.size)
            size = it.size

        })

    }

    fun showBadge(size: Int) {


        tvUnreadChats.text = size.toString() //String that you want to show in badge

        if (size.toString() == "0") {
            tvUnreadChats.visibility = View.GONE
            ggg.visibility = View.GONE
        } else {
            tvUnreadChats.visibility = View.VISIBLE
            ggg.visibility = View.VISIBLE
        }
        itemView.removeView(chatBadge)
        itemView.addView(chatBadge)

    }

    fun showBadge2(size: Int) {


        tvUnreadChats1.text = size.toString() //String that you want to show in badge

        if (size.toString() == "0") {
            tvUnreadChats1.visibility = View.GONE
            ggg1.visibility = View.GONE
        } else {
            tvUnreadChats1.visibility = View.VISIBLE
            ggg1.visibility = View.VISIBLE
        }
        itemView2.removeView(basketBadge)
        itemView2.addView(basketBadge)

    }

    override fun udateCartFromSearch(size: Int) {
        viewModel.getCartSize().observe(this, Observer {
            showBadge(it.size)
        })

    }

    override fun updateFromWish(pos: Int) {
        viewModel.getCartSize().observe(this, Observer {
            showBadge(it.size)
        })

    }

    override fun updatingBasket(size: Int) {

        Constants.getProductsOfBasket(this).execute()
        //showBadge(Constants.cartSize)
        showBadge2(Constants.basketSize)
    }


    override fun appliedCoupons(
        couponApplied: Int,
        couponAmount: Double,
        code: String,
        type: String
    ) {

        coupon_applied=couponApplied
        coupon_amount=couponAmount
        codeD=code
        typea=type
    }

    override fun getAppliedCoupon(): Int {
       return coupon_applied;
    }

    override fun getCouponDiscount(): Double {
        return  coupon_amount
    }

    override fun getCode(): String {
        return codeD
    }

    override fun getType(): String {
        return  typea
    }
}
