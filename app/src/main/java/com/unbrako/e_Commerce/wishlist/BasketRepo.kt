package com.unbrako.e_Commerce.wishlist

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.os.AsyncTask
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.cart.quotaion.QuotationModel
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import com.unbrako.e_Commerce.dataBase.BasketModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "NAME_SHADOWING")
class BasketRepo(var application: Application) {
    var array = ArrayList<BasketModel>()
    var mData = MutableLiveData<ArrayList<BasketModel>>()
    val mData1 = MutableLiveData<QuotationModel>()

    fun getDataComment(): MutableLiveData<QuotationModel> {
        return mData1
    }

    fun getBasketValues(): MutableLiveData<ArrayList<BasketModel>> {
        GetProductsOfBasket().execute()
        mData.value = array
        return mData

    }

    fun addingComment(
        products: String,
        subtotal: String,
        total: String,
        comment: String,
        tax: String,
        progressBar: ProgressBar
    ) {

        val retrofit = Constants.getWebClient3()

        progressBar.visibility = View.VISIBLE
        val authCode = Constants.getPrefs(application).getString(Constants.token, "")

        val service = retrofit?.create(Webservice::class.java)
        val call: Call<ResponseBody> = service!!.saveInquiry(authCode!!, products, subtotal, total, comment, tax)

        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)

                        val success = json.optBoolean("success")
                        val msg = json.optString("msg")
                        val inquiryNo = json.optString("inquiry_no")
                        val total = json.optString("total")

                        val quotationModel = QuotationModel()
                        quotationModel.success = success
                        quotationModel.msg = msg
                        quotationModel.inquiry_no = inquiryNo
                        quotationModel.total = total

                        mData1.value = quotationModel

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                progressBar.visibility = View.GONE
                Toast.makeText(application, "No Response", Toast.LENGTH_SHORT).show()
            }

        })

    }

    fun deletingBasket() {

        DeleteBasket(application).execute()
    }

    @SuppressLint("StaticFieldLeak")
    inner class DeleteBasket(
        val context: Context
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            //Done due to crash in DB having null value from api
            val db = Constants.getDataBase(context)

            db!!.productDao().deleteBasket()

            array = db.productDao().getAllBasketModelArray() as ArrayList<BasketModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mData.value = array
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class GetProductsOfBasket : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(application)

            array = db!!.productDao().getAllBasketModelArray() as ArrayList<BasketModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mData.value = array
        }
    }
}