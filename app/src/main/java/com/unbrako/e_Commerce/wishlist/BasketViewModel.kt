package com.unbrako.e_Commerce.wishlist

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.cart.quotaion.QuotationModel
import com.unbrako.e_Commerce.dataBase.BasketModel

class BasketViewModel(application: Application):AndroidViewModel(application) {
    fun getBasketValues(): MutableLiveData<ArrayList<BasketModel>> {

        return refVar.getBasketValues()
    }

    fun addComment(
        products: String,
        subtotal: String,
        fullAmount:String,
        comment: String,
        tax: String,
        progressBar: ProgressBar
    ) {
        refVar.addingComment(products,subtotal,fullAmount,comment,tax,progressBar)
    }

    fun getAddData() : MutableLiveData<QuotationModel> {
        return  refVar.getDataComment()
    }

    fun deleteBasket() {
refVar.deletingBasket()
    }


    var refVar:BasketRepo=BasketRepo(application)

}
