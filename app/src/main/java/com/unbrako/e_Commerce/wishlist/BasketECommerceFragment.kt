package com.unbrako.e_Commerce.wishlist

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.BasketEventListner
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.FragmentEcommerceBasketBinding
import com.unbrako.e_Commerce.UpdateBasket
import com.unbrako.e_Commerce.catalog.ModelTax
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.signup.SignUpModel
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Manisha Thakur on 1/4/19.
 */
@Suppress("NAME_SHADOWING")
class BasketECommerceFragment : Fragment(), BasketEventListner, AdapterBasketEco.Calculations {

    lateinit var mBinding: FragmentEcommerceBasketBinding
    private var quotation_no: String = "0"
    private lateinit var adapterCartEcommerce: AdapterBasketEco
    private lateinit var viewModel: BasketViewModel
    lateinit var listener: BasketEventListner
    lateinit var update: UpdateBasket
    var cartArray: ArrayList<BasketModel> = ArrayList()
    var totalPrice = 0.0
    var totalPriceWithGst = 0.0
    var check: Int = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_ecommerce_basket,
            container, false
        )
        viewModel = ViewModelProviders.of(this)[BasketViewModel::class.java]

        setHasOptionsMenu(true)
        return mBinding.root
    }


    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.rvCart.isNestedScrollingEnabled = false
        mBinding.rvCart.layoutManager = LinearLayoutManager(activity)
        mBinding.rvCart.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        adapterCartEcommerce = AdapterBasketEco(activity!!, this@BasketECommerceFragment)
        mBinding.rvCart.adapter = adapterCartEcommerce


        viewModel.getAddData().observe(this, Observer {
            if (it.success) {
                showAlert(it.msg)

            } else {
                Toast.makeText(context, it.msg, Toast.LENGTH_LONG).show()
            }

        })

        viewModel.getBasketValues().observe(this, Observer {
            if (it.size > 0) {
                quotation_no = it[0].tag
                cartArray = it
                adapterCartEcommerce.update(it)
                mBinding.noData.visibility = View.GONE
                mBinding.nestedScrollView.visibility = View.VISIBLE
                var price = 0.0
                var tax = 0.0
                for (i in 0 until it.size) {
                    val modelSearchEco = it[i]
                    val gson = Gson()
                    val json = gson.toJson(modelSearchEco)
                    val obj: CartModel = gson.fromJson(json, CartModel::class.java)


//                    it[i].price.replace("₹", "").replace(",", "")
                    price += (it[i].selectQuant.toDouble() * Constants.getDiscountedPriceCart(obj,activity!!) * it[i].saleable.toDouble())
                    tax += getProductTax(
                        it[i].tax_class_id,
                        (it[i].selectQuant.toDouble() * Constants.getDiscountedPriceCart(obj,activity!!) * it[i].saleable.toDouble())
                    )
                }
                mBinding.tvPriceCost.text = "₹" + Constants.currencyFormt(price)
                mBinding.tvTaxCost.text = "₹" + Constants.currencyFormt(tax)
                mBinding.textView8.text = "₹" + Constants.currencyFormt((price + tax))
                totalPrice = price
                totalPriceWithGst = price + tax

            } else {
                mBinding.noData.visibility = View.VISIBLE
                mBinding.nestedScrollView.visibility = View.GONE
            }
            update.updatingBasket(it.size)
        })

//        passing the context to fragment for interface
        mBinding.paynow = this

        addComments()
        mBinding.editText1.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }
        mBinding.editText1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let{
                    val remaining = it.length
                    mBinding.counter.text = "$remaining/500"
                }

            }

        })
    }

    private fun showAlert(msg: String) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle("Alert!")
        builder.setMessage(msg)
        builder.setPositiveButton("OK") { dialog, which ->
            Constants.deleteBasket(activity!!).execute()
            viewModel.deleteBasket()
            viewModel.getBasketValues().observe(this, Observer {
                update.updatingBasket(it.size)
            })


        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun addComments() {
        mBinding.editText.setOnTouchListener(View.OnTouchListener { v, event ->
            if (mBinding.editText.hasFocus()) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_SCROLL -> {
                        v.parent.requestDisallowInterceptTouchEvent(false)
                        return@OnTouchListener true
                    }
                }
            }
            false
        })
    }


    override fun quotationRequest(addComment: String) {

        if (mBinding.editText1.text.isEmpty()) {
            Toast.makeText(activity, "Please Add Comment", Toast.LENGTH_SHORT).show()
        } else {


            val orders: ArrayList<BasketModel> = cartArray
            val jsonArray2 = JSONArray()
            var subtotal = 0.0
            var tax = 0.0
            for (i in 0 until orders.size) {
                val modelSearchEco = orders[i]
                val gson = Gson()
                val json = gson.toJson(modelSearchEco)
                val obj: CartModel = gson.fromJson(json, CartModel::class.java)

                subtotal += (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * Constants.getDiscountedPriceCart(obj,activity!!))

                val jsonObject = JSONObject()

                jsonObject.put("id", orders[i].product_id)
                jsonObject.put("qty", (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()).toString())
             //   jsonObject.put("price", Constants.getDiscountedPriceCart(obj,activity!!))
                jsonObject.put("price", orders[i].price)
//                var dd=0.0
//                try {
//                    dd = intent.getDoubleExtra("couponDiscount",0.0)
//                } catch (e: Exception) {
//
//                }


                jsonObject.put("discount_percentage", calculateProductDiscount(orders[i],0.0))
                jsonObject.put(
                    "tax",
                    getProductTax(
                        orders[i].tax_class_id,
                        Constants.getDiscountedPriceCart(obj,activity!!) * orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()
                    ).toString()
                )
                jsonObject.put(
                    "total",
                    (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * Constants.getDiscountedPriceCart(obj,activity!!)).toString()
                )
                jsonArray2.put(jsonObject)

                tax += getProductTax(
                    orders[i].tax_class_id,
                    Constants.getDiscountedPriceCart(obj,activity!!) * orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()
                )
            }

            val products = jsonArray2.toString()

            viewModel.addComment(

                products,
                subtotal.toString(),
                totalPriceWithGst.toString(),
                mBinding.editText1.text.toString(),
                tax.toString(), mBinding.progress
            )

        }
    }
    private fun calculateProductDiscount(
        cartModel: BasketModel,
        couponDiscount: Double
    ): String? {
        val gson = Gson()
        val json1 = Constants.getPrefs(activity!!).getString("profile", "")
        val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
        val flash_sale = Constants.getSharedPrefs(activity!!).getString("flash_sale", "")

        if (cartModel.isexempted=="1"){

            if (flash_sale!="off"){
                val basePrice=(100*profileData.discount.toDouble())/100
                val amountLeft=100-basePrice
                val amountAfterFlashDiscount=(amountLeft*cartModel.flash_discount.toDouble())/100
                return (basePrice+amountAfterFlashDiscount).toString()
            }else{
                val basePrice=(100*profileData.discount.toDouble())/100
                val amountLeft=100-basePrice
                val amountAfterFlashDiscount=(amountLeft*couponDiscount)/100
                return (basePrice+amountAfterFlashDiscount).toString()
            }
        }else{
            if (flash_sale=="off") {

                return couponDiscount.toString()
            }else{
                if (cartModel.flash_discount.isEmpty()){
                    cartModel.flash_discount = "0"
                }
                val basePrice=(100*cartModel.flash_discount.toDouble())/100
                val amountLeft=100-basePrice
                val amountAfterFlashDiscount=(amountLeft*couponDiscount)/100
                return (basePrice+amountAfterFlashDiscount).toString()
            }
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as UpdateBasket
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as UpdateBasket
    }

    fun getProductTax(tax_class_id: String, price: Double): Double {
        val gson = Gson()
        val response = Constants.getSharedPrefs(activity!!).getString("rate", "")
        val lstArrayList = gson.fromJson<ArrayList<ModelTax>>(
            response,
            object : TypeToken<List<ModelTax>>() {
            }.type
        )
        for (i in 0 until lstArrayList.size) {
            if (lstArrayList[i].tax_class_id == tax_class_id) {
                if (lstArrayList[i].rate.isEmpty()) {
                    lstArrayList[i].rate = "0"
                }
                return (price * lstArrayList[i].rate.toDouble()) / 100
                break
            }
        }
        return 0.0
    }

    @SuppressLint("SetTextI18n")
    override fun calculateTotal(

    ) {


        viewModel.getBasketValues().observe(this, Observer {
            if (it.size > 0) {

                cartArray = it
                adapterCartEcommerce.update(it)
                mBinding.noData.visibility = View.GONE
                mBinding.nestedScrollView.visibility = View.VISIBLE
                var price = 0.0
                var tax = 0.0
                for (i in 0 until it.size) {

                    val modelSearchEco = it[i]
                    val gson = Gson()
                    val json = gson.toJson(modelSearchEco)
                    val obj: CartModel = gson.fromJson(json, CartModel::class.java)

                    price += (obj.selectQuant.toDouble() * Constants.getDiscountedPriceCart(obj,activity!!) * it[i].saleable.toDouble())
                    tax += getProductTax(
                        it[i].tax_class_id,
                        (it[i].selectQuant.toDouble() * Constants.getDiscountedPriceCart(obj,activity!!) * it[i].saleable.toDouble())
                    )
                }
                mBinding.tvPriceCost.text = "₹" + Constants.currencyFormt(price)

                mBinding.tvTaxCost.text = "₹" + Constants.currencyFormt(tax)
                mBinding.textView8.text = "₹" + Constants.currencyFormt((price + tax))
                totalPrice = price
                totalPriceWithGst = price + tax
            } else {
                mBinding.noData.visibility = View.VISIBLE
                mBinding.nestedScrollView.visibility = View.GONE
            }
            update.updatingBasket(it.size)
        })

    }


}
