package com.unbrako.e_Commerce.wishlist

import android.app.Application
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class WishListRepository (var application: Application){

    var productsArray: ArrayList<FetchProductsModel> = ArrayList()
    val mData = MutableLiveData<ArrayList<FetchProductsModel>>()
    val mData2 = MutableLiveData<ArrayList<ModelWishList>>()
    val array: ArrayList<ModelWishList> = ArrayList()

    fun getItems( ){

        mData.value = productsArray
    }

    fun getMData() : LiveData<ArrayList<FetchProductsModel>>{

        return mData
    }


    fun geWishListData(adapterWishlistEco: AdapterWishListEco, progressBar: RelativeLayout, noData: TextView) {
        progressBar.visibility= View.VISIBLE
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.getWishList( auth!!)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility= View.GONE
                if (response.isSuccessful) {
                    array.clear()
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optString("success")


                    if (success == "true") {
                        val products = json.optJSONArray("products")
                        for (i in 0 until products.length()) {
                            val oo = products.optJSONObject(i)
                            val model = ModelWishList()
                            model.product_id = oo.optString("product_id")
                            model.sku = oo.optString("sku")
                            model.quantity = oo.optString("stock")
                            model.price = oo.optString("price")
                            model.nameWishlist = oo.optString("name")
                            model.imageWishlist = oo.optString("image")
                            model.tax_class_id = oo.optString("tax_class_id")
                            model.stock = oo.optString("stock")
                            if (oo.optString("rating").trim().isEmpty()||oo.optString("rating")==null||oo.optString("rating")=="null"){
                                model.rating = "0"
                            }else{
                                model.rating = oo.optString("rating")
                            }

                            model.display_price = oo.optString("display_price")
                            model.category_id = oo.optString("category_id")
                            model.saleable = oo.optString("saleable")
                            model.uom = oo.optString("uom")
                            model.flash_discount = oo.optString("flash_discount")
                            model.isexempted = oo.optString("isexempted")
                            model.selectQuant = "1"
                            array.add(model)
                        }
                        mData2.value=array
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility= View.GONE
            }


        })
    }

    fun getData2(): MutableLiveData<ArrayList<ModelWishList>> {

        return mData2
    }

}


