package com.unbrako.e_Commerce.wishlist

import android.app.Application
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.dataBase.FetchProductsModel

class ViewModelWishList (application: Application) : AndroidViewModel(application)  {


    var catalogRepository : WishListRepository = WishListRepository(application)

    fun getList(activity: FragmentActivity){
        return  catalogRepository.getItems()
    }

    fun getMDataWish() : LiveData<ArrayList<FetchProductsModel>>{

        return catalogRepository.getMData()

    }

    fun getWishList(adapterWishlistEco: AdapterWishListEco, progressBar: RelativeLayout, noData: TextView) {

        catalogRepository.geWishListData(adapterWishlistEco,progressBar,noData)
    }

    fun getData():MutableLiveData<ArrayList<ModelWishList>> {
    return catalogRepository.getData2()
    }


}
