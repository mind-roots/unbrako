package com.unbrako.e_Commerce.wishlist

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.RecyclerWishlistEcommerceBinding
import com.unbrako.e_Commerce.account.wishlist.WishlistActivity
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.catalog.productDetails.ProductDetailActivity
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import kotlinx.android.synthetic.main.recycler_wishlist_ecommerce.view.*
import kotlinx.android.synthetic.main.recycler_wishlist_ecommerce.view.addToCart
import kotlinx.android.synthetic.main.recycler_wishlist_ecommerce.view.cutOff
import kotlinx.android.synthetic.main.recycler_wishlist_ecommerce.view.partNo
import kotlinx.android.synthetic.main.recycler_wishlist_ecommerce.view.spinner_des
import kotlinx.android.synthetic.main.recycler_wishlist_ecommerce.view.tvThree
import kotlinx.android.synthetic.main.recycler_wishlist_ecommerce.view.tvTwo
import kotlinx.android.synthetic.main.recycler_wishlist_ecommerce.view.tv_one
import kotlinx.android.synthetic.main.recycler_wishlist_ecommerce.view.tvfour


@Suppress("DEPRECATION")
class AdapterWishListEco(
    val mContext: Context,
    private var wishlistECommerceFragment: WishlistActivity
) :
    RecyclerView.Adapter<AdapterWishListEco.MyViewHolder>() {

    lateinit var refVar: RecyclerWishlistEcommerceBinding
    private var isEditable = false
    var list: ArrayList<ModelWishList> = ArrayList()
    private var blank = wishlistECommerceFragment as BlankView
    private var callApi = wishlistECommerceFragment as WishListApiCall
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        refVar =
            DataBindingUtil.inflate(
                LayoutInflater.from(mContext),
                R.layout.recycler_wishlist_ecommerce,
                parent,
                false
            )
        return MyViewHolder(refVar.root)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val modelSearchEco = list[position]
        val model = FetchProductsModel()
        model.product_id = modelSearchEco.product_id
        model.name = modelSearchEco.nameWishlist
        model.description = modelSearchEco.description
        model.sku = modelSearchEco.sku
        model.quantity = modelSearchEco.quantity
        model.image = modelSearchEco.imageWishlist
        model.price = modelSearchEco.price
        model.status = modelSearchEco.statusWishlist
        model.wish = true
        model.rating = modelSearchEco.rating.toDouble()
        model.saleable = modelSearchEco.saleable
        model.display_price = modelSearchEco.display_price
        model.category_id = modelSearchEco.category_id
        model.tax_class_id = modelSearchEco.tax_class_id
        model.selectQuant = modelSearchEco.selectQuant
        model.stock = modelSearchEco.stock
        model.flash_discount = modelSearchEco.flash_discount
        model.isexempted = modelSearchEco.isexempted


//        -----------------------------------------------Wishlist edit delete---------------------------------------//

        if (isEditable) {
            holder.itemView.checkWishlist.visibility = View.VISIBLE
        } else {
            holder.itemView.checkWishlist.visibility = View.GONE
        }
        holder.itemView.checkWishlist.isChecked = modelSearchEco.isChecked

        holder.itemView.checkWishlist.setOnClickListener {

            if (modelSearchEco.isChecked) {
                list[position].isChecked = false

                notifyDataSetChanged()
                blank.updateSelection(false, position)
            } else {
                list[position].isChecked = true
                notifyDataSetChanged()
                blank.updateSelection(true, position)
            }
        }

//        ---------------------------------------------------------------------------------------------------------//

        refVar.wishlist = modelSearchEco
        if (modelSearchEco.nameWishlist.contains("®")) {
            val vv = modelSearchEco.nameWishlist.split("®")
            holder.itemView.tv_one.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
        } else {
            holder.itemView.tv_one.text = modelSearchEco.nameWishlist
        }


        if (!list[position].imageWishlist.isEmpty()) {
            Picasso.get().load(list[position].imageWishlist).placeholder(R.drawable.pro_img_new)
                .fit().centerCrop()
                .into(holder.itemView.iv_cart)

        }

        holder.itemView.fullLay.setOnClickListener {
            val intent =
                Intent(mContext, ProductDetailActivity::class.java).putExtra("model", model)
                    .putExtra("from", "wish")
                    .putExtra("pos", position.toString())
            mContext.startActivity(intent)
        }
//        holder.itemView.tvTwo.text =
//            "₹" + Constants.currencyFormt(
//                (Constants.getDiscountedPrice(
//                    model,
//                    mContext
//                ) * list[position].saleable.toDouble())
//            )
        val discount = Constants.getDiscountApplies(model, mContext)
        val discountString =
            " per " + Constants.formatValues2(list[position].saleable) + " " + list[position].uom + discount

        val spannable = SpannableString(discountString)
//        spannable.setSpan(
//            ForegroundColorSpan(Color.parseColor("#F37836")),
//            discountString.length - discount.length + 2, discountString.length - 1,
//            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//        )
      //  holder.itemView.tvfour.text = spannable
        val gson = Gson()
        val json = gson.toJson(model)
        val fetchProductsModel: CartModel = gson.fromJson(json, CartModel::class.java)
        holder.itemView.tvfour.text = Constants.getPriceText2(fetchProductsModel,mContext,2)
        holder.itemView.tvTwo.text  = Constants.getPriceText(fetchProductsModel,mContext,2)
        if (list[position].display_price.toDouble() > 0) {
            holder.itemView.cutOff.visibility = View.VISIBLE
            holder.itemView.tvThree.text =
                "₹" + Constants.currencyFormt((list[position].display_price.toDouble() * list[position].saleable.toDouble()))
        } else {
            holder.itemView.cutOff.visibility = View.GONE
        }
        holder.itemView.tvThree.paintFlags =
            holder.itemView.tvThree.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

        holder.itemView.partNo.text = list[position].sku
        holder.itemView.spinner_des.text = modelSearchEco.selectQuant
        holder.itemView.spinner_des.setOnClickListener {
            openDialogQuantity(mContext, position, model)
        }

        holder.itemView.addToCart.setOnClickListener {
            callApi.addToCartLayout(model, position, holder.itemView.spinner_des.text.toString())
        }

    }

    private fun openDialogQuantity(mContext: Context, position: Int, quantity: FetchProductsModel) {

        var color: String
        lateinit var dialog: AlertDialog

        // Initialize an array of colors
        val array =
            arrayOf(
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24",
                "25",
                "26",
                "27",
                "28",
                "29",
                "30",
                "31",
                "32",
                "33",
                "34",
                "35",
                "36",
                "37",
                "38",
                "39",
                "40",
                "41",
                "42",
                "43",
                "44",
                "45",
                "46",
                "47",
                "48",
                "49",
                "50"
            )
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(mContext)
        builder.setTitle("Select Quantity")


        builder.setSingleChoiceItems(array, quantity.selectQuant.toInt() - 1) { _, which ->
            color = array[which]

            list[position].selectQuant = color
            Constants.UpdateToCarts(mContext, quantity).execute()
            notifyDataSetChanged()
            dialog.dismiss()
        }

        dialog = builder.create()
        dialog.show()

    }

    interface WishListApiCall {
        fun addToCartLayout(
            model: FetchProductsModel,
            position: Int,
            spinnerDes: String
        )
    }

    fun update(items: ArrayList<ModelWishList>) {
        this.list = items
        notifyDataSetChanged()
    }

    fun getEditableList(b: Boolean) {
        isEditable = b
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTwo: TextView = itemView.findViewById(R.id.tvTwo)
    }

    interface BlankView {
        fun blankView()
        fun updateSelection(b: Boolean, position: Int)
    }
}
