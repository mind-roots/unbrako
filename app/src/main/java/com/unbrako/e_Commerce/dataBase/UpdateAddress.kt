package com.unbrako.e_Commerce.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity
class UpdateAddress {
    @ColumnInfo(name = "Success")
    var success : Boolean = false

    @ColumnInfo(name = "Message")
    lateinit var message : String
}