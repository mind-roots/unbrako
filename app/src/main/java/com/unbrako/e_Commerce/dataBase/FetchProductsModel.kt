package com.unbrako.e_Commerce.dataBase

import android.os.Parcel
import android.os.Parcelable



class FetchProductsModel() : Parcelable, Comparable<FetchProductsModel>{
     override fun compareTo(other: FetchProductsModel): Int {
          TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     var stock: String = "0"
     var count: String = "0"

     var product_id: String=""
     var name: String=""

     var description: String=""

     var sku: String=""

     var quantity: String=""

     var image: String=""

     var price: String=""

     var rating: Double = 0.0
     var status: String=""
     var display_price: String=""
     var category_id: String=""
     var selectQuant: String=""
     var saleable: String=""
     var tax_class_id: String=""
     var uom: String=""
     var flash_discount: String=""
     var isexempted: String=""
     var basket: String = "0"
     var wish: Boolean=false

     constructor(parcel: Parcel) : this() {
          stock = parcel.readString()!!
          count = parcel.readString()!!
          product_id = parcel.readString()!!
          name = parcel.readString()!!
          description = parcel.readString()!!
          sku = parcel.readString()!!
          quantity = parcel.readString()!!
          image = parcel.readString()!!
          price = parcel.readString()!!
          rating = parcel.readDouble()
          status = parcel.readString()!!
          display_price = parcel.readString()!!
          category_id = parcel.readString()!!
          selectQuant = parcel.readString()!!
          saleable = parcel.readString()!!
          tax_class_id = parcel.readString()!!
          uom = parcel.readString()!!
          flash_discount = parcel.readString()!!
          isexempted = parcel.readString()!!
          basket = parcel.readString()!!
          wish = parcel.readByte() != 0.toByte()
     }

     override fun writeToParcel(parcel: Parcel, flags: Int) {
          parcel.writeString(stock)
          parcel.writeString(count)
          parcel.writeString(product_id)
          parcel.writeString(name)
          parcel.writeString(description)
          parcel.writeString(sku)
          parcel.writeString(quantity)
          parcel.writeString(image)
          parcel.writeString(price)
          parcel.writeDouble(rating)
          parcel.writeString(status)
          parcel.writeString(display_price)
          parcel.writeString(category_id)
          parcel.writeString(selectQuant)
          parcel.writeString(saleable)
          parcel.writeString(tax_class_id)
          parcel.writeString(uom)
          parcel.writeString(flash_discount)
          parcel.writeString(isexempted)
          parcel.writeString(basket)
          parcel.writeByte(if (wish) 1 else 0)
     }

     override fun describeContents(): Int {
          return 0
     }

     companion object CREATOR : Parcelable.Creator<FetchProductsModel> {
          override fun createFromParcel(parcel: Parcel): FetchProductsModel {
               return FetchProductsModel(parcel)
          }

          override fun newArray(size: Int): Array<FetchProductsModel?> {
               return arrayOfNulls(size)
          }
     }


}