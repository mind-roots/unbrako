package com.unbrako.e_Commerce.dataBase

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.unbrako.dataBase.HomeServiceModel

/**
 * Created by Manisha Thakur on 9/4/19.
 */
interface ProductDaoEcommerce {


    @Query("SELECT * FROM HomeServiceModel")
    abstract fun getHomeServiceData(): List<HomeServiceModel>


    @Delete
    abstract fun delete(homeServiceModel: HomeServiceModel)

    //--------------------------------------------------Inserting data into table---------------------------------------//

    @Insert
    abstract fun insertHomeService(task: HomeServiceModel)


    //--------------------------------------------------Updating data into table---------------------------------------//

    @Update
    abstract fun updateHomeService(task: HomeServiceModel)


    //--------------------------------------------------Deleting data into table---------------------------------------//


    @Query("DELETE FROM HomeServiceModel")
    abstract fun deleteHomeService()




}