package com.unbrako.e_Commerce.dataBase

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class FavouriteModel {
    @PrimaryKey
    @ColumnInfo(name = "ProductId")
    lateinit var product_id: String

    @ColumnInfo(name = "Name")
    var name: String=""

    @ColumnInfo(name = "Description")
    var description: String=""

    @ColumnInfo(name = "MetaTitle")
    var meta_title: String=""

    @ColumnInfo(name = "MetaDescription")
    var meta_description: String=""

    @ColumnInfo(name = "MetaKeyword")
    var meta_keyword: String=""

    @ColumnInfo(name = "Tag")
    var tag: String=""

    @ColumnInfo(name = "Model")
    var model: String=""

    @ColumnInfo(name = "Sku")
    var sku: String=""

    @ColumnInfo(name = "Upc")
    var upc: String=""

    @ColumnInfo(name = "Ean")
    var ean: String=""

    @ColumnInfo(name = "Jan")
    var jan: String=""

    @ColumnInfo(name = "Isbn")
    var isbn: String=""

    @ColumnInfo(name = "Mpn")
    var mpn: String=""

    @ColumnInfo(name = "Location")
    var location: String=""

    @ColumnInfo(name = "quantity")
    var quantity: String=""

    @ColumnInfo(name = "StockStatus")
    var stock_status: String=""

    @ColumnInfo(name = "Image")
    var image: String=""

    @ColumnInfo(name = "ManufacturerId")
    var manufacturer_id: String=""

    @ColumnInfo(name = "Manufacturer")
    var manufacturer: String=""

    @ColumnInfo(name = "Price")
    var price: String=""

    @ColumnInfo(name = "Special")
    var special: String=""

    @ColumnInfo(name = "Reward")
    var reward: String=""

    @ColumnInfo(name = "Points")
    var points: String=""

    @ColumnInfo(name = "TaxClassId")
    var tax_class_id: String=""

    @ColumnInfo(name = "DateAvailable")
    var date_available: String=""

    @ColumnInfo(name = "Weight")
    var weight: String=""

    @ColumnInfo(name = "WeightClassId")
    var weight_class_id: String=""

    @ColumnInfo(name = "Length")
    var length: String=""

    @ColumnInfo(name = "Width")
    var width: String=""

    @ColumnInfo(name = "Height")
    var height: String=""

    @ColumnInfo(name = "length_class_id")
    var length_class_id: String=""

    @ColumnInfo(name = "Subtract")
    var subtract: String=""

    @ColumnInfo(name = "Rating")
    var rating: String=""

    @ColumnInfo(name = "Reviews")
    var reviews: String=""

    @ColumnInfo(name = "Minimum")
    var minimum: String=""

    @ColumnInfo(name = "SortOrder")
    var sort_order: String=""

    @ColumnInfo(name = "Status")
    var status: String=""

    @ColumnInfo(name = "DateAdded")
    var date_added: String=""

    @ColumnInfo(name = "DateModified")
    var date_modified: String=""

    @ColumnInfo(name = "Viewed")
    var viewed: String=""

    @ColumnInfo(name = "favorite")
    var favorite: Boolean = false
}