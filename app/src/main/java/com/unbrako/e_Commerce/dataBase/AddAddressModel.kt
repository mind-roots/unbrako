package com.unbrako.e_Commerce.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity
class AddAddressModel {
    @ColumnInfo(name = "Success")
    var success : Boolean = false

    @ColumnInfo(name = "AddressId")
    lateinit var address_id : String
}