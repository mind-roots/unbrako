package com.unbrako.e_Commerce.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity
class OrdersHistoryModel {

    @ColumnInfo(name = "Total")
    lateinit var total : String

    @ColumnInfo(name = "Page")
    lateinit var page : String

}