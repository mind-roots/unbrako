package com.unbrako.e_Commerce.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity
class LogoutModel {

    @ColumnInfo(name = "Success")
    var success : Boolean = false

    @ColumnInfo(name = "Error")
    lateinit var error : String
}