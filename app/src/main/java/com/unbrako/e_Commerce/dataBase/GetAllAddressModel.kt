package com.unbrako.e_Commerce.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity
class GetAllAddressModel {

    @ColumnInfo(name = "AddressId")
    lateinit var  address_id : String

    @ColumnInfo(name = "CustomerId")
    lateinit var  customer_id : String

    @ColumnInfo(name = "FirstName")
    lateinit var  firstname : String

    @ColumnInfo(name = "LastName")
    lateinit var  lastname : String

    @ColumnInfo(name = "Company")
    lateinit var  company : String

    @ColumnInfo(name = "Address1")
    lateinit var  address_1 : String

    @ColumnInfo(name = "Address2")
    lateinit var  address_2 : String

    @ColumnInfo(name = "City")
    lateinit var  city : String

    @ColumnInfo(name = "PostCode")
    lateinit var  postcode : String

    @ColumnInfo(name = "CountryId")
    lateinit var  country_id : String

    @ColumnInfo(name = "ZoneId")
    lateinit var  zone_id : String

    @ColumnInfo(name = "CustomField")
    lateinit var  custom_field : String

}