package com.unbrako.e_Commerce.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity

/**
 * Created by Manisha Thakur on 9/4/19.
 */
@Entity
class LoginModel {

    @ColumnInfo(name = "Success")
    var success : Boolean = false

    @ColumnInfo(name = "Error")
    lateinit var error : String
}
