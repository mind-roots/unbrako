package com.unbrako.e_Commerce.dataBase

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@SuppressLint("ParcelCreator")
@Entity
class CartModel() : Parcelable {
    @PrimaryKey
    @ColumnInfo(name = "ProductId")
    lateinit var product_id: String

    @ColumnInfo(name = "Name")
    var name: String = ""
    @ColumnInfo(name = "selectQuant")
    var selectQuant: String = ""

    @ColumnInfo(name = "Description")
    var description: String = ""

    @ColumnInfo(name = "MetaTitle")
    var meta_title: String = ""

    @ColumnInfo(name = "MetaDescription")
    var meta_description: String = ""

    @ColumnInfo(name = "MetaKeyword")
    var meta_keyword: String = ""

    @ColumnInfo(name = "Tag")
    var tag: String = "0"

    @ColumnInfo(name = "Model")
    var model: String = ""

    @ColumnInfo(name = "Sku")
    var sku: String = ""

    @ColumnInfo(name = "Upc")
    var upc: String = ""

    @ColumnInfo(name = "Ean")
    var ean: String = ""

    @ColumnInfo(name = "Jan")
    var jan: String = ""

    @ColumnInfo(name = "Isbn")
    var isbn: String = ""

    @ColumnInfo(name = "Mpn")
    var mpn: String = ""

    @ColumnInfo(name = "Location")
    var location: String = ""

    @ColumnInfo(name = "quantity")
    var quantity: String = ""

    @ColumnInfo(name = "StockStatus")
    var stock: String = ""
    @ColumnInfo(name = "saleable")
    var saleable: String = ""
    @ColumnInfo(name = "display_price")
    var display_price: String = ""
    @ColumnInfo(name = "category_id")
    var category_id: String = ""

    @ColumnInfo(name = "Image")
    var image: String = ""

    @ColumnInfo(name = "ManufacturerId")
    var manufacturer_id: String = ""

    @ColumnInfo(name = "Manufacturer")
    var manufacturer: String = ""

    @ColumnInfo(name = "Price")
    var price: String = ""

    @ColumnInfo(name = "Special")
    var special: String = ""

    @ColumnInfo(name = "Reward")
    var reward: String = ""

    @ColumnInfo(name = "Points")
    var points: String = ""

    @ColumnInfo(name = "TaxClassId")
    var tax_class_id: String = ""

    @ColumnInfo(name = "DateAvailable")
    var date_available: String = ""

    @ColumnInfo(name = "Weight")
    var weight: String = ""

    @ColumnInfo(name = "WeightClassId")
    var weight_class_id: String = ""

    @ColumnInfo(name = "Length")
    var length: String = ""

    @ColumnInfo(name = "Width")
    var width: String = ""

    @ColumnInfo(name = "Height")
    var height: String = ""

    @ColumnInfo(name = "length_class_id")
    var length_class_id: String = ""

    @ColumnInfo(name = "Subtract")
    var subtract: String = ""

    @ColumnInfo(name = "Rating")
    var rating: String = ""

    @ColumnInfo(name = "Reviews")
    var reviews: String = ""

    @ColumnInfo(name = "Minimum")
    var minimum: String = ""

    @ColumnInfo(name = "SortOrder")
    var sort_order: String = ""

    @ColumnInfo(name = "Status")
    var status: String = ""

    @ColumnInfo(name = "DateAdded")
    var date_added: String = ""

    @ColumnInfo(name = "DateModified")
    var date_modified: String = ""

    @ColumnInfo(name = "Viewed")
    var viewed: String = ""
    @ColumnInfo(name = "uom")
    var uom: String = ""
    @ColumnInfo(name = "flash_discount")
    var flash_discount: String = ""
    @ColumnInfo(name = "isexempted")
    var isexempted: String = ""
    @ColumnInfo(name = "favorite")
    var favorite: Boolean = false

    @ColumnInfo(name = "discount_percentage")
    var discount_percentage: String = "0"

    constructor(parcel: Parcel) : this() {
        product_id = parcel.readString()!!
        name = parcel.readString()!!
        selectQuant = parcel.readString()!!
        description = parcel.readString()!!
        meta_title = parcel.readString()!!
        meta_description = parcel.readString()!!
        meta_keyword = parcel.readString()!!
        tag = parcel.readString()!!
        model = parcel.readString()!!
        sku = parcel.readString()!!
        upc = parcel.readString()!!
        ean = parcel.readString()!!
        jan = parcel.readString()!!
        isbn = parcel.readString()!!
        mpn = parcel.readString()!!
        location = parcel.readString()!!
        quantity = parcel.readString()!!
        stock = parcel.readString()!!
        saleable = parcel.readString()!!
        display_price = parcel.readString()!!
        category_id = parcel.readString()!!
        image = parcel.readString()!!
        manufacturer_id = parcel.readString()!!
        manufacturer = parcel.readString()!!
        price = parcel.readString()!!
        special = parcel.readString()!!
        reward = parcel.readString()!!
        points = parcel.readString()!!
        tax_class_id = parcel.readString()!!
        date_available = parcel.readString()!!
        weight = parcel.readString()!!
        weight_class_id = parcel.readString()!!
        length = parcel.readString()!!
        width = parcel.readString()!!
        height = parcel.readString()!!
        length_class_id = parcel.readString()!!
        subtract = parcel.readString()!!
        rating = parcel.readString()!!
        reviews = parcel.readString()!!
        minimum = parcel.readString()!!
        sort_order = parcel.readString()!!
        status = parcel.readString()!!
        date_added = parcel.readString()!!
        date_modified = parcel.readString()!!
        viewed = parcel.readString()!!
        uom = parcel.readString()!!
        flash_discount = parcel.readString()!!
        isexempted = parcel.readString()!!
        favorite = parcel.readByte() != 0.toByte()
        discount_percentage = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(product_id)
        parcel.writeString(name)
        parcel.writeString(selectQuant)
        parcel.writeString(description)
        parcel.writeString(meta_title)
        parcel.writeString(meta_description)
        parcel.writeString(meta_keyword)
        parcel.writeString(tag)
        parcel.writeString(model)
        parcel.writeString(sku)
        parcel.writeString(upc)
        parcel.writeString(ean)
        parcel.writeString(jan)
        parcel.writeString(isbn)
        parcel.writeString(mpn)
        parcel.writeString(location)
        parcel.writeString(quantity)
        parcel.writeString(stock)
        parcel.writeString(saleable)
        parcel.writeString(display_price)
        parcel.writeString(category_id)
        parcel.writeString(image)
        parcel.writeString(manufacturer_id)
        parcel.writeString(manufacturer)
        parcel.writeString(price)
        parcel.writeString(special)
        parcel.writeString(reward)
        parcel.writeString(points)
        parcel.writeString(tax_class_id)
        parcel.writeString(date_available)
        parcel.writeString(weight)
        parcel.writeString(weight_class_id)
        parcel.writeString(length)
        parcel.writeString(width)
        parcel.writeString(height)
        parcel.writeString(length_class_id)
        parcel.writeString(subtract)
        parcel.writeString(rating)
        parcel.writeString(reviews)
        parcel.writeString(minimum)
        parcel.writeString(sort_order)
        parcel.writeString(status)
        parcel.writeString(date_added)
        parcel.writeString(date_modified)
        parcel.writeString(viewed)
        parcel.writeString(uom)
        parcel.writeString(flash_discount)
        parcel.writeString(isexempted)
        parcel.writeByte(if (favorite) 1 else 0)
        parcel.writeString(discount_percentage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CartModel> {
        override fun createFromParcel(parcel: Parcel): CartModel {
            return CartModel(parcel)
        }

        override fun newArray(size: Int): Array<CartModel?> {
            return arrayOfNulls(size)
        }
    }


}