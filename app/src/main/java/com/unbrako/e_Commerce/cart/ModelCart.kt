package com.unbrako.e_Commerce.cart

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.dataBase.CartModel

class ModelCart(application: Application):AndroidViewModel(application){

    var refVar = RepoCart(application)

    fun getCartValues(): MutableLiveData<ArrayList<CartModel>> {

        return refVar.getCartValues()
    }



    fun verifyCoupon(amount: Double, code: String): MutableLiveData<ModelVerify> {
       return refVar.verifyCode(amount,code)
    }


}