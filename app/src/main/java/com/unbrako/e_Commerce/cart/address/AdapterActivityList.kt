package com.unbrako.e_Commerce.cart.address

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.unbrako.Constants
import com.unbrako.R
import kotlinx.android.synthetic.main.address_list_layout.view.*

class AdapterActivityList(var context: Context) : RecyclerSwipeAdapter<AdapterActivityList.MyViewHolder>() {


    private var items = ArrayList<GetAddressModel>()
    var listener = context as TickVisible

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(context).inflate(R.layout.address_list_layout, parent, false)
        return MyViewHolder(view)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val model = items[position]

        holder.itemView.constraint.setOnClickListener {
            listener.updateImage(model,position)
            Constants.getPrefs(context).edit().putString("shippingId", items[position].address_id).apply()
            listener.finishActivity(items[position])
        }

        if (model.status) {
            holder.itemView.imageTick.visibility = View.VISIBLE
        } else {
            holder.itemView.imageTick.visibility = View.GONE
        }

        holder.itemView.swipe.addDrag(
            SwipeLayout.DragEdge.Right,
            holder.itemView.swipe
                .findViewById(R.id.bottom_wrapper3)
        )

        holder.itemView.swipe.addSwipeListener(object : SwipeLayout.SwipeListener  {
            override fun onOpen(layout: SwipeLayout?) {
                mItemManger.closeAllExcept(layout)

            }

            override fun onUpdate(layout: SwipeLayout?, leftOffset: Int, topOffset: Int) {
            }

            override fun onStartOpen(layout: SwipeLayout?) {
            }

            override fun onStartClose(layout: SwipeLayout?) {
            }

            override fun onHandRelease(layout: SwipeLayout?, xvel: Float, yvel: Float) {
            }

            override fun onClose(layout: SwipeLayout?) {
            }

        })

        holder.itemView.bottom_wrapper3.setOnClickListener {
            holder.itemView.swipe.toggle(false)
            holder.itemView.swipe.close()

            notifyDataSetChanged()

        }

        mItemManger.bindView(holder.itemView, position)

        holder.itemView.name.text = items[position].firstname + ", " + items[position].lastname
        holder.itemView.address.text = items[position].address1 + ", " + items[position].city+ ", " + items[position].state


        holder.itemView.edit.setOnClickListener {
            holder.itemView.swipe.toggle(false)
            holder.itemView.swipe.close()
            listener.sendAddress(items[position],position)
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun update(items: ArrayList<GetAddressModel>) {

        this.items = items
        notifyDataSetChanged()
    }

    interface TickVisible {
        fun updateImage(position: GetAddressModel, position1: Int)
        fun sendAddress(items: GetAddressModel, position: Int)
        fun finishActivity(getAddressModel: GetAddressModel)

    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe
    }

}
