package com.unbrako.e_Commerce.cart.checkOut

import android.annotation.SuppressLint
import android.app.Application
import android.net.Uri
import android.os.AsyncTask
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.unbrako.Constants
import com.unbrako.JSONParser
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.signup.SignUpModel
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class RepoCheckOut(var application: Application) {
    var array = ArrayList<ModleUrl>()
    var mData = MutableLiveData<ArrayList<ModleUrl>>()
    var mDataS = MutableLiveData<ModelStatus>()
    var onPaymentSuccess = MutableLiveData<String>()
    var loadUrl = ""
    var userPaymentId = ""
    fun apiPaymentRequest(
        progressBar: ProgressBar,
        amount: String,
        purpose: String,
        buyer_name: String,
        email: String,
        phone: String
    ) {
        Payment(progressBar, amount, purpose, buyer_name, email, phone).execute()
    }

    @SuppressLint("StaticFieldLeak")
    inner class Payment(
        var progressBar: ProgressBar,
        var amount: String,
        var purpose: String,
        var buyer_name: String,
        var email: String,
        var phone: String
    ) : AsyncTask<Void, Void, String>() {
        var response: String = ""

        override fun doInBackground(vararg params: Void?): String? {

            response = insertMethod(
                amount.replace("₹", ""),
                purpose,
                buyer_name,
                email,
                phone,
                "false",
                "false",
                "false",
                "http://www.example.com/redirect/",
                "http://www.example.com/webhook/"
            )

            if (response.isNotEmpty()) {
                val json = JSONObject(response)
                val payment = json.optJSONObject("payment_request")
                loadUrl = payment.optString("longurl")
                userPaymentId = payment.optString("id")
            }
            return null
        }


        override fun onPreExecute() {
            super.onPreExecute()
            progressBar.visibility = View.VISIBLE

        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            progressBar.visibility = View.GONE
            if (loadUrl.isNotEmpty()) {
                val modelUrl = ModleUrl()
                modelUrl.url = loadUrl
                modelUrl.id = userPaymentId
                array.clear()
                array.add(modelUrl)
                mData.value = array


            } else {
                Toast.makeText(application, "Unable to get Data ", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun insertMethod(
        amount: String, purpose: String, buyer_name: String,
        email: String, phone: String, allow_repeated_payments: String, send_email: String,
        send_sms: String, redirect_url: String, webhook: String
    ): String {
        val res: String?
        val parser = JSONParser()

        //  var ammo=Constants.currencyFormt2(amount.toDouble())
        val builder = Uri.Builder()
            .appendQueryParameter("amount", amount)
            .appendQueryParameter("purpose", purpose)
            .appendQueryParameter("buyer_name", buyer_name)
            .appendQueryParameter("email", email)
            .appendQueryParameter("phone", phone)
            .appendQueryParameter("allow_repeated_payments", allow_repeated_payments)
            .appendQueryParameter("send_email", send_email)
            .appendQueryParameter("send_sms", send_sms)
            .appendQueryParameter("email", email)
            .appendQueryParameter("redirect_url", redirect_url)
            .appendQueryParameter("webhook", webhook)

//        res = parser.getJSONFromUrl("https://test.instamojo.com/api/1.1/payment-requests/", builder, application)
        res = parser.getJSONFromUrl("https://www.instamojo.com/api/1.1/payment-requests/", builder, application)
        return res
    }

    fun gettingUrl(): MutableLiveData<ArrayList<ModleUrl>> {

        return mData
    }

    fun creatingOrder(
        products: String,
        subtotal: String,
        paymentId: String,
        progressBar: ProgressBar,
        string: String,
        comment: String,
        tax: String,
        fullTotal: String,
        quotation_no: String,
        shippingCost: String,
        couponApplied: String,
        couponAmount: String,
        billingAddressId: String,
        appliedCredits: String
    ) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE

        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS).build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .client(httpClient.build()).build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.createOrder(
            auth!!,
            products,
            subtotal,
            "1",
            fullTotal,
            "instamojo",
            comment,
            paymentId,
            "debit card",
            string,
            tax,
            quotation_no,
            shippingCost,
            couponApplied,
            couponAmount,
            billingAddressId,
            appliedCredits
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
                        if (success) {
                            val m = ModelStatus()
                            m.status = success
                            m.order_id = json.optString("order_id")
                            m.total = json.optString("total")
                            m.msg = json.optString("msg")
                            mDataS.value = m
                        } else {
                            val m = ModelStatus()
                            m.status = success
//                            m.order_id = json.optString("order_id")
//                            m.total = json.optString("total")
                            m.msg = json.optString("msg")
                            mDataS.value = m
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                //  mData.value = array
                progressBar.visibility = View.GONE
            }
        })

    }

    fun getStatus(): MutableLiveData<ModelStatus> {

        return mDataS
    }

    fun getProfileData(
        gstNumber: TextView,
        company: TextView,
        availableCredits: TextView,
        appliedCredit: EditText,
        fgdg6: TextView
    ) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.getProfile(auth!!)
        call.enqueue(object : Callback<ResponseBody> {

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
//
                        if (success) {

                            val signUpObj = json.getJSONObject("profile")
                            val editorJson = Constants.getPrefs(application).edit()
                            editorJson.putString("signUpObj", signUpObj.toString())
                            editorJson.apply()
                            val loginModel = SignUpModel()
                            loginModel.customer_id = signUpObj.optString("customer_id")

                            loginModel.customer_group_id = signUpObj.optString("customer_group_id")
                            loginModel.store_id = signUpObj.optString("store_id")
                            loginModel.language_id = signUpObj.optString("language_id")
                            loginModel.firstname = signUpObj.optString("firstname")
                            loginModel.lastname = signUpObj.optString("lastname")
                            loginModel.email = signUpObj.optString("email")
                            loginModel.telephone = signUpObj.optString("telephone")
                            loginModel.fax = signUpObj.optString("fax")
                            loginModel.password = signUpObj.optString("password")
                            loginModel.salt = signUpObj.optString("salt")
                            loginModel.cart = signUpObj.optString("cart")
                            loginModel.wishlist = signUpObj.optString("wishlist")
                            loginModel.newsletter = signUpObj.optString("newsletter")
                            loginModel.address_id = signUpObj.optString("address_id")
                            loginModel.custom_field = signUpObj.optString("custom_field")
                            loginModel.ip = signUpObj.optString("ip")
                            loginModel.status = signUpObj.optString("status")
                            loginModel.safe = signUpObj.optString("safe")
                            loginModel.token = signUpObj.optString("token")
                            loginModel.code = signUpObj.optString("code")
                            loginModel.date_added = signUpObj.optString("date_added")
                            loginModel.company_name = signUpObj.optString("company_name")
                            loginModel.gst = signUpObj.optString("gst")
                            loginModel.landline = signUpObj.optString("landline")
                            loginModel.customer_type = signUpObj.optString("customer_type")
                            loginModel.discount = signUpObj.optString("discount")
                            loginModel.credit = signUpObj.optString("credit")

                            val gson = Gson()
                            val json1 = gson.toJson(loginModel)
                            Constants.getPrefs(application).edit().putString("profile", json1)
                                .apply()
                            gstNumber.text = loginModel.gst
                            company.text = loginModel.company_name
                            availableCredits.text = "(Available: " + loginModel.credit + ")"
                            try {
                                val dd: Double = loginModel.credit.toDouble()
                                if (dd > 0) {
                                    appliedCredit.visibility = View.VISIBLE
                                    fgdg6.visibility = View.VISIBLE
                                    availableCredits.visibility = View.VISIBLE
                                } else {
                                    appliedCredit.visibility = View.GONE
                                    fgdg6.visibility = View.GONE
                                    availableCredits.visibility = View.GONE
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    } catch (e: Exception) {
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })
    }

    fun checkPayment(userPaymentId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://www.instamojo.com/api/1.1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<JsonObject> = service.getPayment(userPaymentId)
        call.enqueue(object : Callback<JsonObject> {

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()
                        val payments = res?.getAsJsonObject("payment_request")?.getAsJsonArray("payments")
                        if (payments != null) {
                            if(!payments.isEmpty){
                                val payment_id = payments.get(0).asJsonObject.get("payment_id").asString
                                onPaymentSuccess.value = payment_id;
                            }
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                print("aaaaaaaaaaaaaaaaaaaa - $t")
            }

        })
    }


}
