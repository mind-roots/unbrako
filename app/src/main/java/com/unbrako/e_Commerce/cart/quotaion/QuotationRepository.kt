package com.unbrako.e_Commerce.cart.quotaion

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QuotationRepository(var application: Application) {

    val mData1 = MutableLiveData<QuotationModel>()


    fun getmDataComment(): LiveData<QuotationModel> {
        return mData1
    }

    fun addingCOmment(
        authCode: String,
        products: String,
        subtotal: String,
        fulltotal: String,
        comment: String,
        tax: String,
        progressBar: ProgressBar
    ) {

        val retrofit = Constants.getWebClient3()

        progressBar.visibility= View.VISIBLE

        val service = retrofit?.create(Webservice::class.java)
        val call: Call<ResponseBody> = service!!.saveInquiry(authCode, products, subtotal, fulltotal, comment, tax)

        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility= View.GONE
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)

                     val success = json.optBoolean("success")
                     val msg = json.optString("msg")
                     val inquiry_no = json.optString("inquiry_no")
                     val total = json.optString("total")

                        val quotationModel = QuotationModel()
                        quotationModel.success = success
                        quotationModel.msg = msg
                        quotationModel.inquiry_no = inquiry_no
                        quotationModel.total = total

                        mData1.value = quotationModel

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                progressBar.visibility= View.GONE
                Toast.makeText(application, "No Response", Toast.LENGTH_SHORT).show()
            }

        })

    }
}
