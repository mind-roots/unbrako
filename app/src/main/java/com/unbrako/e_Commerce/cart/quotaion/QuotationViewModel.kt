package com.unbrako.e_Commerce.cart.quotaion

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class QuotationViewModel(application: Application):AndroidViewModel(application) {
    fun addComment(
        authCode: String,
        products: String,
        subtotal: String,
        fulltotal: String,
        comment: String,
        tax: String,
        progressBar: ProgressBar
    ) {
        repo.addingCOmment(authCode,products,subtotal,fulltotal,comment,tax,progressBar)
    }


    fun getAddData() : LiveData<QuotationModel> {
        return  repo.getmDataComment()
    }

    var repo:QuotationRepository  = QuotationRepository(application)

}
