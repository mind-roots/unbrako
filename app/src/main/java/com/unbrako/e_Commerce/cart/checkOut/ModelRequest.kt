package com.unbrako.e_Commerce.cart.checkOut

class ModelRequest {
    lateinit var amount:String
    lateinit var purpose:String
    lateinit var buyer_name:String
    lateinit var email:String
    lateinit var phone:String
    lateinit var allow_repeated_payments:String
    lateinit var send_email:String
    lateinit var send_sms:String
    lateinit var redirect_url:String
    lateinit var webhook:String

}