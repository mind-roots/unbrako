package com.unbrako.e_Commerce.cart.address

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityThankyouActivityBinding

class ThankyouActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityThankyouActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_thankyou_activity)
        onClick()
        if (!intent.getStringExtra("quotation_no").equals("0")){
            Constants.deleteBasket(this).execute()
        }else {
            Constants.deleteCart(this).execute()
            Constants.getPrefs(application).edit().putString("showHome", "yes").apply()
            Constants.getPrefs(application).edit().putString("checkOut", "yes").apply()
        }
        if(intent.getStringExtra("customer_type").equals("3")){
            mBinding.amountText.text = "Amount to be paid - "
        }else{
            mBinding.amountText.text = "Amount paid - "
        }
    }

    @SuppressLint("SetTextI18n")
    private fun onClick() {

        if (!intent.getStringExtra("quotation_no").equals("0")){
            mBinding.done.text = "Back"
        }
//        mBinding.amountPaid.text = intent.getStringExtra("total")
        mBinding.amountPaid.text = Constants.currencyFormt(intent.getStringExtra("total")!!.toDouble())
        mBinding.orderNumber.text = intent.getStringExtra("orderId")
        mBinding.textView29.text = intent.getStringExtra("msg")
        mBinding.done.setOnClickListener {

            onBackPressed()
        }
    }

    override fun onBackPressed() {


        finish()
    }
}
