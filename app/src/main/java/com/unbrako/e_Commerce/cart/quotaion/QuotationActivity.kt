package com.unbrako.e_Commerce.cart.quotaion

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityQuotationBinding
import com.unbrako.e_Commerce.catalog.ModelTax
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.signup.SignUpModel
import org.json.JSONArray
import org.json.JSONObject

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "UNREACHABLE_CODE")
class QuotationActivity : AppCompatActivity(), CommentClick {


    lateinit var mBinding: ActivityQuotationBinding
    lateinit var mViewModel: QuotationViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_quotation)
        mViewModel = ViewModelProviders.of(this).get(QuotationViewModel::class.java)
        setToolbar()

        mBinding.click = this


        mViewModel.getAddData().observe(this, Observer {
            // Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()

            if (it.success) {
                showAlert(it.msg)

            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }

        })
        //mBinding.quotationaddComment.inputType = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES

        mBinding.quotationaddComment.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            @SuppressLint("SetTextI18n")
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let{
                    val remaining = it.length
                    mBinding.counter.text = "$remaining/500"
                }

            }

        })
    }


    private fun showAlert(msg: String) {
        val builder = AlertDialog.Builder(this@QuotationActivity)

        // Set the alert dialog title
        builder.setTitle("Alert!")

        // Display a message on alert dialog
        builder.setMessage(msg)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, which ->
            Constants.deleteCart(this).execute()
            //  Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show()
            Constants.getPrefs(this).edit().putString("gotoCart", "yes").apply()
            finish()
        }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }


    override fun setClick(addComment: String) {

        if (mBinding.quotationaddComment.text.isEmpty()) {
            Toast.makeText(this, "Please Add Comment", Toast.LENGTH_SHORT).show()
        } else {
            val authCode = Constants.getPrefs(this).getString(Constants.token, "")

            val orders: ArrayList<CartModel> = intent.getParcelableArrayListExtra<CartModel>("array")!!
            val jsonArray2 = JSONArray()
            var subtotal = 0.0
            var tax = 0.0
            for (i in 0 until orders.size) {
                var dd=0.0
                try {
                    dd = intent.getDoubleExtra("couponDiscount",0.0)
                } catch (e: Exception) {

                }
                val discount =
                    (orders[i].quantity.toDouble() / orders[i].saleable.toDouble() * orders[i].price.replace(
                        "₹",
                        ""
                    ).replace(
                        ",",
                        ""
                    ).toDouble() * orders[i].saleable.toDouble()) * orders[i].discount_percentage.toDouble() / 100

                subtotal += (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * orders[i].price.replace(
                    "₹",
                    ""
                ).toDouble())

                val jsonObject = JSONObject()

                jsonObject.put("id", orders[i].product_id)
                jsonObject.put("qty", (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()).toString())
                jsonObject.put("price", orders[i].price.replace("₹", "").replace(",", ""))
                jsonObject.put(
                    "tax",
                    getProductTax(
                        orders[i].tax_class_id,
                        orders[i].price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() * orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()
                    ).toString()
                )
                jsonObject.put("discount_percentage", calculateProductDiscount(orders[i],dd))
                jsonObject.put(
                    "total",
                    (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * orders[i].price.replace(
                        "₹",
                        ""
                    ).toDouble()).toString()
                )
                jsonArray2.put(jsonObject)

                tax += getProductTax(
                    orders[i].tax_class_id,
                    orders[i].price.replace("₹", "").replace(
                        ",",
                        ""
                    ).toDouble() * orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()
                )
            }

            val products = jsonArray2.toString()

            mViewModel.addComment(
                authCode!!,
                products,
                subtotal.toString(),
                intent.getStringExtra("fullTotal")!!,
                mBinding.quotationaddComment.text.toString(),
                tax.toString(), mBinding.progressBar
            )

        }


    }

    private fun calculateProductDiscount(
        cartModel: CartModel,
        couponDiscount: Double
    ): String? {
        val gson = Gson()
        val json1 = Constants.getPrefs(this).getString("profile", "")
        val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
        val flash_sale = Constants.getSharedPrefs(this).getString("flash_sale", "")

        if (cartModel.isexempted=="1"){

            if (flash_sale!="off"){
                val basePrice=(100*profileData.discount.toDouble())/100
                val amountLeft=100-basePrice
                val amountAfterFlashDiscount=(amountLeft*cartModel.flash_discount.toDouble())/100
                return (basePrice+amountAfterFlashDiscount).toString()
            }else{
                val basePrice=(100*profileData.discount.toDouble())/100
                val amountLeft=100-basePrice
                val amountAfterFlashDiscount=(amountLeft*couponDiscount)/100
                return (basePrice+amountAfterFlashDiscount).toString()
            }
        }else{
            if (flash_sale=="off") {

                return couponDiscount.toString()
            }else{
                if (cartModel.flash_discount.isEmpty()){
                    cartModel.flash_discount = "0"
                }
                val basePrice=(100*cartModel.flash_discount.toDouble())/100
                val amountLeft=100-basePrice
                val amountAfterFlashDiscount=(amountLeft*couponDiscount)/100
                return (basePrice+amountAfterFlashDiscount).toString()
            }
        }

    }


    fun getProductTax(tax_class_id: String, price: Double): Double {
        val gson = Gson()
        val response = Constants.getSharedPrefs(this).getString("rate", "")
        val lstArrayList = gson.fromJson<ArrayList<ModelTax>>(
            response,
            object : TypeToken<List<ModelTax>>() {

            }.type
        )
        for (i in 0 until lstArrayList.size) {
            if (lstArrayList[i].tax_class_id == tax_class_id) {
                if (lstArrayList[i].rate.isEmpty()) {
                    lstArrayList[i].rate = "0"
                }

                return (price * lstArrayList[i].rate.toDouble()) / 100
                break
            }
        }
        return 0.0
    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {

        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Place Enquiry"

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
