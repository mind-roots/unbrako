package com.unbrako.e_Commerce.cart.address

interface AddressEventListener {

    fun AddAddress(firstName : String, lastName : String, address : String, city : String, country : String, state : String, pincode : String)

}