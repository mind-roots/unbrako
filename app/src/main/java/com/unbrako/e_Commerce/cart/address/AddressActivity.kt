package com.unbrako.e_Commerce.cart.address

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityAddressBinding
import com.unbrako.e_Commerce.catalog.ZoneModel
import com.unbrako.e_Commerce.signup.SignUpModel


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddressActivity : AppCompatActivity(), AddressEventListener {


    lateinit var mBinding: ActivityAddressBinding
    lateinit var mViewModel: AddressListViewModel
    val position: Int = 0
    var items2: GetAddressModel = GetAddressModel()


    val newArray: ArrayList<String> = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_address)
        mViewModel = ViewModelProviders.of(this)[AddressListViewModel::class.java]

        setToolbar()
        mBinding.addAddress = this

        mViewModel.mAddAddressData().observe(this, Observer {
            if (it) {
                finish()
            }
        })
        setState()
        setAddressData()
    }

    fun setState() {

        mBinding.editState.setOnClickListener {
            openDialogQuantity(this)
        }
    }

    private fun openDialogQuantity(mContext: Context) {
        var color = "1"
        lateinit var dialog: android.app.AlertDialog

        val gson = Gson()
        val jsonText = Constants.getPrefs(this).getString("state", "")
        val stateArray = gson.fromJson<Array<ZoneModel>>(jsonText, Array<ZoneModel>::class.java)

        val arrayList: ArrayList<String> = ArrayList<String>()

        val emptyStringArray = arrayOfNulls<String>(stateArray.size)
        for (i in stateArray.indices) {
            arrayList.add(stateArray[i].name)
            emptyStringArray[i] = stateArray[i].name
        }

        arrayList.toArray()
        val builder = android.app.AlertDialog.Builder(mContext)

        // Set a title for alert dialog
        builder.setTitle("Select State")

        builder.setSingleChoiceItems(emptyStringArray, -1) { _, which ->
            color = emptyStringArray[which].toString()
            mBinding.editState.text = color
            items2.lastname
//            list[position].quantity = color
//            Constants.UpdateToCart(mContext, list[position]).execute()
//            cal.calculateTotal()
            dialog.dismiss()
        }

        dialog = builder.create()
        dialog.show()

    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbar.MainToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        if (intent.hasExtra("profile") || intent.hasExtra("editAddress")) {
            mBinding.toolbar.title.text = "Edit Address"
        } else {
            mBinding.toolbar.title.text = "Add Address"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun AddAddress(
        firstName1: String,
        lastName: String,
        address: String,
        city: String,
        country: String,
        state: String,
        pincode: String
    ) {

        val model = ModelAddAddress()
        model.company = firstName1
        model.firstName = lastName.split(" ")[0]
        var lastNamew = ""
        var lastNamewee = lastName.split(" ")
        for (l in 0 until lastNamewee.size) {
            if (l > 0) {
                lastNamew = lastNamew + " " + lastNamewee[l]
            }
        }
        model.lastName = lastNamew
        model.address = address
        model.city = city
        model.country = country
        model.state = state
        model.pincode = pincode

        if (intent.hasExtra("editAddress")) {
            val mm: GetAddressModel = intent.getParcelableExtra("editAddress")!!

            mm.company = firstName1
            mm.firstname = lastName.split(" ")[0]
            var lastNamew = ""
            val lastNamewqq = lastName.split(" ")
            for (l in 0 until lastNamewqq.size) {
                if (l > 0) {
                    lastNamew = lastNamew + " " + lastNamewqq[l]
                }
            }
            mm.lastname = lastNamew
            mm.address1 = address
            mm.city = city
            mm.state = state
            mm.pincode = pincode

            when {
                firstName1.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.company_error), Toast.LENGTH_SHORT).show()
                    return
                }
                lastName.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.person_error), Toast.LENGTH_SHORT).show()
                    return
                }
                lastName.trim().split(" ").size < 2 -> {
                    Toast.makeText(this, "Please enter full name", Toast.LENGTH_SHORT).show()
                    return
                }
                address.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.address_error), Toast.LENGTH_SHORT).show()
                    return
                }
                city.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.city_error), Toast.LENGTH_SHORT).show()
                    return
                }

                pincode.isEmpty() -> {
                    Toast.makeText(this, "Enter 6 digit pin code.", Toast.LENGTH_SHORT).show()
                    return
                }
                pincode.length < 6 -> {
                    Toast.makeText(this, "Enter 6 digit pin code.", Toast.LENGTH_SHORT).show()
                    return
                }
                state.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.state_error), Toast.LENGTH_SHORT).show()
                    return
                }

            }
            var bill = "0"
            if (intent.hasExtra("bill")) {
                bill = "1"
            }
            mViewModel.GetUdateAllAddress(mm, mBinding.progressBar, bill)

            mViewModel.getupdateAddress().observe(this, Observer {
                if (it.success) {
                    val intent1 = Intent()
                    setResult(1234, intent1)
                    finish()
                }
            })

        } else {
            when {
                firstName1.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.company_error), Toast.LENGTH_SHORT).show()
                    return
                }
                lastName.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.person_error), Toast.LENGTH_SHORT).show()
                    return
                }
                lastName.trim().split(" ").size < 2 -> {
                    Toast.makeText(this, "Please enter full name", Toast.LENGTH_SHORT).show()
                    return
                }
                address.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.address_error), Toast.LENGTH_SHORT).show()
                    return
                }
                city.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.city_error), Toast.LENGTH_SHORT).show()
                    return
                }
                pincode.isEmpty() -> {
                    Toast.makeText(this, "Enter 6 digit pin code.", Toast.LENGTH_SHORT).show()
                    return
                }
                pincode.length < 6 -> {
                    Toast.makeText(this, "Enter 6 digit pin code.", Toast.LENGTH_SHORT).show()
                    return
                }
                state.isEmpty() -> {
                    Toast.makeText(this, getString(R.string.state_error), Toast.LENGTH_SHORT).show()
                    return
                }

            }
            var bill = "0"
            if (intent.hasExtra("bill")) {
                bill = "1"
            }
            mViewModel.addAddressData(model, mBinding.progressBar, bill)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setAddressData() {
         if (intent.hasExtra("editAddress")) {
            items2 = intent.getParcelableExtra<GetAddressModel>("editAddress")!!

            if (intent.hasExtra("editAddress")) {
                mBinding.add.text = "Update Address"
                mBinding.editFirstName.setText(items2.company)
                mBinding.editLastName.setText(items2.firstname + " " + items2.lastname)
                mBinding.editAddress.setText(items2.address1)
                mBinding.editCity.setText(items2.city)
                mBinding.pincode.setText(items2.postcode)
                val zoneModel = ZoneModel()
                zoneModel.zone_id = "0"
                zoneModel.name = items2.state
                mBinding.editState.text = zoneModel.name


            } else {
                mBinding.add.text = "Add"
                val mVariable = Gson()
                val json = Constants.getPrefs(this).getString("profile", "")
                val profileData: SignUpModel = mVariable.fromJson(json, SignUpModel::class.java)
                mBinding.editFirstName.setText(profileData.company_name.toString())
                mBinding.editLastName.setText(profileData.firstname + " " + profileData.lastname)

//                mViewModel.getAllAddress(mBinding.progressBar)
//
//                mViewModel.mGetAddressData().observe(this, Observer {
//
//                    if (it.isNotEmpty()) {
//
//                        if (it.size > 0) {
//                            for (i in 0 until it.size) {
//                                if (Constants.getPrefs(this).getString("shippingId", "").isEmpty()) {
//                                    it[i].status = i == 0
//                                    mBinding.editFirstName.setText(it[0].company)
//                                    mBinding.editLastName.setText(it[0].firstname + " " + it[0].lastname)
//
//                                } else {
//                                    it[i].status =
//                                        Constants.getPrefs(this).getString("shippingId", "") == it[i].address_id
//                                    if (Constants.getPrefs(this).getString("shippingId", "") == it[i].address_id) {
//                                        mBinding.editFirstName.setText(it[i].company)
//                                        mBinding.editLastName.setText(it[i].firstname + " " + it[i].lastname)
//                                    }
//                                }
//                            }
//                            // mAdapter.update(it)
//                        }
//                    }
//                })
            }
        } else {
             mBinding.add.text = "Add"
             val mVariable = Gson()
             val json = Constants.getPrefs(this).getString("profile", "")
             val profileData: SignUpModel = mVariable.fromJson(json, SignUpModel::class.java)
             mBinding.editFirstName.setText(profileData.company_name.toString())
             mBinding.editLastName.setText(profileData.firstname + " " + profileData.lastname)

//             mViewModel.getAllAddress(mBinding.progressBar)
//
//            mViewModel.mGetAddressData().observe(this, Observer {
//
//                if (it.isNotEmpty()) {
//
//                    if (it.size > 0) {
//                        for (i in 0 until it.size) {
//                            if (Constants.getPrefs(this).getString("shippingId", "").isEmpty()) {
//                                it[i].status = i == 0
//                                mBinding.editFirstName.setText(it[0].company)
//                                mBinding.editLastName.setText(it[0].firstname + " " + it[0].lastname)
//
//                            } else {
//                                it[i].status = Constants.getPrefs(this).getString("shippingId", "") == it[i].address_id
//                                if (Constants.getPrefs(this).getString("shippingId", "") == it[i].address_id) {
//                                    mBinding.editFirstName.setText(it[i].company)
//                                    mBinding.editLastName.setText(it[i].firstname + " " + it[i].lastname)
//
//                                }
//                            }
//                        }
//                        // mAdapter.update(it)
//                    }
//                }
//            })
        }
    }

}
