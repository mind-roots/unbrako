package com.unbrako.e_Commerce.cart.Payment

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.unbrako.R
import com.unbrako.databinding.ActivityPaymentBinding
import kotlinx.android.synthetic.main.etoolbar_layout.view.*

class PaymentActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityPaymentBinding
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_payment)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_payment)
        setToolBar()
        val url = intent.getStringExtra("url")

        mBinding.webView.settings.javaScriptEnabled = true
        mBinding.webView.settings.domStorageEnabled = true
        mBinding.webView.settings.allowFileAccess = true
        mBinding.webView.settings.useWideViewPort = true
        mBinding.webView.settings.pluginState = WebSettings.PluginState.ON
//        mBinding.webView.settings.userAgentString = "Android WebView"
        mBinding.webView.webChromeClient = WebChromeClient()
        mBinding.webView.loadUrl(url!!)
//        mBinding.webView.addJavascriptInterface( MyJavaScriptInterface(),
//            "android")
       // mBinding.webView.webViewClient = WebViewClient()

// you need to setWebViewClient for forcefully open in your webview
        mBinding.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                Log.i("UUUU: ", url)
                view.loadUrl(url)

                if (url.contains("example.com")){
                    val uri = Uri.parse(url)
                   val paymentId =  uri.getQueryParameter("payment_id")
                   var paymentRequestId =  uri.getQueryParameter("payment_request_id")
                    var intent= Intent()
                    intent.putExtra("paymentId",paymentId)
                    setResult(123,intent)
                    finish()

                }else if (url.contains("intnet")){
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    startActivity(intent)
                }

                return true
            }

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId==android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
    private fun setToolBar() {
        setSupportActionBar(mBinding.checkoutToolbar.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.checkoutToolbar.toolbarTitle.text = "Payment"
    }

}

internal class MyJavaScriptInterface {
    @JavascriptInterface
    fun onUrlChange(url: String) {
        Log.d("hydrated", "onUrlChange$url")
    }
}
