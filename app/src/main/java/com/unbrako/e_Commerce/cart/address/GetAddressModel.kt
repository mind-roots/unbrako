package com.unbrako.e_Commerce.cart.address

import android.os.Parcel
import android.os.Parcelable

class GetAddressModel() : Parcelable {


    var address_id : String = ""
    var customer_id : String = ""
    var firstname : String = ""
    var lastname : String = ""
    var company : String = ""
    var address_2 : String = ""
    var city : String = ""
    var postcode : String = ""
    var country_id : String = ""
    var zone_id : String = ""
    var custom_field : String = ""
    var address1 : String = ""
    var state : String = ""
    var pincode : String = ""
    var status : Boolean = false
    var success: Boolean = true


    constructor(parcel: Parcel) : this() {
        address_id = parcel.readString()!!
        customer_id = parcel.readString()!!
        firstname = parcel.readString()!!
        lastname = parcel.readString()!!
        company = parcel.readString()!!
        address_2 = parcel.readString()!!
        city = parcel.readString()!!
        postcode = parcel.readString()!!
        country_id = parcel.readString()!!
        zone_id = parcel.readString()!!
        custom_field = parcel.readString()!!
        address1 = parcel.readString()!!
        state = parcel.readString()!!
        pincode = parcel.readString()!!
        status = parcel.readByte() != 0.toByte()
        success = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(address_id)
        parcel.writeString(customer_id)
        parcel.writeString(firstname)
        parcel.writeString(lastname)
        parcel.writeString(company)
        parcel.writeString(address_2)
        parcel.writeString(city)
        parcel.writeString(postcode)
        parcel.writeString(country_id)
        parcel.writeString(zone_id)
        parcel.writeString(custom_field)
        parcel.writeString(address1)
        parcel.writeString(state)
        parcel.writeString(pincode)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeByte(if (success) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GetAddressModel> {
        override fun createFromParcel(parcel: Parcel): GetAddressModel {
            return GetAddressModel(parcel)
        }

        override fun newArray(size: Int): Array<GetAddressModel?> {
            return arrayOfNulls(size)
        }
    }


}
