package com.unbrako.e_Commerce.cart.quotaion

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface Webservice {

    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/saveInquiry")
    fun saveInquiry(
        @Field("auth_code") auth_code: String,
        @Field("products") products: String,
        @Field("sub_total") sub_total: String,
        @Field("total") total: String,
        @Field("comment") comment: String,
        @Field("tax") tax: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getInquires")
    fun getInquires(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/myInvoice")
    fun myInvoice(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>
@FormUrlEncoded
    @POST("?route=mobileapi/catalog/getDisputeMsg")
    fun getMessage(
        @Field("auth_code") auth_code: String,
        @Field("dispute_id") dispute_id: String
    ): Call<ResponseBody>

     @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getDisputes")
    fun getDisputes(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getInvoiceDetails")
    fun getInvoiceDetails(
        @Field("auth_code") auth_code: String,
        @Field("invoice_no") invoicNo: String
    ): Call<ResponseBody>
@FormUrlEncoded
    @POST("?route=mobileapi/catalog/getDisputeDetails")
    fun getDisputeDetails(
        @Field("auth_code") auth_code: String,
        @Field("dispute_id") invoicNo: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getQuotationInfo")
    fun quoteInfo(
        @Field("auth_code") auth_code: String,
        @Field("quotation_no") quote_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/declineQuote")
    fun declineQuote(
        @Field("auth_code") auth_code: String,
        @Field("quotation_no") quote_id: String,
        @Field("comment") comment: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getInquiryInfo")
    fun getInquiryInfo(
        @Field("auth_code") auth_code: String,
        @Field("inquiry_no") inquiry_no: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/applyCoupon")
    fun verifyCoupon(
        @Field("auth_code") auth_code: String,
        @Field("amount") amount: String,
        @Field("code") code: String
    ): Call<ResponseBody>




}


