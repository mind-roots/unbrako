package com.unbrako.e_Commerce.cart.checkOut

import android.app.Application
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class CheckOutModel(application: Application) : AndroidViewModel(application) {
    fun paymentRequest(
        progressBar: ProgressBar,
        amount: String,
        purpose: String,
        buyer_name: String,
        email: String,
        phone: String
    ) {
        refVar.apiPaymentRequest(progressBar, amount, purpose, buyer_name, email, phone)
    }

    fun getUrl() :MutableLiveData<ArrayList<ModleUrl>>{

        return refVar.gettingUrl()
    }
    fun onPaymentSuccess() :MutableLiveData<String>{

        return refVar.onPaymentSuccess
    }

    fun createOrder(
        products: String,
        subtotal: String,
        paymentId: String,
        progressBar: ProgressBar,
        string: String,
        comment: String,
        tax: String,
        fullTotal: String,
        quotation_no: String,
        shipping_cost: String,
        coupon_applied: String,
        coupon_amount: String,
        billingAddressId: String,
        appliedCredits: String
    ){

  refVar.creatingOrder(products,subtotal,paymentId,
      progressBar,string,comment,tax, fullTotal,quotation_no
      ,shipping_cost,coupon_applied,coupon_amount,billingAddressId,appliedCredits)
    }

    fun createOrderStatus():  MutableLiveData<ModelStatus> {

        return refVar.getStatus()
    }

    fun getProfile(
        gstNumber: TextView,
        company: TextView,
        availableCredits: TextView,
        appliedCredit: EditText,
        fgdg6: TextView
    ) {

        refVar.getProfileData(gstNumber,company,availableCredits,appliedCredit,fgdg6)
    }

    fun checkPaymentStatus(userPaymentId: String) {
        refVar.checkPayment(userPaymentId)
    }


    var refVar:RepoCheckOut=RepoCheckOut(application)

}