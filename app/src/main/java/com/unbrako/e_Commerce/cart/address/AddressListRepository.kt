package com.unbrako.e_Commerce.cart.address

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class AddressListRepository(var application: Application) {


    val mData = MutableLiveData<Boolean>()
    val mgetAddress = MutableLiveData<ArrayList<GetAddressModel>>()
    val mgupdateAddress = MutableLiveData<GetAddressModel>()
    val mgupdateAddress1 = ArrayList<GetAddressModel>()



    fun mAddAddress(): LiveData<Boolean> {

        return mData

    }

    fun mGetAddress(): LiveData<ArrayList<GetAddressModel>> {
        return mgetAddress
    }

    fun addAddressRepo(
        model: ModelAddAddress,
        progressBar: ProgressBar,
        bill: String
    ) {


        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.addAddress(
            model.company,
            model.firstName,
            model.lastName,
            model.address,
            model.city,
            model.country,
            model.state,
            auth!!,
            model.pincode,bill
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                progressBar.visibility = View.GONE


                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
                        if (success) {

                            mData.value = true

                        }else{
                            Toast.makeText(application,json.optString("msg"),Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.VISIBLE
            }
        })


    }


    fun getAllAddressRepo(progressBar: ProgressBar) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        //progressBar.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.getalladdresses(auth!!)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                   // progressBar.visibility = View.GONE


                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
                        if (success) {

                            val allAddress = json.getJSONArray("all_address")
                            mgupdateAddress1.clear()
                            for (i in 0 until allAddress.length()) {
                                val getAddressModel = GetAddressModel()

                                val allAddressJson = allAddress.getJSONObject(i)

                                getAddressModel.address_id = allAddressJson.optString("address_id")
                                getAddressModel.customer_id = allAddressJson.optString("customer_id")
                                getAddressModel.firstname = allAddressJson.optString("firstname")
                                getAddressModel.lastname = allAddressJson.optString("lastname")
                                getAddressModel.company = allAddressJson.optString("company_name")
                                getAddressModel.address_2 = allAddressJson.optString("address_2")
                                getAddressModel.city = allAddressJson.optString("city")
                                getAddressModel.postcode = allAddressJson.optString("postcode")
                                getAddressModel.country_id = allAddressJson.optString("country_id")
                                getAddressModel.zone_id = allAddressJson.optString("zone_id")
                                getAddressModel.custom_field = allAddressJson.optString("custom_field")
                                getAddressModel.address1 = allAddressJson.optString("address1")
                                getAddressModel.state = allAddressJson.optString("state")
                               // getAddressModel.state = allAddressJson.optString("allAddressModel")

                                mgupdateAddress1.add(getAddressModel)

                            }
                            mgetAddress.value = mgupdateAddress1

                        }
                    } catch (e: Exception) {

                        e.printStackTrace()

                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
               // progressBar.visibility = View.VISIBLE
            }
        })

    }

    fun getMUpdateAddress(): LiveData<GetAddressModel>{

        return mgupdateAddress

    }

    fun getUpdateAllAddress(
        allAddressModel: GetAddressModel,
        progressBar: ProgressBar,
        bill: String
    ) {


        progressBar.visibility = View.VISIBLE

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.updateAddress(allAddressModel.firstname, allAddressModel.lastname, allAddressModel.address1, allAddressModel.city, "99",allAddressModel.state, allAddressModel.address_id, allAddressModel.company,auth!!,allAddressModel.pincode,bill)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    progressBar.visibility = View.GONE

                    Constants.setLoggedIn(application, true)

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
                        if (success) {
                            allAddressModel.success = true
                            mgupdateAddress.value=allAddressModel
                        }

                    } catch (e: Exception) {
                    }
                }
            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.VISIBLE
            }
        })
    }

    fun getAddressList(): ArrayList<ModelActivityList>{
        val mData = ArrayList<ModelActivityList>()
        val arrayList = ArrayList<ModelActivityList>()

        var modelActivityList = ModelActivityList()
        modelActivityList.heading = "Oliver"
        modelActivityList.addressDetails ="Shipping addresses are the addressng ."
        modelActivityList.status = true

        arrayList.add(modelActivityList)
        modelActivityList = ModelActivityList()
        modelActivityList.heading = "Oliver"
        modelActivityList.addressDetails ="Shipping addresses are the addressng ."
        modelActivityList.status = false

        arrayList.add(modelActivityList)
        modelActivityList = ModelActivityList()
        modelActivityList.heading = "Oliver"
        modelActivityList.addressDetails = "Shipping addresses are the addressng ."
        modelActivityList.status = false

        arrayList.add(modelActivityList)
        modelActivityList = ModelActivityList()
        modelActivityList.heading = "Oliver"
        modelActivityList.addressDetails ="Shipping addresses are the address to which you would like your order shipped. Mailing addresses are the address where you currently receive mail."
        modelActivityList.status = false

        arrayList.add(modelActivityList)
        modelActivityList = ModelActivityList()
        modelActivityList.heading = "Oliver"
        modelActivityList.addressDetails = "Shipping addresses are the address to which you would currently."
        modelActivityList.status = false
        arrayList.add(modelActivityList)

//        mData.value = arrayList
        return arrayList


    }


}