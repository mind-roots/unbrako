package com.unbrako.e_Commerce.cart.address

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class AddressListViewModel(application: Application) : AndroidViewModel(application) {

    var addressListRepository : AddressListRepository = AddressListRepository(application)

    fun addAddressData(
        model: ModelAddAddress,
        progressBar: ProgressBar,
        bill: String
    ) {

        return addressListRepository.addAddressRepo(model, progressBar,bill)
    }

    fun mAddAddressData() : LiveData<Boolean>{
     return   addressListRepository.mAddAddress()
    }

    fun mGetAddressData() : LiveData<ArrayList<GetAddressModel>>{
     return   addressListRepository.mGetAddress()
    }

    fun getAllAddress(progressBar: ProgressBar) {
        return addressListRepository.getAllAddressRepo(progressBar)
    }

    fun getupdateAddress(): LiveData<GetAddressModel> {
        return addressListRepository.getMUpdateAddress()

    }

    fun GetUdateAllAddress(
        model: GetAddressModel,
        progressBar: ProgressBar,
        bill: String
    ) {
        addressListRepository.getUpdateAllAddress(model, progressBar,bill)
    }

//    fun getMData() : ArrayList<ModelActivityList>{
//
//    }


    fun getData() : ArrayList<ModelActivityList>{
        return addressListRepository.getAddressList()
    }

}