package com.unbrako.e_Commerce.cart

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import com.unbrako.e_Commerce.dataBase.CartModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RepoCart(var application: Application) {

    var array = ArrayList<CartModel>()
    var mData = MutableLiveData<ArrayList<CartModel>>()
    var verifyData = MutableLiveData<ModelVerify>()
    fun getCartValues(): MutableLiveData<ArrayList<CartModel>> {

        getProductsOfCart().execute()
        mData.value=array
        return mData
    }

    fun verifyCode(amount: Double, code: String):MutableLiveData<ModelVerify> {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()


        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.verifyCoupon(auth!!, amount.toString(),code)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success1 = json.optBoolean("success")


                    if (success1) {
                        val model=ModelVerify()
                        model.error=json.optString("error")
                        model.success=success1
                        val dd=json.optJSONObject("coupon_info")
                        model.type=dd.optString("type")
                        model.discount=dd.optString("discount")
                        model.code=dd.optString("code")
                        verifyData.value=model
                    }else{
                        val model=ModelVerify()
                        model.error=json.optString("error")
                        model.success=success1
                        verifyData.value=model
                    }


                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val model=ModelVerify()
                model.error="Network Error"
                model.success=false
                 verifyData.value=model

            }

        })
        return verifyData

    }



    inner class getProductsOfCart : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(application)

            array =  db!!.productDao().getAllCart() as ArrayList<CartModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mData.value = array
        }
    }


}