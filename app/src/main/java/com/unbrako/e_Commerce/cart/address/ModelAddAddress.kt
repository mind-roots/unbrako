package com.unbrako.e_Commerce.cart.address

class ModelAddAddress {

    var company: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var address: String = ""
    var city: String = ""
    var country: String = ""
    var state: String = ""
    var pincode: String = ""
    var success: Boolean = true

}
