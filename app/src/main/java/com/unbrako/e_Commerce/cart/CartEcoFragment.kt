package com.unbrako.e_Commerce.cart

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.CartEventListener
import com.unbrako.Constants
import com.unbrako.databinding.ChangeDeliveryTypeBinding
import com.unbrako.databinding.FragmentEcommerceCartBinding
import com.unbrako.e_Commerce.cart.checkOut.CheckOutActivity
import com.unbrako.e_Commerce.catalog.ModelTax
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.updateCart
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.annotation.SuppressLint
import android.widget.Toast
import com.unbrako.R
import com.unbrako.databinding.ItemApplycouponBinding
import com.unbrako.e_Commerce.cart.quotaion.QuotationActivity


/**
 * Created by Manisha Thakur on 1/4/19.
 */
@Suppress(
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "UNREACHABLE_CODE",
    "DEPRECATION"
)
class CartEcoFragment : Fragment(), CartEventListener, AdapterCartEcommerce.Calculations {


    private var hasFlash: Boolean = false
    private var quotation_no: String = "0"
    private lateinit var adapterCartEco: AdapterCartEcommerce
    private lateinit var mBinding: FragmentEcommerceCartBinding
    private lateinit var viewModel: ModelCart
    lateinit var listener: CartEventListener
    lateinit var update: updateCart
    lateinit var appliedCoupon: AppliedCoupon
    var cartArray: ArrayList<CartModel> = ArrayList()
    var totalPrice = 0.0
    var totalPriceWithGst = 0.0
    var check: Int = -1
    var price = 0.0
    var tax = 0.0
    var couponDiscount = 0.0
    var coupon_applied = 0
    var coupon_amount = ""
    var type = ""
    var code = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_ecommerce_cart, container, false)
        viewModel = ViewModelProviders.of(this)[ModelCart::class.java]

        return mBinding.root
    }


    override fun onResume() {
        super.onResume()

        val flash_sale = Constants.getSharedPrefs(requireActivity()).getString("flash_sale", "")
        /* if (flash_sale!="off"){
             mBinding.applyCoupon.visibility=View.GONE
             mBinding.dddf.visibility=View.GONE
             mBinding.dddf.visibility=View.GONE
             mBinding.viewPrice.visibility=View.INVISIBLE
             couponDiscount=0.0
         }else{
             mBinding.applyCoupon.visibility=View.VISIBLE
             mBinding.dddf.visibility=View.VISIBLE
             mBinding.dddf.visibility=View.VISIBLE
             mBinding.viewPrice.visibility=View.VISIBLE
         }*/
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.rvCart.isNestedScrollingEnabled = false
        mBinding.rvCart.layoutManager = LinearLayoutManager(activity)
        mBinding.rvCart.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )
        adapterCartEco = AdapterCartEcommerce(requireActivity(), this@CartEcoFragment)
        mBinding.rvCart.adapter = adapterCartEco
        // appliedCoupon.appliedCoupons(0,0.0,"","")
        viewModel.getCartValues().observe(viewLifecycleOwner, Observer {
            if (it.size > 0) {
                checkStockAvailable(it)
                appliedCoupon.appliedCoupons(0, 0.0, "", "")
                hasFlash = checkHasFlash(it)

                cartArray = it
                adapterCartEco.update(it)
                mBinding.noData.visibility = View.GONE
                mBinding.nestedScrollView.visibility = View.VISIBLE

                for (i in 0 until it.size) {
                    val modelSearchEco = it[i]
                    val gson = Gson()
                    val json = gson.toJson(modelSearchEco)
                    val obj: CartModel = gson.fromJson(json, CartModel::class.java)

//                    it[i].price.replace("₹", "").replace(",", "")
                    price += (it[i].selectQuant.toDouble() * Constants.getDiscountedPriceCart(
                        obj,
                        requireActivity()
                    ) * it[i].saleable.toDouble())
                    tax += getProductTax(
                        it[i].tax_class_id,
                        (it[i].selectQuant.toDouble() * Constants.getDiscountedPriceCart(
                            obj,
                            requireActivity()
                        ) * it[i].saleable.toDouble())
                    )
                }
                val taxUnbrako =
                    Constants.getSharedPrefs(requireContext()).getString("unbrako_tax", "0")!!
                        .toDouble()

                mBinding.tvPriceCost.text = "₹" + Constants.currencyFormt(price)
                //val rate = Constants.getPrefs(activity!!).getString("rate", "0").toDouble()
                mBinding.tvTaxCost.text = "₹" + Constants.currencyFormt(price * taxUnbrako / 100)
                mBinding.textView8.text =
                    "₹" + Constants.currencyFormt((price + price * taxUnbrako / 100))
                totalPrice = price
                totalPriceWithGst = price + price * taxUnbrako / 100



                if (appliedCoupon.getAppliedCoupon() == 1) {
                    couponDiscount = appliedCoupon.getCouponDiscount()
                    type = appliedCoupon.getType()
                    mBinding.tvDiscount.visibility = View.VISIBLE
                    mBinding.tvDiscountCost.visibility = View.VISIBLE
                    mBinding.cross.visibility = View.VISIBLE
                    showPriceInTexts(price)
                    mBinding.applyCoupon.text = "Applied coupon: ${appliedCoupon.getCode()}"
                } else {
                    mBinding.cross.visibility = View.GONE
                    mBinding.tvDiscount.visibility = View.GONE
                    mBinding.tvDiscountCost.visibility = View.GONE
                }
            } else {
                mBinding.noData.visibility = View.VISIBLE
                mBinding.nestedScrollView.visibility = View.GONE
            }

        })
        mBinding.paynow = this

        mBinding.rlEdit.setOnClickListener {
            showDialog()
        }
        mBinding.applyCoupon.setOnClickListener {
            val flash_sale = Constants.getSharedPrefs(requireActivity()).getString("flash_sale", "")
            if (hasFlash && flash_sale == "on") {
                Toast.makeText(
                    activity,
                    "You can not apply coupon during flash sale",
                    Toast.LENGTH_LONG
                ).show()

            } else {
                showCouponDialog(price)
            }
        }
        mBinding.cross.setOnClickListener {


            coupon_applied = 0
            couponDiscount = 0.0

            showPriceInTexts(price)

            appliedCoupon.appliedCoupons(0, 0.0, "", "")

            if (appliedCoupon.getAppliedCoupon() == 1) {
                couponDiscount = appliedCoupon.getCouponDiscount()
                type = appliedCoupon.getType()
                mBinding.tvDiscount.visibility = View.VISIBLE
                mBinding.tvDiscountCost.visibility = View.VISIBLE
                mBinding.cross.visibility = View.VISIBLE
                showPriceInTexts(price)
                mBinding.applyCoupon.text = "Applied coupon: ${appliedCoupon.getCode()}"
            } else {
                mBinding.applyCoupon.text = "Tap here to add Coupon code"
                mBinding.cross.visibility = View.GONE
                mBinding.tvDiscount.visibility = View.GONE
                mBinding.tvDiscountCost.visibility = View.GONE
            }
        }
        addComments()


    }

    private fun checkStockAvailable(list: ArrayList<CartModel>) {
        var text = "• Standard Delivery (15-30 days) \n• Delivery for unavailable stock (30 - 60 days)"
        for (item in list) {
            if (item.stock.isEmpty()) {
                text = "• Standard Delivery (15-30 days) \n• Delivery for unavailable stock (30 - 60 days)"
                break
            }
            var value = Integer.valueOf(item.stock)
            if (value < 1) {
                text = "• Standard Delivery (15-30 days) \n• Delivery for unavailable stock (30 - 60 days)"
                break
            }
        }
        mBinding.deliveryType.text = text
    }

    private fun checkHasFlash(it: java.util.ArrayList<CartModel>): Boolean {
        var hasFlash = false
        for (item in it) {
            if (item.flash_discount.isEmpty()) {
                item.flash_discount = "0"
            }
            if (item.flash_discount.toDouble() > 0) {
                hasFlash = true
                break
            }
        }

        return hasFlash
    }

    @SuppressLint("SetTextI18n")
    private fun showCouponDialog(amount: Double) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
        builder.setTitle("Apply Coupon")

        val mbinding: ItemApplycouponBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(activity),
                R.layout.item_applycoupon,
                null,
                false
            )


        builder.setView(mbinding.root)
        val alertDialog = builder.create()

        //builder.setCancelable(false)
        mbinding.cancel.setOnClickListener {
            alertDialog.dismiss()
        }
        //mbinding.couponNumber.addTextChangedListener()
        mbinding.verify.setOnClickListener {
            mbinding.progress.visibility = View.VISIBLE
            viewModel.verifyCoupon(amount, mbinding.couponNumber.text.toString())
                .observe(viewLifecycleOwner, Observer {
                    mbinding.progress.visibility = View.GONE
                    if (!it.success) {
                        coupon_applied = 0
                        mbinding.text.text = it.error
                    } else {
                        type = it.type
                        coupon_applied = 1
                        couponDiscount = it.discount.toDouble()
                        mBinding.tvDiscount.visibility = View.VISIBLE
                        mBinding.tvDiscountCost.visibility = View.VISIBLE
                        showPriceInTexts(price)
                        mBinding.applyCoupon.text = "Applied coupon: " + it.code
                        appliedCoupon.appliedCoupons(coupon_applied, couponDiscount, it.code, type)


                        if (appliedCoupon.getAppliedCoupon() == 1) {
                            couponDiscount = appliedCoupon.getCouponDiscount()
                            type = appliedCoupon.getType()
                            mBinding.tvDiscount.visibility = View.VISIBLE
                            mBinding.tvDiscountCost.visibility = View.VISIBLE
                            mBinding.cross.visibility = View.VISIBLE
                            showPriceInTexts(price)
                            mBinding.applyCoupon.text = "Applied coupon: ${appliedCoupon.getCode()}"
                        } else {
                            mBinding.cross.visibility = View.GONE
                            mBinding.tvDiscount.visibility = View.GONE
                            mBinding.tvDiscountCost.visibility = View.GONE
                        }
                        alertDialog.dismiss()
                    }

                    // calculateTotal()
                })
        }


        alertDialog.show()

    }

    @SuppressLint("ClickableViewAccessibility")
    private fun addComments() {
        mBinding.editText.setOnTouchListener(OnTouchListener { v, event ->
            if (mBinding.editText.hasFocus()) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_SCROLL -> {
                        v.parent.requestDisallowInterceptTouchEvent(false)
                        return@OnTouchListener true
                    }
                }
            }
            false
        })
    }


    override fun payNow() {
        startActivity(
            Intent(requireActivity(), CheckOutActivity::class.java).putExtra(
                "amount",
                mBinding.textView8.text.toString()
            ).putExtra(
                "array",
                cartArray
            ).putExtra(
                "fullTotal",
                totalPriceWithGst.toString()
            )
                .putExtra(
                    "comment",
                    mBinding.editText.text.toString()
                ).putExtra(
                    "quotation_no",
                    "0"
                ).putExtra(
                    "shipping_cost",
                    "0"
                ).putExtra(
                    "coupon_applied",
                    coupon_applied.toString()
                ).putExtra(
                    "coupon_amount",
                    coupon_amount.toString()
                ).putExtra(
                    "couponDiscount",
                    couponDiscount.toString()
                )
                .putExtra(
                    "price",
                    findFinalPrice(coupon_amount, couponDiscount, price)

                )

        )

    }

    private fun findFinalPrice(
        couponAmount: String,
        couponDiscount: Double,
        price: Double
    ): String? {
        var newcoupon = "0"
        if (couponAmount.isNotEmpty()) {
            newcoupon = couponAmount
        }
        return (price - newcoupon.toDouble()).toString()
    }

    override fun quotationRequest() {
        startActivity(
            Intent(activity, QuotationActivity::class.java).putParcelableArrayListExtra(
                "array",
                cartArray
            ).putExtra("fullTotal", totalPriceWithGst.toString()).putExtra(
                "couponDiscount",
                couponDiscount
            )
        )
    }

    @SuppressLint("SetTextI18n")
    override fun calculateTotal(

    ) {
        mBinding.tvDiscount.visibility = View.GONE
        mBinding.tvDiscountCost.visibility = View.GONE
        couponDiscount = 0.0
        coupon_applied = 0
        appliedCoupon.appliedCoupons(coupon_applied, couponDiscount, code, type)
        mBinding.applyCoupon.text = "Tap here to add Coupon code"
        viewModel.getCartValues().observe(this, Observer {
            if (it.size > 0) {

                cartArray = it
                adapterCartEco.update(it)
                mBinding.noData.visibility = View.GONE
                mBinding.nestedScrollView.visibility = View.VISIBLE
                price = 0.0
                var tax = 0.0
                for (i in 0 until it.size) {
                    val modelSearchEco = it[i]
                    val gson = Gson()
                    val json = gson.toJson(modelSearchEco)
                    val obj: CartModel = gson.fromJson(json, CartModel::class.java)

//                    it[i].price.replace("₹", "").replace(",", "")
                    price += (it[i].selectQuant.toDouble() * Constants.getDiscountedPriceCart(
                        obj,
                        requireActivity()
                    ) * it[i].saleable.toDouble())
                    tax += getProductTax(
                        it[i].tax_class_id,
                        (it[i].selectQuant.toDouble() * Constants.getDiscountedPriceCart(
                            obj,
                            requireActivity()
                        ) * it[i].saleable.toDouble())
                    )
                }
                mBinding.tvPriceCost.text = "₹" + Constants.currencyFormt(price)
                //val rate = Constants.getPrefs(activity!!).getString("rate", "0").toDouble()
                if (coupon_applied == 1) {
                    showPriceInTexts(price)
                    mBinding.applyCoupon.text = "Applied coupon: $code"
                } else {
                    val taxUnbrako =
                        Constants.getSharedPrefs(requireContext()).getString("unbrako_tax", "0")!!
                            .toDouble()
                    mBinding.tvTaxCost.text =
                        "₹" + Constants.currencyFormt(price * taxUnbrako / 100)
                    mBinding.textView8.text =
                        "₹" + Constants.currencyFormt((price + price * taxUnbrako / 100))
                    totalPrice = price
                    totalPriceWithGst = price + price * taxUnbrako / 100
                }
//
            } else {
                mBinding.noData.visibility = View.VISIBLE
                mBinding.nestedScrollView.visibility = View.GONE
            }
            update.updatingCart(it.size)
        })

    }

    @SuppressLint("SetTextI18n")
    private fun showPriceInTexts(price: Double) {

        val taxUnbrako =
            Constants.getSharedPrefs(requireContext()).getString("unbrako_tax", "0")!!.toDouble()
        if (type == "P") {
            mBinding.tvDiscountCost.text =
                "₹" + Constants.currencyFormt(price * couponDiscount / 100)
            mBinding.tvTaxCost.text =
                "₹" + Constants.currencyFormt(((price - price * couponDiscount / 100) * taxUnbrako / 100))
            mBinding.textView8.text =
                "₹" + Constants.currencyFormt(((price - price * couponDiscount / 100) + ((price - price * couponDiscount / 100) * taxUnbrako / 100)))
            totalPrice = price
            coupon_amount = (price * couponDiscount / 100).toString()
            totalPriceWithGst =
                (price - price * couponDiscount / 100) + ((price - price * couponDiscount / 100) * taxUnbrako / 100)
        } else {
            mBinding.tvDiscountCost.text = "₹" + Constants.currencyFormt(couponDiscount)
            mBinding.textView8.text =
                "₹" + Constants.currencyFormt(((price - couponDiscount) + ((price - couponDiscount) * taxUnbrako / 100)))
            mBinding.tvTaxCost.text =
                "₹" + Constants.currencyFormt(((price - couponDiscount) * taxUnbrako / 100))

            totalPrice = price
            totalPriceWithGst =
                price - couponDiscount + ((price - couponDiscount) * taxUnbrako / 100)
            coupon_amount = couponDiscount.toString()
        }

    }

    private fun getProductTax(tax_class_id: String, price: Double): Double {
        val gson = Gson()
        val response = Constants.getSharedPrefs(requireActivity()).getString("rate", "")
        val lstArrayList = gson.fromJson<ArrayList<ModelTax>>(
            response,
            object : TypeToken<List<ModelTax>>() {

            }.type
        )
        for (i in 0 until lstArrayList.size) {
            if (lstArrayList[i].tax_class_id == tax_class_id) {
                if (lstArrayList[i].rate.isEmpty()) {
                    lstArrayList[i].rate = "0"
                }
                return (price * lstArrayList[i].rate.toDouble()) / 100
                break
            }
        }
        return 0.0
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as updateCart
        appliedCoupon = context as AppliedCoupon
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as updateCart
        appliedCoupon = activity as AppliedCoupon
    }


    @SuppressLint("SetTextI18n")
    private fun showDialog() {

        val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())

        builder.setTitle("Change Delivery")
            .setMessage("The standard delivery is free of cost. If you want a more personalised delivery service, please select the option and enter your delivery requirements in the comments section and we will get back to you with a price for the same.")
        val mbinding: ChangeDeliveryTypeBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(activity),
                R.layout.change_delivery_type,
                null,
                false
            )

        if (check == 0) {
            mbinding.standard.isChecked = true
            mbinding.custom.isChecked = false
        }
        if (check == 1) {
            mbinding.standard.isChecked = false
            mbinding.custom.isChecked = true
        }


        builder.setView(mbinding.root)


        builder.setCancelable(false)

        builder.setPositiveButton("Ok")
        { dialog, which ->
            if (mbinding.standard.isChecked) {
                checkStockAvailable(cartArray)
//                mBinding.deliveryType.text = "Standard Delivery (15-30 days)"
                mBinding.days.text = "Free"
                mBinding.lay.visibility = View.GONE
                check = 0

            } else if (mbinding.custom.isChecked) {

                mBinding.deliveryType.text = "Custom Delivery"
                mBinding.days.text = "TBA"
                mBinding.lay.visibility = View.VISIBLE

                check = 1
            }
            dialog.dismiss()

        }
        builder.setNegativeButton("Cancel") { dialog, which ->
        }

        builder.create()
        builder.show()

    }

    interface AppliedCoupon {
        fun appliedCoupons(
            couponApplied: Int,
            couponAmount: Double,
            code: String,
            type: String
        )

        fun getAppliedCoupon(): Int
        fun getCouponDiscount(): Double
        fun getType(): String
        fun getCode(): String

    }
}