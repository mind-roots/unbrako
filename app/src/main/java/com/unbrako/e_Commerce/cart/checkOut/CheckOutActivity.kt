package com.unbrako.e_Commerce.cart.checkOut

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityCheckOutBinding
import com.unbrako.e_Commerce.account.AccountActivity
import com.unbrako.e_Commerce.cart.Payment.PaymentActivity
import com.unbrako.e_Commerce.cart.address.*
import com.unbrako.e_Commerce.catalog.ModelTax
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.signup.SignUpModel
import org.json.JSONArray
import org.json.JSONObject


@Suppress(
    "UNREACHABLE_CODE", "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "NAME_SHADOWING"
)
class CheckOutActivity : AppCompatActivity() {

    private var createOrder: Int = 0
    private var appliedCredits: String = "0"
    lateinit var mBinding: ActivityCheckOutBinding
    lateinit var mViewModelAddressList: AddressListViewModel
    lateinit var viewModel: CheckOutModel
    var running = 0
    var runnings = 0
    var backgroundChange: ArrayList<GetAddressModel> = ArrayList()
    var billingAddressArray: ArrayList<GetAddressModel> = ArrayList()
    var customerType = ""
    var userPaymentId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_check_out)
        viewModel = ViewModelProviders.of(this)[CheckOutModel::class.java]
        mViewModelAddressList = ViewModelProviders.of(this)[AddressListViewModel::class.java]
        setToolBar()
        addGstDetails()
        placingOrder()
        editAddress()
        addNewAddress()

        mBinding.amountPrice.text = intent.getStringExtra("amount")



        setPriceAndGst()

        // No Card No credit
        val mVariable = Gson()
        val json = Constants.getPrefs(this).getString("profile", "")
        val profileData: SignUpModel = mVariable.fromJson(json, SignUpModel::class.java)

        customerType = profileData.customer_type

        if (profileData.customer_type == "3") {
            mBinding.fgdg.visibility = View.GONE
            mBinding.selectPayment.visibility = View.GONE
            mBinding.placeOrder.text = "Place Order"
            mBinding.selectPayment.text = "No Card/No credit"
        }


        mBinding.selectPayment.setOnClickListener {
            createAlertDialog()
        }

        mBinding.appliedCredit.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            @SuppressLint("SetTextI18n")
            override fun onTextChanged(
                text: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                val mVariable = Gson()
                val json = Constants.getPrefs(applicationContext).getString("profile", "")
                val obj: SignUpModel = mVariable.fromJson(json, SignUpModel::class.java)
                val length: Int = mBinding.appliedCredit.text.length
                appliedCredits = text.toString()
                if (appliedCredits == "." || appliedCredits == ",") {
                    appliedCredits = "0"
                }
                try {
                    if (length > 0) {
                        mBinding.reaminLay.visibility = View.GONE

                        if (appliedCredits.toString().toDouble() > obj.credit.toDouble()) {
                            Toast.makeText(
                                applicationContext,
                                "You cannot use more than " + obj.credit + " credits.",
                                Toast.LENGTH_LONG
                            ).show()

                            if (length > 0) {
                                mBinding.appliedCredit.text.delete(length - 1, length)
                            }

                            return
                        } else if (appliedCredits.toString()
                                .toDouble() > intent.getStringExtra("price")!!.replace(
                                "₹",
                                ""
                            ).replace(",", "").toDouble()
                        ) {
                            Toast.makeText(
                                applicationContext,
                                "You cannot use more than Price.",
                                Toast.LENGTH_LONG
                            ).show()
                            val length: Int = mBinding.appliedCredit.getText().length
                            if (length > 0) {
                                mBinding.appliedCredit.text.delete(length - 1, length)
                            }
                            return
                        }
                        leftPrice()

                    } else {
                        mBinding.reaminLay.visibility = View.GONE
                        leftPrice()
                    }
                } catch (e: Exception) {
                    appliedCredits = "0"
                }

            }
        })
    }



    @SuppressLint("SetTextI18n")
    private fun setPriceAndGst() {
        val subTotal: Double = intent.getStringExtra("price")!!.toDouble()
        var dis = 0.0
        var ship = 0.0
        var total = 0.0
        try {
            dis = intent.getStringExtra("coupon_amount")!!.toDouble()
        } catch (e: Exception) {

        }
        try {
            ship = intent.getStringExtra("shipping_cost")!!.toDouble()
        } catch (e: Exception) {

        }
        val taxUnbrako = Constants.getSharedPrefs(this).getString("unbrako_tax", "0")!!.toDouble()
        val tax = (subTotal - appliedCredits.toDouble()) * taxUnbrako / 100

        mBinding.tvPriceCost.text = "₹" + Constants.formatValues2(intent.getStringExtra("price")!!)
//        if (dis==0.0){
//            mBinding.tvDiscountCost.visibility=View.GONE
//            mBinding.tvDiscount.visibility=View.GONE
//        }else{
//            mBinding.tvDiscountCost.visibility=View.VISIBLE
//            mBinding.tvDiscount.visibility=View.VISIBLE
//        }
//        if (ship==0.0){
//            mBinding.tvShippingCost.visibility=View.GONE
//            mBinding.tvShipping.visibility=View.GONE
//        }else{
//            mBinding.tvShippingCost.visibility=View.VISIBLE
//            mBinding.tvShipping.visibility=View.VISIBLE
//        }
        mBinding.tvDiscountCost.text = "₹" + Constants.formatValues2(dis.toString())
        mBinding.tvShippingCost.text = "₹" + Constants.formatValues2(ship.toString())
        mBinding.tvTaxCost.text = "₹" + Constants.formatValues2(tax.toString())
        mBinding.tvTotal.text = "₹" + Constants.formatValues2((subTotal + tax).toString())
    }

    @SuppressLint("SetTextI18n")
    private fun leftPrice() {
        val subTotal: Double = intent.getStringExtra("price")!!.toDouble()

        val taxUnbrako = Constants.getSharedPrefs(this).getString("unbrako_tax", "0")!!.toDouble()
        if (appliedCredits.isEmpty()) {
            appliedCredits = "0"
        }
        val tax = (subTotal - appliedCredits.toDouble()) * taxUnbrako / 100


        mBinding.remainingAmountPrice.text =
            "₹" + Constants.formatValues2((subTotal - appliedCredits.toDouble() + tax).toString())
        mBinding.tvTaxCost.text = "₹" + Constants.formatValues2(tax.toString())
        mBinding.tvTotal.text =
            "₹" + Constants.formatValues2((subTotal - appliedCredits.toDouble() + tax).toString())

        if ((subTotal - appliedCredits.toDouble() + tax) > 0) {
            mBinding.selectPayment.visibility = View.VISIBLE
            mBinding.fgdg.visibility = View.VISIBLE
        } else {
            mBinding.selectPayment.visibility = View.GONE
            mBinding.fgdg.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun createAlertDialog() {

        val alertBox = AlertDialog.Builder(this)
        val mVariable = Gson()
        val json = Constants.getPrefs(this).getString("profile", "")
        val profileData: SignUpModel = mVariable.fromJson(json, SignUpModel::class.java)
        val selection = resources.getStringArray(R.array.selection_payment)
        val selection1 = resources.getStringArray(R.array.selection_payment1)
        if (profileData.customer_type == "1") {
            alertBox.setTitle("Select Payment method")
                .setItems(selection1) { _, pos ->
                    mBinding.selectPayment.text = selection1[pos]

                    mBinding.placeOrder.text = "Pay Now"

                }
        } else {
            alertBox.setTitle("Select Payment method")
                .setItems(selection) { _, pos ->
                    mBinding.selectPayment.text = selection[pos]
                    if (selection[pos] == "Pay by Card") {
                        mBinding.placeOrder.text = "Pay Now"
                    } else {
                        mBinding.placeOrder.text = "Place Order"
                    }
                }
        }

        alertBox.show()
    }


    private fun getGstDetail() {
        val mVariable = Gson()
        val json = Constants.getPrefs(this).getString("profile", "")
        val obj: SignUpModel = mVariable.fromJson(json, SignUpModel::class.java)

        viewModel.getProfile(
            mBinding.gstNumber,
            mBinding.companyName,
            mBinding.availableCredits,
            mBinding.appliedCredit,
            mBinding.fgdg6
        )

        mBinding.ivgst.setOnClickListener {

            val intent = Intent(this, AccountActivity::class.java)
            startActivity(intent)

        }
    }

    @SuppressLint("InflateParams")
    private fun showCustomAlert(msg: String) {
        val context = applicationContext
        val inflater = layoutInflater
        val toast1 = inflater.inflate(R.layout.toast, null)
        val texTm: TextView = toast1.findViewById(R.id.text)
        texTm.text = msg
        val toast = Toast(context)
        toast.view = toast1
        toast.setGravity(
            Gravity.BOTTOM,
            0, 0
        )
        toast.duration = Toast.LENGTH_LONG
        toast.show()
    }

    private fun checkPaymentStatus() {
        if(userPaymentId.isNotEmpty()) {
            viewModel.checkPaymentStatus(userPaymentId)
        }
    }


    private fun placingOrder() {
        val array: ArrayList<CartModel> = intent.getParcelableArrayListExtra("array")!!
        var price = 0.0
        for (i in 0 until array.size) {
            price += (array[i].selectQuant.toDouble() * array[i].price.replace("₹", "").replace(
                ",",
                ""
            ).toDouble() * array[i].saleable.toDouble())
        }

       // browser intent set here
        viewModel.getUrl().observe(this, Observer {
            if (it.size > 0) {
                if (running == 1) {
                    running = 0
                    userPaymentId = it[0].id
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse( it[0].url))
                    startActivity(browserIntent)
//                    startActivityForResult(
//                        Intent(this, PaymentActivity::class.java).putExtra("url", it[0].url),
//                        123
//                    )
                }
            }
        })
        viewModel.onPaymentSuccess().observe(this, Observer {
            creatingOrder(
                it,
                billingAddressArray[0].address_id,
                appliedCredits,
                customerType
            )
        })
        mBinding.placeOrder.setOnClickListener {
            val subTotal: Double = intent.getStringExtra("price")!!.toDouble()

            val taxUnbrako =
                Constants.getSharedPrefs(this).getString("unbrako_tax", "0")!!.toDouble()
            if (appliedCredits.isEmpty()) {
                appliedCredits = "0"
            }
            val tax = (subTotal - appliedCredits.toDouble()) * taxUnbrako / 100

            if (billingAddressArray.size <= 0) {
                Toast.makeText(this, "Please select billing address.", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if ((subTotal - appliedCredits.toDouble() + tax) > 0) {

                if (mBinding.selectPayment.text.toString() == "Select" || mBinding.selectPayment.text.toString()
                        .isEmpty()
                ) {
                    Toast.makeText(this, "Please select Payment method.", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }

            }
            if (Constants.getPrefs(application).getString("shippingId", "")!!.isEmpty()) {
                showCustomAlert("Please add shipping Address")
            } else {

                when {
                    (subTotal - appliedCredits.toDouble() + tax) <= 0 -> {
                        creatingOrder(
                            "0",
                            billingAddressArray[0].address_id,
                            appliedCredits,
                            customerType
                        )
                    }
                    mBinding.selectPayment.text.toString() != "Pay by Card" -> {

                        creatingOrder(
                            "0",
                            billingAddressArray[0].address_id,
                            appliedCredits,
                            customerType
                        )
                    }
                    else -> {

                        var amm = 0.0
                        try {
                            amm = mBinding.appliedCredit.text.toString().toDouble()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        running = 1
                        val mVariable = Gson()
                        val json = Constants.getPrefs(this).getString("profile", "")
                        val profileData: SignUpModel =
                            mVariable.fromJson(json, SignUpModel::class.java)
                        viewModel.paymentRequest(
                            mBinding.progressBar,
                            Constants.formatValues2(
                                (intent.getStringExtra("fullTotal")!!.toDouble() - amm).toString()
                            ),
                            "Unbrako Order",
                            profileData.company_name,
                            profileData.email,
                            profileData.telephone
                        )
                        //                    }


                    }
                }

            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 123) {
            val mVariable = Gson()
            val json = Constants.getPrefs(this).getString("profile", "")
            val obj: SignUpModel = mVariable.fromJson(json, SignUpModel::class.java)
            creatingOrder(
                data!!.getStringExtra("paymentId")!!,
                billingAddressArray[0].address_id,
                appliedCredits,
                customerType
            )
        }

        if (resultCode == 1234) {
            val address =
                backgroundChange[0].company + " " + backgroundChange[0].firstname + " " + backgroundChange[0].lastname + " " + backgroundChange[0].address1 + " " + backgroundChange[0].city + " " + backgroundChange[0].state + " " + backgroundChange[0].postcode
            mBinding.fName.text = address
        }

    }

    private fun creatingOrder(
        paymentId: String,
        billingAddressId: String,
        appliedCredits: String,
        customerType: String
    ) {
        createOrder = 1
        val orders: ArrayList<CartModel> = intent.getParcelableArrayListExtra<CartModel>("array")!!
        val jsonArray2 = JSONArray()
        var subtotal = 0.0
        var subtotal2 = 0.0
        var tax = 0.0
        for (i in 0 until orders.size) {
            val modelSearchEco = orders[i]
            val gson = Gson()
            val json = gson.toJson(modelSearchEco)
            val obj: CartModel = gson.fromJson(json, CartModel::class.java)

            val discount =
                (orders[i].quantity.toDouble() / orders[i].saleable.toDouble() * orders[i].price.replace(
                    "₹",
                    ""
                ).replace(
                    ",",
                    ""
                )
                    .toDouble() * orders[i].saleable.toDouble()) * orders[i].discount_percentage.toDouble() / 100

            subtotal += (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * orders[i].price.toDouble() - discount)
            subtotal2 += (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * orders[i].price.toDouble())

            val jsonObject = JSONObject()

            jsonObject.put("id", orders[i].product_id)
            jsonObject.put(
                "qty",
                (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()).toString()
            )
            jsonObject.put("price", orders[i].price)
            //jsonObject.put("discount_percentage", orders[i].discount_percentage)
            var dd = 0.0
            try {
                dd = intent.getDoubleExtra("couponDiscount", 0.0)
            } catch (e: Exception) {

            }
            if (intent.hasExtra("quotationCheck")) {
                jsonObject.put("discount_percentage", orders[i].discount_percentage)
            } else {
                jsonObject.put("discount_percentage", calculateProductDiscount(orders[i], dd))
            }
            jsonObject.put(
                "tax",
                getProductTax(
                    orders[i].tax_class_id,
                    orders[i].price.toDouble() * orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() - discount
                ).toString()
            )
            jsonObject.put(
                "total",
                (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * orders[i].price.toDouble()).toString()
            )
            jsonArray2.put(jsonObject)

            tax += getProductTax(
                orders[i].tax_class_id,
                orders[i].price.toDouble() * orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()
            )
        }

        val products = jsonArray2.toString()
        var dis = 0.0
        var ship = 0.0
        try {
            dis = intent.getStringExtra("coupon_amount")!!.toDouble()
        } catch (e: Exception) {

        }
        try {
            ship = intent.getStringExtra("shipping_cost")!!.toDouble()
        } catch (e: Exception) {

        }
        val taxUnbrako = Constants.getSharedPrefs(this).getString("unbrako_tax", "0")!!.toDouble()

        tax = (subtotal2 - dis + ship) * taxUnbrako / 100

        var amm = 0.0
        try {
            amm = mBinding.appliedCredit.text.toString().toDouble()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        viewModel.createOrder(
            products,
            getSubTotal(subtotal2),
            paymentId,
            mBinding.progressBar,
            Constants.getPrefs(application).getString("shippingId", "")!!,
            intent.getStringExtra("comment")!!,
            Constants.formatValues2(tax.toString()),
            getTotal(),
            intent.getStringExtra("quotation_no")!!,
            intent.getStringExtra("shipping_cost")!!,
            intent.getStringExtra("coupon_applied")!!,
            intent.getStringExtra("coupon_amount")!!,
//            , intent.getStringExtra("coupon_amount"),
            billingAddressId,
            appliedCredits
        )
        viewModel.createOrderStatus().observe(this,
            Observer {
                if (it.status) {
                    if (runnings == 0) {
                        runnings = 1
                        val orders: ArrayList<CartModel> =
                            intent.getParcelableArrayListExtra<CartModel>("array")!!
                        var subtotal = 0.0
                        for (i in 0 until orders.size) {

                            subtotal += (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * orders[i].price.toDouble())
                        }
                        // showCustomAlertSow(data!!.getStringExtra("paymentId"), subtotal)
                        showCustomAlertSow(
                            it.order_id,
                            it.total,
                            paymentId,
                            intent.getStringExtra("quotation_no")!!,
                            it.msg,customerType
                        )
                    }
                }
            })
    }

    private fun getTotal(): String {

        val basePrice = intent.getStringExtra("price")
        val remaingPrice = basePrice!!.toDouble() - appliedCredits.toDouble()
        val taxUnbrako = Constants.getSharedPrefs(this).getString("unbrako_tax", "0")!!.toDouble()

        val tax = (remaingPrice * taxUnbrako) / 100
        return Constants.formatValues2((remaingPrice + tax).toString())
    }

    private fun getSubTotal(subtotal2: Double): String {

        return (subtotal2).toString()
    }

    private fun calculateProductDiscount(
        cartModel: CartModel,
        couponDiscount: Double
    ): String? {
        val gson = Gson()
        val json1 = Constants.getPrefs(this).getString("profile", "")
        val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
        val flash_sale = Constants.getSharedPrefs(this).getString("flash_sale", "")

        if (cartModel.isexempted == "1") {

            if (flash_sale != "off") {
                val basePrice = (100 * profileData.discount.toDouble()) / 100
                val amountLeft = 100 - basePrice
                val amountAfterFlashDiscount =
                    (amountLeft * cartModel.flash_discount.toDouble()) / 100
                return (basePrice + amountAfterFlashDiscount).toString()
            } else {
                val basePrice = (100 * profileData.discount.toDouble()) / 100
                val amountLeft = 100 - basePrice
                val amountAfterFlashDiscount = (amountLeft * couponDiscount) / 100
                return (basePrice + amountAfterFlashDiscount).toString()
            }
        } else {
            if (flash_sale == "off") {

                return couponDiscount.toString()
            } else {
                if (cartModel.flash_discount.isEmpty()) {
                    cartModel.flash_discount = "0"
                }
                val basePrice = (100 * cartModel.flash_discount.toDouble()) / 100
                val amountLeft = 100 - basePrice
                val amountAfterFlashDiscount = (amountLeft * couponDiscount) / 100
                return (basePrice + amountAfterFlashDiscount).toString()
            }
        }

    }

    private fun getProductTax(tax_class_id: String, price: Double): Double {
        val mVariable = Gson()
        val response = Constants.getSharedPrefs(this).getString("rate", "")
        val lstArrayList = mVariable.fromJson<ArrayList<ModelTax>>(
            response,
            object : TypeToken<List<ModelTax>>() {

            }.type
        )
        for (i in 0 until lstArrayList.size) {
            if (lstArrayList[i].tax_class_id == tax_class_id) {
                if (lstArrayList[i].rate.isEmpty()) {
                    lstArrayList[i].rate = "0"
                }

                return (price * lstArrayList[i].rate.toDouble()) / 100
                break
            }
        }
        return 0.0
    }

    private fun showCustomAlertSow(
        orderId: String,
        subtotal: String,
        paymentId: String,
        stringExtra: String,
        msg: String,
        customerType: String
    ) {


        val intent = Intent(this, ThankyouActivity::class.java)
        intent.putExtra("total", subtotal)
        intent.putExtra("orderId", orderId)
        intent.putExtra("paymentId", paymentId)
        intent.putExtra("quotation_no", stringExtra)
        intent.putExtra("msg", msg)
        intent.putExtra("customer_type",customerType)
        startActivity(intent)

        finish()

    }

    @SuppressLint("SetTextI18n")
    private fun setToolBar() {
        setSupportActionBar(mBinding.checkoutToolbar.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.checkoutToolbar.toolbarTitle.text = "Check Out"
    }

    private fun editAddress() {

        mBinding.ivAddEdit.setOnClickListener {
            val intent = Intent(this, AddressListActivity::class.java)
            startActivity(intent)
        }
        mBinding.ivAddEdit1.setOnClickListener {
            startActivity(
                Intent(this, AddressActivity::class.java).putExtra(
                    "editAddress",
                    billingAddressArray[0]
                ).putExtra("bill", "1")
            )

        }
    }

    private fun addNewAddress() {
        mBinding.ivAdd.setOnClickListener {
            if (billingAddressArray.size == 0) {
                Toast.makeText(this, "Please add billing address first.", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            startActivity(Intent(this, AddressActivity::class.java))

        }
        mBinding.ivAdd1.setOnClickListener {
            startActivity(Intent(this, AddressActivity::class.java).putExtra("bill", "1"))

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun addGstDetails() {

        mBinding.ivgst.setOnClickListener {
            startActivity(Intent(this, AccountActivity::class.java))
        }
    }


    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        checkPaymentStatus()
        val value = Constants.getPrefs(application).getString("checkOut", "")

        if (value!!.isNotEmpty()) {
            Constants.getPrefs(application).edit().remove("checkOut").apply()

            finish()
        }

        if (createOrder == 0) {
            getGstDetail()
            mViewModelAddressList.getAllAddress(mBinding.progressBar)

            mViewModelAddressList.mGetAddressData().observe(this, Observer {
                if (Constants.getPrefs(application).getString("shippingId", "")!!.isEmpty()) {
                    if (it.size == 0) {
                        mBinding.tvShipAdd.text = "Add Shipping Address"
                        mBinding.tvShipAdd1.text = "Add Billing Address"
                        mBinding.addressLayout.visibility = View.GONE
                        mBinding.addressLayout1.visibility = View.GONE
                        mBinding.ivAdd.visibility = View.VISIBLE
                        mBinding.ivAdd1.visibility = View.VISIBLE
                        mBinding.ivAddEdit1.visibility = View.GONE
                        mBinding.ivAddEdit.visibility = View.GONE

                    } else {

                        mBinding.tvShipAdd.text = "Shipping Address"
                        mBinding.tvShipAdd1.text = "Billing Address"
                        mBinding.addressLayout.visibility = View.VISIBLE
                        mBinding.addressLayout1.visibility = View.VISIBLE
                        mBinding.ivAdd1.visibility = View.GONE
                        mBinding.ivAdd.visibility = View.GONE
                        mBinding.ivAddEdit1.visibility = View.VISIBLE
                        mBinding.ivAddEdit.visibility = View.VISIBLE

                        val address =
                            it[0].company + " " + it[0].firstname + " " + it[0].lastname + " " + it[0].city + " " + it[0].address1 + " " + it[0].state + " " + it[0].postcode
                        mBinding.fName.text = address
                        mBinding.fName1.text = address
                        billingAddressArray.clear()
                        billingAddressArray.add(it[0])
                        Constants.getPrefs(application).edit()
                            .putString("shippingId", it[0].address_id).apply()
                        Constants.getPrefs(application).edit()
                            .putString("bililingId", it[0].address_id).apply()
                        val mVariable = Gson()
                        val json = Constants.getPrefs(this).getString("profile", "")
                        val profileData: SignUpModel =
                            mVariable.fromJson(json, SignUpModel::class.java)
                        profileData.address_id = it[0].address_id
                        val json1 = mVariable.toJson(profileData)
                        Constants.getPrefs(application).edit().putString("profile", json1).apply()
                    }
                } else {
                    for (i in 0 until it.size) {
                        if (Constants.getPrefs(application).getString(
                                "shippingId",
                                ""
                            ) == it[i].address_id
                        ) {
                            Constants.getPrefs(application).edit()
                                .putString("shippingId", it[i].address_id)
                                .apply()


                            mBinding.tvShipAdd.text = "Shipping Address"
                            mBinding.addressLayout.visibility = View.VISIBLE
                            mBinding.addressLayout1.visibility = View.VISIBLE
                            mBinding.ivAdd.visibility = View.GONE
                            mBinding.ivAdd1.visibility = View.GONE
                            mBinding.ivAddEdit.visibility = View.VISIBLE
                            mBinding.ivAddEdit1.visibility = View.VISIBLE
                            val address =
                                it[i].company + " " + it[i].firstname + " " + it[i].lastname.trim() + " " + it[i].address1 + " " + it[i].city + " " + it[i].state + " " + it[i].postcode
                            mBinding.fName.text = address


                        }
                        val mVariable = Gson()
                        val json = Constants.getPrefs(this).getString("profile", "")
                        val profileData: SignUpModel =
                            mVariable.fromJson(json, SignUpModel::class.java)
                        Constants.getPrefs(application).edit()
                            .putString("billingId", profileData.address_id).apply()
                        if (Constants.getPrefs(application).getString(
                                "billingId",
                                ""
                            ) == it[i].address_id
                        ) {
                            Constants.getPrefs(application).edit()
                                .putString("billingId", it[i].address_id)
                                .apply()



                            mBinding.tvShipAdd1.text = "Billing Address"
                            mBinding.addressLayout.visibility = View.VISIBLE
                            mBinding.addressLayout1.visibility = View.VISIBLE
                            mBinding.ivAdd.visibility = View.GONE
                            mBinding.ivAdd1.visibility = View.GONE
                            mBinding.ivAddEdit.visibility = View.VISIBLE
                            mBinding.ivAddEdit1.visibility = View.VISIBLE
                            val address =
                                it[i].company + " " + it[i].firstname + " " + it[i].lastname.trim() + " " + it[i].address1 + " " + it[i].city + " " + it[i].state + " " + it[i].postcode
                            billingAddressArray.clear()
                            billingAddressArray.add(it[i])
                            mBinding.fName1.text = address

                        }
                    }
                }
            })
        }
    }


}
