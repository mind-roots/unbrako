package com.unbrako.e_Commerce.cart

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.Paint
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
//import com.unbrako.databinding.CartRecyclerOneBinding
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import kotlinx.android.synthetic.main.cart_recycler_one.view.*
import kotlinx.android.synthetic.main.cart_recycler_one.view.cutOff
import kotlinx.android.synthetic.main.cart_recycler_one.view.partNo
import kotlinx.android.synthetic.main.cart_recycler_one.view.spinner_des
import kotlinx.android.synthetic.main.cart_recycler_one.view.tvThree
import kotlinx.android.synthetic.main.cart_recycler_one.view.tvTwo
import kotlinx.android.synthetic.main.cart_recycler_one.view.tv_one
import kotlinx.android.synthetic.main.cart_recycler_one.view.tvfive


class AdapterCartEcommerce(
    var mContext: Context,
    var cartEcommerceFragment: CartEcoFragment
) : RecyclerView.Adapter<AdapterCartEcommerce.MyViewHolder>() {

    var list: ArrayList<CartModel> = ArrayList()
    //    lateinit var mBinding: CartRecyclerOneBinding
    var cal = cartEcommerceFragment as Calculations

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(mContext).inflate(R.layout.cart_recycler_one, parent, false)
        //mBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.cart_recycler_one, parent, false)

        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val modelSearchEcommerce = list[position]
        // mBinding.cartValue = modelSearchEcommerce

        if (modelSearchEcommerce.name.contains("®")) {
            val vv = modelSearchEcommerce.name.split("®")
            holder.itemView.tv_one.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
        } else {
            holder.itemView.tv_one.text = modelSearchEcommerce.name
        }
        holder.itemView.tvTwo.text = Constants.getPriceText(modelSearchEcommerce, mContext, 2)
        holder.itemView.tvThree.text =Constants.getPriceText2(modelSearchEcommerce,mContext,2)


//        holder.itemView.tvTwo.text = "₹" + Constants.currencyFormt(
//            (Constants.getDiscountedPriceCart(list[position],mContext) * list[position].saleable.toDouble())
//        )
//        holder.itemView.tvThree.text = " per " + (list[position].saleable)+" "+list[position].uom+Constants.getDiscountAppliesCart(list[position],mContext)
        if (list[position].display_price.toDouble() > 0) {
            holder.itemView.tvfive.text =
                "₹" + Constants.currencyFormt((list[position].display_price.toDouble() * list[position].saleable.toDouble()))
            holder.itemView.cutOff.visibility = View.VISIBLE
        } else {
            holder.itemView.cutOff.visibility = View.GONE
        }
        if (list[position].image.isNotEmpty()) {
            Picasso.get().load(list[position].image).placeholder(R.drawable.pro_img_new).fit().centerCrop()
                .into(holder.itemView.iv_cart)
        }

        holder.itemView.partNo.text = list[position].sku

        holder.itemView.tvfive.paintFlags = holder.itemView.tvfive.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        holder.itemView.tv_delete.setOnClickListener {
            Constants.AddToCart(mContext, false, modelSearchEcommerce).execute()
            list.removeAt(position)
            cal.calculateTotal()
        }
        holder.itemView.spinner_des.text = modelSearchEcommerce.selectQuant

        holder.itemView.spinner_des.setOnClickListener {
            openDialogQuantity(mContext, position, list[position].selectQuant)

        }
        val saleable = modelSearchEcommerce.saleable.toDouble()
        val quantity = modelSearchEcommerce.selectQuant.toDouble()
        val stock = modelSearchEcommerce.stock.toDouble()
        if (stock>0) {
            holder.itemView.notavailable.text = "The available stock for this product is ${modelSearchEcommerce.stock}"
        }
        if (saleable * quantity > stock) {

            holder.itemView.notavailable.visibility = View.VISIBLE
        } else {
            holder.itemView.notavailable.visibility = View.GONE
        }

    }

    private fun openDialogQuantity(mContext: Context, position: Int, quantity: String) {

        var color = "1"
        lateinit var dialog: AlertDialog

        // Initialize an array of colors
        val array =
            arrayOf(
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24",
                "25",
                "26",
                "27",
                "28",
                "29",
                "30",
                "31",
                "32",
                "33",
                "34",
                "35",
                "36",
                "37",
                "38",
                "39",
                "40",
                "41",
                "42",
                "43",
                "44",
                "45",
                "46",
                "47",
                "48",
                "49",
                "50"
            )

        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(mContext)

        // Set a title for alert dialog
        builder.setTitle("Select Quantity")


        builder.setSingleChoiceItems(array, list[position].selectQuant.toInt() - 1) { _, which ->
            color = array[which]

            val gson = Gson()
            val innerJson = gson.toJson(list[position])
            val obj = gson.fromJson(innerJson, FetchProductsModel::class.java)
            obj.selectQuant = color
            //  if(Constants.IsStockAvailable(obj)) {
            list[position].selectQuant = color
            Constants.UpdateToCart(mContext, list[position]).execute()
            cal.calculateTotal()
//            }else{
//
//                Toast.makeText(mContext,"Stock Unavailable", Toast.LENGTH_LONG).show()
//            }


            // Dismiss the dialog
            dialog.dismiss()
        }

        dialog = builder.create()
        dialog.show()

    }


    fun update(it: ArrayList<CartModel>) {
        list = it
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTwo: TextView = itemView.findViewById(R.id.tvTwo)
    }

    interface Calculations {
        fun calculateTotal()
    }
}