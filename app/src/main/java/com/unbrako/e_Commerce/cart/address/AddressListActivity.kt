package com.unbrako.e_Commerce.cart.address

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityAddressListBinding

class AddressListActivity : AppCompatActivity(), AdapterActivityList.TickVisible {

    private var pos: Int = 0
    private var addressArray: ArrayList<GetAddressModel> = ArrayList()
    var backgroundChange: ArrayList<GetAddressModel> = ArrayList()
    lateinit var mBinding: ActivityAddressListBinding
    lateinit var mViewModel: AddressListViewModel
    lateinit var mAdapter: AdapterActivityList

    private var getAddressModel = GetAddressModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_address_list)
        mViewModel = ViewModelProviders.of(this)[AddressListViewModel::class.java]
        setToolbar()
        serAdapter()
    }

    private fun serAdapter() {
        mBinding.recyclerAddress.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterActivityList(this)
        mBinding.recyclerAddress.adapter = mAdapter
        mBinding.recyclerAddress.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.include3.MainToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include3.title.text = "All Address"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.update_address, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        if (item.itemId == R.id.addAddress) {
            startActivity(Intent(this, AddressActivity::class.java))

        }
        return super.onOptionsItemSelected(item)
    }

    override fun updateImage(models: GetAddressModel, position1: Int) {


        for (i in 0 until addressArray.size) {
            if (addressArray[i].address_id == models.address_id) {

                models.status = true
                addressArray[position1] = models
            } else {
                addressArray[i].status = false
            }
        }
        mAdapter.update(addressArray)
    }

    override fun onResume() {
        super.onResume()

        mViewModel.getAllAddress(mBinding.progressBar)

        mViewModel.mGetAddressData().observe(this, Observer {
            addressArray = it
            if (it.isNotEmpty()) {
                if (it.size > 0) {
                    for (i in 0 until it.size) {
                        if (Constants.getPrefs(this).getString("shippingId", "")!!.isEmpty()) {
                            it[i].status = i == 0
                        } else {
                            it[i].status = Constants.getPrefs(this).getString("shippingId", "") == it[i].address_id
                        }
                    }
                    mAdapter.update(it)
                }
            }
        })
    }

    override fun sendAddress(items: GetAddressModel, position: Int) {
        pos = position
        val intent = Intent(this, AddressActivity::class.java).putExtra("editAddress", items)
        startActivityForResult(intent, 1234)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 1234) {
            //addressArray[pos].
        }

    }

    override fun finishActivity(getAddressModel: GetAddressModel) {
//        var intent= Intent(this, AddressActivity::class.java)
//        intent.putExtra("selectAddress",getAddressModel)
//        startActivity(intent)
        finish()
    }
}
