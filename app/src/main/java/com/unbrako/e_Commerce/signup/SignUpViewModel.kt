package com.unbrako.e_Commerce.signup

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class SignUpViewModel(application: Application) : AndroidViewModel(application) {

    var refVar:SignUpRepo = SignUpRepo(application)

    fun getSignUpData(model: ModelSignUpInput, progressBar : ProgressBar){

        return refVar.getSignUpData(model, progressBar)

    }

    fun getSignUpMData() : LiveData<SignUpModel>{

        return refVar.getMData()

    }
    fun getStatus(): LiveData<ModelStatus> {
        return refVar.getSt()
    }

}