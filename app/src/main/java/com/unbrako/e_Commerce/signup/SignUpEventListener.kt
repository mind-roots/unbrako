package com.unbrako.e_Commerce.signup

interface SignUpEventListener {

    fun onSignUpClick(cName :String, gstNo :String, fName :String, lName :String, email :String, phoneNo :String, password :String, cPassword :String, landline :String)

    fun onSignInText()
}