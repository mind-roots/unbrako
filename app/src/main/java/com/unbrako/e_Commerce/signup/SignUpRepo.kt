package com.unbrako.e_Commerce.signup

import android.annotation.SuppressLint
import android.app.Application
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import com.google.gson.Gson




class SignUpRepo(var application: Application) {

    val mData = MutableLiveData<SignUpModel>()
    val mDataS = MutableLiveData<ModelStatus>()



    fun getSignUpData(model: ModelSignUpInput, progressBar : ProgressBar) {
        getSignUpResponse(model, progressBar)
    }

    fun getMData() : LiveData<SignUpModel>{
        return mData
    }

    private fun getSignUpResponse(model: ModelSignUpInput, progressBar : ProgressBar) {

        progressBar.visibility = View.GONE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(SignUpListener.SignUpService::class.java)
        val call: Call<ResponseBody> =
            service.signUp(model.firstName, model.lastName, model.phoneNo, model.emailId, model.password, model.deviceId, "android", model.companyName, model.gstNo, model.landline)
        call.enqueue(object : Callback<ResponseBody> {

            @SuppressLint("CommitPrefEdits")
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                if (response.isSuccessful) {
                    val mm=ModelStatus()

                    try {
                        val res = response.body()!!.string()
                        val jsonObject = JSONObject(res)
                        val token = jsonObject.optString("token")
                        val success = jsonObject.optString("success")
                        val signUpModel = SignUpModel()

                        val editor = Constants.getPrefs(application).edit()
                        editor.putString(Constants.token, token)
                        editor.apply()


                        if (success == "true") {


                            mm.status=1

                            val signUpObj = jsonObject.getJSONObject("customer")
                            val editorJson = Constants.getPrefs(application).edit()
                            editorJson.putString("signUpObj", signUpObj.toString())
                            editorJson.apply()
                            mm.msg=jsonObject.optString("msg")

                            signUpModel.customer_id = signUpObj.optString("customer_id")
                            signUpModel.customer_group_id = signUpObj.optString("customer_group_id")
                            signUpModel.store_id = signUpObj.optString("store_id")
                            signUpModel.language_id = signUpObj.optString("language_id")
                            signUpModel.firstname = signUpObj.optString("firstname")
                            signUpModel.lastname = signUpObj.optString("lastname")
                            signUpModel.email = signUpObj.optString("email")
                            signUpModel.telephone = signUpObj.optString("telephone")
                            signUpModel.fax = signUpObj.optString("fax")
                            signUpModel.password = signUpObj.optString("password")
                            signUpModel.salt = signUpObj.optString("salt")
                            signUpModel.cart = signUpObj.optString("cart")
                            signUpModel.wishlist = signUpObj.optString("wishlist")
                            signUpModel.newsletter = signUpObj.optString("newsletter")
                            signUpModel.address_id = signUpObj.optString("address_id")
                            signUpModel.custom_field = signUpObj.optString("custom_field")
                            signUpModel.ip = signUpObj.optString("ip")
                            signUpModel.status = signUpObj.optString("status")
                            signUpModel.safe = signUpObj.optString("safe")
                            signUpModel.token = signUpObj.optString("token")
                            signUpModel.code = signUpObj.optString("code")
                            signUpModel.date_added = signUpObj.optString("date_added")
                            var profilegst=JSONObject(signUpModel.custom_field)
                            signUpModel.gst = profilegst.optString("2")
                            signUpModel.company_name = profilegst.optString("1")
                            signUpModel.landline = signUpObj.optString("landline")
                            signUpModel.customer_type = signUpObj.optString("customer_type")
                        }else{
                            mm.msg=jsonObject.optString("error")
                            mm.status=0

                        }
                        val gson = Gson()
                        val json = gson.toJson(signUpModel)
                        Constants.getPrefs(application).edit().putString("profile", json).apply()
                        mData.value = signUpModel
                        mDataS.value=mm

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.VISIBLE

                Toast.makeText(application,"No Response",Toast.LENGTH_SHORT).show()
            }


        })

    }

    fun getSt(): LiveData<ModelStatus> {

        return mDataS
    }

}