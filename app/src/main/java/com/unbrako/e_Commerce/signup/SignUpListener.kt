package com.unbrako.e_Commerce.signup

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

class SignUpListener {




    interface SignUpService {
        @FormUrlEncoded
        @POST("?route=mobileapi/catalog/Register")
        fun signUp(
            @Field("firstname") firstname: String,
            @Field("lastname") lastname: String,
            @Field("telephone") telephone: String,
            @Field("email") email   : String,
            @Field("password") password: String,
            @Field("push_token") push_token: String,
            @Field("platform") platform: String,
            @Field("CompanyName") CompanyName: String,
            @Field("GSTNo") GSTNo: String,
            @Field("landline") landline: String)
                : Call<ResponseBody>
    }
}