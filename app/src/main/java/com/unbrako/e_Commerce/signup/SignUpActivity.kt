package com.unbrako.e_Commerce.signup


import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivitySignUpBinding
import com.unbrako.e_Commerce.DashboardEcommerceActivity
import com.unbrako.e_Commerce.login.LoginActivity


@Suppress("DEPRECATION")
class SignUpActivity : AppCompatActivity(), View.OnClickListener, SignUpEventListener {

    lateinit var mBinding: ActivitySignUpBinding
    private lateinit var signUpViewModel: SignUpViewModel
    var shown=0
    var hitService=0
    var shown2=0
    var modelS=ModelStatus()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
        mBinding.register = this
        setToolbar()
        onClick()
        setSpanText()

        signUpViewModel.getSignUpMData().observe(this, Observer {
            mBinding.progress.visibility=View.GONE
            if (it != null) {
                if (it.status == "1") {
                    val intent = Intent(this, DashboardEcommerceActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        })


    signUpViewModel.getStatus().observe(this, Observer {

        mBinding.progress.visibility=View.GONE
            modelS = it
            if (modelS.status != -1) {

                if (modelS.status == 0) {


                        showLoginDialogErrorw(this, modelS.msg, 0)

                } else if (modelS.status == 1) {
                    showLoginDialogErrorw(this, modelS.msg, 1)
                }
            } else {
                showLoginDialogErrorw(this, modelS.msg, 1)
            }

    })
    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.includeProfile.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.includeProfile.toolbarTitle.text =getString(R.string.title)
    }


    private fun onClick() {
        mBinding.signinTV.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

    }

    private fun setSpanText() {
        val wordSpan =
            SpannableString("Already have an account? Sign In.")

        wordSpan.setSpan(
            ForegroundColorSpan(Color.parseColor("#F37836")),
            25,
            33,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        mBinding.signinTV.text = wordSpan
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSignUpClick(cName: String, gstNo: String, fName: String, lName: String, email: String, phoneNo: String, password: String, cPassword: String, landline: String) {

        when {
            cName.isEmpty() ->{
                Toast.makeText(this, getString(R.string.company_error), Toast.LENGTH_SHORT).show()
                return
            }
            gstNo.isEmpty() -> {
                Toast.makeText(this, getString(R.string.gst_error), Toast.LENGTH_SHORT).show()
            return
            }
            ! Constants.isGstValid(gstNo)
            -> {
                Toast.makeText(this, "Enter valid GST Number", Toast.LENGTH_SHORT).show()
                return

            }
            fName.isEmpty() ->{
                Toast.makeText(this, getString(R.string.fName_error), Toast.LENGTH_SHORT).show()
                return
            }
            lName.isEmpty() ->{
                Toast.makeText(this, getString(R.string.lName_error), Toast.LENGTH_SHORT).show()
                return
            }
            email.isEmpty() -> {
                Toast.makeText(this, getString(R.string.email_error), Toast.LENGTH_SHORT).show()
                return
            }
            phoneNo.isEmpty() -> {
                Toast.makeText(this, getString(R.string.phone_error), Toast.LENGTH_SHORT).show()
                return
            }
            password.isEmpty() -> {
                Toast.makeText(this, getString(R.string.password_error), Toast.LENGTH_SHORT).show()
                return
            }
            cPassword.isEmpty() -> {
                Toast.makeText(this, getString(R.string.password_error), Toast.LENGTH_SHORT).show()
                return
            }
        }

        val validEmail= Constants.isValidEmail(email)
        if (!validEmail) {
            Toast.makeText(this, "Enter valid Email. ", Toast.LENGTH_LONG).show()
        return
        }
        if (password != cPassword) {

            Toast.makeText(this, getText(R.string.cPassword_error), Toast.LENGTH_SHORT).show()
        } else {

            val model = ModelSignUpInput()
            model.companyName = cName
            model.firstName = fName
            model.lastName = lName
            model.emailId = email
            model.password = password
            model.phoneNo = phoneNo
            model.gstNo = gstNo
            model.deviceId = "45788"
            model.landline = landline

            if (phoneNo.length < 10) {
                Toast.makeText(this, "Please Enter 10 digit mobile number.", Toast.LENGTH_SHORT).show()
                return
            }
            mBinding.progress.visibility=View.VISIBLE
                signUpViewModel.getSignUpData(model, mBinding.progressBarSignup)

        }

    }

    private fun showLoginDialogErrorw(
        context: SignUpActivity,
        msg: String,
        i: Int
    ) {
        android.app.AlertDialog.Builder(context)
            .setTitle("Alert!")
            .setMessage(msg)
            .setPositiveButton("Ok") { dialog, which -> dialog.dismiss()
               if(i==1){
                   finish()
               }
            }
            .show()
    }

    override fun onSignInText() {

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }



}
