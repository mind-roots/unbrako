package com.unbrako.e_Commerce.signup

import android.os.Parcel
import android.os.Parcelable

class SignUpModel() : Parcelable{

    var customer_id : String = ""
    var customer_group_id : String = ""
    var store_id : String = ""
    var language_id : String = ""
    var firstname : String = ""
    var lastname : String = ""
    var email : String = ""
    var telephone : String = ""
    var fax : String = ""
    var password : String = ""
    var salt : String = ""
    var cart : String = ""
    var wishlist : String = ""
    var newsletter : String = ""
    var address_id : String = ""
    var custom_field : String = ""
    var ip : String = ""
    var status : String = ""
    var safe : String = ""
    var token : String = ""
    var code : String = ""
    var date_added : String = ""
    var gst : String = ""
    var company_name : String = ""
    var landline : String = ""
    var customer_type : String = ""
    var discount : String = ""
    var credit : String = ""

    constructor(parcel: Parcel) : this() {
        customer_id = parcel.readString()!!
        customer_group_id = parcel.readString()!!
        store_id = parcel.readString()!!
        language_id = parcel.readString()!!
        firstname = parcel.readString()!!
        lastname = parcel.readString()!!
        email = parcel.readString()!!
        telephone = parcel.readString()!!
        fax = parcel.readString()!!
        password = parcel.readString()!!
        salt = parcel.readString()!!
        cart = parcel.readString()!!
        wishlist = parcel.readString()!!
        newsletter = parcel.readString()!!
        address_id = parcel.readString()!!
        custom_field = parcel.readString()!!
        ip = parcel.readString()!!
        status = parcel.readString()!!
        safe = parcel.readString()!!
        token = parcel.readString()!!
        code = parcel.readString()!!
        date_added = parcel.readString()!!
        gst = parcel.readString()!!
        company_name = parcel.readString()!!
        landline = parcel.readString()!!
        customer_type = parcel.readString()!!
        discount = parcel.readString()!!
        credit = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(customer_id)
        parcel.writeString(customer_group_id)
        parcel.writeString(store_id)
        parcel.writeString(language_id)
        parcel.writeString(firstname)
        parcel.writeString(lastname)
        parcel.writeString(email)
        parcel.writeString(telephone)
        parcel.writeString(fax)
        parcel.writeString(password)
        parcel.writeString(salt)
        parcel.writeString(cart)
        parcel.writeString(wishlist)
        parcel.writeString(newsletter)
        parcel.writeString(address_id)
        parcel.writeString(custom_field)
        parcel.writeString(ip)
        parcel.writeString(status)
        parcel.writeString(safe)
        parcel.writeString(token)
        parcel.writeString(code)
        parcel.writeString(date_added)
        parcel.writeString(gst)
        parcel.writeString(company_name)
        parcel.writeString(landline)
        parcel.writeString(customer_type)
        parcel.writeString(discount)
        parcel.writeString(credit)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SignUpModel> {
        override fun createFromParcel(parcel: Parcel): SignUpModel {
            return SignUpModel(parcel)
        }

        override fun newArray(size: Int): Array<SignUpModel?> {
            return arrayOfNulls(size)
        }
    }

}