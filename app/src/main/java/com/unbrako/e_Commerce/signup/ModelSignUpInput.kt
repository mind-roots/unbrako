package com.unbrako.e_Commerce.signup

import android.os.Parcel
import android.os.Parcelable

class ModelSignUpInput() : Parcelable {
    lateinit var deviceId: String
    lateinit var companyName: String
    lateinit var firstName: String
    lateinit var lastName: String
    lateinit var emailId: String
    lateinit var phoneNo: String
    lateinit var password: String
    lateinit var gstNo: String
    lateinit var landline: String

    constructor(parcel: Parcel) : this() {
        deviceId = parcel.readString()!!
        companyName = parcel.readString()!!
        firstName = parcel.readString()!!
        lastName = parcel.readString()!!
        emailId = parcel.readString()!!
        phoneNo = parcel.readString()!!
        password = parcel.readString()!!
        gstNo = parcel.readString()!!
        landline = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(deviceId)
        parcel.writeString(companyName)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(emailId)
        parcel.writeString(phoneNo)
        parcel.writeString(password)
        parcel.writeString(gstNo)
        parcel.writeString(landline)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelSignUpInput> {
        override fun createFromParcel(parcel: Parcel): ModelSignUpInput {
            return ModelSignUpInput(parcel)
        }

        override fun newArray(size: Int): Array<ModelSignUpInput?> {
            return arrayOfNulls(size)
        }
    }
}