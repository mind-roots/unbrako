package com.unbrako.e_Commerce.catalog

import android.os.Parcel
import android.os.Parcelable


class CatalogModel()  : Parcelable{

     var category_id = ""
     var image = ""
     var parent_id = ""
     var status = ""
     var name = ""
     var measurment = ""

     constructor(parcel: Parcel) : this() {
          category_id = parcel.readString()!!
          image = parcel.readString()!!
          parent_id = parcel.readString()!!
          status = parcel.readString()!!
          name = parcel.readString()!!
          measurment = parcel.readString()!!
     }

     override fun writeToParcel(parcel: Parcel, flags: Int) {
          parcel.writeString(category_id)
          parcel.writeString(image)
          parcel.writeString(parent_id)
          parcel.writeString(status)
          parcel.writeString(name)
          parcel.writeString(measurment)
     }

     override fun describeContents(): Int {
          return 0
     }

     companion object CREATOR : Parcelable.Creator<CatalogModel> {
          override fun createFromParcel(parcel: Parcel): CatalogModel {
               return CatalogModel(parcel)
          }

          override fun newArray(size: Int): Array<CatalogModel?> {
               return arrayOfNulls(size)
          }
     }


}