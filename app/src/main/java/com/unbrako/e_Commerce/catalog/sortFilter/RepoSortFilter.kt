package com.unbrako.e_Commerce.catalog.sortFilter

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.os.AsyncTask
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FavouriteModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit


class RepoSortFilter(var application: Application) {
    var array = ArrayList<FetchProductsModel>()
    var mData = MutableLiveData<ArrayList<FetchProductsModel>>()

    var mlist = MutableLiveData<ModelPost>()

    fun getDetails(): MutableLiveData<ModelPost> {
        return mlist
    }

    fun getSortFilterData(category_id: String, progressBar: ProgressBar, sortFilterCl: LinearLayout, wishList: java.util.ArrayList<ModelWishList>, sort: String, page: String, yesClear: String, type: String) {
        if (yesClear=="yesClear"){

            array.clear()

        }
        getDataFromAPI(category_id, progressBar,  sortFilterCl, wishList,sort,page, type)
    }

    private fun getDataFromAPI(catId: String, progressBar: ProgressBar, sortFilterCl: LinearLayout, wishList: java.util.ArrayList<ModelWishList>, sort: String, page: String, type: String) {

//        geWishListData(catId, progressBar, no_Data, sortFilterCl: ConstraintLayout, wishList: java.util.ArrayList<ModelWishList>, adapterSearchEcommerce: AdapterSortFilterEcommerce, sort: String, page: String)

        if (sort.toDouble()>0){
//            array.clear()
        }
        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.fetchPorducts(auth!!, catId, sort,page, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optString("success")
                        val products = json.getJSONArray("products")
                        if (success == "true") {
                            array.clear()
                            for (i in 0 until products.length()) {

                                val homeServiceModel = FetchProductsModel()
                                val categoryObject = products.getJSONObject(i)
                                homeServiceModel.product_id = categoryObject.optString("product_id")
                                homeServiceModel.name = categoryObject.optString("name")
                                homeServiceModel.sku = categoryObject.optString("sku")
                                homeServiceModel.quantity = categoryObject.optString("stock")
                                homeServiceModel.image = categoryObject.optString("image")
                                homeServiceModel.price = categoryObject.optString("price")
                                homeServiceModel.rating = categoryObject.optDouble("rating")
                                homeServiceModel.category_id = categoryObject.optString("category_id")
                                homeServiceModel.display_price = categoryObject.optString("display_price")
                                homeServiceModel.saleable = categoryObject.optString("saleable")
                                homeServiceModel.tax_class_id = categoryObject.optString("tax_class_id")
                                homeServiceModel.stock = categoryObject.optString("stock")
                                homeServiceModel.uom = categoryObject.optString("uom")
                                homeServiceModel.flash_discount = categoryObject.optString("flash_discount")
                                homeServiceModel.isexempted = categoryObject.optString("isexempted")
                                homeServiceModel.selectQuant ="1"
                                var exist=0
                                for (i in 0 until wishList.size){
                                    if (wishList[i].product_id == homeServiceModel.product_id){
                                        exist=1
                                        break
                                    }
                                }
                                homeServiceModel.wish = exist==1

                                array.add(homeServiceModel)
                            }

                            val model = ModelPost()
                            model.count = json.optInt("total_product")
                            model.list = array
                            mlist.value = model
                           // mData.value=array

                        }else{

                            mlist.value = ModelPost()
                            progressBar.visibility = View.GONE
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressBar.visibility = View.GONE
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.GONE
            }
        })

    }

    fun updateArrayPosition(
        position: Int,
        cartFav: String,
        adapterSearchEcommerce: AdapterSortFilterEcommerce,
        basketArray: List<BasketModel>
    ) {
        if (cartFav == "Fav") {
            val gson = Gson()
            val innerJson = gson.toJson(array[position])
            val obj = gson.fromJson(innerJson, FavouriteModel::class.java)
        } else {
            val gson = Gson()
            val innerJson = gson.toJson(array[position])
            val obj = gson.fromJson(innerJson, CartModel::class.java)
            CheckIfExistInCartList(obj, application).execute()

        }
        // mData.value = array
        adapterSearchEcommerce.update(array,basketArray)
    }

    @SuppressLint("StaticFieldLeak")
    inner class CheckIfExistInCartList(
        var model: CartModel,
        var context: Context
    ) : AsyncTask<Void, Void, Void>() {
        var exist = 0
        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)
            val cartItems: ArrayList<CartModel> =
                db!!.productDao().getAllCart() as ArrayList<CartModel>

            for (j in 0 until cartItems.size) {
                if (model.product_id == cartItems[j].product_id) {
                    exist = 1
                }
            }

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (exist == 0) {
                Constants.AddToCart(application, true, model).execute()
            } else {
                Toast.makeText(application, "Already Added in Cart", Toast.LENGTH_LONG).show()
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class CheckIfExistInFavouriteList(
        var array: ArrayList<FetchProductsModel>,
        var context: Context
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)
            val favouriteItems: ArrayList<FavouriteModel> =
                db!!.productDao().getAllFavourite() as ArrayList<FavouriteModel>
            for (i in 0 until array.size) {
                for (j in 0 until favouriteItems.size) {
                    if (array[i].product_id == favouriteItems[j].product_id) {
//                        array[i].favorite = true
                    }
                }
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            //  mData.value = array
        }
    }


    fun addingWishList(model: FetchProductsModel, position: Int, adapterSearchEcommerce: AdapterSortFilterEcommerce) {
        var type = "1"
        type = if (model.wish) {
            "2"
        } else {
            "1"
        }
        val authCode = Constants.getPrefs(application).getString(Constants.token, "")

        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS).build()
        var  retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .client(httpClient.build()).build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.addWishList(authCode!!, model.product_id, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optString("success")

                        if (success == "true") {

//                            model.wish = type == "1"
//                            array[position] = model
                            //     mData.value=array
                        }
//                        adapterSearchEcommerce.update(array,"no")
                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
//                Toast.makeText(application, "dcd",Toast.LENGTH_SHORT).show()
            }
        })

    }

    fun upDateS(
        exist: String?,
        adapterSearchEcommerce: AdapterSortFilterEcommerce,
        basketArray: List<BasketModel>
    ) {
        val id = Constants.getPrefs(application).getString("likeId", "")
        for (i in 0 until array.size) {
            if (id == array[i].product_id) {
                array[i].wish = exist == "yes"
            }
        }
        adapterSearchEcommerce.update(array,basketArray)
    }

    fun getFilterData(
        category_id: String,
        progressBar: ProgressBar,
        sortFilterCl: LinearLayout,
        wishList: ArrayList<ModelWishList>,
        adapterSearchEcommerce: AdapterSortFilterEcommerce,
        valuess: String,
        toString: String,
        yesclear: String,
        type: String,
        sorting: String,
        basketArray: List<BasketModel>,
        noDataText: TextView
    ) {

        //if (yesclear=="Yesclear"){
        array.clear()
        //}
        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.fetchFilterPorducts(auth!!, category_id,toString, valuess,type,sorting)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optString("success")
                        val products = json.getJSONArray("filter")
                        if (success == "true") {
                            //array.clear()
                            for (i in 0 until products.length()) {

                                val homeServiceModel = FetchProductsModel()
                                val categoryObject = products.getJSONObject(i)
                                homeServiceModel.product_id = categoryObject.optString("product_id")
                                homeServiceModel.name = categoryObject.optString("name")
                                homeServiceModel.sku = categoryObject.optString("sku")
                                homeServiceModel.quantity = categoryObject.optString("stock")
                                homeServiceModel.image = categoryObject.optString("image")
                                homeServiceModel.price = categoryObject.optString("price")
                                homeServiceModel.category_id = categoryObject.optString("category_id")
                                homeServiceModel.display_price = categoryObject.optString("display_price")
                                homeServiceModel.saleable = categoryObject.optString("saleable")
                                homeServiceModel.tax_class_id = categoryObject.optString("tax_class_id")
                                homeServiceModel.stock = categoryObject.optString("stock")
                                homeServiceModel.uom = categoryObject.optString("uom")
                                homeServiceModel.isexempted = categoryObject.optString("isexempted")
                                homeServiceModel.flash_discount = categoryObject.optString("flash_discount")
                                homeServiceModel.selectQuant ="1"
                                var exist=0
                                for (i in 0 until wishList.size){
                                    if (wishList[i].product_id == homeServiceModel.product_id){
                                        exist=1
                                        break
                                    }
                                }
                                homeServiceModel.wish = exist==1

                                if (categoryObject.optString("rating") == null || categoryObject.optString("rating") == "null") {
                                    homeServiceModel.rating = 0.0
                                } else {
                                    homeServiceModel.rating = categoryObject.optDouble("rating")
                                }

                                array.add(homeServiceModel)

                            }

                            if (array.size > 0) {
                                noDataText.visibility = View.GONE
                                sortFilterCl.visibility = View.VISIBLE
                                for (i in 0 until wishList.size) {
                                    for (j in 0 until array.size) {
                                        if (wishList[i].product_id == array[j].product_id) {
                                            array[j].wish = true
                                        }
                                    }
                                }
                            }else{
                                noDataText.visibility = View.VISIBLE
                            }
                            adapterSearchEcommerce.update(array,basketArray)

                        } else {

                            adapterSearchEcommerce.update(array,basketArray)
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressBar.visibility = View.GONE
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                //  mData.value = array
                adapterSearchEcommerce.update(array,basketArray)
                progressBar.visibility = View.GONE
//                noDataText.visibility = View.VISIBLE
            }
        })
    }

    fun GetProducts(): MutableLiveData<ArrayList<FetchProductsModel>> {

        return mData
    }

    var array2 = ArrayList<CartModel>()
    var mData2 = MutableLiveData<ArrayList<CartModel>>()


    fun getCartValues(textCartItemCount: TextView) {
        try {
            GetProductsOfCart(textCartItemCount).execute()
        }catch (e: Exception){}

        mData2.value=array2
    }

    fun getCartArray(): ArrayList<CartModel> {

        return array2
    }


    @SuppressLint("StaticFieldLeak")
    inner class GetProductsOfCart(var textCartItemCount: TextView) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(application)

            array2 =  db!!.productDao().getAllCart() as ArrayList<CartModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mData2.value = array2
            textCartItemCount.text=array2.size.toString()
        }
    }


//    getWishlist

    fun geWishListData(catId: String, progressBar: ProgressBar, no_Data: TextView, sortFilterCl: ConstraintLayout, wishList: java.util.ArrayList<ModelWishList>, adapterSearchEcommerce: AdapterSortFilterEcommerce, sort: String, page: String) {
        progressBar.visibility= View.VISIBLE
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.getWishList( auth!!)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility= View.GONE
                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optString("success")
                    val array: ArrayList<ModelWishList> = ArrayList()
                    if (success == "true") {
                        val products = json.optJSONArray("products")
                        for (i in 0 until products.length()) {
                            val oo = products.optJSONObject(i)
                            val model = ModelWishList()
                            model.product_id = oo.optString("product_id")
                            model.sku = oo.optString("sku")
                            model.quantity = oo.optString("stock")
                            model.price = oo.optString("price")
                            model.nameWishlist = oo.optString("name")
                            model.imageWishlist = oo.optString("image")
                            model.tax_class_id = oo.optString("tax_class_id")
                            if (oo.optString("rating").trim().isEmpty()||oo.optString("rating")==null||oo.optString("rating")=="null"){
                                model.rating = "0"
                            }else{
                                model.rating = oo.optString("rating")
                            }
                            model.display_price = oo.optString("display_price")
                            model.category_id = oo.optString("category_id")
                            model.saleable = oo.optString("saleable")
                            model.selectQuant = "1"
                            array.add(model)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility= View.GONE
            }


        })
    }

    fun setPreviousArray(
        basketArray: List<BasketModel>,
        adapterSearchEcommerce: AdapterSortFilterEcommerce
    ) {
        adapterSearchEcommerce.update(array,basketArray)
    }

    fun getFlashDiscountProduct(
        auth: String,
        page: Int,
        wishList: ArrayList<ModelWishList>
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.getFlashProducts(auth, page.toString())
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optString("success")
                        val products = json.getJSONArray("products")
                        if (success == "true") {
                            array.clear()
                            for (i in 0 until products.length()) {

                                val homeServiceModel = FetchProductsModel()
                                val categoryObject = products.getJSONObject(i)
                                homeServiceModel.product_id = categoryObject.optString("product_id")
                                homeServiceModel.name = categoryObject.optString("name")
                                homeServiceModel.sku = categoryObject.optString("sku")
                                homeServiceModel.quantity = categoryObject.optString("stock")
                                homeServiceModel.image = categoryObject.optString("image")
                                homeServiceModel.price = categoryObject.optString("price")
                                homeServiceModel.rating = categoryObject.optDouble("rating")
                                homeServiceModel.category_id = categoryObject.optString("category_id")
                                homeServiceModel.display_price = categoryObject.optString("display_price")
                                homeServiceModel.saleable = categoryObject.optString("saleable")
                                homeServiceModel.tax_class_id = categoryObject.optString("tax_class_id")
                                homeServiceModel.stock = categoryObject.optString("stock")
                                homeServiceModel.uom = categoryObject.optString("uom")
                                homeServiceModel.flash_discount = categoryObject.optString("flash_discount")
                                homeServiceModel.isexempted = categoryObject.optString("isexempted")
                                homeServiceModel.selectQuant ="1"
                                var exist=0
                                for (i in 0 until wishList.size){
                                    if (wishList[i].product_id == homeServiceModel.product_id){
                                        exist=1
                                        break
                                    }
                                }
                                homeServiceModel.wish = exist==1

                                array.add(homeServiceModel)
                            }

                            val model = ModelPost()
                           // model.count = json.optInt("total_product")
                            model.list = array
                            mlist.value = model
                            // mData.value=array

                        }else{

                            mlist.value = ModelPost()
                           // progressBar.visibility = View.GONE
                        }
                    } catch (e: Exception) {
                        mlist.value = ModelPost()
                        e.printStackTrace()
                       // progressBar.visibility = View.GONE
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                mlist.value = ModelPost()
                //  mData.value = array
//                adapterSearchEcommerce.update(array,basketArray)
//                progressBar.visibility = View.GONE
//                noDataText.visibility = View.VISIBLE
            }
        })
    }

    class ModelPost(){
        var count: Int = 0
        var list = ArrayList<FetchProductsModel>()
    }
}

