package com.unbrako.e_Commerce.catalog

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.signup.SignUpModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by Manisha Thakur on 15/4/19.
 */
class CatalogRepository(var application: Application) {


    val mData = MutableLiveData<ArrayList<CatalogModel>>()
    val mDataTax = MutableLiveData<ArrayList<ModelTax>>()
    val mDataWishList = MutableLiveData<ArrayList<ModelWishList>>()
    var productsArray: ArrayList<CatalogModel> = ArrayList<CatalogModel>()
    var zoneData=MutableLiveData<ArrayList<ZoneModel>>()
    fun getItems(progressBar: ProgressBar) {

        getProducts(progressBar)

    }

    private fun getProducts(progressBar: ProgressBar) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        progressBar.visibility = View.VISIBLE
         productsArray = ArrayList<CatalogModel>()
        val taxArray: ArrayList<ModelTax> = ArrayList<ModelTax>()
        val wishListArray: ArrayList<ModelWishList> = ArrayList<ModelWishList>()
        val zoneArray: ArrayList<ZoneModel> = ArrayList<ZoneModel>()

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.getAllService("112.196.18.4", auth)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optString("success")

                        if (success == "true") {
                            val unbrako_tax=json.optString("unbrako_tax")
                            val credit=json.optString("credit")
                            val approval=json.optString("approval")
                            val flash_sale=json.optString("flash_sale")
                            val flash_sale_msg=json.optString("flash_sale_msg")
                            val banner=json.optString("banner")
                            val mVariable = Gson()
                            val jsonrr = Constants.getPrefs(application).getString("profile", "")
                            val objrr: SignUpModel = mVariable.fromJson(jsonrr, SignUpModel::class.java)
                            objrr.credit=credit
                            val json1rr = mVariable.toJson(objrr)
                            Constants.getPrefs(application).edit().putString("profile", json1rr).apply()
                            Constants.getSharedPrefs(application).edit().putString("unbrako_tax",unbrako_tax)
                                .putString("flash_sale",flash_sale)
                                .putString("approval",approval)
                                .putString("banner",banner)
                                .putString("flash_sale_msg",flash_sale_msg).apply()
                            val categories = json.getJSONArray("categories")
                            val tax = json.getJSONArray("tax")
                            val wishlist = json.getJSONArray("wishlist")
                            val zone = json.getJSONArray("zone")
                            val payment_method = json.getJSONArray("payment_method")

                            for (i in 0 until categories.length()) {
                                val homeServiceModel = CatalogModel()
                                val categoryObject = categories.getJSONObject(i)

                                homeServiceModel.category_id = categoryObject.optString("category_id")
                                homeServiceModel.image = categoryObject.optString("image")
                                homeServiceModel.parent_id = categoryObject.optString("parent_id")
                                homeServiceModel.status = categoryObject.optString("status")
                                homeServiceModel.name = categoryObject.optString("name")
                                homeServiceModel.measurment = categoryObject.optString("measurment_type")

                                productsArray.add(homeServiceModel)
                            }

                            for (j in 0 until tax.length()) {

                                val homeServiceModel = ModelTax()
                                val taxObj = tax.getJSONObject(j)

                                homeServiceModel.descriptionTax = taxObj.optString("descriptionTax")
                                homeServiceModel.title = taxObj.optString("title")
                                homeServiceModel.rate = taxObj.optString("rate")
                                homeServiceModel.tax_class_id = taxObj.optString("tax_class_id")

                                taxArray.add(homeServiceModel)
                            }

                            for (k in 0 until wishlist.length()) {
                                val homeServiceModel = ModelWishList()
                                val wishlistObj = wishlist.getJSONObject(k)

                                homeServiceModel.product_id = wishlistObj.optString("product_id")
                                homeServiceModel.sku = wishlistObj.optString("sku")
                                homeServiceModel.quantity = wishlistObj.optString("quantity")
                                homeServiceModel.price = wishlistObj.optString("price")
                                homeServiceModel.statusWishlist = wishlistObj.optString("statusWishlist")
                                homeServiceModel.nameWishlist = wishlistObj.optString("nameWishlist")
                                homeServiceModel.description = wishlistObj.optString("description")
                                homeServiceModel.imageWishlist = wishlistObj.optString("imageWishlist")
                                homeServiceModel.rating = wishlistObj.optString("rating")
                                homeServiceModel.sort_order = wishlistObj.optString("sort_order")

                                wishListArray.add(homeServiceModel)

                            }
                            for (m in 0 until zone.length()) {
                                val homeServiceModel = ZoneModel()
                                val wishlistObj = zone.getJSONObject(m)

                                homeServiceModel.zone_id = wishlistObj.optString("zone_id")
                                homeServiceModel.name = wishlistObj.optString("name")
                                zoneArray.add(homeServiceModel)

                            }
                            mData.value = productsArray
                            mDataTax.value = taxArray
                            mDataWishList.value = wishListArray
                            zoneData.value = zoneArray
                        }

                    } catch (e: Exception) {
                        val homeServiceModel = CatalogModel()
                        productsArray.add(homeServiceModel)
                        mData.value = productsArray

                        e.printStackTrace()

                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                progressBar.visibility = View.GONE

                val homeServiceModel = CatalogModel()
                productsArray.add(homeServiceModel)
                mData.value = productsArray
            }

        })

    }

    fun setCategoryData(): LiveData<ArrayList<CatalogModel>> {

        return mData

    }

    fun getWishList(): LiveData<ArrayList<ModelWishList>> {

        return mDataWishList
    }

    fun getAllCats(): ArrayList<CatalogModel> {

        return productsArray
    }

    fun getTaxRate(): LiveData<ArrayList<ModelTax>> {

        return mDataTax
    }

    fun getZoneArray(): MutableLiveData<ArrayList<ZoneModel>> {

        return zoneData
    }

}