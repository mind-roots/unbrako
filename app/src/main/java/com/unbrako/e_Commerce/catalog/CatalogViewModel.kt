package com.unbrako.e_Commerce.catalog

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Created by Manisha Thakur on 15/4/19.
 */
class CatalogViewModel(application: Application) : AndroidViewModel(application)  {


    var catalogRepository : CatalogRepository = CatalogRepository(application)

    fun getCatageoryData() : LiveData<ArrayList<CatalogModel>>{

        return catalogRepository.setCategoryData()
    }


    fun getList(progressBar : ProgressBar) {
          catalogRepository.getItems(progressBar)
    }

    fun getWishListArray(): LiveData<ArrayList<ModelWishList>> {
    return catalogRepository.getWishList()
    }

    fun getCat(): ArrayList<CatalogModel> {

        return catalogRepository.getAllCats()
    }

    fun getTaxArray(): LiveData<ArrayList<ModelTax>>  {

        return catalogRepository.getTaxRate()
    }

    fun getZoneArray(): MutableLiveData<ArrayList<ZoneModel>> {
        return catalogRepository.getZoneArray()
    }
}