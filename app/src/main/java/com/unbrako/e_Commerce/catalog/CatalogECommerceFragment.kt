package com.unbrako.e_Commerce.catalog

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.FragmentEcommerceCatalogBinding
import android.content.Intent
import com.bumptech.glide.Glide
import com.unbrako.e_Commerce.UpdateingCart
import com.unbrako.e_Commerce.catalog.sortFilter.CatalogSortFilterActivity
import com.google.gson.Gson
import com.unbrako.intro.SelectApp

/**
 * Created by Manisha Thakur on 1/4/19.
 */
class CatalogECommerceFragment : Fragment(), RadioGroup.OnCheckedChangeListener

    , AdapterMainCatalog.checkIfsubCategory {

    private lateinit var mAdapterMainCatalog: AdapterMainCatalog
    lateinit var mBinding: FragmentEcommerceCatalogBinding
    lateinit var viewModel: CatalogViewModel
    var status: Boolean = false
    var wishList: ArrayList<ModelWishList> = ArrayList()
    lateinit var update: UpdateingCart

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_ecommerce_catalog, container, false)
        viewModel = ViewModelProviders.of(this)[CatalogViewModel::class.java]
        setHasOptionsMenu(true)
        viewModel.getList(mBinding.progressBar)
        return mBinding.root
    }

    @SuppressLint("WrongConstant")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val layoutManager1 = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        mBinding.recyclerView.layoutManager = layoutManager1
        mBinding.recyclerView.isNestedScrollingEnabled = false
        mAdapterMainCatalog = AdapterMainCatalog(requireActivity(), this@CatalogECommerceFragment)
        mBinding.recyclerView.adapter = mAdapterMainCatalog
        mBinding.include4.Eradio.setOnCheckedChangeListener(this)
        mBinding.bannerLay.setOnClickListener {
            val intent = Intent(activity, CatalogSortFilterActivity::class.java)
            intent.putExtra("flashList", "")
            intent.putExtra("wishList", wishList)
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        Constants.getProductsOfCart(requireActivity()).execute()
        update.updateCarrtCat(Constants.cartSize)


        viewModel.getWishListArray().observe(this, Observer {

            wishList.addAll(it)

        })
        viewModel.getTaxArray().observe(this, Observer {

            if (it.size > 0) {
                val editor = Constants.getSharedPrefs(requireActivity()).edit()
                val gson = Gson()
                val json = gson.toJson(it)
                editor.putString("rate", json)
                editor.apply()
            }

        })
        viewModel.getZoneArray().observe(this, Observer {

            if (it.size > 0) {

                val gson = Gson()
                val textList = ArrayList<ZoneModel>()
                textList.addAll(it)
                val jsonText = gson.toJson(textList)
                Constants.getPrefs(requireActivity()).edit().putString("state", jsonText).apply()
            }

        })
        viewModel.getCatageoryData().observe(this, Observer {
            val select = Constants.getSharedPrefs(requireActivity()).getString("SelectedE", "1")

            if (select == "1") {
                mBinding.include4.eRbMetric.isChecked = true
                val items: ArrayList<CatalogModel> = ArrayList()
                for (i in 0 until it.size) {
                    if (it[i].measurment == "1" && it[i].parent_id == "0" || it[i].measurment == "3" && it[i].parent_id == "0") {
                        items.add(it[i])
                    }
                }
                mAdapterMainCatalog.update(items, wishList)
            } else {
                mBinding.include4.eRbInch.isChecked = true
                val items: ArrayList<CatalogModel> = ArrayList()
                for (i in 0 until it.size) {
                    if (it[i].measurment == "2" && it[i].parent_id == "0" || it[i].measurment == "3" && it[i].parent_id == "0") {
                        items.add(it[i])

                    }
                }
                mAdapterMainCatalog.update(items, wishList)
            }
            if (it.size > 0) {
                mBinding.noData.visibility = View.GONE
            } else {
                mBinding.noData.visibility = View.VISIBLE
            }

            showFlashSaleMessage()
        })

    }

    private fun showFlashSaleMessage() {
        val flash_sale = Constants.getSharedPrefs(requireActivity()).getString("flash_sale", "")
        val flash_sale_msg = Constants.getSharedPrefs(requireActivity()).getString("flash_sale_msg", "")
        val banner = Constants.getSharedPrefs(requireActivity()).getString("banner", "")
        if (flash_sale == "off"||flash_sale!!.isEmpty()) {
            //mBinding.flashSaleText.visibility=View.GONE
            mBinding.bannerLay.visibility = View.GONE
        } else{
            mBinding.bannerLay.visibility = View.VISIBLE

            Glide.with(this)
                .load(banner)
                .into(mBinding.imageviewCard)
            //mBinding.flashSaleText.text=flash_sale_msg
//            mBinding.flashSaleText.text="Test flash Test flash Test flash Test flash Test flash Test flash Test flash Test flash Test flash Test flash Test flash Test flash Test flash "
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

        if (mBinding.include4.eRbMetric.isChecked) {
            val prefs = Constants.getSharedPrefs(requireActivity()).edit()
            prefs.putString("SelectedE", "1")
            prefs.apply()
            mBinding.include4.eRbMetric.setTextColor(Color.parseColor("#000000"))
            mBinding.include4.eRbInch.setTextColor(Color.GRAY)
            viewModel.getCatageoryData().observe(this, Observer {
                val items: ArrayList<CatalogModel> = ArrayList()
                for (i in 0 until it.size) {
                    if (it[i].measurment == "1" && it[i].parent_id == "0" || it[i].measurment == "3" && it[i].parent_id == "0") {
                        items.add(it[i])
                    }
                }
                mAdapterMainCatalog.update(items, wishList)
                showFlashSaleMessage()
            })

        }

        if (mBinding.include4.eRbInch.isChecked) {

            val prefs = Constants.getSharedPrefs(requireActivity()).edit()
            prefs.putString("SelectedE", "0")
            prefs.apply()
            mBinding.include4.eRbInch.setTextColor(Color.parseColor("#000000"))
            mBinding.include4.eRbMetric.setTextColor(Color.GRAY)
            viewModel.getCatageoryData().observe(this, Observer {

                val items: ArrayList<CatalogModel> = ArrayList()
                for (i in 0 until it.size) {
                    if (it[i].measurment == "2" && it[i].parent_id == "0" || it[i].measurment == "3" && it[i].parent_id == "0") {
                        items.add(it[i])
                    }
                }
                mAdapterMainCatalog.update(items, wishList)
                showFlashSaleMessage()
            })
        }
        mBinding.recyclerView.smoothScrollToPosition(0)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.ecommerce_cataloog_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == R.id.cart_read) {
            val prefs = Constants.getSharedPrefs(requireActivity()).edit()
            prefs.putString("App", "main")
            prefs.apply()
            val intent = Intent(requireActivity(), SelectApp::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            requireActivity().finish()
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("WrongConstant")
    override fun checkingSubCategory(model: CatalogModel) {
        val prefs = Constants.getSharedPrefs(requireActivity())
        val ss = prefs.getString("SelectedE", "1")

        val items: ArrayList<CatalogModel> = viewModel.getCat()
        val items2: ArrayList<CatalogModel> = ArrayList()
        for (i in 0 until items.size) {
//            if (items[i].parent_id == model.category_id || items[i].measurment == "3" && items[i].parent_id != "0") {
            if (items[i].parent_id == model.category_id) {
                if (ss == "1") {
                    if ("3" == items[i].measurment || items[i].measurment == "1")
                        items2.add(items[i])

                } else {
                    if ("3" == items[i].measurment || items[i].measurment == "2")
                        items2.add(items[i])
                }
            }
        }
        if (items2.size > 0) {
            val intent = Intent(activity, SubCategoryActivity::class.java)
            intent.putExtra("model", model)
            intent.putExtra("SubCat", items2)
            intent.putExtra("wishList", wishList)
            startActivity(intent)
            // mAdapterMainCatalog.update(items2,wishList)
        } else {
            val intent = Intent(activity, CatalogSortFilterActivity::class.java)
            intent.putExtra("model", model)
            intent.putExtra("wishList", wishList)
            startActivity(intent)
        }


    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as UpdateingCart
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as UpdateingCart
    }

}