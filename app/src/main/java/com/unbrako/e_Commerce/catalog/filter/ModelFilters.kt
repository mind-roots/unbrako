package com.unbrako.e_Commerce.catalog.filter

import android.os.Parcel
import android.os.Parcelable

class ModelFilters() : Parcelable{

    var key : Int = 0
    var name : String=""
    var value: String=""
    var background : Boolean = false
    var check : Boolean = false

    constructor(parcel: Parcel) : this() {
        key = parcel.readInt()
        name = parcel.readString()!!
        value = parcel.readString()!!
        background = parcel.readByte() != 0.toByte()
        check = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(key)
        parcel.writeString(name)
        parcel.writeString(value)
        parcel.writeByte(if (background) 1 else 0)
        parcel.writeByte(if (check) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelFilters> {
        override fun createFromParcel(parcel: Parcel): ModelFilters {
            return ModelFilters(parcel)
        }

        override fun newArray(size: Int): Array<ModelFilters?> {
            return arrayOfNulls(size)
        }
    }

}