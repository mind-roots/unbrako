package com.unbrako.e_Commerce.catalog.filter

import android.app.Application
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

/**
 * Created by Manisha Thakur on 24/4/19.
 */
class ViewModelFilter(application: Application) : AndroidViewModel(application) {

    var mRepoFilter: RepoFilter = RepoFilter(application)

    fun getFormattedData(intent: Intent?):LiveData<List<FilterModel>> {
      return mRepoFilter.getFilterData(intent)
    }

}