package com.unbrako.e_Commerce.catalog.sortFilter

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.e_Commerce.catalog.productDetails.ProductDetailActivity
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import kotlinx.android.synthetic.main.sort_filter_layout.view.*


class AdapterSortFilterEcommerce(var mContext: Context) :
    RecyclerView.Adapter<AdapterSortFilterEcommerce.ViewHolder>() {

    var list: ArrayList<FetchProductsModel> = ArrayList()
    var basketArray: List<BasketModel> = ArrayList()

    var callApi = mContext as WishListApiCall
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.sort_filter_layout, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, positionT: Int) {

        val modelSearchEcommerce = list[positionT]

        if (list[positionT].price.isEmpty()) {
            list[positionT].price = "0"
        }
        if (list[positionT].saleable.isEmpty()) {
            list[positionT].saleable = "0"
        }
        if (list[positionT].display_price.isEmpty()) {
            list[positionT].display_price = "0"
        }

//        holder.itemView.tvThree.text =
//            "₹" + Constants.currencyFormt((list[positionT].price.toDouble() * list[positionT].saleable.toDouble()))



//        val price=(Constants.getDiscountedPrice(list[positionT],mContext) * list[positionT].saleable.toDouble())
//        val cut= Constants.CuttOfPrice(list[positionT],mContext)
//        val cut2="₹" + Constants.currencyFormt(Constants.CuttOfPrice(list[positionT],mContext)).toString()+""
//        val stringFull=("₹" + Constants.currencyFormt((price))+"\n"+cut2)
//        val stringFull2=("per " + Constants.formatValues2(list[positionT].saleable)+" "+list[positionT].uom)+(Constants.getDiscountApplies(list[positionT],mContext)).toString()
//        val start=("₹" + Constants.currencyFormt((price))).length
//        val end=("₹" + Constants.currencyFormt((price))).length+cut2.length
//        val string = SpannableString(stringFull)
//        val string2 = SpannableString(stringFull2)
//
//        val discountStart=("per " + Constants.formatValues2(list[positionT].saleable)+" "+list[positionT].uom).length
//        val discountEnd=("per " + Constants.formatValues2(list[positionT].saleable)+" "+list[positionT].uom).length+(Constants.getDiscountApplies(list[positionT],mContext)).toString().length
//        string.setSpan(StrikethroughSpan(), start, end+1, 0)
//        string.setSpan( RelativeSizeSpan(.8f), start,end+1, 0)
//        string2.setSpan( ForegroundColorSpan(mContext.getColor(R.color.colorAccent)), discountStart+2, discountEnd-1, 0)
//        if (cut!=0.0&&cut!=price){
////            holder.itemView.tvThree.text =
////                "₹" + Constants.currencyFormt((price))+"(₹"+string+")"
//          holder.itemView.tvThree.text =string
////            holder.itemView.tvFour.text = "per " + Constants.formatValues2(list[positionT].saleable)+" "+list[positionT].uom+Constants.getDiscountApplies(list[positionT],mContext)
//            holder.itemView.tvFour.text = string2
//
//        }else{
//            holder.itemView.tvThree.text =
//                "₹" + Constants.currencyFormt((price))
//            holder.itemView.tvFour.text = " per " + Constants.formatValues2(list[positionT].saleable)+" "+list[positionT].uom
//
//        }

        val gson = Gson()
        val json = gson.toJson(list[positionT])
        val fetchProductsModel: CartModel = gson.fromJson(json, CartModel::class.java)

        holder.itemView.tvThree.text = Constants.getPriceText(fetchProductsModel, mContext, 2)
        holder.itemView.tvFour.text =Constants.getPriceText2(fetchProductsModel,mContext,2)


       if (list[positionT].display_price.toDouble() > 0) {
            holder.itemView.cutOff.visibility = View.VISIBLE
            holder.itemView.tvFive.text =
                "₹" + Constants.currencyFormt((list[positionT].display_price.toDouble() * list[positionT].saleable.toDouble()))
        } else {
            holder.itemView.cutOff.visibility = View.GONE
        }
        holder.itemView.tvFive.paintFlags = holder.itemView.tvFive.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        if (list[positionT].image.isNotEmpty()) {
            Picasso.get().load(list[positionT].image).placeholder(R.drawable.pro_img_new).fit().centerCrop()
                .into(holder.itemView.circular_img)
        }

        if (modelSearchEcommerce.name.contains("®")) {
            //val vv =  modelSearchEcommerce.name.split("®")
            holder.itemView.tv_one.text = Html.fromHtml(modelSearchEcommerce.name.replace("®", "<sup>®</sup>"))
            //holder.itemView.tv_one.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
        } else {
            holder.itemView.tv_one.text = modelSearchEcommerce.name
        }

        holder.itemView.iv_fav.setOnClickListener {
            callApi.wishListApi(modelSearchEcommerce, positionT)
            if (modelSearchEcommerce.wish) {
                holder.iv_fav.setImageResource(R.drawable.ic_favorite_not)
                modelSearchEcommerce.wish = false
            } else {
                holder.iv_fav.setImageResource(R.drawable.ic_favorite)
                modelSearchEcommerce.wish = true
            }


        }
        holder.itemView.iv_basket.setOnClickListener {
            //callApi.wishListApi(modelSearchEcommerce, positionT)
            //if (Constants.IsStockAvailable(modelSearchEcommerce)) {
            var exist = 0
            for (i in 0 until basketArray.size) {
                if (basketArray[i].product_id == modelSearchEcommerce.product_id) {
                    exist = 1
                }
            }
            if (exist == 1) {

                callApi.addToBasketLayout(
                    modelSearchEcommerce,
                    positionT,
                    holder.itemView.spinner_des.text.toString(),
                    "0"
                )
            } else {
                callApi.addToBasketLayout(
                    modelSearchEcommerce,
                    positionT,
                    holder.itemView.spinner_des.text.toString(),
                    "1"
                )

            }
//            }else{
//                if (modelSearchEcommerce.stock.toDouble() < 0) {
//
//                    Toast.makeText(mContext,"Stock Unavailable",Toast.LENGTH_LONG).show()
//                } else {
//                    Toast.makeText(mContext,"Quantity exceed than available stock.",Toast.LENGTH_LONG).show()
//
//                }
//            }



        }
        var exist=0
        for (i in 0 until basketArray.size){
            if(basketArray[i].product_id==modelSearchEcommerce.product_id){
                exist=1
            }
        }
        if (exist==1){

            holder.iv_basket.setImageResource(R.drawable.ic_enquire_basket_selected)
        } else {
            holder.iv_basket.setImageResource(R.drawable.ic_enquire_basket_unselected_dark)



        }
        holder.itemView.tvTwo.text = modelSearchEcommerce.price
        holder.itemView.partNo.text = modelSearchEcommerce.sku

        if (modelSearchEcommerce.wish) {
            holder.iv_fav.setImageResource(R.drawable.ic_favorite)
        } else {
            holder.iv_fav.setImageResource(R.drawable.ic_favorite_not)

        }

        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, ProductDetailActivity::class.java).putExtra("model", modelSearchEcommerce)
                .putExtra("from", "sort").putExtra("pos", positionT.toString())
            mContext.startActivity(intent)
        }

        holder.itemView.spinner_des.text = modelSearchEcommerce.selectQuant
        holder.itemView.spinner_des.setOnClickListener {
            openDialogQuantity(mContext, positionT, list[positionT])

        }

        holder.itemView.addToCart.setOnClickListener {
           // if (Constants.IsStockAvailable(modelSearchEcommerce)) {
                callApi.addToCartLayout(modelSearchEcommerce, positionT, holder.itemView.spinner_des.text.toString())
//            }else{
//                if (modelSearchEcommerce.stock.toDouble() < 0) {
//
//                    Toast.makeText(mContext,"Stock Unavailable",Toast.LENGTH_LONG).show()
//                } else {
//                    Toast.makeText(mContext,"Quantity exceed than available stock.",Toast.LENGTH_LONG).show()
//
//                }
//            }
        }
    }



    private fun openDialogQuantity(mContext: Context, position: Int, quantity: FetchProductsModel) {

        var color = "1"
        lateinit var dialog: AlertDialog

        // Initialize an array of colors
        val array =
            arrayOf(
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24",
                "25",
                "26",
                "27",
                "28",
                "29",
                "30",
                "31",
                "32",
                "33",
                "34",
                "35",
                "36",
                "37",
                "38",
                "39",
                "40",
                "41",
                "42",
                "43",
                "44",
                "45",
                "46",
                "47",
                "48",
                "49",
                "50"
            )

        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(mContext)

        // Set a title for alert dialog
        builder.setTitle("Select Quantity")


        builder.setSingleChoiceItems(array, quantity.selectQuant.toInt() - 1) { _, which ->
            color = array[which]

            list[position].selectQuant = color
            Constants.UpdateToCarts(mContext, list[position]).execute()
//            cal.calculateTotal()
            notifyDataSetChanged()
            // Dismiss the dialog
            dialog.dismiss()
        }

        dialog = builder.create()
        dialog.show()

    }


    fun update(
        it: ArrayList<FetchProductsModel>,
        basketArray1: List<BasketModel>
    ) {
        if (it.size > 0){
            list = it
        basketArray = basketArray1
        notifyDataSetChanged()
    }else{
            list = it
            basketArray = basketArray1
            notifyDataSetChanged()
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iv_fav: ImageView = itemView.findViewById(R.id.iv_fav)
        var iv_basket: ImageView = itemView.findViewById(R.id.iv_basket)
        var spinnerDes: TextView = itemView.findViewById(R.id.spinner_des)

    }

    interface WishListApiCall {
        fun wishListApi(model: FetchProductsModel, position: Int)
        fun addToCartLayout(
            model: FetchProductsModel,
            position: Int,
            spinnerDes: String
        )

        fun addToBasketLayout(
            model: FetchProductsModel,
            position: Int,
            spinnerDes: String,
            delete: String
        )
    }

}
