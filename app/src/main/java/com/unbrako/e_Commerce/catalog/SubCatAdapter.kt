package com.unbrako.e_Commerce.catalog

import android.content.Intent
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.unbrako.R
import com.unbrako.e_Commerce.catalog.sortFilter.CatalogSortFilterActivity
import kotlinx.android.synthetic.main.catalog_recycler_card.view.imageview_card
import kotlinx.android.synthetic.main.catalog_recycler_card.view.textview_card_title
import kotlinx.android.synthetic.main.catalog_recycler_card_sub.view.*
import java.util.ArrayList

class SubCatAdapter(var subCategoryActivity: SubCategoryActivity, var items2: ArrayList<CatalogModel>, var wishList: ArrayList<ModelWishList>) : RecyclerView.Adapter<SubCatAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(subCategoryActivity).inflate(R.layout.catalog_recycler_card_sub, parent, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return items2.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if ( items2[position].name.contains("®")) {
            val vv =  items2[position].name.split("®")
            holder.itemView.textview_card_title.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
        }else{
            holder.itemView.textview_card_title.text = items2[position].name

        }


        if (items2[position].image.isEmpty()) {
            holder.itemView.imageview_card2.setImageResource(R.mipmap.login_logo)
            holder.itemView.cardView2.setCardBackgroundColor(subCategoryActivity.getColor(R.color.colorAccent))
            holder.itemView.cardView2.visibility = View.VISIBLE
            holder.itemView.cardLay.visibility = View.GONE
        } else {
            holder.itemView.cardView2.visibility = View.GONE
            holder.itemView.cardLay.visibility = View.VISIBLE
            Picasso.get().load(items2[position].image).into(holder.itemView.imageview_card)
        }
        holder.itemView.setOnClickListener {
            val intent = Intent(subCategoryActivity, CatalogSortFilterActivity::class.java)
            intent.putExtra("model", items2[position])
            intent.putExtra("wishList", wishList)
            subCategoryActivity.startActivity(intent)
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}