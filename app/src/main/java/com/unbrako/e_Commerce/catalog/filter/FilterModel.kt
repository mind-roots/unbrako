package com.unbrako.e_Commerce.catalog.filter

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Manisha Thakur on 24/4/19.
 */

@Entity
class FilterModel() : Parcelable{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "key")
    var key : Int = 0
    @ColumnInfo(name = "name")
    var name : String=""

    @ColumnInfo(name = "value")
    var value: String=""

    @ColumnInfo(name = "background")
    var background : Boolean = false

    @ColumnInfo(name = "check")
    var check : Boolean = false

    constructor(parcel: Parcel) : this() {
        key = parcel.readInt()
        name = parcel.readString()!!
        value = parcel.readString()!!
        background = parcel.readByte() != 0.toByte()
        check = parcel.readByte() != 0.toByte()
    }

//    @ColumnInfo(name = "Diameter")
//    var Diameter : String=""
//
//    @ColumnInfo(name = "DIN")
//    var DIN : String=""
//
//    @ColumnInfo(name = "Finish")
//    var Finish : String=""
//
//    @ColumnInfo(name = "Grade")
//    var Grade : String=""
//
//    @ColumnInfo(name = "ISO")
//    var ISO : String=""
//
//    @ColumnInfo(name = "Length")
//    var Length : String=""
//
//    @ColumnInfo(name = "Pitch")
//    var Pitch : String=""
//
//    @ColumnInfo(name = "Size")
//    var Size : String=""
//
//    @ColumnInfo(name = "System of Measurement (Inch or Metric)")
//    var System : String=""
//
//    @ColumnInfo(name = "Thread Type (Full or Half)")
//    var Thread : String=""
//
//    constructor(parcel: Parcel) : this() {
//        key = parcel.readInt()
//        ASME = parcel.readString()
//        BS = parcel.readString()
//        Diameter = parcel.readString()
//        DIN = parcel.readString()
//        Finish = parcel.readString()
//        Grade = parcel.readString()
//        ISO = parcel.readString()
//        Length = parcel.readString()
//        Pitch = parcel.readString()
//        Size = parcel.readString()
//        System = parcel.readString()
//        Thread = parcel.readString()
//    }
//
//    override fun writeToParcel(parcel: Parcel, flags: Int) {
//        parcel.writeInt(key)
//        parcel.writeString(ASME)
//        parcel.writeString(BS)
//        parcel.writeString(Diameter)
//        parcel.writeString(DIN)
//        parcel.writeString(Finish)
//        parcel.writeString(Grade)
//        parcel.writeString(ISO)
//        parcel.writeString(Length)
//        parcel.writeString(Pitch)
//        parcel.writeString(Size)
//        parcel.writeString(System)
//        parcel.writeString(Thread)
//    }
//
//    override fun describeContents(): Int {
//        return 0
//    }
//
//    companion object CREATOR : Parcelable.Creator<FilterModel> {
//        override fun createFromParcel(parcel: Parcel): FilterModel {
//            return FilterModel(parcel)
//        }
//
//        override fun newArray(size: Int): Array<FilterModel?> {
//            return arrayOfNulls(size)
//        }
//    }
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(key)
        parcel.writeString(name)
        parcel.writeString(value)
        parcel.writeByte(if (background) 1 else 0)
        parcel.writeByte(if (check) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FilterModel> {
        override fun createFromParcel(parcel: Parcel): FilterModel {
            return FilterModel(parcel)
        }

        override fun newArray(size: Int): Array<FilterModel?> {
            return arrayOfNulls(size)
        }
    }
}