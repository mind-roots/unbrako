package com.unbrako.e_Commerce.catalog.sortFilter

import android.app.Application
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel

class SortFilterViewModel(application: Application) : AndroidViewModel(application) {

    var refVar: RepoSortFilter = RepoSortFilter(application)

    fun getDetails(): MutableLiveData<RepoSortFilter.ModelPost> {
        //get data from the repo
        return refVar.getDetails()
    }

    fun getSortFilterData(
        category_id: String,
        progressBar: ProgressBar,
        sortFilterCl: LinearLayout,
        wishList: java.util.ArrayList<ModelWishList>,
        sort: String,
        page: String,
        YesClear: String,
        type: String
    ) {
        refVar.getSortFilterData(category_id, progressBar, sortFilterCl, wishList, sort, page, YesClear, type)
    }

    fun updateArray(

        position: Int,
        cartFav: String,
        adapterSearchEco: AdapterSortFilterEcommerce,
        basketArray: List<BasketModel>
    ) {

        refVar.updateArrayPosition(position, cartFav, adapterSearchEco, basketArray)
    }

    fun addToWishList(model: FetchProductsModel, position: Int, adapterSearchEco: AdapterSortFilterEcommerce) {

        refVar.addingWishList(model, position, adapterSearchEco)
    }

    fun updateSort(
        exist: String?,
        adapterSearchEco: AdapterSortFilterEcommerce,
        basketArray: List<BasketModel>
    ) {
        refVar.upDateS(exist, adapterSearchEco, basketArray)
    }

    fun getFilterData(
        category_id: String,
        progressBar: ProgressBar,
        sortFilterCl: LinearLayout,
        wishList: ArrayList<ModelWishList>,
        adapterSearchEco: AdapterSortFilterEcommerce,
        valuesOf: String,
        toString: String,
        yesClear: String,
        type: String,
        sorting: String,
        basketArray: List<BasketModel>,
        noDataText: TextView
    ) {
        refVar.getFilterData(
            category_id,
            progressBar,
            sortFilterCl,
            wishList,
            adapterSearchEco,
            valuesOf,
            toString,
            yesClear,
            type,
            sorting,
            basketArray, noDataText
        )

    }


    fun getCartValues(textCartItemCount: TextView) {
        refVar.getCartValues(textCartItemCount)

    }

    fun getCartSize(): ArrayList<CartModel> {

        return refVar.getCartArray()
    }

    fun setAdapter(
        basketArray: List<BasketModel>,
        adapterSearchEcommerce: AdapterSortFilterEcommerce
    ) {
        refVar.setPreviousArray(basketArray,adapterSearchEcommerce)    }

    fun getFlashDiscountProduct(
        auth: String,
        page: Int,
        wishList: ArrayList<ModelWishList>
    ) {
        refVar.getFlashDiscountProduct(auth,page,wishList)
    }

}