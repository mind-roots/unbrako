package com.unbrako.e_Commerce.catalog.productDetails

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.Paint
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuItemCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityProductDetailBinding
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel


class ProductDetailActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityProductDetailBinding
    private lateinit var viewModel: ViewModelDescription
    lateinit var adapter: AttrAdapter
    lateinit var textCartItemCount: TextView
    lateinit var textBasketItemCount: TextView
    lateinit var badgeFrame: FrameLayout
    lateinit var basketbadgeFrame: FrameLayout
    var basketArray: List<BasketModel> = ArrayList()
    var model: FetchProductsModel = FetchProductsModel()
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail)
        viewModel = ViewModelProviders.of(this)[ViewModelDescription::class.java]

        init()

        viewModel.getCartValuess()
        var from = intent.getStringExtra("from")
        model = intent.getParcelableExtra<FetchProductsModel>("model")!!
        val position = intent.getStringExtra("pos")
        spinnerAdapter()
        GetProductsInBasketList(this).execute()
        mBinding.spinnerDes.text = "1"
        if (model.image.isNotEmpty()) {
            Picasso.get().load(model.image).into(mBinding.imageProduct)
        }
        mBinding.partNo.text = "Part No - " + model.sku

        val gson = Gson()
        val json = gson.toJson(model)
        val fetchProductsModel: CartModel = gson.fromJson(json, CartModel::class.java)

        mBinding.tvTwo.text = Constants.getPriceText(fetchProductsModel,this,1)
        mBinding.off.text  =Constants.getPriceText2(fetchProductsModel, this, 1)

//        mBinding.tvTwo.text =
//            "₹" + Constants.currencyFormt(Constants.getDiscountedPrice(model,this) * model.saleable.toDouble()).toString()
//        mBinding.off.text = " per " + (model.saleable).toString()+" "+model.uom+Constants.getDiscountApplies(model,this)
        if (model.display_price.toDouble() > 0) {
            mBinding.tvThree.text =
                "₹" + Constants.currencyFormt(model.display_price.toDouble() * model.saleable.toDouble()).toString()
            mBinding.cutOff.visibility = View.VISIBLE
        } else {
            mBinding.cutOff.visibility = View.GONE
        }
        mBinding.tvThree.paintFlags = mBinding.tvThree.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        if (model.wish) {
            mBinding.ivFav.setImageResource(R.drawable.ic_favorite)
        } else {
            mBinding.ivFav.setImageResource(R.drawable.ic_favorite_not)
        }
        mBinding.ivFav.setOnClickListener {

            if (model.wish) {
                mBinding.ivFav.setImageResource(R.drawable.ic_favorite_not)

            } else {
                mBinding.ivFav.setImageResource(R.drawable.ic_favorite)

            }

            if (model.wish) {
                model.wish = false
                viewModel.addWishList(model, "2", mBinding.ivFav)
            } else {
                viewModel.addWishList(model, "1", mBinding.ivFav)
//
                model.wish = true
//               Constants.getPrefs(this).edit().putString("likeD","yes").apply()
//               mBinding.ivFav.setImageResource(R.drawable.ic_favorite)
            }

            Constants.getPrefs(this).edit().putString("likepos", position).apply()
            Constants.getPrefs(this).edit().putString("likeId", model.product_id).apply()

        }
        mBinding.ivBasket.setOnClickListener {

            var exist = 0
            for (i in 0 until basketArray.size) {
                if (basketArray[i].product_id == model.product_id) {
                    exist = 1
                }
            }
            if (exist == 1) {

              //  if (Constants.IsStockAvailable(model)) {
                    val gson = Gson()
                    val innerJson = gson.toJson(model)
                    val obj = gson.fromJson(innerJson, BasketModel::class.java)
                    CheckIfExistInBasketList(obj, application, "0").execute()

//                } else {
//                    if (model.stock.toDouble() < 0) {
//                        showCustomAlert("Stock Unavailable")
//                    } else {
//                        showCustomAlert("Quantity exceed than available stock.")
//                    }
//                }
                mBinding.ivBasket.setImageResource(R.drawable.ic_enquire_basket_unselected_dark)
            } else {
                //if (Constants.IsStockAvailable(model)) {
                    val gson = Gson()
                    val innerJson = gson.toJson(model)
                    val obj = gson.fromJson(innerJson, BasketModel::class.java)
                    CheckIfExistInBasketList(obj, application, "1").execute()
                    mBinding.ivBasket.setImageResource(R.drawable.ic_enquire_basket_selected)
//                } else {
//                    mBinding.ivBasket.setImageResource(R.drawable.ic_enquire_basket_unselected_dark)
//                    if (model.stock.toDouble() < 0) {
//                        showCustomAlert("Stock Unavailable")
//                    } else {
//                        showCustomAlert("Quantity exceed than available stock.")
//                    }
//                }

            }



        }

        viewModel.getDescriptionLive().observe(this, Observer {
            mBinding.description = it
            model.tax_class_id = it.tax_class_id

            //mBinding.tvDesc.text =Html.fromHtml(it.description)
            //mBinding.tvDesc.text =Html.fromHtml(Html.fromHtml(it.description).toString())

            if (it.description.contains("®")) {

                mBinding.tvDesc.text = Html.fromHtml(it.description.replace("®", "<sup>®</sup>"))
//                mBinding.tvDesc.text  = it.description

            } else {
                try {
                    mBinding.tvDesc.text = Html.fromHtml(it.description)
                }catch (e:Exception){
                    print(e)
                }
            }
        })
        viewModel.getAttributes().observe(this, Observer {

            if (it.size > 0) {
                adapter = AttrAdapter(this, it)
                var linearLayout = LinearLayoutManager(this)
                mBinding.bottomList.layoutManager = linearLayout
                mBinding.bottomList.adapter = adapter
            }
        })

        viewModel.getProductDEscription(model.product_id, mBinding.progressBarDesc, mBinding.tvDesc)

        mBinding.tvThree.paintFlags = mBinding.tvThree.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG




        mBinding.addToCart.setOnClickListener {
           // if (Constants.IsStockAvailable(model)) {
                val gson = Gson()
                val innerJson = gson.toJson(model)
                val obj = gson.fromJson(innerJson, CartModel::class.java)
                CheckIfExistInCartList(obj, application).execute()
//            } else {
//                if (model.stock.toDouble() < 0) {
//                    showCustomAlert("Stock Unavailable")
//                } else {
//                    showCustomAlert("Quantity exceed than available stock.")
//                }
//            }

        }
        viewModel.getCartItems().observe(this, Observer {
            if (it.size > 0) {
                try {
                    textCartItemCount.visibility = View.VISIBLE;
                    textCartItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            } else {
                try {
                    textCartItemCount.visibility = View.GONE;
                } catch (e: Exception) {

                }

            }
        })
        viewModel.getBasketValuess()
        viewModel.getBasketItems().observe(this, Observer {
            if (it.size > 0) {
                try {
                    textBasketItemCount.visibility = View.VISIBLE;
                    textBasketItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            } else {
                try {
                    textBasketItemCount.visibility = View.GONE;
                } catch (e: Exception) {

                }

            }
        })



        if (model.name.contains("®")) {
            val vv = model.name.split("®")
            mBinding.textView10.text = Html.fromHtml(model.name.replace("®", "<sup>®</sup>"))
        } else {
            mBinding.textView10.text = model.name
        }

    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        setSupportActionBar(mBinding.descToolbar.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""

        mBinding.descToolbar.toolbarTitle.text = "Details"

    }

    private fun spinnerAdapter() {
        mBinding.spinnerDes.setOnClickListener {
            var color = "1"
            lateinit var dialog: AlertDialog

            // Initialize an array of colors
            var array =
                arrayOf(
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21",
                    "22",
                    "23",
                    "24",
                    "25",
                    "26",
                    "27",
                    "28",
                    "29",
                    "30",
                    "31",
                    "32",
                    "33",
                    "34",
                    "35",
                    "36",
                    "37",
                    "38",
                    "39",
                    "40",
                    "41",
                    "42",
                    "43",
                    "44",
                    "45",
                    "46",
                    "47",
                    "48",
                    "49",
                    "50"
                )

            // Initialize a new instance of alert dialog builder object
            val builder = AlertDialog.Builder(this)

            // Set a title for alert dialog
            builder.setTitle("Select Quantity")


            builder.setSingleChoiceItems(array, mBinding.spinnerDes.text.toString().toInt() - 1) { _, which ->
                color = array[which]
                mBinding.spinnerDes.text = color
                model.quantity = color
                // Dismiss the dialog
                dialog.dismiss()
            }

            dialog = builder.create()
            dialog.show()

        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.catalog_cart, menu)
        val menuItem: MenuItem = menu!!.findItem(R.id.cart)
        val menuItem2: MenuItem = menu!!.findItem(R.id.basket)


        val actionView: View = MenuItemCompat.getActionView(menuItem)
        val actionView2: View = MenuItemCompat.getActionView(menuItem2)
        textCartItemCount = actionView.findViewById(R.id.cart_badge)
        textBasketItemCount = actionView2.findViewById(R.id.basket_badge)
        badgeFrame = actionView.findViewById(R.id.badgeFrame)
        basketbadgeFrame = actionView2.findViewById(R.id.basketbadgeFrame)
        viewModel.getCartItems().observe(this, Observer {
            if (it.size > 0) {
                try {
                    textCartItemCount.visibility = View.VISIBLE
                    textCartItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            } else {
                try {
                    textCartItemCount.visibility = View.GONE
                } catch (e: Exception) {

                }

            }
        })
       viewModel.getBasketItems().observe(this, Observer {
            if (it.size > 0) {
                try {
                    textBasketItemCount.visibility = View.VISIBLE
                    textBasketItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            } else {
                try {
                    textBasketItemCount.visibility = View.GONE
                } catch (e: Exception) {

                }

            }
        })
        badgeFrame.setOnClickListener {
            Constants.getPrefs(this).edit().putString("showCart", "yes").apply()
            finish()
        }
        basketbadgeFrame.setOnClickListener {
            Constants.getPrefs(this).edit().putString("gotoEnquiry", "yes").apply()
            finish()
        }

//        menu!!.findItem(R.id.cart).isVisible=false
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        if (item.itemId == R.id.cart) {
            Constants.getPrefs(this).edit().putString("showCart", "yes").apply()
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    @SuppressLint("StaticFieldLeak")
    inner class CheckIfExistInCartList(
        var model: CartModel,
        var context: Context
    ) : AsyncTask<Void, Void, Void>() {
        var exist = 0
        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)
            val cartItems: ArrayList<CartModel> =
                db!!.productDao().getAllCart() as ArrayList<CartModel>

            for (j in 0 until cartItems.size) {
                if (model.product_id == cartItems[j].product_id) {
                    exist = 1
                }
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (exist == 0) {
                model.selectQuant = mBinding.spinnerDes.text.toString()
                Constants.AddToCart(application, true, model).execute()
                showCustomAlert("Added to Cart")
            } else {
                showCustomAlert("Already Added in Cart")
                //Toast.makeText(application, "Already Added in Cart", Toast.LENGTH_LONG).show()
            }
            viewModel.getCartValuess()

        }
    }

    fun showCustomAlert(msg: String) {
        val context = applicationContext
        // Create layout inflator object to inflate toast.xml file
        val inflater = layoutInflater

        // Call toast.xml file for toast layout
        val toast1 = inflater.inflate(R.layout.toast, null)
        val textm: TextView = toast1.findViewById(R.id.text)
        textm.text = msg
        val toast = Toast(context)

        // Set layout to toast
        toast.view = toast1
        toast.setGravity(
            Gravity.BOTTOM,
            0, 0
        )
        toast.duration = Toast.LENGTH_LONG
        toast.show()
    }

    @SuppressLint("StaticFieldLeak")
    inner class GetProductsInBasketList(

        var context: Context
    ) : AsyncTask<Void, Void, Void>() {
        var exist = 0
        var basketArray1: List<BasketModel> = ArrayList()
        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)

            basketArray1 = db!!.productDao().getAllBasketModelArray()

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

            basketArray = basketArray1
            var exist = 0
            for (i in 0 until basketArray1.size) {
                if (model.product_id == basketArray1[i].product_id) {
                    exist = 1
                }
            }
            if (exist == 1) {
                mBinding.ivBasket.setImageResource(R.drawable.ic_enquire_basket_selected)
            } else {
                mBinding.ivBasket.setImageResource(R.drawable.ic_enquire_basket_unselected_dark)
            }


        }

    }

    @SuppressLint("StaticFieldLeak")
    inner class CheckIfExistInBasketList(
        var model: BasketModel,
        var context: Context,
        var delete: String
    ) : AsyncTask<Void, Void, Void>() {
        var exist = 0
        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)
            if (delete == "0") {
                db!!.productDao().deleteBasketTable(model.product_id)


            } else {
                db!!.productDao().insertBasket(model)
                //remove
            }

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            viewModel.getBasketValuess()
            if (delete == "0") {


                showCustomAlert("Removed from Enquiry")
            } else {
                showCustomAlert("Added to Enquiry")
            }
            GetProductsInBasketList(context).execute()
        }

    }
}