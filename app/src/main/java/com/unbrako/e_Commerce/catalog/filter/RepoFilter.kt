package com.unbrako.e_Commerce.catalog.filter

import android.app.Application
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Created by Manisha Thakur on 24/4/19.
 */
class RepoFilter(var application: Application) {

    var array = ArrayList<FilterModel>()
    var mData = MutableLiveData<List<FilterModel>>()

    init {

    }

    fun getFilterData(intent: Intent?):LiveData<List<FilterModel>>{
        array = intent!!.getParcelableArrayListExtra("model")!!
        mData.value = array
        return mData
    }


}
