package com.unbrako.e_Commerce.catalog.sortFilter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.*
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuItemCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.FilterSortBinding
import com.unbrako.databinding.StockLayoutBinding
import com.unbrako.e_Commerce.catalog.CatalogModel
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.catalog.filter.FilterActivity
import com.unbrako.e_Commerce.catalog.filter.ModelChecked
import com.unbrako.e_Commerce.catalog.productDetails.ViewModelDescription
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import com.unbrako.e_Commerce.search.FavoriteInterface
import kotlinx.android.synthetic.main.activity_catalog_sort_filter.*
import kotlinx.android.synthetic.main.etoolbar_layout.*
import kotlinx.android.synthetic.main.etoolbar_layout.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class CatalogSortFilterActivity : AppCompatActivity(), FavoriteInterface,
    AdapterSortFilterEcommerce.WishListApiCall {

    private var isStock: Int = 0
    private var sorting: String = "0"
    private var isLoadingData: Boolean = false
    private var isLastPageData: Boolean = false
    private var offSet: Int = 1
    private var isRestart = false
    var filterData: Int = 0
    var valuess = ""
    var check: Int = -1
    private var sortedArray: ArrayList<FetchProductsModel> = ArrayList()
    private lateinit var viewModel: SortFilterViewModel
    private lateinit var viewModel1: ViewModelDescription
    private lateinit var adapterSearchEcommerce: AdapterSortFilterEcommerce
    var wishList: ArrayList<ModelWishList> = ArrayList()
    var model = CatalogModel()
    var noOfProducts = 1
    lateinit var textCartItemCount: TextView
    lateinit var textBasketItemCount: TextView
    lateinit var badgeFrame: FrameLayout
    lateinit var basketbadgeFrame: FrameLayout
    lateinit var linearLayout2: LinearLayout
    lateinit var noDataText: TextView
    var selectedArrayList: ArrayList<ModelChecked> = ArrayList()
    var basketArray: List<BasketModel> = ArrayList()
    var metricOrInch = ""
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catalog_sort_filter)
        Constants.getPrefs(this).edit().remove("selectedFilter").apply()

        viewModel = ViewModelProviders.of(this)[SortFilterViewModel::class.java]
        viewModel1 = ViewModelProviders.of(this)[ViewModelDescription::class.java]
        val manager = LinearLayoutManager(this)
        rv_sortFilter.layoutManager = manager
        noDataText = findViewById(R.id.no_DataText)
        linearLayout2 = findViewById(R.id.linearLayout2)

        if (intent.hasExtra("flashList")) {
            init(model)
            wishList = intent.getParcelableArrayListExtra<ModelWishList>("wishList")!!
            GetProductsInBasketList(this, "0").execute()


        } else {

            model = intent.getParcelableExtra<CatalogModel>("model")!!
            wishList = intent.getParcelableArrayListExtra<ModelWishList>("wishList")!!
            init(model)
            metricOrInch = Constants.getSharedPrefs(this).getString("SelectedE", "1")!!
            if (metricOrInch == "0") {
                metricOrInch = "2"
            }

            GetProductsInBasketList(this, "0").execute()

        }
        viewModel.getDetails().observe(this, androidx.lifecycle.Observer {
            if (selectedArrayList.size > 0) {
                progressBar2.visibility = View.INVISIBLE
            } else {
                progressBar.visibility = View.INVISIBLE
            }
            progressBar2.visibility = View.GONE
            isLoadingData = false

            sortedArray.addAll(it.list)

            val noRepeat: ArrayList<FetchProductsModel> = ArrayList<FetchProductsModel>();

            for (event in sortedArray) {
                var isFound = false
                // check if the event name exists in noRepeat
                for (e in noRepeat) {
                    if (e.sku == event.sku || (e == event)) {
                        isFound = true
                        break
                    }
                }
                if (!isFound) noRepeat.add(event)
            }
            sortedArray.clear()
            sortedArray.addAll(noRepeat)
            isRestart = false
            isLastPageData = it.count == sortedArray.size

            if (sortedArray.size > 0) {
                noDataText.visibility = View.GONE
                sortFilterCl.visibility = View.VISIBLE
                for (i in 0 until wishList.size) {
                    for (j in 0 until sortedArray.size) {
                        if (wishList[i].product_id == sortedArray[j].product_id) {
                            sortedArray[j].wish = true
                        }
                    }
                }

            } else {
                noDataText.visibility = View.VISIBLE
            }

//            when (isStock) {
//                1 -> {
//                    val inOutStock: ArrayList<FetchProductsModel> = ArrayList()
//
//                    for (i in 0 until sortedArray.size) {
//
//                        val saleable = sortedArray[i].saleable.toDouble()
//                        val quantity = sortedArray[i].selectQuant.toDouble()
//                        val stock = sortedArray[i].stock.toDouble()
//
//                        if (saleable * quantity < stock) {
//                            inOutStock.add(sortedArray[i])
//                        }
//                    }
//                    adapterSearchEcommerce.update(inOutStock, basketArray)
//                    Toast.makeText(this, "in Stock" + inOutStock.size.toString(), Toast.LENGTH_SHORT).show()
//
//                }
//                2 -> {
//
//
//                    val inOutStock: ArrayList<FetchProductsModel> = ArrayList()
//
//
//                    for (i in 0 until sortedArray.size) {
//
//                        val saleable = sortedArray[i].saleable.toDouble()
//                        val quantity = sortedArray[i].selectQuant.toDouble()
//                        val stock = sortedArray[i].stock.toDouble()
//
//                        if (saleable * quantity > stock) {
//                            inOutStock.add(sortedArray[i])
//                        }
//                    }
//
//                    adapterSearchEcommerce.update(inOutStock, basketArray)
//                    Toast.makeText(this, "out Stock" + inOutStock.size.toString(), Toast.LENGTH_SHORT).show()
//                }
//            }
            adapterSearchEcommerce.update(sortedArray, basketArray)


        })

        // paging
        rv_sortFilter.addOnScrollListener(object :
            PaginationScrollListener(rv_sortFilter.layoutManager as LinearLayoutManager?) {
            override fun isLastPage(): Boolean {
                return isLastPageData
            }

            override fun isLoading(): Boolean {
                return isLoadingData
            }

            override fun loadMoreItems() {
                isLoadingData = true

                offSet++
                if (intent.hasExtra("flashList")) {
                    progressBar2.visibility = View.VISIBLE
                    val auth = Constants.getPrefs(application).getString(Constants.token, "")
                    viewModel.getFlashDiscountProduct(auth!!, offSet, wishList)
                } else {
                    if (selectedArrayList.size > 0) {
                        progressBar2.visibility = View.VISIBLE
                        viewModel.getFilterData(
                            model.category_id,
                            progressBar2,
                            sortFilterCl,
                            wishList,
                            adapterSearchEcommerce, valuess, offSet.toString(), "Yesclear"
                            , metricOrInch, sorting, basketArray, noDataText
                        )
                    } else {
                        progressBar.visibility = View.VISIBLE
                        viewModel.getSortFilterData(
                            model.category_id,
                            progressBar,
                            sortFilterCl,
                            wishList,
                            sorting,
                            offSet.toString(),
                            "noClear",
                            metricOrInch
                        )
                    }
                }
            }

        })


        sort.setOnClickListener {
            showDialog()
        }
        filterD.setOnClickListener {
            val intent = Intent(this, FilterActivity::class.java)
            intent.putExtra("model", model)
            startActivityForResult(intent, 123)
        }

//        checkStockAvail()
    }


    override fun onResume() {
        super.onResume()


        adapterSearchEcommerce.notifyDataSetChanged()


        val cart = Constants.getPrefs(this).getString("showCart", "no")
        val vv = Constants.getPrefs(this).getString("gotoEnquiry", "no")
        if (cart == "yes" || vv == "yes") {
            finish()
        }

        val exist = Constants.getPrefs(this).getString("likeD", "")
        if (exist!!.isNotEmpty()) {
            Constants.getPrefs(this).edit().remove("likeD").apply()
            viewModel.updateSort(exist, adapterSearchEcommerce, basketArray)

        }
        viewModel1.getCartValuess()
        viewModel1.getCartItems().observe(this, androidx.lifecycle.Observer {

            if (it.size > 0) {
                try {
                    textCartItemCount.visibility = View.VISIBLE
                    textCartItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            } else {
                try {
                    textCartItemCount.visibility = View.GONE
                } catch (e: Exception) {

                }
            }

        })
        viewModel1.getBasketValuess()
        viewModel1.getBasketItems().observe(this, androidx.lifecycle.Observer {

            //            for(h in 0 until sortedArray.size){
//                if (sortedArray[h].product_id==it[i].product_id){
//
//                }
//            }
            GetProductsInBasketList(this, "1").execute()

            if (it.size > 0) {
                try {
                    textBasketItemCount.visibility = View.VISIBLE
                    textBasketItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            } else {
                try {
                    textBasketItemCount.visibility = View.GONE
                } catch (e: Exception) {

                }
            }

        })

    }

    private fun init(model: CatalogModel) {
        setSupportActionBar(sortToolbar.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        if (intent.hasExtra("flashList")) {
            linearLayout2.visibility = View.GONE
            toolbar_title.text = "Flash Sale"
        } else {
            linearLayout2.visibility = View.VISIBLE
            if (model.name.contains("®")) {
                val vv = model.name.split("®")
                toolbar_title.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
            } else {
                sortToolbar.toolbar_title.text = model.name

            }
        }
        rv_sortFilter.layoutManager = LinearLayoutManager(this)
        rv_sortFilter.isNestedScrollingEnabled = false
        adapterSearchEcommerce = AdapterSortFilterEcommerce(this)
        rv_sortFilter.adapter = adapterSearchEcommerce
        rv_sortFilter.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

    }

    override fun addToFav(model: FetchProductsModel, position: Int) {

//        viewModel.updateArray(position, "Fav").observe(this, Observer {
//            adapterSearchEcommerce.update(it)
//        })
    }

    override fun addToCart(modelSearchEcommerce: FetchProductsModel, position: Int) {
        viewModel.updateArray(position, "Cart", adapterSearchEcommerce, basketArray)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.cart) {
            Constants.getPrefs(this).edit().putString("showCart", "yes").apply()
            finish()
        }

        return super.onOptionsItemSelected(item)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 123) {
            sortedArray.clear()
            selectedArrayList = data!!.getParcelableArrayListExtra("selected")!!
            if (selectedArrayList.size > 0) {
                valuess = ""
                var id = ""
                val idArray: ArrayList<String> = ArrayList()
                val jsonArray2 = JSONArray()
                for (i in 0 until selectedArrayList.size) {
                    id = selectedArrayList[i].id
                    var exist = 0
                    if (idArray.size > 0) {
                        for (k in 0 until idArray.size) {
                            if (idArray[k] == id) {
                                exist = 1
                            }
                        }
                    }

                    idArray.add(id)
                    val jsonArray = JSONArray()
                    val jsonObject = JSONObject()
                    if (exist == 0) {
                        for (j in 0 until selectedArrayList.size) {
                            if (id == selectedArrayList[j].id) {

                                jsonArray.put(selectedArrayList[j].value)
                            }
                        }
                        jsonObject.put("id", id)
                        jsonObject.put("value", jsonArray)
                        jsonArray2.put(jsonObject)
                    }

                }

                valuess = jsonArray2.toString()
                filterData = 1
                noOfProducts = 1
                var metricOrInch = Constants.getSharedPrefs(this).getString("SelectedE", "1")
                if (metricOrInch == "0") {
                    metricOrInch = "2"
                }
                viewModel.getFilterData(
                    model.category_id,
                    progressBar2,
                    sortFilterCl,
                    wishList,
                    adapterSearchEcommerce, valuess, noOfProducts.toString(), "Yesclear"
                    , metricOrInch!!, sorting, basketArray, noDataText
                )

            } else {

                var metricOrInch = Constants.getSharedPrefs(this).getString("SelectedE", "1")
                if (metricOrInch == "0") {
                    metricOrInch = "2"
                }
                offSet = 1
                viewModel.getSortFilterData(
                    model.category_id,
                    progressBar2,
                    sortFilterCl,
                    wishList,
                    "0",
                    offSet.toString(),
                    "yesClear",
                    metricOrInch!!
                )
            }
        } else {
            //adapterSearchEcommerce.update(selectedArrayList,basketArray)
            // viewModel.setAdapter(basketArray,adapterSearchEcommerce)
        }
    }

    private fun showDialog() {

        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Sort By")
        val mBinding: FilterSortBinding =
            DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.filter_sort, null, false)

        if (check == 0) {
            mBinding.atoz.isChecked = true
            mBinding.ztoa.isChecked = false
            mBinding.htol.isChecked = false
            mBinding.ltoh.isChecked = false
            sorting = "1"
        }
        if (check == 1) {
            mBinding.atoz.isChecked = false
            mBinding.ztoa.isChecked = true
            mBinding.htol.isChecked = false
            mBinding.ltoh.isChecked = false
            sorting = "2"
        }
        if (check == 2) {
            mBinding.atoz.isChecked = false
            mBinding.ztoa.isChecked = false
            mBinding.htol.isChecked = true
            mBinding.ltoh.isChecked = false
            sorting = "4"
        }
        if (check == 3) {
            mBinding.atoz.isChecked = false
            mBinding.ztoa.isChecked = false
            mBinding.htol.isChecked = false
            mBinding.ltoh.isChecked = true
            sorting = "3"
        }

        builder.setView(mBinding.root)
        builder.setCancelable(false)
        builder.setPositiveButton("Ok")
        { dialog, which ->
            offSet = 1
            sortedArray.clear()
            when {

                mBinding.atoz.isChecked -> {
                    showSortedArray("1")
                    check = 0
                }
                mBinding.ztoa.isChecked -> {
                    showSortedArray("2")
                    check = 1

                }
                mBinding.htol.isChecked -> {
                    showSortedArray("4")
                    check = 2
                }
                mBinding.ltoh.isChecked -> {
                    showSortedArray("3")
                    check = 3
                }
            }
            dialog.dismiss()
        }
        builder.setNegativeButton("Cancel") { dialog, which ->
        }
        builder.create()
        builder.show()
    }

    private fun showSortedArray(sort: String) {
        sorting = sort
        var metricOrInch = Constants.getSharedPrefs(this).getString("SelectedE", "1")
        if (metricOrInch == "0") {
            metricOrInch = "2"
        }
        if (selectedArrayList.size > 0) {

            valuess = ""
            var id = ""
            val idArray: ArrayList<String> = ArrayList()
            val jsonArray2 = JSONArray()
            for (i in 0 until selectedArrayList.size) {
                id = selectedArrayList[i].id
                var exist = 0
                if (idArray.size > 0) {
                    for (k in 0 until idArray.size) {
                        if (idArray[k] == id) {
                            exist = 1
                        }
                    }
                }

                idArray.add(id)
                val jsonArray = JSONArray()
                val jsonObject = JSONObject()
                if (exist == 0) {
                    for (j in 0 until selectedArrayList.size) {
                        if (id == selectedArrayList[j].id) {

                            jsonArray.put(selectedArrayList[j].value)
                        }
                    }
                    jsonObject.put("id", id)
                    jsonObject.put("value", jsonArray)
                    jsonArray2.put(jsonObject)
                }

            }

            valuess = jsonArray2.toString()
            filterData = 1
            noOfProducts = 1
            var metricOrInch = Constants.getSharedPrefs(this).getString("SelectedE", "1")
            if (metricOrInch == "0") {
                metricOrInch = "2"
            }
            viewModel.getFilterData(
                model.category_id,
                progressBar2,
                sortFilterCl,
                wishList,
                adapterSearchEcommerce, valuess, noOfProducts.toString(), "Yesclear"
                , metricOrInch!!, sorting, basketArray, noDataText
            )
        } else {
            viewModel.getSortFilterData(
                model.category_id,
                progressBar2,
                sortFilterCl,
                wishList,
                sort,
                offSet.toString(),
                "yesClear",
                metricOrInch!!
            )
        }

    }


    override fun wishListApi(model: FetchProductsModel, position: Int) {
        viewModel.addToWishList(model, position, adapterSearchEcommerce)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.catalog_cart, menu)

        val menuItem: MenuItem = menu!!.findItem(R.id.cart)
        val menuItem2: MenuItem = menu.findItem(R.id.basket)


        val actionView: View = MenuItemCompat.getActionView(menuItem)
        val actionView2: View = MenuItemCompat.getActionView(menuItem2)
        textCartItemCount = actionView.findViewById(R.id.cart_badge)
        textBasketItemCount = actionView2.findViewById(R.id.basket_badge)
        badgeFrame = actionView.findViewById(R.id.badgeFrame)
        basketbadgeFrame = actionView2.findViewById(R.id.basketbadgeFrame)

        viewModel1.getCartItems().observe(this, androidx.lifecycle.Observer {

            if (it.size > 0) {

                try {
                    textCartItemCount.visibility = View.VISIBLE
                    textCartItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            } else if (it.size.toString() == "0") {
                try {

                    textCartItemCount.visibility = View.GONE
                } catch (e: Exception) {

                }

            }

        })
        badgeFrame.setOnClickListener {
            Constants.getPrefs(this).edit().putString("showCart", "yes").apply()
            finish()
        }
        basketbadgeFrame.setOnClickListener {
            Constants.getPrefs(this).edit().putString("gotoEnquiry", "yes").apply()
            finish()
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun addToBasketLayout(
        model: FetchProductsModel,
        position: Int,
        spinnerDes: String,
        delete: String
    ) {
        // if (Constants.IsStockAvailable(model)) {
        val gson = Gson()
        val innerJson = gson.toJson(model)
        val obj = gson.fromJson(innerJson, BasketModel::class.java)
        CheckIfExistInBasketList(obj, application, spinnerDes, delete).execute()
        viewModel1.getBasketValuess()
//        } else {
//            if (model.quantity.toDouble() < 0) {
//                showCustomAlert("Stock Unavailable")
//            } else {
//                showCustomAlert("Quantity exceed than available stock.")
//            }
//        }
    }

    override fun addToCartLayout(
        model: FetchProductsModel,
        position: Int,
        spinnerDes: String
    ) {
        //  if (Constants.IsStockAvailable(model)) {
        val gson = Gson()
        val innerJson = gson.toJson(model)
        val obj = gson.fromJson(innerJson, CartModel::class.java)
        CheckIfExistInCartList(obj, application, spinnerDes).execute()
        viewModel1.getCartItems().observe(this, androidx.lifecycle.Observer {

            if (it.size > 0) {
                try {
                    textCartItemCount.visibility = View.VISIBLE
                    textCartItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            } else {
                try {
                    textCartItemCount.visibility = View.GONE
                } catch (e: Exception) {

                }
            }
        })
//        } else {
//            if (model.quantity.toDouble() < 0) {
//                showCustomAlert("Stock Unavailable")
//            } else {
//                showCustomAlert("Quantity exceed than available stock.")
//            }
//        }

    }

    @SuppressLint("StaticFieldLeak")
    inner class CheckIfExistInCartList(
        var model: CartModel,
        var context: Context,
        var spinnerDes: String
    ) : AsyncTask<Void, Void, Void>() {
        var exist = 0
        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)
            val cartItems: ArrayList<CartModel> =
                db!!.productDao().getAllCart() as ArrayList<CartModel>

            for (j in 0 until cartItems.size) {
                if (model.product_id == cartItems[j].product_id) {
                    exist = 1
                }
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (exist == 0) {
                model.quantity = spinnerDes

                Constants.AddToCart(application, true, model).execute()
                showCustomAlert("Added to Cart")
            } else {
                showCustomAlert("Already Added in Cart")
            }
            textCartItemCount.visibility = View.VISIBLE
            viewModel.getCartValues(textCartItemCount)
            viewModel.getCartSize()
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class CheckIfExistInBasketList(
        var model: BasketModel,
        var context: Context,
        var spinnerDes: String,
        var delete: String
    ) : AsyncTask<Void, Void, Void>() {
        var exist = 0
        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)
            if (delete == "0") {
                db!!.productDao().deleteBasketTable(model.product_id)


            } else {
                db!!.productDao().insertBasket(model)
                //remove
            }

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (delete == "0") {


                showCustomAlert("Removed from Enquiry")
            } else {
                showCustomAlert("Added to Enquiry")
            }
            GetProductsInBasketList(context, "1").execute()
        }

    }

    @SuppressLint("StaticFieldLeak")
    inner class GetProductsInBasketList(

        var context: Context,
        var value: String
    ) : AsyncTask<Void, Void, Void>() {
        var exist = 0
        var basketArray1: List<BasketModel> = ArrayList()
        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)

            basketArray1 = db!!.productDao().getAllBasketModelArray()

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (intent.hasExtra("flashList")) {
                basketArray = basketArray1
                progressBar2.visibility = View.VISIBLE
                val auth = Constants.getPrefs(application).getString(Constants.token, "")
                viewModel.getFlashDiscountProduct(auth!!, offSet, wishList)
            } else {
                var metricOrInch = Constants.getSharedPrefs(context).getString("SelectedE", "1")
                if (metricOrInch == "0") {
                    metricOrInch = "2"
                }
                basketArray = basketArray1
                if (value == "0") {
                    viewModel.getSortFilterData(
                        model.category_id,
                        progressBar2,
                        sortFilterCl,
                        wishList,
                        "0",
                        offSet.toString(),
                        "YesClear",
                        metricOrInch!!
                    )
                } else {
                    if (sortedArray.size > 0) {
                        adapterSearchEcommerce.update(sortedArray, basketArray)
                    }
                }
            }
        }
    }

    fun showCustomAlert(msg: String) {
        val context = applicationContext
        // Create layout inflator object to inflate toast.xml file
        val inflater = layoutInflater

        // Call toast.xml file for toast layout
        val toast1 = inflater.inflate(R.layout.toast, null)
        val textm: TextView = toast1.findViewById(R.id.text)
        textm.text = msg
        val toast = Toast(context)

        // Set layout to toast
        toast.view = toast1
        toast.setGravity(
            Gravity.BOTTOM,
            0, 0
        )
        toast.duration = Toast.LENGTH_LONG
        toast.show()
    }

    private fun checkStockAvail() {

//        llStock.setOnClickListener {

        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Sort By")
        val mBinding: StockLayoutBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(this),
                R.layout.stock_layout,
                null,
                false
            )


        if (check == 0) {
            mBinding.inStock.isChecked = true
            mBinding.outStock.isChecked = false
            mBinding.allItems.isChecked = false

        }
        if (check == 1) {
            mBinding.inStock.isChecked = false
            mBinding.outStock.isChecked = true
            mBinding.allItems.isChecked = false

        }
        if (check == 2) {
            mBinding.inStock.isChecked = false
            mBinding.outStock.isChecked = false
            mBinding.allItems.isChecked = true

        }

        builder.setView(mBinding.root)
        builder.setCancelable(false)
        builder.setPositiveButton("Ok")
        { dialog, which ->

            when {

                mBinding.inStock.isChecked -> {
                    inStockData()
                    check = 0
                    isStock = 1
                }
                mBinding.outStock.isChecked -> {
                    outStockData()
                    check = 1
                    isStock = 2

                }
                mBinding.allItems.isChecked -> {
                    allProductItems()
                    check = 2
                    isStock = 0
                }
            }
            dialog.dismiss()
        }
        builder.setNegativeButton("Cancel") { dialog, which ->
        }
        builder.create()
        builder.show()

//        }

    }

    private fun allProductItems() {

        adapterSearchEcommerce.update(sortedArray, basketArray)
        Toast.makeText(this, "available" + sortedArray.size.toString(), Toast.LENGTH_SHORT).show()


    }

    private fun outStockData() {
        val inOutStock: ArrayList<FetchProductsModel> = ArrayList()


        for (i in 0 until sortedArray.size) {

            val saleable = sortedArray[i].saleable.toDouble()
            val quantity = sortedArray[i].selectQuant.toDouble()
            val stock = sortedArray[i].stock.toDouble()

            if (saleable * quantity > stock) {
                inOutStock.add(sortedArray[i])
            }
        }

        adapterSearchEcommerce.update(inOutStock, basketArray)
        Toast.makeText(this, "out Stock" + inOutStock.size.toString(), Toast.LENGTH_SHORT).show()
    }

    private fun inStockData() {
        val inOutStock: ArrayList<FetchProductsModel> = ArrayList()

        for (i in 0 until sortedArray.size) {

            val saleable = sortedArray[i].saleable.toDouble()
            val quantity = sortedArray[i].selectQuant.toDouble()
            val stock = sortedArray[i].stock.toDouble()

            if (saleable * quantity < stock) {
                inOutStock.add(sortedArray[i])
            }
        }
        adapterSearchEcommerce.update(inOutStock, basketArray)
        Toast.makeText(this, "in Stock" + inOutStock.size.toString(), Toast.LENGTH_SHORT).show()
    }

}


