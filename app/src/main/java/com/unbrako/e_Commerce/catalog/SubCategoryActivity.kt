package com.unbrako.e_Commerce.catalog

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Html
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuItemCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivitySubCategoryBinding
import com.unbrako.e_Commerce.catalog.productDetails.ViewModelDescription
import kotlinx.android.synthetic.main.e_catalog_product.view.*

class SubCategoryActivity : AppCompatActivity() {

    var items2: ArrayList<CatalogModel> = ArrayList()
    var model: CatalogModel = CatalogModel()
    private lateinit var manager: androidx.recyclerview.widget.GridLayoutManager
    lateinit var mAdapter: SubCatAdapter
    var wishList: ArrayList<ModelWishList> = ArrayList()
    lateinit var mBinding: ActivitySubCategoryBinding
    lateinit var textCartItemCount: TextView
    lateinit var textBasketItemCount: TextView
    lateinit var badgeFrame: FrameLayout
    lateinit var basketbadgeFrame: FrameLayout
    private lateinit var viewModel: ViewModelDescription


    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_sub_category)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sub_category)
        viewModel = ViewModelProviders.of(this)[ViewModelDescription::class.java]
        setSupportActionBar(mBinding.dashToolbar.MainToolbar)
        title = ""

        supportActionBar!!.elevation = 0f
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        items2 = intent.getParcelableArrayListExtra("SubCat")!!
        wishList = intent.getParcelableArrayListExtra("wishList")!!
        model = intent.getParcelableExtra("model")!!

        if (model.name.contains("®")) {
            var vv = model.name.split("®")
            mBinding.dashToolbar.title.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
        } else {
            mBinding.dashToolbar.title.text = model.name

        }


        manager = androidx.recyclerview.widget.GridLayoutManager(
            this,
            2,
            androidx.recyclerview.widget.GridLayoutManager.VERTICAL,
            false
        )
        mBinding.recyclerView.layoutManager = manager
        mBinding.recyclerView.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
        mAdapter = SubCatAdapter(this, items2, wishList)
        mBinding.recyclerView.adapter = mAdapter

        viewModel.getCartItems().observe(this, Observer {
            if (it.size > 0) {
                try {
                    textCartItemCount.visibility=View.VISIBLE;
                    textCartItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            }else{
                try {
                textCartItemCount.visibility=View.GONE
                } catch (e: Exception) {

                }
            }
        })

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.cart) {
            Constants.getPrefs(this).edit().putString("showCart", "yes").apply()
            finish()
        }
        return super.onOptionsItemSelected(item)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.catalog_cart, menu)

        val menuItem: MenuItem = menu!!.findItem(R.id.cart)
        val menuItem2: MenuItem = menu!!.findItem(R.id.basket)


        val actionView: View = MenuItemCompat.getActionView(menuItem)
        val actionView2: View = MenuItemCompat.getActionView(menuItem2)
        textCartItemCount = actionView.findViewById(R.id.cart_badge)
        textBasketItemCount = actionView2.findViewById(R.id.basket_badge)
        badgeFrame = actionView.findViewById(R.id.badgeFrame)
        basketbadgeFrame = actionView2.findViewById(R.id.basketbadgeFrame)
        viewModel.getCartItems().observe(this, Observer {
            if (it.size > 0) {
                try {
                    textCartItemCount.visibility = View.VISIBLE
                    textCartItemCount.text = it.size.toString()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                try {

                if (it.size.toString() == "0") {
                    textCartItemCount.visibility = View.GONE
                }
            } catch (e: Exception) {

        }
            }
        })
        badgeFrame.setOnClickListener {
            Constants.getPrefs(this).edit().putString("showCart", "yes").apply()
            finish()
        }
        basketbadgeFrame.setOnClickListener {
            Constants.getPrefs(this).edit().putString("gotoEnquiry", "yes").apply()
            finish()
        }
        return super.onCreateOptionsMenu(menu)
    }


    override fun onResume() {
        super.onResume()
        val cart = Constants.getPrefs(this).getString("showCart", "no")
        val basket = Constants.getPrefs(this).getString("gotoEnquiry", "no")
        if (cart == "yes"||basket == "yes") {
            finish()
        }
        viewModel.getCartValuess()
        viewModel.getBasketValuess()
        viewModel.getBasketItems().observe(this, androidx.lifecycle.Observer {

            if (it.size > 0) {
                try {
                    textBasketItemCount.visibility = View.VISIBLE
                    textBasketItemCount.text = it.size.toString()
                } catch (e: Exception) {

                }
            } else {
                try {
                    textBasketItemCount.visibility = View.GONE
                } catch (e: Exception) {

                }
            }

        })
    }


}
