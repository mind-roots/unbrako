package com.unbrako.e_Commerce.catalog.filter

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class FiltersViewModel(application: Application) : AndroidViewModel(application) {

    var mRepoFilter: FilterRepository = FilterRepository(application)

    fun getData(): MutableLiveData<ArrayList<ModelFilters>> {
        return mRepoFilter.getFilterData()
    }

    fun getProducts(
        progressBar: ProgressBar,
        value: String,
        category_id: String,
        metricOrInch: String
    ) {

        mRepoFilter.getProducts(progressBar,value,category_id, metricOrInch)
    }
}