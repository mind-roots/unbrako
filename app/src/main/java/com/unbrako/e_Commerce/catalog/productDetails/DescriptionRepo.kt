package com.unbrako.e_Commerce.catalog.productDetails

import android.app.Application
import android.os.AsyncTask
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class DescriptionRepo(var application: Application) {


    var mData = MutableLiveData<DescModel>()
    var mDataAttr = MutableLiveData<ArrayList<ModelAttributes>>()

    fun getItems(catId: String, progressBarDesc: RelativeLayout, tvDesc: TextView) {

        getDescriptionDetail(catId, progressBarDesc, tvDesc)

    }

    private fun getDescriptionDetail(catId: String, progressBarDesc: RelativeLayout, tvDesc: TextView) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBarDesc.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.details(auth, catId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBarDesc.visibility = View.GONE

                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val jsonObject = JSONObject(res)
                        val success = jsonObject.optString("success")
                        val descModel = DescModel()
                        val array: ArrayList<ModelAttributes> = ArrayList()
                        if (success == "true") {

                            val products = jsonObject.getJSONObject("products")

                            descModel.product_id = products.optString("product_id")
                            descModel.name = products.optString("name")
                            descModel.sku = products.optString("sku")
                            descModel.quantity = products.optString("stock")
                            descModel.image = products.optString("image")
                            descModel.price = products.optString("price")
                            descModel.rating = products.optString("rating")
                            descModel.status = products.optString("status")
                            descModel.tax_class_id = products.optString("tax_class_id")
                            descModel.saleable = products.optString("saleable")
                            descModel.description = products.optString("description")
                            descModel.uom = products.optString("uom")
                            descModel.selectQuant = "1"

                            val attributes = products.optJSONArray("attribute")
                            for (i in 0 until attributes.length()) {
                                val jj = attributes.optJSONObject(i)
                                val mode = ModelAttributes()
                                mode.name = jj.optString("name")
                                mode.text = jj.optString("text")
                                array.add(mode)
                            }
                        }else{
                             val error = jsonObject.optString("error")
                        if(error == "Invalid Auth."){

                        }
                        }
                        mDataAttr.value = array
                        mData.value = descModel


                    } catch (e: Exception) {

                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                progressBarDesc.visibility = View.VISIBLE

            }

        })
    }

    fun getData(): LiveData<DescModel> {
        return mData
    }

    fun addWishData(model: FetchProductsModel, type: String, ivFav: ImageView) {
        val authCode = Constants.getPrefs(application).getString(Constants.token, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.addWishList(authCode!!, model.product_id, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optString("success")

                        if (success == "true") {
                            if (type == "2") {
                                ivFav.setImageResource(R.drawable.ic_favorite_not)
                                Constants.getPrefs(application).edit().putString("likeD", "no").apply()
                            } else {
                                Constants.getPrefs(application).edit().putString("likeD", "yes").apply()
                                ivFav.setImageResource(R.drawable.ic_favorite)
                            }

                        } else {

                        }

                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

            }
        })
    }

    fun getAttr(): MutableLiveData<ArrayList<ModelAttributes>> {

        return mDataAttr
    }

    var array2 = ArrayList<CartModel>()
    var mData2 = MutableLiveData<ArrayList<CartModel>>()
    fun getCartValues() {

        getProductsOfCart().execute()
        mData2.value=array2

    }

    fun getCartSize(): MutableLiveData<ArrayList<CartModel>> {

        return mData2
    }
    var array3 = ArrayList<BasketModel>()
    var mData3 = MutableLiveData<ArrayList<BasketModel>>()
    fun getBasketVales() {

        getProductsOfBasket().execute()
        mData3.value=array3
    }

    fun getBasketSize(): MutableLiveData<ArrayList<BasketModel>> {
return mData3
    }

    inner class getProductsOfCart : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(application)

            array2 =  db!!.productDao().getAllCart() as ArrayList<CartModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mData2.value = array2
        }
    }
inner class getProductsOfBasket : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(application)

            array3 =  db!!.productDao().getAllBasketModelArray() as ArrayList<BasketModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mData3.value = array3
        }
    }
}