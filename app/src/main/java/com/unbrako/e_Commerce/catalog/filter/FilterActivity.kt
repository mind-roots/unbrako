package com.unbrako.e_Commerce.catalog.filter

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityFilterBinding
import com.unbrako.e_Commerce.catalog.CatalogModel
import org.json.JSONObject
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class FilterActivity : AppCompatActivity(), FilterEventListener {


    private lateinit var mBinding: ActivityFilterBinding
    private lateinit var mViewModelFilter: ViewModelFilter
    private lateinit var mViewModelFilter1: FiltersViewModel
    private lateinit var mAdapterTitles: AdapterFilterOneEcommerce
    private lateinit var mAdapterValues: AdapterFilterTwoEcommerce
    var id = "0"
    var model: CatalogModel = CatalogModel()
    var selectedArrayList: ArrayList<ModelChecked> = ArrayList()
    var selectedArrayListTemp: ArrayList<ModelChecked> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_filter)
        mViewModelFilter = ViewModelProviders.of(this)[ViewModelFilter::class.java]
        mViewModelFilter1 = ViewModelProviders.of(this)[FiltersViewModel::class.java]
        mBinding.listener = this
        model = intent.getParcelableExtra<CatalogModel>("model")!!
        init(model)
        setLayout()
        if (Constants.getPrefs(this).getString("selectedFilter", "")!!.length > 1) {
            val gson = Gson()
            selectedArrayList = gson.fromJson(
                Constants.getPrefs(this).getString("selectedFilter", ""),
                object : TypeToken<List<ModelChecked>>() {

                }.type
            )
           // selectedArrayListTemp.addAll(selectedArrayList)
        }
        val value = Constants.getPrefs(this).getString(Constants.token, "defaultValue")

        var metricOrInch = Constants.getSharedPrefs(this).getString("SelectedE", "1")
        if (metricOrInch == "0") {
            metricOrInch = "2"
        }

        mViewModelFilter1.getProducts(mBinding.progressBar, value!!, model.category_id, metricOrInch!!)
        this.mViewModelFilter1.getData().observe(this, Observer {
            if (it.size > 0) {
                selectFilter(it[0], 0)
                mAdapterTitles.update(it as ArrayList<ModelFilters>, "yes")
                mAdapterValues.update(getValues(it[0], selectedArrayList))
            }
        })
        if (selectedArrayList.size>0){
            mBinding.apply.setTextColor(Color.parseColor("#F37836"))
        }else{
            mBinding.apply.setTextColor(Color.parseColor("#464646"))
        }
    }

    private fun init(model: CatalogModel) {
        setSupportActionBar(mBinding.toolbar4)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        title = ""
    }

    private fun setLayout() {
        // Adapter for titles
        mBinding.filterTitles.layoutManager = LinearLayoutManager(this)
        mBinding.filterTitles.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        mAdapterTitles = AdapterFilterOneEcommerce(this)
        mBinding.filterTitles.adapter = mAdapterTitles

        // Adapter for values
        mBinding.filterValues.layoutManager = LinearLayoutManager(this)
        mAdapterValues = AdapterFilterTwoEcommerce(this)
        mBinding.filterValues.adapter = mAdapterValues
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.filter_clear, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when {
            item!!.itemId == android.R.id.home -> finish()
            item.itemId == R.id.clear -> {

                selectedArrayListTemp.addAll(selectedArrayList)
                selectedArrayList.clear()
                mBinding.apply.setTextColor(Color.parseColor("#464646"))
                val value = Constants.getPrefs(this).getString(Constants.token, "defaultValue")

                var metricOrInch = Constants.getSharedPrefs(this).getString("SelectedE", "1")

                if (metricOrInch == "0") {
                    metricOrInch = "2"
                }

                mViewModelFilter1.getProducts(mBinding.progressBar, value!!, model.category_id, metricOrInch!!)
                mViewModelFilter1.getData().observe(this, Observer {
                    if (it.size > 0) {
                        selectFilter(it[0], 0)
                        mAdapterTitles.update(it as ArrayList<ModelFilters>, "no")
                        mAdapterValues.update(getValues(it[0], selectedArrayList))
                    }
                })
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // click on Filter titles on Left side
    override fun selectFilter(model: ModelFilters, position: Int) {
        val obj = JSONObject(model.value)
        id = obj.optString("id")
        mAdapterTitles.selection(position, model)
        mAdapterValues.update(getValues(model, selectedArrayList))
    }

    override fun checkedFilter(model: ModelValues, position: Int) {
        val mm = ModelChecked()
        mm.id = id
        mm.value = model.value
        if (!model.isChecked) {

            selectedArrayList.add(mm)
        } else {

            selectedArrayList.removeAt(0)
        }

        if (selectedArrayList.size > 0) {
            mBinding.apply.setTextColor(Color.parseColor("#F37836"))
        } else {
            mBinding.apply.setTextColor(resources.getColor(R.color.grey))
        }
      mAdapterValues.selection( position)
        val newArray:ArrayList<ModelValues> = AdapterFilterTwoEcommerce.getList(mAdapterValues)
        val newSelectedArray=selectedArrayList
        for (i in 0 until newArray.size){
            for(j in 0 until selectedArrayList.size-1) {
                if (!newArray[i].isChecked &&selectedArrayList[j].value==newArray[i].value){
                    newSelectedArray.removeAt(j)
                }

            }
        }

        selectedArrayList=newSelectedArray
    }


    override fun onApplyFilter() {
        //Toast.makeText(this, "Applied", Toast.LENGTH_LONG).show()

       // if (selectedArrayList.size > 0) {

            val prefsEditor = Constants.getPrefs(this).edit()
            val gson = Gson()
            val json = gson.toJson(selectedArrayList)
            prefsEditor.putString("selectedFilter", json)
            prefsEditor.apply()

            val intent = Intent()
            intent.putExtra("selected", selectedArrayList)
            setResult(123, intent)
            finish()
//        } else {
//            finish()
//        }
    }

    override fun onCancelFilter() {
        //Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
//        if (selectedArrayListTemp.size>0){
//            val prefsEditor = Constants.getPrefs(this).edit()
//            val gson = Gson()
//            val json = gson.toJson(selectedArrayListTemp)
//            prefsEditor.putString("selectedFilter", json)
//            prefsEditor.apply()
//
//            val intent = Intent()
//            intent.putExtra("selected", selectedArrayListTemp)
//            setResult(123, intent)
//            finish()
//        }else{
            finish()
//        }


    }


    private fun getValues(
        model: ModelFilters,
        selectedArrayList: ArrayList<ModelChecked>
    ): ArrayList<ModelValues> {

        val obj = JSONObject(model.value)
        val jsArr = obj.optJSONArray("values")
        val mResults = ArrayList<ModelValues>()

        for (i in 0 until jsArr.length()) {
            val value = ModelValues()
            value.value = jsArr[i] as String
            for (j in 0 until selectedArrayList.size) {

                if (selectedArrayList[j].id == id) {
                    if (selectedArrayList[j].value == value.value) {
                        value.isChecked = true
                    }
                }

            }
            mResults.add(value)
        }
        return mResults
    }
}
