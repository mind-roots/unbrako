package com.unbrako.e_Commerce.catalog.productDetails

import android.app.Application
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel

/**
 * Created by Manisha Thakur on 22/4/19.
 */
class ViewModelDescription(application: Application) : AndroidViewModel(application) {

    var refVar: DescriptionRepo = DescriptionRepo(application)

    fun getDescriptionLive(): LiveData<DescModel> {

        return refVar.getData()

    }


    fun getProductDEscription(
        catId: String,
        progressBarDesc: RelativeLayout,
        tvDesc: TextView
    ) {

        refVar.getItems(catId, progressBarDesc, tvDesc)

    }

    fun addWishList(model: FetchProductsModel, type: String, ivFav: ImageView) {
        refVar.addWishData(model, type, ivFav)
    }

    fun getAttributes(): MutableLiveData<ArrayList<ModelAttributes>> {

        return refVar.getAttr()
    }

    fun getCartValuess() {

        refVar.getCartValues()
    }

    fun getCartItems(): MutableLiveData<ArrayList<CartModel>> {

        return refVar.getCartSize()
    }

    fun getBasketValuess() {

        return refVar.getBasketVales()
    }

    fun getBasketItems(): MutableLiveData<ArrayList<BasketModel>> {

        return refVar.getBasketSize()
    }


}