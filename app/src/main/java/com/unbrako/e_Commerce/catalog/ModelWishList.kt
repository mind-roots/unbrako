package com.unbrako.e_Commerce.catalog

import android.os.Parcel
import android.os.Parcelable

class ModelWishList() : Parcelable{
    var stock = ""
    var product_id = ""
    var sku = ""
    var quantity = ""
    var price = ""
    var statusWishlist = ""
    var nameWishlist = ""
    var description = ""
    var imageWishlist = ""
    var rating = ""
    var sort_order = ""
    var saleable = ""
    var category_id = ""
    var display_price = ""
    var tax_class_id = ""
    var selectQuant = ""
    var uom = ""
    var flash_discount = ""
    var isexempted = ""
    var isChecked : Boolean = false

    constructor(parcel: Parcel) : this() {
        stock = parcel.readString()!!
        product_id = parcel.readString()!!
        sku = parcel.readString()!!
        quantity = parcel.readString()!!
        price = parcel.readString()!!
        statusWishlist = parcel.readString()!!
        nameWishlist = parcel.readString()!!
        description = parcel.readString()!!
        imageWishlist = parcel.readString()!!
        rating = parcel.readString()!!
        sort_order = parcel.readString()!!
        saleable = parcel.readString()!!
        category_id = parcel.readString()!!
        display_price = parcel.readString()!!
        tax_class_id = parcel.readString()!!
        selectQuant = parcel.readString()!!
        uom = parcel.readString()!!
        flash_discount = parcel.readString()!!
        isexempted = parcel.readString()!!
        isChecked = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(stock)
        parcel.writeString(product_id)
        parcel.writeString(sku)
        parcel.writeString(quantity)
        parcel.writeString(price)
        parcel.writeString(statusWishlist)
        parcel.writeString(nameWishlist)
        parcel.writeString(description)
        parcel.writeString(imageWishlist)
        parcel.writeString(rating)
        parcel.writeString(sort_order)
        parcel.writeString(saleable)
        parcel.writeString(category_id)
        parcel.writeString(display_price)
        parcel.writeString(tax_class_id)
        parcel.writeString(selectQuant)
        parcel.writeString(uom)
        parcel.writeString(flash_discount)
        parcel.writeString(isexempted)
        parcel.writeByte(if (isChecked) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelWishList> {
        override fun createFromParcel(parcel: Parcel): ModelWishList {
            return ModelWishList(parcel)
        }

        override fun newArray(size: Int): Array<ModelWishList?> {
            return arrayOfNulls(size)
        }
    }

}