package com.unbrako.e_Commerce.catalog

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.unbrako.R
import com.unbrako.databinding.ECatalogProductBinding
import kotlinx.android.synthetic.main.e_catalog_product.view.*
import kotlinx.android.synthetic.main.e_catalog_product.view.imageview_card
import kotlinx.android.synthetic.main.e_catalog_product.view.textview_card_title


/**
 * Created by Manisha Thakur on 4/4/19.
 */
class AdapterMainCatalog(var mContext: Context, catalogECommerceFragment: CatalogECommerceFragment) :
    RecyclerView.Adapter<AdapterMainCatalog.ViewHolder>() {

    lateinit var mBinding: ECatalogProductBinding
    private var list: ArrayList<CatalogModel> = ArrayList(

    )
    var wishList: ArrayList<ModelWishList> = ArrayList()
    var sub = catalogECommerceFragment as checkIfsubCategory
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.e_catalog_product, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = list[position]
        // mBinding.pimage = model

        if (model.name.contains("®")) {
            val vv = model.name.split("®")
            holder.itemView.textview_card_title.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
        } else {
            holder.itemView.textview_card_title.text = model.name

        }

        if (list[position].image.isEmpty()) {
            holder.itemView.imageView1.setImageResource(R.mipmap.login_logo)
               holder.itemView.cardView2.setCardBackgroundColor(mContext.getColor(R.color.colorAccent))
             holder.itemView.imageview_card.visibility = View.GONE
           //  holder.itemView.cardView2.visibility = View.VISIBLE
             holder.itemView.cardView2.visibility = View.VISIBLE

        } else {
            holder.itemView.cardView2.visibility = View.GONE
           // holder.itemView.imageView1.visibility = View.GONE

            holder.itemView.imageview_card.visibility = View.VISIBLE
            Picasso.get().load(list[position].image).into(holder.itemView.imageview_card)
        }
        holder.itemView.mainLay.setOnClickListener {
            sub.checkingSubCategory(model)

        }
    }


    fun update(
        items: ArrayList<CatalogModel>,
        wishList: ArrayList<ModelWishList>
    ) {

        this.list = items
        this.wishList = wishList
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface checkIfsubCategory {
        fun checkingSubCategory(model: CatalogModel)
    }
}