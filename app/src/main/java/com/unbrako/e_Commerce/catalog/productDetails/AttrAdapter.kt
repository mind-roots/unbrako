package com.unbrako.e_Commerce.catalog.productDetails

import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import kotlinx.android.synthetic.main.attrlay.view.*

class AttrAdapter(
    var context: ProductDetailActivity,
    var list: ArrayList<ModelAttributes>
) : RecyclerView.Adapter<AttrAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.attrlay, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position%2==0){
            holder.itemView.name.setBackgroundColor(Color.parseColor("#FFFFFF"))
            holder.itemView.text.setBackgroundColor(Color.parseColor("#FFFFFF"))
        }else{
            holder.itemView.name.setBackgroundColor(Color.parseColor("#F5F5F5"))
            holder.itemView.text.setBackgroundColor(Color.parseColor("#F5F5F5"))
        }
        holder.itemView.name.text = list[position].name
        holder.itemView.text.text = list[position].text

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}