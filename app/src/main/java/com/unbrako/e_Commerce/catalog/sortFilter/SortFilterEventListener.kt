package com.unbrako.e_Commerce.catalog.sortFilter

/**
 * Created by Manisha Thakur on 29/4/19.
 */
interface SortFilterEventListener {
    fun onSort()
    fun onFilter()
}