package com.unbrako.e_Commerce.catalog.filter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.databinding.FilterRecyclerTwoBinding
import kotlinx.android.synthetic.main.filter_recycler_two.view.*

class AdapterFilterTwoEcommerce(val mContext: Context) :
    RecyclerView.Adapter<AdapterFilterTwoEcommerce.MyViewHolder>() {
    var list = ArrayList<ModelValues>()
    var selectFilter = mContext as FilterEventListener

    lateinit var refVar: FilterRecyclerTwoBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        refVar = FilterRecyclerTwoBinding.inflate(LayoutInflater.from(mContext), parent, false)
        return MyViewHolder(refVar)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.bind(model)
        holder.itemView.tv_checked.text = model.value
        if (model.isChecked) {
            holder.itemView.ivCheck.setImageResource(R.drawable.ic_check_)

        } else {
            holder.itemView.ivCheck.setImageResource(R.drawable.ic_uncheck)

        }
        holder.itemView.setOnClickListener {
            selectFilter.checkedFilter(model,  position)
        }
    }

    fun update(valueArray: ArrayList<ModelValues>) {
        list = valueArray
        notifyDataSetChanged()
    }

    fun selection(
        position: Int
    ) {

        for (i in 0 until list.size){
            if (i==position){
                list[i].isChecked = !list[i].isChecked
//            }else{
//
//                list[i].isChecked = false
            }
        }
        notifyDataSetChanged()

    }

    class MyViewHolder(var binding: FilterRecyclerTwoBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: ModelValues) {
            binding.value = obj
            binding.executePendingBindings()
        }

    }

    companion object {
        fun getList(adapterFilterTwoEcommerce: AdapterFilterTwoEcommerce) : ArrayList<ModelValues> {
            return adapterFilterTwoEcommerce.list
        }
    }


}