package com.unbrako.e_Commerce.catalog.productDetails

/**
 * Created by Manisha Thakur on 22/4/19.
 */
class DescModel {
    var product_id: String = ""
    var name: String = ""
    var description: String = ""
    var sku: String = ""
    var quantity: String = ""
    var image: String = ""
    var price: String = ""
    var rating: String = ""
    var status: String = ""
    var tax_class_id: String = ""
    var saleable: String = ""
    var selectQuant: String = ""
    var uom: String = ""
}