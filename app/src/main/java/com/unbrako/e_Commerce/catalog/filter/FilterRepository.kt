package com.unbrako.e_Commerce.catalog.filter

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class FilterRepository(var application: Application) {

    val mData = MutableLiveData<ArrayList<ModelFilters>>()
    var arrayFilter = ArrayList<ModelFilters>()
    var mDataFilter = MutableLiveData<ArrayList<ModelFilters>>()

    fun getFilterData(): MutableLiveData<ArrayList<ModelFilters>> {
        return mData

    }

    fun getProducts(
        progressBar: ProgressBar,
        authcode: String,
        filter_category_id: String,
        metricOrInch: String
    ) {
        progressBar.visibility= View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service
            .filtersData(authcode, filter_category_id, metricOrInch)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility= View.GONE

            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility= View.GONE
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optString("success")
                        if (success == "true") {
                            arrayFilter.clear()
                            val filterObject = json.getJSONObject("filters")
                            val it = filterObject.keys()

                            while (it.hasNext()) {
                                val key = it.next() as String
                                val value = filterObject.getString(key)
                                val modelFilters = ModelFilters()
                                modelFilters.name = key
                                modelFilters.value = value.toString()
                                arrayFilter.add(modelFilters)
                            }
                            mDataFilter.value = arrayFilter
                            mData.value = arrayFilter

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }


            }

        })
    }
}