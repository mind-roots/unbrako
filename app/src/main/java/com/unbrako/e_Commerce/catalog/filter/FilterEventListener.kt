package com.unbrako.e_Commerce.catalog.filter

interface FilterEventListener {

    fun selectFilter(model: ModelFilters, position : Int)
    fun checkedFilter(model: ModelValues, position: Int)
    fun onApplyFilter()
    fun onCancelFilter()
}