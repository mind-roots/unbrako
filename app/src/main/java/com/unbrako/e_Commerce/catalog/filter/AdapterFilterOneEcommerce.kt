package com.unbrako.e_Commerce.catalog.filter

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.databinding.FilterRecyclerOneBinding

class AdapterFilterOneEcommerce
    (val mContext: Context) :
    RecyclerView.Adapter<AdapterFilterOneEcommerce.MyViewHolder>() {
    var selectFilter = mContext as FilterEventListener

    lateinit var refVar: FilterRecyclerOneBinding
    var list: ArrayList<ModelFilters> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        refVar = FilterRecyclerOneBinding.inflate(LayoutInflater.from(mContext), parent, false)
        return MyViewHolder(refVar)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val modelSearchEcommerce = list[position]

        holder.bind(modelSearchEcommerce)

        if (modelSearchEcommerce.background) {
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"))
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#F5F5F5"))
        }
        holder.itemView.setOnClickListener {
            selectFilter.selectFilter(modelSearchEcommerce, position)
        }

    }

    fun update(it: ArrayList<ModelFilters>, s: String) {
        this.list = it
        for(i in 0 until it.size){
            if (i==0){
                list[0].background=true
            }else{
                list[i].background=false
            }
        }


        notifyDataSetChanged()
    }

    fun selection(position: Int, model: ModelFilters) {
        for (i in 0 until list.size) {
            list[i].background = i == position
        }
        notifyDataSetChanged()

    }

    class MyViewHolder(var binding: FilterRecyclerOneBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: ModelFilters) {
            binding.filter = obj
            binding.executePendingBindings()
        }

    }
}