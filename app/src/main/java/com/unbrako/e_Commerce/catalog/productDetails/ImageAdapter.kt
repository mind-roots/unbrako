package com.unbrako.e_Commerce.catalog.productDetails

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso
import com.unbrako.R

/**
 * Created by Manisha Thakur on 22/4/19.
 */
@BindingAdapter("productImageA")
fun getProductImage(imageView: ImageView, string: String?){


    if (string != null) {
        if (!string.isEmpty()) {
            Picasso.get()
                .load(R.mipmap.ic_launcher)
                .into(imageView)
        }
    }

}