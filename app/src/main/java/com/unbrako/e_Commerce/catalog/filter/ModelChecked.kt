package com.unbrako.e_Commerce.catalog.filter

import android.os.Parcel
import android.os.Parcelable

class ModelChecked() :Parcelable {
    lateinit var id:String
    lateinit var value:String

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()!!
        value = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelChecked> {
        override fun createFromParcel(parcel: Parcel): ModelChecked {
            return ModelChecked(parcel)
        }

        override fun newArray(size: Int): Array<ModelChecked?> {
            return arrayOfNulls(size)
        }
    }
}