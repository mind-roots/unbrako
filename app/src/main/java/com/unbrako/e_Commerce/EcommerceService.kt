package com.unbrako.e_Commerce

import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by Manisha Thakur on 10/4/19.
 */
interface EcommerceService {


    ///////////////////////////////////////////catalog////////////////////////////////////////////////////////////

    @FormUrlEncoded
    @POST("?route=mobileapi/catalog")
    fun getAllService(
        @Field("ip_address") catalog: String,
        @Field("auth_code") auth_code: String?
    )
            : Call<ResponseBody>


///////////////////////////////////////////Fetch Products////////////////////////////////////////////////////////////

    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/fetch_products")
    fun fetchPorducts(
        @Field("auth_code") catalog: String,
        @Field("category_id") category_id: String,
        @Field("sort_by") sort: String,
        @Field("page") page: String,
        @Field("type") type: String
    )
            : Call<ResponseBody>


    ///////////////////////////////////////////product description////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/product_details")
    fun details(
        @Field("auth_code") auth_code: String?,
        @Field("product_id") product_id: String
    )
            : Call<ResponseBody>


///////////////////////////////////////////Logout////////////////////////////////////////////////////////////

    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/logout")
    fun logout(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


    ///////////////////////////////////////////Search////////////////////////////////////////////////////////////

    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/search")
    fun search(
        @Field("auth_code") auth_code: String,
        @Field("product_name") product_name: String
    ): Call<ResponseBody>


    ///////////////////////////////////////////add wishList////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/addWishlist")
    fun addWishList(
        @Field("auth_code") auth_code: String,
        @Field("product_id") product_id: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    ///////////////////////////////////////////get payment detail////////////////////////////////////////////////////////////
//    @Headers(
//        "X-Api-Key: 86524a9d14d203c840b7c044d790d989",
//        "X-Auth-Token: 8090595ff536a780a4cc0306bf2acb23"
//    )
    @Headers(
        "accept:application/json",
        "X-Api-Key: 86524a9d14d203c840b7c044d790d989",
        "X-Auth-Token: 8090595ff536a780a4cc0306bf2acb23"
    )
    @GET("payment-requests/{id}")
    fun getPayment(@Path("id") userPaymentId: String): Call<JsonObject>


    ///////////////////////////////////////////get wishList////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getWishlist")
    fun getWishList(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    ///////////////////////////////////////////get profile////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getProfile")
    fun getProfile(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    ///////////////////////////////////////////update profile////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/updateProfile")
    fun updateProfile(
        @Field("auth_code") auth_code: String,
        @Field("company_name") company_name: String,
        @Field("gst") gst: String,
        @Field("firstname") firstname: String,
        @Field("lastname") lastname: String,
        @Field("email") email: String,
        @Field("telephone") telephone: String,
        @Field("password") password: String,
        @Field("landline") landline: String
    ): Call<ResponseBody>

    ///////////////////////////////////////////order history////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/MyOrders")
    fun orderHistory(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    ///////////////////////////////////////////forgot Password////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/forgotPassword")
    fun forgotPassword(
        @Field("email") email: String
    ): Call<ResponseBody>

    ///////////////////////////////////////////change Password////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/changePassword")
    fun changePassword(
        @Field("auth_code") auth_code: String,
        @Field("old_password") old_password: String,
        @Field("new_password") new_password: String
    ): Call<ResponseBody>


//    @Headers("X-Api-Key: f246820b2daf20e6bb6a1aec341a9414",
//        "X-Auth-Token: df432af2632933bdd68073d7511cff68")

    @Headers(
        "X-Api-Key: 86524a9d14d203c840b7c044d790d989",
        "X-Auth-Token: 8090595ff536a780a4cc0306bf2acb23"
    )
    @POST("")
    fun paymentRequest(
        @Query("amount") amount: String,
        @Query("purpose") purpose: String,
        @Query("buyer_name") buyer_name: String,
        @Query("email") email: String,
        @Query("phone") phone: String,
        @Query("allow_repeated_payments") allow_repeated_payments: String,
        @Query("send_email") send_email: String,
        @Query("send_sms") send_sms: String,
        @Query("redirect_url") redirect_url: String,
        @Query("webhook") webhook: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getFilters")
    fun filtersData(
        @Field("auth_code") auth_code: String,
        @Field("filter_category_id") filter_category_id: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/filters")
    fun fetchFilterPorducts(
        @Field("auth_code") catalog: String,
        @Field("filter_category_id") category_id: String,
        @Field("page") page: String,
        @Field("filter") filter: String,
        @Field("type") type: String,
        @Field("sort_by") sorting: String
    )
            : Call<ResponseBody>


    //abstract fun fetchFilterPorducts(auth: String?, category_id: String, valuess: String): Call<ResponseBody>

    //abstract fun filtersData(authcode: String, filter_category_id: String): Call<ResponseBody>


    ///////////////////////////////////////////addaddress ////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/addaddress")
    fun addAddress(
        @Field("company_name") firstname: String,
        @Field("firstname") firstname1: String,
        @Field("lastname") lastname: String,
        @Field("address1") address1: String,
        @Field("city") city: String,
        @Field("country_id") country_id: String,
        @Field("state") state: String,
        @Field("auth_code") auth_code: String,
        @Field("postcode") pincode: String,
        @Field("default_address") bill: String
    ): Call<ResponseBody>

    ///////////////////////////////////////////updateaddress ////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/updateaddress")
    fun updateAddress(
        @Field("firstname") first_name: String,
        @Field("lastname") last_name: String,
        @Field("address1") address1: String,
        @Field("city") city: String,
        @Field("country_id") country: String,
        @Field("state") state: String,
        @Field("address_id") address_id: String,
        @Field("company_name") company: String,
        @Field("auth_code") auth_code: String,
        @Field("postcode") postcode: String,
        @Field("default_address") bill: String

    ): Call<ResponseBody>


    ///////////////////////////////////////////MyOrders ////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/MyOrdersV1")
    fun MyOrdersV1(@Field("auth_code") auth_code: String): Call<ResponseBody>

    ///////////////////////////////////////////MyOrders info ////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/orderInfo")
    fun orderInfo(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String
    ): Call<ResponseBody>

    ///////////////////////////////////////////getalladdresses ////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getalladdresses")
    fun getalladdresses(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/create")
    fun createOrder(
        @Field("auth_code") auth_code: String,
        @Field("products") products: String,
        @Field("sub_total") sub_total: String,
        @Field("shipping") shipping: String,
        @Field("total") total: String,
        @Field("payment_code") payment_code: String,
        @Field("comment") comments: String,
        @Field("transaction_id") transaction_id: String,
        @Field("payment_method") payment_method: String,
        @Field("address_id") address_id: String,
        @Field("tax") tax: String,
        @Field("quotation_no") quotation_no: String,
        @Field("shipping_cost") shippingCost: String,
        @Field("coupon_applied") coupon_applied: String,
        @Field("coupon_amount") coupon_amount: String,
        @Field("billing_id") billingAddressId: String,
        @Field("credit") credit: String
    ): Call<ResponseBody>


    @Multipart
    @POST("?route=mobileapi/catalog/createDispute")
    fun createDispute(
        @Part("auth_code") auth_code: RequestBody,
        @Part("products") products: RequestBody,
        @Part("invoice_no") tax: RequestBody,
        @Part("reason") quotation_no: RequestBody,
        @Part("total") total: RequestBody,
        @Part multipartTypedOutput: Array<MultipartBody.Part?>

    ): Call<ResponseBody>

    @Multipart
    @POST("?route=mobileapi/catalog/addDisputeMsg")
    fun addDisputeMsg(
        @Part("auth_code") auth_code: RequestBody,
        @Part("dispute_id") dispute_id: RequestBody,
        @Part("msg") tax: RequestBody,
        @Part multipartTypedOutput: Array<MultipartBody.Part?>

    ): Call<ResponseBody>

    @Multipart
    @POST("?route=mobileapi/catalog/addDisputeMsg")
    fun addDisputeMsg2(
        @Part("auth_code") auth_code: RequestBody,
        @Part("dispute_id") dispute_id: RequestBody,
        @Part("msg") tax: RequestBody,
        @Part multipartTypedOutput: ArrayList<MultipartBody.Part>

    ): Call<ResponseBody>

    ///////////////////////////////////////////MyOrders info////////////////////////////////////////////////////////////
    @FormUrlEncoded
    @POST("?route=mobileapi/catalog/getFlashDiscountProduct")
    fun getFlashProducts(
        @Field("auth_code") auth_code: String,
        @Field("page") page: String
    ): Call<ResponseBody>


}