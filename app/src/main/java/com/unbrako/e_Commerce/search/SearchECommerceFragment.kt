package com.unbrako.e_Commerce.search

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.FragmentEcommerceSearchBinding
import com.unbrako.e_Commerce.UpdatindCartFromSearch
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import kotlinx.android.synthetic.main.activity_catalog_sort_filter.*
import org.json.JSONArray


/**
 * Created by Manisha Thakur on 1/4/19.
 */
@Suppress("DEPRECATION")
class SearchECommerceFragment : Fragment(), SearchView.OnQueryTextListener,
    AdapterSearchEcommerce.WishListApiCall,HistoryListAdapter.onHistoryItemClick {

    private lateinit var adapterSearchEcommerce: AdapterSearchEcommerce
    private lateinit var historyListAdapter: HistoryListAdapter
    private lateinit var mBinding: FragmentEcommerceSearchBinding
    private lateinit var mViewModel: ViewModelSearch
    lateinit var update: UpdatindCartFromSearch
    var serarchArray: ArrayList<SearchModelEcommerce> = ArrayList()

    private var isLoadingData: Boolean = false
    private var isLastPageData: Boolean = false
    private var offSet: Int = 1
    var searchQuery = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_ecommerce_search, container, false)
        mViewModel = ViewModelProviders.of(this)[ViewModelSearch::class.java]
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.rvSearch.layoutManager = LinearLayoutManager(activity)
        adapterSearchEcommerce =
            AdapterSearchEcommerce(this.requireActivity(), this@SearchECommerceFragment)
        mBinding.rvSearch.adapter = adapterSearchEcommerce
        mBinding.rvSearch.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )
        mBinding.searchView.isIconified = false
        mBinding.searchView.setOnQueryTextListener(this)
        mBinding.searchCancel.setOnClickListener {
            mBinding.searchView.setQuery("", false)
            Constants.hideSoftKeyboard(mBinding.searchCancel, requireActivity())
            mBinding.layout.visibility = View.VISIBLE
            mBinding.rvSearch.visibility = View.GONE
        }

        // search history
        historyListAdapter = HistoryListAdapter(this.requireContext(),this)
        mBinding.historyList.layoutManager = LinearLayoutManager(activity)
        mBinding.historyList.adapter = historyListAdapter
        mBinding.historyList.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )
        historyListAdapter.update(getHistoryArrayList())

        mViewModel.getmData().observe(viewLifecycleOwner, Observer {
            serarchArray = it
            if (it.size > 0) {
                mBinding.rvSearch.visibility = View.VISIBLE
                mBinding.layout.visibility = View.GONE

                adapterSearchEcommerce.update(it)
            } else {
                mBinding.layout.visibility = View.VISIBLE
                mBinding.rvSearch.visibility = View.GONE

            }
        })
    }

    private fun getHistoryArrayList(): ArrayList<String> {
        var arrayList = ArrayList<String>()
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val histData = sharedPref!!.getString("historyList", "")
        if(histData!!.isNotEmpty()) {
            val list = Gson().fromJson<ArrayList<String>>(histData, ArrayList::class.java)
            arrayList.addAll(list.take(5))
        }
        return arrayList
    }


    override fun onQueryTextSubmit(query: String?): Boolean {

        if (query!!.isNotEmpty()) {
            if (query.isNotEmpty()) {
                Constants.hideSoftKeyboard(mBinding.searchCancel, requireActivity())
                mViewModel.getRepoData(query, mBinding.progressBar)
                saveTextInHistory(query)
            }

        }
        return false
    }

    override fun onQueryTextChange(text: String): Boolean {
        searchQuery = text
        updateHistoryList(text)
        if (text.isNotEmpty()) {
            if (text.isNotEmpty()) {
                mViewModel.getRepoData(text, mBinding.progressBar)
            }
        }
        return false

    }

    private fun updateHistoryList(text: String) {
        var list = ArrayList<String>()
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val histData = sharedPref!!.getString("historyList", "")
        if(histData!!.isNotEmpty()) {
            val jArray = Gson().fromJson<ArrayList<String>>(histData, ArrayList::class.java)
            for (item in jArray) {
                if (item.startsWith(text)) {
                    list.add(item)
                }
            }
            historyListAdapter.update(list)
        }
    }

    private fun saveTextInHistory(query: String) {
        var histList = ArrayList<String>()
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val histData = sharedPref!!.getString("historyList", "")
        if (histData!!.isEmpty()) {
            histList.add(query)
        } else {
            histList.add(query)
            val jArray = Gson().fromJson<ArrayList<String>>(histData, ArrayList::class.java)
            histList.addAll(jArray)
            val setlist: Set<String> = LinkedHashSet<String>(histList)
            histList = ArrayList(setlist)
        }
        var jsArray = ""
        if(histData.length > 5){
            jsArray =JSONArray(histList.take(5)).toString()
        }else {
             jsArray = JSONArray(histList).toString()
        }
        val sharedPref2 = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref2.edit()) {
            putString("historyList", jsArray)
            apply()
        }
    }


    override fun addToCartLayout(model: FetchProductsModel, position: Int, spinnerDes: String) {


        if ((model.saleable.toDouble() * model.selectQuant.toDouble()) < model.quantity.toDouble()) {

            val gson = Gson()
            val innerJson = gson.toJson(model)
            val obj = gson.fromJson(innerJson, CartModel::class.java)
            CheckIfExistInCartList(obj, requireActivity(), spinnerDes).execute()
        } else {
            if (model.quantity.toDouble() < 0) {
                showCustomAlert("Stock Unavailable")
            } else {
                showCustomAlert("Quantity exceed than available stock.")
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    inner class CheckIfExistInCartList(
        var model: CartModel,
        var context: Context,
        var spinnerDes: String
    ) : AsyncTask<Void, Void, Void>() {
        var exist = 0
        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)
            val cartItems: ArrayList<CartModel> =
                db!!.productDao().getAllCart() as ArrayList<CartModel>

            for (j in 0 until cartItems.size) {
                if (model.product_id == cartItems[j].product_id) {
                    exist = 1
                }
            }

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (exist == 0) {
                model.quantity = spinnerDes
                Constants.AddToCart(context, true, model).execute()
                showCustomAlert("Added to Cart")
                Constants.getProductsOfCart(activity!!).execute()
                update.udateCartFromSearch(Constants.cartSize)
            } else {
                showCustomAlert("Already Added in Cart")
            }

        }

    }

    @SuppressLint("InflateParams")
    fun showCustomAlert(msg: String) {
        val context = requireActivity()
        // Create layout inflator object to inflate toast.xml file
        val inflater = layoutInflater

        // Call toast.xml file for toast layout
        val toast1 = inflater.inflate(R.layout.toast, null)
        val textm: TextView = toast1.findViewById(R.id.text)
        textm.text = msg
        val toast = Toast(context)

        // Set layout to toast
        toast.view = toast1
        toast.setGravity(
            Gravity.BOTTOM,
            0, 0
        )
        toast.duration = Toast.LENGTH_LONG
        toast.show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as UpdatindCartFromSearch
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as UpdatindCartFromSearch
    }

    override fun onClick(text: String) {
        mBinding.searchView.setQuery(text, true)
    }

}