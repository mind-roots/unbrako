package com.unbrako.e_Commerce.search

import android.app.Application
import android.view.View
import android.widget.RelativeLayout
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.catalog.ModelWishList
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "NAME_SHADOWING")
class RepositorySearch(var application: Application) {


    val mData = MutableLiveData<ArrayList<SearchModelEcommerce>>()

    fun getItems(model: String, progressBar: RelativeLayout) {

        geWishListData(model, progressBar)
    }

    fun getmDataValue(): LiveData<ArrayList<SearchModelEcommerce>> {
        return mData

    }

    fun geWishListData(

        model: String,
        progressBar: RelativeLayout

    ) {

           progressBar.visibility = View.VISIBLE

        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.getWishList(auth!!)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE
                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optString("success")
                    val array: ArrayList<ModelWishList> = ArrayList()
                    if (success == "true") {
                        val products = json.optJSONArray("products")
                        for (i in 0 until products.length()) {
                            val oo = products.optJSONObject(i)
                            val model = ModelWishList()
                            model.product_id = oo.optString("product_id")
                            model.nameWishlist = oo.optString("name")
                            model.sku = oo.optString("sku")
                            model.quantity = oo.optString("stock")
                            model.stock = oo.optString("stock")
                            model.price = oo.optString("price")
                            model.display_price = oo.optString("display_price")
                            model.category_id = oo.optString("category_id")
                            model.tax_class_id = oo.optString("tax_class_id")
                            model.saleable = oo.optString("saleable")
                            model.imageWishlist = oo.optString("image")
                            model.uom = oo.optString("uom")
                            model.flash_discount = oo.optString("flash_discount")
                            model.isexempted = oo.optString("isexempted")
                            if (oo.optString("rating").trim().isEmpty() || oo.optString("rating") == null || oo.optString(
                                    "rating"
                                ) == "null"
                            ) {
                                model.rating = "0"
                            } else {
                                model.rating = oo.optString("rating")
                            }

                            array.add(model)
                        }

                        getSearchResult(model, progressBar, array)
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.GONE
            }


        })
    }

    private fun getSearchResult(
        model: String,
        progressBar: RelativeLayout,
        array: ArrayList<ModelWishList>
    ) {

        progressBar.visibility = View.VISIBLE

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.search(auth!!, model)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optString("success")
                    val productsArray: ArrayList<SearchModelEcommerce> = ArrayList<SearchModelEcommerce>()
                    if (success == "true") {
                        val products = json.getJSONArray("products")

                        for (i in 0 until products.length()) {

                            val searchModel = SearchModelEcommerce()
                            val productObject = products.getJSONObject(i)

                            searchModel.product_id = productObject.optString("product_id")
                            searchModel.sku = productObject.optString("sku")
                            searchModel.quantity = productObject.optString("stock")
                            searchModel.stock = productObject.optString("stock")
                            searchModel.price = productObject.optString("price")
                            searchModel.name = productObject.optString("name")
                            searchModel.image = productObject.optString("image")
                            searchModel.rating = productObject.optString("rating")
                            searchModel.display_price = productObject.optString("display_price")
                            searchModel.saleable = productObject.optString("saleable")
                            searchModel.category_id = productObject.optString("category_id")
                            searchModel.tax_class_id = productObject.optString("tax_class_id")
                            searchModel.uom = productObject.optString("uom")
                            searchModel.isexempted = productObject.optString("isexempted")
                            searchModel.flash_discount = productObject.optString("flash_discount")
                            searchModel.selectQuant = "1"
                            var exist = 0
                            for (j in 0 until array.size) {
                                if (array[j].product_id == searchModel.product_id) {
                                    exist = 1
                                }
                            }
                            searchModel.wish = exist==1
                            productsArray.add(searchModel)
                        }
                        mData.value = productsArray

                    } else {
                        mData.value = productsArray

                    }
                }
            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.VISIBLE
            }


        })


    }
}
