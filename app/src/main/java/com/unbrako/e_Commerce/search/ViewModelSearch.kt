package com.unbrako.e_Commerce.search

import android.app.Application
import android.widget.RelativeLayout
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ViewModelSearch (application: Application) : AndroidViewModel(application) {

    var mRepoSearch : RepositorySearch = RepositorySearch(application)

    fun getmData() : LiveData<ArrayList<SearchModelEcommerce>>{

        return  mRepoSearch.getmDataValue()

    }

    fun getRepoData(model: String, progressBar: RelativeLayout) {

        return mRepoSearch.getItems(model, progressBar)

    }


}
