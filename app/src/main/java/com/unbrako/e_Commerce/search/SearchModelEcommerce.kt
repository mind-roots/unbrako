package com.unbrako.e_Commerce.search

import android.os.Parcel
import android.os.Parcelable

class SearchModelEcommerce() : Parcelable{

    var stock = ""
    var product_id = ""
    var sku = ""
    var quantity = ""
    var price = ""
    var status = ""
    var name = ""
    var description = ""
    var image = ""
    var rating = ""
    var sort_order = ""
    var display_price = ""
    var saleable = ""
    var category_id = ""
    var tax_class_id = ""
    var selectQuant = ""
    var uom = ""
    var flash_discount = ""
    var isexempted = ""
    var wish =false

    constructor(parcel: Parcel) : this() {
        stock = parcel.readString()!!
        product_id = parcel.readString()!!
        sku = parcel.readString()!!
        quantity = parcel.readString()!!
        price = parcel.readString()!!
        status = parcel.readString()!!
        name = parcel.readString()!!
        description = parcel.readString()!!
        image = parcel.readString()!!
        rating = parcel.readString()!!
        sort_order = parcel.readString()!!
        display_price = parcel.readString()!!
        saleable = parcel.readString()!!
        category_id = parcel.readString()!!
        tax_class_id = parcel.readString()!!
        selectQuant = parcel.readString()!!
        uom = parcel.readString()!!
        flash_discount = parcel.readString()!!
        isexempted = parcel.readString()!!
        wish = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(stock)
        parcel.writeString(product_id)
        parcel.writeString(sku)
        parcel.writeString(quantity)
        parcel.writeString(price)
        parcel.writeString(status)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(image)
        parcel.writeString(rating)
        parcel.writeString(sort_order)
        parcel.writeString(display_price)
        parcel.writeString(saleable)
        parcel.writeString(category_id)
        parcel.writeString(tax_class_id)
        parcel.writeString(selectQuant)
        parcel.writeString(uom)
        parcel.writeString(flash_discount)
        parcel.writeString(isexempted)
        parcel.writeByte(if (wish) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchModelEcommerce> {
        override fun createFromParcel(parcel: Parcel): SearchModelEcommerce {
            return SearchModelEcommerce(parcel)
        }

        override fun newArray(size: Int): Array<SearchModelEcommerce?> {
            return arrayOfNulls(size)
        }
    }


}