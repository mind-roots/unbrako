package com.unbrako.e_Commerce.search

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
//import com.unbrako.databinding.SearchRecyclerLayoutBinding
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.catalog.productDetails.ProductDetailActivity
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import kotlinx.android.synthetic.main.search_recycler_layout.view.*
import kotlinx.android.synthetic.main.search_recycler_layout.view.addToCart
import kotlinx.android.synthetic.main.search_recycler_layout.view.circular_img
import kotlinx.android.synthetic.main.search_recycler_layout.view.cutOff
import kotlinx.android.synthetic.main.search_recycler_layout.view.iv_fav
import kotlinx.android.synthetic.main.search_recycler_layout.view.partNo
import kotlinx.android.synthetic.main.search_recycler_layout.view.spinner_des
import kotlinx.android.synthetic.main.search_recycler_layout.view.tvThree
import kotlinx.android.synthetic.main.search_recycler_layout.view.tvTwo
import kotlinx.android.synthetic.main.search_recycler_layout.view.tv_one
import kotlinx.android.synthetic.main.sort_filter_layout.view.*

@Suppress("DEPRECATION")
class AdapterSearchEcommerce(var mContext: Context, searchECommerceFragment: SearchECommerceFragment) : RecyclerView.Adapter<AdapterSearchEcommerce.MyViewHolder>() {

    var callApi = searchECommerceFragment as WishListApiCall
    var list: ArrayList<SearchModelEcommerce> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(mContext).inflate(R.layout.search_recycler_layout, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val modelSearchEco = list[position]
        val model = FetchProductsModel()
        model.product_id = modelSearchEco.product_id
        model.name = modelSearchEco.name
        model.description = modelSearchEco.description
        model.sku = modelSearchEco.sku
        model.quantity = modelSearchEco.quantity
        model.image = modelSearchEco.image
        model.price = modelSearchEco.price
        model.status = modelSearchEco.status
        model.display_price = modelSearchEco.display_price
        model.category_id = modelSearchEco.category_id
        model.saleable = modelSearchEco.saleable
        model.tax_class_id = modelSearchEco.tax_class_id
        model.wish = modelSearchEco.wish
        model.selectQuant = modelSearchEco.selectQuant
        model.stock = modelSearchEco.stock
        model.uom = modelSearchEco.uom
        model.flash_discount = modelSearchEco.flash_discount
        model.isexempted = modelSearchEco.isexempted
        if (modelSearchEco.rating == null || modelSearchEco.rating == "null") {
            model.rating = 0.0
        } else {
            model.rating = modelSearchEco.rating.toDouble()
        }
        val gson = Gson()
        val json = gson.toJson(model)
        val fetchProductsModel: CartModel = gson.fromJson(json, CartModel::class.java)


        holder.itemView.tvThree.text = Constants.getPriceText(fetchProductsModel, mContext, 0)
        holder.itemView.tvfour.text =Constants.getPriceText2(fetchProductsModel,mContext,0)

        //holder.bind(modelSearchEcommerce)
//        holder.itemView.tvThree.text =
//            "₹" + Constants.currencyFormt((Constants.getDiscountedPrice(model,mContext) * list[position].saleable.toDouble()))
//      holder.itemView.tvfour.text = " per " + (list[position].saleable)+" "+list[position].uom+Constants.getDiscountApplies(model,mContext)
        if (list[position].display_price.toDouble() > 0) {
            holder.itemView.cutOff.visibility = View.VISIBLE
            holder.itemView.tvfive.text =
                "₹" + Constants.currencyFormt((list[position].display_price.toDouble() * list[position].saleable.toDouble()))
        } else {
            holder.itemView.cutOff.visibility = View.GONE
        }
        holder.itemView.tvfive.paintFlags = holder.itemView.tvfive.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG


       if (list[position].image.isNotEmpty()){
           Picasso.get().load(list[position].image).fit().centerCrop().placeholder(R.drawable.pro_img_new).into(holder.itemView.circular_img)
       }

        if ( modelSearchEco.name.contains("®")) {
            val vv =  modelSearchEco.name.split("®")
            holder.itemView.tv_one.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
        }else{
            holder.itemView.tv_one.text = modelSearchEco.name
        }

        holder.itemView.tvTwo.text = list[position].price
        holder.itemView.partNo.text = list[position].sku
        //holder.itemView.tvTwo.paintFlags = holder.itemView.tvTwo.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        if (modelSearchEco.wish) {
            holder.itemView.iv_fav.setImageResource(R.drawable.ic_favorite)
        } else {
            holder.itemView.iv_fav.setImageResource(R.drawable.ic_favorite_not)
        }
        holder.itemView.iv_fav.setOnClickListener {
            if (modelSearchEco.wish) {
                holder.itemView.iv_fav.setImageResource(R.drawable.ic_favorite_not)
            } else {
                holder.itemView.iv_fav.setImageResource(R.drawable.ic_favorite)
            }
            val mm = ModelWishList()
            mm.product_id = model.product_id

            if (modelSearchEco.wish) {
                list[position].wish = false
                Constants.WishListApiAddRemove(mContext, mm, "2")
            } else {
                list[position].wish = true
                Constants.WishListApiAddRemove(mContext, mm, "1")
            }
            notifyDataSetChanged()
        }
//        holder.itemView.ratingBarSearch.rating = list[position].rating.toFloat()
        holder.itemView.setOnClickListener {
            val intent =
                Intent(mContext, ProductDetailActivity::class.java).putExtra("model", model).putExtra("from", "search")
                    .putExtra("pos", position.toString())
            mContext.startActivity(intent)
        }
        holder.itemView.spinner_des.text = modelSearchEco.selectQuant
        holder.itemView.spinner_des.setOnClickListener {
            openDialogQuantity(mContext, position, model)

        }
        holder.itemView.addToCart.setOnClickListener {
            callApi.addToCartLayout(model, position, holder.itemView.spinner_des.text.toString())
        }

    }
    private fun openDialogQuantity(mContext: Context, position: Int, quantity: FetchProductsModel) {

        var color: String
        lateinit var dialog: AlertDialog

        // Initialize an array of colors
        val array =
            arrayOf(
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24",
                "25",
                "26",
                "27",
                "28",
                "29",
                "30",
                "31",
                "32",
                "33",
                "34",
                "35",
                "36",
                "37",
                "38",
                "39",
                "40",
                "41",
                "42",
                "43",
                "44",
                "45",
                "46",
                "47",
                "48",
                "49",
                "50"
            )

        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(mContext)

        // Set a title for alert dialog
        builder.setTitle("Select Quantity")


        builder.setSingleChoiceItems(array, quantity.selectQuant.toInt()-1) { _, which ->
            color = array[which]

            list[position].selectQuant = color
            Constants.UpdateToCarts(mContext, quantity).execute()
//            cal.calculateTotal()
            notifyDataSetChanged()
            // Dismiss the dialog
            dialog.dismiss()
        }

        dialog = builder.create()
        dialog.show()

    }
    fun update(it: ArrayList<SearchModelEcommerce>) {

        this.list = it
        notifyDataSetChanged()


    }
    interface WishListApiCall {
        fun addToCartLayout(
            model: FetchProductsModel,
            position: Int,
            spinnerDes: String
        )
    }
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}
