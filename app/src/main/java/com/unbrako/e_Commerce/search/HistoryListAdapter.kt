package com.unbrako.e_Commerce.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import kotlinx.android.synthetic.main.search_recycler_layout.view.*
import kotlinx.android.synthetic.main.sort_filter_layout.view.*
import kotlinx.android.synthetic.main.toast.view.*

@Suppress("DEPRECATION")
class HistoryListAdapter(var mContext: Context, searchECommerceFragment: SearchECommerceFragment) :
    RecyclerView.Adapter<HistoryListAdapter.MyViewHolder>() {

    var callApi = searchECommerceFragment as onHistoryItemClick
    var list: ArrayList<String> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.history_list_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.textView.text = list[position]
        holder.itemView.setOnClickListener {
            callApi.onClick(list[position])
        }
    }

    fun update(it: ArrayList<String>) {
        this.list = it
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: TextView
        init {
            textView = itemView.findViewById(R.id.textView)
        }
    }
    interface onHistoryItemClick {
        fun onClick(
            text: String
        )
    }
}
