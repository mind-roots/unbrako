package com.unbrako.e_Commerce.search

import com.unbrako.e_Commerce.dataBase.FetchProductsModel

interface FavoriteInterface {

    fun addToFav(model: FetchProductsModel, position: Int)
    fun addToCart(modelSearchEcommerce: FetchProductsModel, position: Int)
}