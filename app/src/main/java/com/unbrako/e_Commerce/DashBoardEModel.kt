package com.unbrako.e_Commerce

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.account.logout.LogoutModel
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel

class DashBoardEModel(application: Application):AndroidViewModel(application) {
    fun getCartSize():MutableLiveData<ArrayList<CartModel>> {

        return refVal.getCartSize()
    }
fun getBasketSize():MutableLiveData<ArrayList<BasketModel>> {

        return refVal.getBasketSize()
    }

    fun getProfile(dashboardEcommerceActivity: DashboardEcommerceActivity) {

        refVal.getProfileData(dashboardEcommerceActivity)
    }

    fun getForceLogout() : LiveData<String> {
        return refVal.getForceLogout()
    }

//    getForceLogout()

    var refVal:RepoDashBoardEcommerce= RepoDashBoardEcommerce(application)
}