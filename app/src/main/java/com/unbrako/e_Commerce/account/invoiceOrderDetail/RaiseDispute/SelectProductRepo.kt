package com.unbrako.e_Commerce.account.invoiceOrderDetail.RaiseDispute

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.cart.checkOut.ModelStatus
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File
import java.util.concurrent.TimeUnit


class SelectProductRepo(var application: Application) {

    var statusData = MutableLiveData<ModelStatus>()
    fun creatingDispute(
        invoiceId: String,
        reason: String,
        products: String,
        subtotal2: Double,
        imageModelArrayList: Array<File>
    ) {
        val multipartTypedOutput =
            arrayOfNulls<MultipartBody.Part>(imageModelArrayList.size)
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        for (index in imageModelArrayList.indices) {
            val surveyBody: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imageModelArrayList[index])
            multipartTypedOutput[index] =
                MultipartBody.Part.createFormData("imageFiles[]",
                    imageModelArrayList[index].name, surveyBody)
        }

        val memberId1: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(), products)
        val actionType1: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(), invoiceId)
        val reason1: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(), reason)
        val subtotal21: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(), subtotal2.toString())
        val auth1: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(),auth!!)


//        val requestFile = create1(MediaType.parse("multipart/form-data"), file)
//        val body = MultipartBody.Part.createFormData("image", file.name, requestFile)




        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS).build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .client(httpClient.build()).build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.createDispute(
            auth1,
            memberId1,
            actionType1,
            reason1,
            subtotal21,multipartTypedOutput
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
                        if (success) {
                            val m = ModelStatus()
                            m.msg ="Dispute raised"
                            statusData.value = m
                        }else{
                            val m = ModelStatus()
                            m.msg =json.optString("error")
                            statusData.value = m
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                        val m = ModelStatus()
                        m.msg ="Server Error"
                        statusData.value = m
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val m = ModelStatus()
                m.msg ="Network connection failed."
                statusData.value = m
            }
        })

    }

    fun getStatus(): MutableLiveData<ModelStatus> {

        return statusData
    }


}