package com.unbrako.e_Commerce.account

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.BuildConfig
import com.unbrako.R
import kotlinx.android.synthetic.main.recycler_account_ecommerce.view.*
import android.net.Uri
import com.unbrako.Constants
import com.unbrako.e_Commerce.account.DisputeList.DisputeListActivity
import com.unbrako.e_Commerce.account.invoiveDetail.InvoiceActivity
import com.unbrako.e_Commerce.account.orderHistory.OrderHistoryActivity
import com.unbrako.e_Commerce.account.enquiry_history.EnquiryHistoryActivity
import com.unbrako.e_Commerce.account.send_enquiry.SendEnquiryActivity
import com.unbrako.e_Commerce.account.wishlist.WishlistActivity

class AdapterAccountEcommerce(
    val mContext: Context,
    val list: ArrayList<ModelAccountEcommerce>, listen: AccountECommerceFragment
) :
    RecyclerView.Adapter<AdapterAccountEcommerce.MyViewHolder>() {

    var listener = listen as GoToActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.recycler_account_ecommerce, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val modelAccountEcommerce = list[position]

        holder.itemView.ivProfile.setImageResource(modelAccountEcommerce.accountImage)
        holder.itemView.ivNext.setImageResource(modelAccountEcommerce.accountNext)
        holder.itemView.tvProfile.text = modelAccountEcommerce.accountText


        holder.itemView.setOnClickListener {
            var intent: Intent = Intent()

            if (position == 0) {
                intent = Intent(mContext, AccountActivity::class.java)
                mContext.startActivity(intent)
            } else if (position == 1) {
                intent = Intent(mContext, OrderHistoryActivity::class.java)

                mContext.startActivity(intent)
            } else if (position == 2) {
                intent = Intent(mContext, InvoiceActivity::class.java)
                mContext.startActivity(intent)

            } else if (position == 3) {
                intent = Intent(mContext, EnquiryHistoryActivity::class.java)
                mContext.startActivity(intent)

            } else if (position == 4) {
                intent = Intent(mContext, SendEnquiryActivity::class.java)
                mContext.startActivity(intent)
            } else if (position == 5) {
                intent = Intent(mContext, WishlistActivity::class.java)
                mContext.startActivity(intent)

            } else if (position == 6) {
                intent = Intent(mContext, DisputeListActivity::class.java)
                mContext.startActivity(intent)

            } else if (position == 7) {
                Constants.showComposer(mContext, "")
            } else if (position == 8) {
                try {
                    intent = Intent(Intent.ACTION_SEND)
                    intent.type = "text/plain"
                    intent.putExtra(Intent.EXTRA_SUBJECT, "My application name")
                    var shareMessage = "\nLet me recommend you this application\n\n"
                    shareMessage =
                        shareMessage + "share your Invite Code" + BuildConfig.APPLICATION_ID + "\n\n"
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                    mContext.startActivity(Intent.createChooser(intent, "choose one"))
                } catch (e: Exception) {
                }
            } else if (position == 9) {
                val url = "http://www.unbrako.com/terms"
                intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                mContext.startActivity(intent)
            } else if (position == 10) {
                val url = "http://www.unbrako.com/privacy-policy"
                intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                mContext.startActivity(intent)
            } else {
                listener.CallingActivity()
            }
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    interface GoToActivity {
        fun CallingActivity()
    }
}
