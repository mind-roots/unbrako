package com.unbrako.e_Commerce.account.invoiceOrderDetail.invoicedownload

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.R
import com.unbrako.databinding.InvoiceFragmentDownloadBinding
import com.unbrako.e_Commerce.account.invoiceOrderDetail.ModelCertificate
import com.unbrako.e_Commerce.account.invoiceOrderDetail.ModelDownloadDetails

class InvoiceDownloadFragment : Fragment() {
    lateinit var productData: DownLoadData
    lateinit var mAdapter: InvoiceDownLoadAdapter
    lateinit var mAdapterC: InvoiceCertificateAdapter
    lateinit var mBinding: InvoiceFragmentDownloadBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.invoice_fragment_download, container, false)
        setHasOptionsMenu(true)
        setAdapter()
        return mBinding.root
    }

    private fun setAdapter() {
        val orderArray: ArrayList<ModelDownloadDetails> = productData.getDownLoadData()
        val orderCertificateArray: ArrayList<ModelCertificate> = productData.getCertificateData()
        if (orderArray.size > 0) {
            mBinding.rvDownloads.layoutManager = LinearLayoutManager(activity)
            mAdapter = InvoiceDownLoadAdapter(activity!!)
            mBinding.rvDownloads.adapter = mAdapter
            mAdapter.update(orderArray)

        }
        if (orderCertificateArray.size>0){
            mBinding.rvCertificate.layoutManager = LinearLayoutManager(activity)
            mAdapterC = InvoiceCertificateAdapter(activity!!)
            mBinding.rvCertificate.adapter = mAdapterC
            mAdapterC.update(orderCertificateArray)
            mBinding.rvCertificate.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))

            mBinding.textCerti.visibility = View.VISIBLE
            mBinding.divider.visibility = View.VISIBLE
        }else{
            mBinding.textCerti.visibility = View.GONE
            mBinding.divider.visibility = View.GONE

        }
        if (orderArray.size>0||orderCertificateArray.size>0){
            mBinding.noData.visibility = View.GONE
        }else{
            mBinding.noData.visibility = View.VISIBLE
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        productData = context as DownLoadData
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        productData = activity as DownLoadData
    }


    interface DownLoadData {
        fun getDownLoadData(): ArrayList<ModelDownloadDetails>
        fun getCertificateData(): ArrayList<ModelCertificate>
    }

}