package com.unbrako.e_Commerce.account.enquiryOrderDetail.product

import android.os.Parcel
import android.os.Parcelable

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ProductDetail() : Parcelable {

    var order_id: String = ""
    var name: String = ""
    var status: String = ""
    var date_added: String = ""
    var total: String = ""
    var products: Int = 0

    constructor(parcel: Parcel) : this() {
        order_id = parcel.readString()!!
        name = parcel.readString()!!
        status = parcel.readString()!!
        date_added = parcel.readString()!!
        total = parcel.readString()!!
        products = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(order_id)
        parcel.writeString(name)
        parcel.writeString(status)
        parcel.writeString(date_added)
        parcel.writeString(total)
        parcel.writeInt(products)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductDetail> {
        override fun createFromParcel(parcel: Parcel): ProductDetail {
            return ProductDetail(parcel)
        }

        override fun newArray(size: Int): Array<ProductDetail?> {
            return arrayOfNulls(size)
        }
    }

}
