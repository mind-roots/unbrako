package com.unbrako.e_Commerce.account.enquiry_history

import android.app.Application
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class EnquiryHistoryViewModel(application: Application):AndroidViewModel(application) {
    fun getQuotationDetail(progressBar: ProgressBar, noData: TextView) {

        repo.quotationDetailRepo(progressBar,noData)
    }

    fun getQuotes(): MutableLiveData<ArrayList<EnquiryModel>>{
        return  repo.getAllQuotes()
    }

    var repo:RepoEnquiryHistory= RepoEnquiryHistory(application)

}
