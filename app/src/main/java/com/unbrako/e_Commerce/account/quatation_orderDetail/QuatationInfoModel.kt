package com.unbrako.e_Commerce.account.quatation_orderDetail

class QuatationInfoModel {

    var sku: String = ""
    var quantity: String = ""
    var description: String = ""
    var total: String = ""
    var price: String = ""
    var saleable: String = ""
    var image: String = ""
    var partNo: String = ""
    var product_id: String = ""
    var commentss: String = ""
    var user_comment: String = ""
    var display_price: String = ""
    var tax_class_id: String = ""
    var stock: String = ""
    var inquiry_id: String = ""
    var comment: String = ""
    var shipping_cost: String = ""
    var status: String = ""
    var quotation_status: String = ""
    var quotation_msg: String = ""
    var discount_percentage: String = ""
    var uom: String = ""
}