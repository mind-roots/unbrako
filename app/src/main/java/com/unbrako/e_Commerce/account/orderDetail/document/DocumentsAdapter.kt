package com.unbrako.e_Commerce.account.orderDetail.document

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.e_Commerce.account.orderDetail.ModalDocument
import com.unbrako.e_Commerce.account.orderDetail.ModelOrderDetail
import kotlinx.android.synthetic.main.document_adapter_layout.view.*
import kotlin.collections.ArrayList

class DocumentsAdapter(var context: FragmentActivity?) : RecyclerView.Adapter<DocumentsAdapter.ViewHolder>(){

    private var items = ArrayList<ModalDocument>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.document_adapter_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tv_document.text = items[position].name

        holder.itemView.tv_document.setOnClickListener {
            val url = items[position].image
            var intent: Intent = Intent()

            intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            context!!.startActivity(intent)
        }

    }

    fun update(items: ArrayList<ModalDocument>) {

        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
