package com.unbrako.e_Commerce.account.DisputeDetails.Details

class ModelDisputeDetail {
    var id:String=""
    var trans_no:String=""
    var reason:String=""
    var amount:String=""
    var created_at:String=""
    var status:String=""
}