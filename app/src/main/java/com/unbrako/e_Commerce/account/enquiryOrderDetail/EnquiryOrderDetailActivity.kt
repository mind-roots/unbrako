package com.unbrako.e_Commerce.account.enquiryOrderDetail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityQuatorderBinding
import com.unbrako.e_Commerce.account.orderHistory.ModelOrderHistory
import com.unbrako.e_Commerce.account.enquiryOrderDetail.detail.AdapterDetail
import com.unbrako.e_Commerce.account.enquiryOrderDetail.detail.DetailFragment
import com.unbrako.e_Commerce.account.enquiryOrderDetail.product.AdapterProduct
import com.unbrako.e_Commerce.account.enquiryOrderDetail.product.ProdFragment

class EnquiryOrderDetailActivity : AppCompatActivity(), View.OnClickListener, ProdFragment.ProductData,
    DetailFragment.AddressData {


    lateinit var mBinding: ActivityQuatorderBinding
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var mViewModel: QuadOrderViewModel
    lateinit var adapter: AdapterProduct
    lateinit var mAdapter: AdapterDetail
    var inquiryModel: ArrayList<InquiryModel> = ArrayList()
    var quotModel: ArrayList<QuotModel> = ArrayList()
    lateinit var fragment: Fragment
    var position = 0
    var model: ModelOrderHistory = ModelOrderHistory()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_quatorder)
        mViewModel = ViewModelProviders.of(this).get(QuadOrderViewModel::class.java)

    }

    private fun setObserver() {
        mViewModel.getAllInquiry().observe(this, Observer {
            if (it.size > 0) {
                inquiryModel = it
                fragmentManager = supportFragmentManager
                this.fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.detail_container, DetailFragment())
                fragmentTransaction.commit()
            }
        })

        mViewModel.getAllQuot().observe(this, Observer {
            if (it.size > 0) {
                quotModel = it
            }
        })

    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Enquiry #" + intent.getStringExtra("quote_id")
    }

    private fun setFragment() {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.detail_container, DetailFragment())
        fragmentTransaction.commit()
        getFragment()
    }

    private fun getFragment() {
        mBinding.details.setOnClickListener(this)
        mBinding.products.setOnClickListener(this)

    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.detail_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onClick(v: View?) {

        when (v!!.id) {

            R.id.details -> {
                fragment = DetailFragment()
                loadFragment(fragment)
                position = 0
                mBinding.details.setBackgroundResource(R.drawable.selected_order_back)
                mBinding.products.setBackgroundResource(R.drawable.unselected_detail_bg)
                return
            }
            R.id.products -> {

                fragment = ProdFragment()
                loadFragment(fragment)
                position = 1
                mBinding.details.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.products.setBackgroundResource(R.drawable.selected_order_back)
                return
            }

        }

    }

    override fun getProductData(): ArrayList<QuotModel> {
        return quotModel
    }

    override fun getAddressData(): ArrayList<InquiryModel> {
        return inquiryModel
    }

    override fun onResume() {
        super.onResume()
        setToolbar()
        setFragment()

        mBinding.details.setBackgroundResource(R.drawable.selected_order_back)
        mBinding.products.setBackgroundResource(R.drawable.unselected_detail_bg)

        mViewModel.getInquiryInfo(mBinding.progressBar, intent.getStringExtra("quote_id")!!)
        setObserver()
        if (Constants.getPrefs(this).getString("gotoCart", "no") == "yes") {
            finish()
        }
    }


}