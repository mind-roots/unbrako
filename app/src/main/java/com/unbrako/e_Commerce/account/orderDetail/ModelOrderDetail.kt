package com.unbrako.e_Commerce.account.orderDetail

import android.os.Parcel
import android.os.Parcelable

class ModelOrderDetail() : Parcelable {
    var discountPercent: String = ""
    var name: String = ""
    var model: String = ""
    var option: String = ""
    var quantity: String = ""
    var stock: String = ""
    var product_id: String = ""
    var display_price: String = ""
    var saleable: String = ""
    var price: String = ""
    var tax_class_id: String = ""
    var sku: String = ""
    var total: String = ""
    var image: String = ""
    var coupon_discount: String = ""
    var delivery_cost: String = ""
    var uom: String = ""

    constructor(parcel: Parcel) : this() {
        discountPercent = parcel.readString()!!
        name = parcel.readString()!!
        model = parcel.readString()!!
        option = parcel.readString()!!
        quantity = parcel.readString()!!
        stock = parcel.readString()!!
        product_id = parcel.readString()!!
        display_price = parcel.readString()!!
        saleable = parcel.readString()!!
        price = parcel.readString()!!
        tax_class_id = parcel.readString()!!
        sku = parcel.readString()!!
        total = parcel.readString()!!
        image = parcel.readString()!!
        coupon_discount = parcel.readString()!!
        delivery_cost = parcel.readString()!!
        uom = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(discountPercent)
        parcel.writeString(name)
        parcel.writeString(model)
        parcel.writeString(option)
        parcel.writeString(quantity)
        parcel.writeString(stock)
        parcel.writeString(product_id)
        parcel.writeString(display_price)
        parcel.writeString(saleable)
        parcel.writeString(price)
        parcel.writeString(tax_class_id)
        parcel.writeString(sku)
        parcel.writeString(total)
        parcel.writeString(image)
        parcel.writeString(coupon_discount)
        parcel.writeString(delivery_cost)
        parcel.writeString(uom)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelOrderDetail> {
        override fun createFromParcel(parcel: Parcel): ModelOrderDetail {
            return ModelOrderDetail(parcel)
        }

        override fun newArray(size: Int): Array<ModelOrderDetail?> {
            return arrayOfNulls(size)
        }
    }


}