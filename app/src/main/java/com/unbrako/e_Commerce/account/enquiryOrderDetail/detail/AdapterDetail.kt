package com.unbrako.e_Commerce.account.enquiryOrderDetail.detail

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.e_Commerce.account.enquiryOrderDetail.InquiryModel
import kotlinx.android.synthetic.main.order_details_adapter_layout.view.*

class AdapterDetail(var context: FragmentActivity?) :
    RecyclerView.Adapter<AdapterDetail.ViewHolder>() {

    private var items = ArrayList<InquiryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view =
            LayoutInflater.from(context).inflate(R.layout.detail_adapter_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val model = items[position]

        holder.itemView.name.text = items[position].description
        holder.itemView.partNo.text = items[position].stk_code
        holder.itemView.originalPrice.text =
            "₹" + Constants.currencyFormt(items[position].price.toDouble() * (items[position].saleable).toDouble())
        holder.itemView.discountPrice.text =
            " per " + items[position].saleable + " " + items[position].uom
        holder.itemView.qtyPiece.text =
            ((items[position].quantity.toDouble()) / (items[position].saleable).toDouble()).toInt()
                .toString()

        if (model.image.isNotEmpty()) {
            Picasso.get().load(items[position].image).placeholder(R.mipmap.ic_launcher).fit()
                .centerCrop()
                .into(holder.itemView.iv_cart)
        } else {
            holder.itemView.iv_cart.setImageResource(R.mipmap.ic_launcher)
        }

    }

    fun update(it: ArrayList<InquiryModel>) {
        items = it
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
