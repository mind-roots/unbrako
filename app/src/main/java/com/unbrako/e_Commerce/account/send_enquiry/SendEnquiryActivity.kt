package com.unbrako.e_Commerce.account.send_enquiry

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivitySendEnquiryBinding
import com.unbrako.e_Commerce.search.SearchModelEcommerce
import java.util.ArrayList
import android.text.Editable
import android.text.TextWatcher
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.e_Commerce.catalog.ModelTax
import com.unbrako.e_Commerce.signup.SignUpModel
import org.json.JSONArray
import org.json.JSONObject


class SendEnquiryActivity : AppCompatActivity(), SearchView.OnQueryTextListener, AdapterSearchEnquiry.AddtoQuotation {

    private var array: ArrayList<SearchModelEcommerce> = ArrayList()
    private var newArray: ArrayList<SearchModelEcommerce> = ArrayList()
    lateinit var mBinding: ActivitySendEnquiryBinding
    lateinit var mViewModel: SearchEnquiryViewModel
    private lateinit var adapterSearchEnquiry: AdapterSearchEnquiry
    private lateinit var adapternew: AdapterNewArray


    var isrunning = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_send_enquiry)
        mViewModel = ViewModelProviders.of(this).get(SearchEnquiryViewModel::class.java)
        setToolbar()

        mBinding.quotationList.layoutManager = LinearLayoutManager(this)
        adapternew = AdapterNewArray(this, this@SendEnquiryActivity)
        mBinding.quotationList.adapter = adapternew
        mBinding.quotationList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))



        mBinding.rvSearch.layoutManager = LinearLayoutManager(this)
        adapterSearchEnquiry = AdapterSearchEnquiry(this, this@SendEnquiryActivity)
        mBinding.rvSearch.adapter = adapterSearchEnquiry
        mBinding.rvSearch.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        mBinding.searchView.isIconified = false
        mBinding.searchView.setOnQueryTextListener(this)
        mBinding.searchCancel.setOnClickListener {
            mBinding.searchView.setQuery("", false)
            Constants.hideSoftKeyboard(mBinding.searchCancel, this)

            array.clear()
            adapterSearchEnquiry.update(array)
            mBinding.rvSearch.visibility = View.GONE

        }


    }

    private fun showDialogee(newArray: ArrayList<SearchModelEcommerce>) {
        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.cutsom_layout)
        val body = dialog.findViewById(R.id.quotationaddComment) as EditText

        val yesBtn = dialog.findViewById(R.id.submit) as TextView
        val counter = dialog.findViewById(R.id.counter) as TextView
        val progress = dialog.findViewById(R.id.progress) as ProgressBar
        body.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let{
                    val remaining = it.length
                    counter.text = "$remaining/500"
                }

            }

        })
        if (!isrunning) {
            isrunning = true

            yesBtn.setOnClickListener {

                Constants.hideSoftKeyboard(yesBtn, this)
                val authCode = Constants.getPrefs(this).getString(Constants.token, "")
                if (body.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please add comments", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                val orders: ArrayList<SearchModelEcommerce> = newArray
                var id = ""
                var idArray: ArrayList<String> = ArrayList()
                val jsonArray2 = JSONArray()
                var subtotal = 0.0
                var tax = 0.0
                var price = 0.0
                var products = ""
                for (i in 0 until newArray.size) {
                    newArray[i].price.replace("₹", "").replace(",", "")
                    price += (newArray[i].selectQuant.toDouble() * newArray[i].price.replace("₹", "").replace(
                        ",",
                        ""
                    ).toDouble() * newArray[i].saleable.toDouble())
                    tax += getProductTax(
                        newArray[i].tax_class_id,
                        (newArray[i].selectQuant.toDouble() * newArray[i].price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() * newArray[i].saleable.toDouble())
                    )
                }
                for (i in 0 until orders.size) {

                    subtotal += (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * orders[i].price.replace(
                        "₹",
                        ""
                    ).toDouble())

                    val jsonObject = JSONObject()

                    jsonObject.put("id", orders[i].product_id)
                    jsonObject.put("qty", (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()).toString())
                    jsonObject.put("price", orders[i].price.replace("₹", "").replace(",", ""))
                    jsonObject.put("discount_percentage", calculateProductDiscount(orders[i],0.0))
                    jsonObject.put(
                        "tax",
                        getProductTax(
                            orders[i].tax_class_id,
                            orders[i].price.replace("₹", "").replace(
                                ",",
                                ""
                            ).toDouble() * orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble()
                        ).toString()
                    )
                    jsonObject.put(
                        "total",
                        (orders[i].saleable.toDouble() * orders[i].selectQuant.toDouble() * orders[i].price.replace(
                            "₹",
                            ""
                        ).toDouble()).toString()
                    )
                    jsonArray2.put(jsonObject)

                    products = jsonArray2.toString()


                }

                mViewModel.addComment(
                    authCode!!,
                    products,
                    subtotal.toString(),
                    (price + tax).toString(),
                    body.text.toString(),
                    tax.toString(), progress
                )

            }

        }

        dialog.show()

        mViewModel.getAddData().observe(this, Observer {
            //Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            showAlert(it.msg)
            isrunning = false
            if (it.success) {
                dialog.dismiss()
                //finish()
            }

        })
    }

    private fun calculateProductDiscount(cartModel: SearchModelEcommerce, couponDiscount: Double): String? {
        val gson = Gson()
        val json1 = Constants.getPrefs(this).getString("profile", "")
        val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
        val flash_sale = Constants.getSharedPrefs(this).getString("flash_sale", "")

        if (cartModel.isexempted=="1"){

            if (flash_sale!="off"){
                val basePrice=(100*profileData.discount.toDouble())/100
                val amountLeft=100-basePrice
                val amountAfterFlashDiscount=(amountLeft*cartModel.flash_discount.toDouble())/100
                return (basePrice+amountAfterFlashDiscount).toString()
            }else{
                val basePrice=(100*profileData.discount.toDouble())/100
                val amountLeft=100-basePrice
                val amountAfterFlashDiscount=(amountLeft*couponDiscount)/100
                return (basePrice+amountAfterFlashDiscount).toString()
            }
        }else{
            if (flash_sale=="off") {

                return couponDiscount.toString()
            }else{
                if (cartModel.flash_discount.isEmpty()){
                    cartModel.flash_discount = "0"
                }
                val basePrice=(100*cartModel.flash_discount.toDouble())/100
                val amountLeft=100-basePrice
                val amountAfterFlashDiscount=(amountLeft*couponDiscount)/100
                return (basePrice+amountAfterFlashDiscount).toString()
            }
        }

    }

    private fun showAlert(msg: String) {
        val builder = AlertDialog.Builder(this)

        // Set the alert dialog title
        builder.setTitle("Alert!")

        // Display a message on alert dialog
        builder.setMessage(msg)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, which ->
            //Constants.deleteCart(this).execute()
            //  Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show()
           // Constants.getPrefs(this).edit().putString("gotoCart", "yes").apply()
            finish()
        }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }
    fun getProductTax(tax_class_id: String, price: Double): Double {
        val gson = Gson()
        val response = Constants.getSharedPrefs(this).getString("rate", "")
        val lstArrayList = gson.fromJson<ArrayList<ModelTax>>(
            response,
            object : TypeToken<List<ModelTax>>() {

            }.type
        )
        for (i in 0 until lstArrayList.size) {
            if (lstArrayList[i].tax_class_id == tax_class_id) {
                if (lstArrayList[i].rate.isEmpty()) {
                    lstArrayList[i].rate = "0"
                }

                return (price * lstArrayList[i].rate.toDouble()) / 100
                break
            }
        }
        return 0.0
    }

    override fun addToQuotation(model: SearchModelEcommerce) {
        var dd = 0
        for (i in 0 until newArray.size) {
            if (model.product_id.equals(newArray[i].product_id)) {
                dd = 1
                break
            }
        }
        if (dd == 0) {
            newArray.add(model)
        } else {
            Toast.makeText(this, "Product Already Added.", Toast.LENGTH_SHORT).show()
            return
        }
        array.clear()
        mBinding.quotationList.visibility=View.VISIBLE
        mBinding.searchView.setQuery("", false)
        adapterSearchEnquiry.update(array)
        //mBinding.layout.visibility = View.VISIBLE
        mBinding.rvSearch.visibility = View.GONE
        adapternew.update(newArray)
        if (newArray.size > 0) {
            mBinding.dashToolbar.next.visibility = View.VISIBLE
        } else {
            mBinding.dashToolbar.next.visibility = View.INVISIBLE
        }
    }


    override fun onQueryTextSubmit(query: String?): Boolean {

        return false
    }

    override fun onQueryTextChange(query: String?): Boolean {
        if (query!!.length > 1) {
            mBinding.quotationList.visibility=View.GONE
            mViewModel.getRepoData(query, mBinding.progressBar)
            mViewModel.getmData().observe(this, Observer {
                array = it;
                if (it.size > 0) {
                    mBinding.rvSearch.visibility = View.VISIBLE
                    mBinding.layout.visibility = View.GONE
                    mBinding.quotationList.visibility=View.GONE
                    adapterSearchEnquiry.update(it)
                } else {
                    //mBinding.layout.visibility = View.VISIBLE
                    mBinding.rvSearch.visibility = View.GONE
                    mBinding.quotationList.visibility=View.VISIBLE
                }
            })
        }
        return false
    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.dashToolbar.MainToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.dashToolbar.title.text = "New Enquiry"

        mBinding.dashToolbar.next.setOnClickListener {
            showDialogee(newArray)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}