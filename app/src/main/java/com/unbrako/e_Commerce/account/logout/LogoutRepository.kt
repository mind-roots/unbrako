package com.unbrako.e_Commerce.account.logout

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class LogoutRepository(val application: Application) {

    val mData = MutableLiveData<LogoutModel>()

    fun getItems(authCode: String, progressBar : ProgressBar) {
        getProducts(authCode, progressBar)
    }

    fun getMData(): LiveData<LogoutModel> {
        return mData
    }

     fun getProducts(authCode: String, progressBar : ProgressBar) {

        progressBar.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.logout(authCode)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optString("success")
                    val logoutModel = LogoutModel()
                    if (success == "true") {
                        val editor = Constants.getPrefs(application).edit()
                        editor.clear()
                        editor.apply()
                    }
                    mData.value = logoutModel
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.VISIBLE
            }
        })
    }
}