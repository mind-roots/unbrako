package com.unbrako.e_Commerce.account.enquiryOrderDetail

import android.app.Application
import android.widget.RelativeLayout
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class QuadOrderViewModel(application: Application) : AndroidViewModel(application) {

    fun getInquiryInfo(progress: RelativeLayout, inquiry_no: String) {
        repo.inquiryInfo(progress,inquiry_no)
    }

    fun getAllInquiry() : MutableLiveData<ArrayList<InquiryModel>>{
        return repo.getInquiryData()
    }
    fun getAllQuot() : MutableLiveData<ArrayList<QuotModel>>{
        return repo.getQuotData()
    }


    var repo: QuadOrderRepo = QuadOrderRepo(application)

}
