package com.unbrako.e_Commerce.account.DisputeDetails.FullImageView

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.unbrako.R
import it.sephiroth.android.library.imagezoom.ImageViewTouch

class FullScreenImageViewAdapter(
    var context: Context,
    var mData: ArrayList<String>
) :
    PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`

    }

    override fun getCount(): Int {

        return mData.size
    }

    @SuppressLint("SetTextI18n")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context)
            .inflate(R.layout.full_scren_image, container, false)
        val imgDisplay = itemView.findViewById(R.id.fullScreenImg) as ImageViewTouch


            Glide.with(context)
                .load(mData[position])
                .into(imgDisplay)


        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }


}