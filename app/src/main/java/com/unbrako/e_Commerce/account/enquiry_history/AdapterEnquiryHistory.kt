package com.unbrako.e_Commerce.account.enquiry_history

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.e_Commerce.account.enquiryOrderDetail.EnquiryOrderDetailActivity
import kotlinx.android.synthetic.main.order_history_layout.view.*

class AdapterEnquiryHistory(var context: Context) : RecyclerView.Adapter<AdapterEnquiryHistory.ViewHolder>() {
    var list: ArrayList<EnquiryModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.quatation_detail_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val intent = Intent(context, EnquiryOrderDetailActivity::class.java)
            intent.putExtra("quote_id", list[position].inquiry_no)
            context.startActivity(intent)
        }

        holder.itemView.orderId.text = list[position].inquiry_no
        holder.itemView.status.text = list[position].status
        holder.itemView.orderDate.text = list[position].created_at
    }

    fun update(it: java.util.ArrayList<EnquiryModel>) {
        list = it
        notifyDataSetChanged()

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
