package com.unbrako.e_Commerce.account

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityAccountBinding
import com.unbrako.e_Commerce.cart.address.AddressListActivity
import com.unbrako.e_Commerce.cart.address.AddressListViewModel
import com.unbrako.e_Commerce.forgotPassword.ChangePasswordActivity
import com.unbrako.e_Commerce.signup.ModelStatus
import com.unbrako.e_Commerce.signup.SignUpModel
import kotlinx.android.synthetic.main.activity_account.*


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AccountActivity : AppCompatActivity(), UpdateProfileListener {


    lateinit var mBinding: ActivityAccountBinding
    lateinit var viewModel: ProfileViewModel
    lateinit var mViewModel: AddressListViewModel
    var modelS = ModelStatus()
    var shown = 0
    var shown2 = 0
    var shown3 = 0

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_account)
        viewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        mViewModel = ViewModelProviders.of(this)[AddressListViewModel::class.java]

        setToolbar()
        mBinding.update = this


        try {

            val mVariable = Gson()
            val json = Constants.getPrefs(this).getString("profile", "")
            val profileData: SignUpModel = mVariable.fromJson(json, SignUpModel::class.java)
            val profileModel = ProfileModel()
            profileModel.customer_id = profileData.customer_id
            profileModel.customer_group_id = profileData.customer_group_id
            profileModel.store_id = profileData.store_id
            profileModel.language_id = profileData.language_id
            profileModel.firstname = profileData.firstname
            profileModel.lastname = profileData.lastname
            profileModel.email = profileData.email
            profileModel.telephone = profileData.telephone
            profileModel.fax = profileData.fax
            profileModel.password = profileData.password
            profileModel.salt = profileData.salt
            profileModel.cart = profileData.cart
            profileModel.wishlist = profileData.wishlist
            profileModel.newsletter = profileData.newsletter
            profileModel.address_id = profileData.address_id
            profileModel.custom_field = profileData.custom_field
            profileModel.ip = profileData.ip
            profileModel.status = profileData.status
            profileModel.safe = profileData.safe
            profileModel.token = profileData.token
            profileModel.code = profileData.code
            profileModel.date_added = profileData.date_added
            profileModel.company_name = profileData.company_name
            profileModel.gst = profileData.gst
            profileModel.landline = profileData.landline
            profileModel.credit = profileData.credit

            var cred:Double=0.0
            try{
                cred=profileData.credit.toDouble()
            }catch (e:java.lang.Exception){
                e.printStackTrace()
            }
            if (cred>0){
                availableText.visibility=View.VISIBLE
                availableCredits.visibility=View.VISIBLE
                availableCredits.text = "₹ "+Constants.formatValues2(cred.toString())
            }else{
                availableText.visibility=View.GONE
                availableCredits.visibility=View.GONE
            }
            mBinding.profile = profileModel
            mBinding.companyName.setText(profileData.company_name)

        } catch (e: Exception) {

        }
        viewModel.mDataProfile().observe(this, Observer {

            mBinding.profile = it

        })

        viewModel.getUpdate().observe(this, Observer {
            if (it) {
                viewModel.getProfileResponse()
            }
        })


    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        mViewModel.getAllAddress(mBinding.progressBar)

        mViewModel.mGetAddressData().observe(this, Observer {
            if (it.isNotEmpty()) {

                if (it.size > 0) {
                    for (i in 0 until it.size) {
                        if (Constants.getPrefs(this).getString("shippingId", "")!!.isEmpty()) {
                            it[i].status = i == 0
                            mBinding.shippingAddress.text = it[0].firstname + " " + it[0].city

                        } else {
                            it[i].status = Constants.getPrefs(this).getString("shippingId", "") == it[i].address_id
                            if (Constants.getPrefs(this).getString("shippingId", "") == it[i].address_id) {
                                mBinding.shippingAddress.text = it[0].firstname + " " + it[i].city

                            }
                        }
                    }
                }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.includeProfile.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.includeProfile.toolbarTitle.text = "My Profile"

    }


    fun shippingAddress(v: View) {
        startActivity(Intent(this, AddressListActivity::class.java).putExtra("profile", "others"))
    }

    fun tVPassword(v: View) {
        startActivity(Intent(this, ChangePasswordActivity::class.java).putExtra("profile", "others"))
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun profileEventListener(
        companyName: String,
        gstNumber: String,
        firstName: String,
        lastName: String,
        emailId: String,
        phoneNumber: String,
        landline: String
    ) {

        val profileModel = ProfileModel()

        profileModel.company_name = companyName
        profileModel.gst = gstNumber
        profileModel.firstname = firstName
        profileModel.lastname = lastName
        profileModel.email = emailId
        profileModel.telephone = phoneNumber
        profileModel.landline = landline


        when {
            companyName.isEmpty() -> {
                Toast.makeText(this, getString(R.string.company_error), Toast.LENGTH_SHORT).show()
                return
            }
            gstNumber.isEmpty() -> {
                Toast.makeText(this, getString(R.string.gst_error), Toast.LENGTH_SHORT).show()
                return

            }
           ! Constants.isGstValid(gstNumber)
            -> {
                Toast.makeText(this, "Enter valid GST Number", Toast.LENGTH_SHORT).show()
                return

            }
            firstName.isEmpty() -> {
                Toast.makeText(this, getString(R.string.fName_error), Toast.LENGTH_SHORT).show()
                return
            }
            lastName.isEmpty() -> {
                Toast.makeText(this, getString(R.string.lName_error), Toast.LENGTH_SHORT).show()
                return

            }
            emailId.isEmpty() -> {
                Toast.makeText(this, getString(R.string.email_error), Toast.LENGTH_SHORT).show()
                return

            }
            phoneNumber.isEmpty() -> {
                Toast.makeText(this, getString(R.string.phone_error), Toast.LENGTH_SHORT).show()
                return
            }
        }


        viewModel.getUpdateProfile(mBinding.progressBar, profileModel)
        viewModel.getUpdateProfileStatus().observe(this, Observer {

            if (it) {
                finish()
            }
        })
    }
}