package com.unbrako.e_Commerce.account.invoiveDetail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.R
import com.unbrako.databinding.ActivityInvoiceLayoutBinding

class InvoiceActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityInvoiceLayoutBinding
    lateinit var mViewModel: InVoiceDetailViewModel
    lateinit var adapter: AdapterInvoiceDetail



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_invoice_layout)
        mViewModel=ViewModelProviders.of(this).get(InVoiceDetailViewModel::class.java)
        setToolbar()
        setAdapter()
        setObserver()
        mViewModel.getInvoiceDetail(mBinding.progressBar, mBinding.noData)
    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Invoice History"
    }

    private fun setAdapter() {
        val manager = LinearLayoutManager(this)
        adapter = AdapterInvoiceDetail(this)
        mBinding.rvInvoiceDetail.layoutManager = manager
        mBinding.rvInvoiceDetail.adapter = adapter
        mBinding.rvInvoiceDetail.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    private fun setObserver(){
        mViewModel.getInvoiceData().observe(this, Observer {
            if (it.size > 0) {
                it.reverse()
                adapter.update(it)
            }
        })
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}