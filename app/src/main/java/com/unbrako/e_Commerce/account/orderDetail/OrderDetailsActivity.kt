package com.unbrako.e_Commerce.account.orderDetail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.unbrako.R
import com.unbrako.databinding.OrderDetailsBinding
import com.unbrako.e_Commerce.account.orderDetail.details.DetailsFragment
import com.unbrako.e_Commerce.account.orderDetail.document.DocumentFragment
import com.unbrako.e_Commerce.account.orderDetail.product.ProductFragment
import com.unbrako.e_Commerce.account.orderHistory.ModelOrderHistory

class OrderDetailsActivity : AppCompatActivity(), View.OnClickListener, ProductFragment.ProductData,
    DocumentFragment.DocumentData, DetailsFragment.AddressData {

    lateinit var mBinding: OrderDetailsBinding
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var mViewModel: ViewModelOrderDetail
    var modelOrderDetail: ArrayList<ModelOrderDetail> = ArrayList<ModelOrderDetail>()
    var modelTotal: ArrayList<ModelTotal> = ArrayList<ModelTotal>()
    var modelDocument: ArrayList<ModalDocument> = ArrayList<ModalDocument>()
    var modelAddress: ShippingAddressModel = ShippingAddressModel()
    lateinit var fragment: Fragment
    var position = 0
    var model: ModelOrderHistory = ModelOrderHistory()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.order_details)
        mViewModel = ViewModelProviders.of(this)[ViewModelOrderDetail::class.java]

        mViewModel.getOrderResponseDetails(mBinding.progressBar, intent.getStringExtra("order_id")!!)
        setToolbar()
        serAdapter()
        mBinding.documents.visibility = View.GONE
    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Order Details"
    }

    private fun setFragment() {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.detail_container, DetailsFragment())
        fragmentTransaction.commit()

        getFragment()

    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.detail_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getFragment() {

        mBinding.details.setOnClickListener(this)
        mBinding.products.setOnClickListener(this)
        mBinding.documents.setOnClickListener(this)

    }


    private fun serAdapter() {

        mViewModel.getAllData().observe(this, Observer {
            if (it.size > 0) {
                modelOrderDetail = it
            }
        })

        mViewModel.getTotalData().observe(this, Observer {
            if (it.size > 0) {
                modelTotal = it
            }
        })

        mViewModel.getAddressData().observe(this, Observer {
            modelAddress = it
            setFragment()
        })
    }


    override fun onClick(v: View?) {

        when (v!!.id) {

            R.id.details -> {
                fragment = DetailsFragment()
                loadFragment(fragment)
                position = 0
                mBinding.details.setBackgroundResource(R.drawable.selected_order_back)
                mBinding.products.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.documents.setBackgroundResource(R.drawable.unselected_detail_bg)
                return

            }

            R.id.products -> {
                fragment = ProductFragment()
                loadFragment(fragment)
                position = 1
                mBinding.details.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.products.setBackgroundResource(R.drawable.selected_order_back)
                mBinding.documents.setBackgroundResource(R.drawable.unselected_detail_bg)
                return
            }

            R.id.documents -> {
                fragment = DocumentFragment()
                loadFragment(fragment)
                position = 2
                mBinding.details.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.products.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.documents.setBackgroundResource(R.drawable.selected_order_back)
                return
            }
        }

    }

    override fun getProductData(): ArrayList<ModelOrderDetail> {
        return modelOrderDetail
    }


    override fun getTotalData(): ArrayList<ModelTotal> {
        return modelTotal
    }

    override fun getDocumentData(): ArrayList<ModalDocument> {
        return modelDocument
    }

    override fun getAddressData(): ShippingAddressModel {
        return modelAddress
    }

    override fun getOrderData(): ModelOrderHistory {

        return intent.getParcelableExtra<ModelOrderHistory>("order_information")!!
    }
}
