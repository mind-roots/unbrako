package com.unbrako.e_Commerce.account.orderDetail

import android.os.Parcel
import android.os.Parcelable

class ModelTotal() :Parcelable {
    var title:String=""
    var text:String=""

    constructor(parcel: Parcel) : this() {
        title = parcel.readString()!!
        text = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(text)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelTotal> {
        override fun createFromParcel(parcel: Parcel): ModelTotal {
            return ModelTotal(parcel)
        }

        override fun newArray(size: Int): Array<ModelTotal?> {
            return arrayOfNulls(size)
        }
    }
}