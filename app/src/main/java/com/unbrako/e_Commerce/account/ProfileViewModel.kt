package com.unbrako.e_Commerce.account

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.signup.ModelStatus

class ProfileViewModel(application: Application) : AndroidViewModel(application)  {

    var profileRepo: ProfileRepo = ProfileRepo(application)

    fun mDataProfile() : LiveData<ProfileModel>{

        return profileRepo.mDataProfile()

    }

    fun getProfileResponse(){

        return profileRepo.getResponseData()

    }

    fun getUpdateProfile(progressBar: ProgressBar, profileModel: ProfileModel){
        profileRepo.updateProfile(progressBar, profileModel)
    }

    fun getUpdate():LiveData<Boolean>{
        return profileRepo.readUpdate()
    }

    fun getUpdateProfileStatus() :MutableLiveData<Boolean>{

        return profileRepo.status()
    }

}