package com.unbrako.e_Commerce.account.orderDetail

class OrderDetailsModel {

    var image : Int = 0
    var name : String = ""
    var originalPrice : String = ""
    var discountPrice : String = ""
    var qty : String = ""

}