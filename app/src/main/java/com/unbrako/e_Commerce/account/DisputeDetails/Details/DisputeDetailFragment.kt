package com.unbrako.e_Commerce.account.DisputeDetails.Details


import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.FragmentDisputeDetailBinding
import com.unbrako.e_Commerce.account.DisputeDetails.Products.DisputeProductAdapter


/**
 * A simple [Fragment] subclass.
 */
class DisputeDetailFragment : Fragment() {

    lateinit var mBinding: FragmentDisputeDetailBinding
    lateinit var addressData: DisputeData

    lateinit var mAdapter: DisputeProductAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dispute_detail, container, false)
        setAdapter()
        setHasOptionsMenu(true)
        setData()
        return mBinding.root

    }
    private fun setAdapter() {
        val orderArray: ArrayList<ModelDisputeProductDetail> = addressData.getProductData()
        if (orderArray.size>0){
            mBinding.linearLayout4.visibility=View.VISIBLE
            mBinding.lastView.visibility=View.VISIBLE
        }else{
            mBinding.linearLayout4.visibility=View.GONE
            mBinding.lastView.visibility=View.GONE
        }
        mBinding.recyclerView2.layoutManager = LinearLayoutManager(activity)
        mAdapter = DisputeProductAdapter(activity)
        mBinding.recyclerView2.adapter = mAdapter
        mBinding.recyclerView2.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        mAdapter.update(orderArray)
    }
    private fun setData() {
        val data= addressData.disputeData()
        mBinding.orderId.text=data.id
        mBinding.transID.text=data.trans_no
        mBinding.reason.text=data.reason
        mBinding.amount.text= Constants.formatValues2(data.amount)
        mBinding.create.text=data.created_at
        mBinding.status.text=data.status
    }

    override fun onAttach(context: android.content.Context) {
        super.onAttach(context)
        addressData = context as DisputeData

    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        addressData = activity as DisputeData
    }

    interface DisputeData {

        fun disputeData(): ModelDisputeDetail
        fun getProductData(): java.util.ArrayList<ModelDisputeProductDetail>


    }
}
