package com.unbrako.e_Commerce.account.DisputeDetails

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.account.DisputeDetails.Details.ModelDisputeDetail
import com.unbrako.e_Commerce.account.DisputeDetails.Details.ModelDisputeProductDetail
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.ArrayList

class DisputeDetailRepo(var application: Application) {


    val mData = MutableLiveData<ArrayList<ModelDisputeProductDetail>>()
    val disputeDetail = MutableLiveData<ModelDisputeDetail>()

    fun disputeData(): MutableLiveData<ModelDisputeDetail> {
        return disputeDetail
    }

    fun getAllDisputeDetails(disputeNo: String) {
        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.getDisputeDetails(auth!!, disputeNo)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optBoolean("success")
                    val productList: ArrayList<ModelDisputeProductDetail> = ArrayList()
                    val modelInfo= ModelDisputeDetail()


                    if (success) {
                        val data = json.optJSONObject("data")
                        val invoiceInfo = data.optJSONObject("dispute_info")
                        modelInfo.id = invoiceInfo.optString("id")
                        modelInfo.trans_no = invoiceInfo.optString("trans_no")
                        modelInfo.reason = invoiceInfo.optString("reason")
                        modelInfo.amount = invoiceInfo.optString("amount")
                        modelInfo.created_at = invoiceInfo.optString("created_at")
                        modelInfo.status = invoiceInfo.optString("status")

                        disputeDetail.value = modelInfo
                       val invoiceProduct = data.optJSONArray("dispute_product")



                        for (i in 0 until invoiceProduct.length()) {
                            val invoiceModel =
                                ModelDisputeProductDetail()
                            val obj = invoiceProduct.getJSONObject(i)

                            invoiceModel.sku = obj.optString("sku")
                            invoiceModel.name = obj.optString("name")
                            invoiceModel.price = obj.optString("price")
                            invoiceModel.quantity = obj.optString("quantity")
                            invoiceModel.image = obj.optString("image")
                            invoiceModel.product_id = obj.optString("product_id")
                            invoiceModel.saleable = obj.optString("saleable")
                            invoiceModel.stock = obj.optString("stock")
                            invoiceModel.tax_class_id = obj.optString("tax_class_id")
                            invoiceModel.display_price = obj.optString("display_price")
                            invoiceModel.uom = obj.optString("uom")
                            invoiceModel.line_price = obj.optString("line_price")
                            productList.add(invoiceModel)
                        }
                        mData.value=productList

                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

            }
        })

    }

    fun getProducts(): MutableLiveData<ArrayList<ModelDisputeProductDetail>> {
        return mData
    }
}