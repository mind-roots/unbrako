package com.unbrako.e_Commerce.account.invoiveDetail

import android.app.Application
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class InVoiceDetailViewModel(application: Application) : AndroidViewModel(application) {

    var repo: InvoiceDetailRepo = InvoiceDetailRepo(application)

    fun getInvoiceData(): MutableLiveData<ArrayList<InvoiceModel>> {
        return  repo.getInvoice()
    }

    fun getInvoiceDetail(progressBar: ProgressBar, noData: TextView) {

        repo.inVoiceDetailRepo(progressBar,noData)

    }


}
