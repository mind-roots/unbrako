package com.unbrako.e_Commerce.account.logout

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class LogoutViewModel(application: Application) : AndroidViewModel(application) {

    var refVar: LogoutRepository = LogoutRepository(application)

    fun getLogoutData(authCode: String, progressLogout: ProgressBar){

        return refVar.getItems(authCode, progressLogout)

    }

    fun getmLogoutData() : LiveData<LogoutModel>{

        return refVar.getMData()

    }
}