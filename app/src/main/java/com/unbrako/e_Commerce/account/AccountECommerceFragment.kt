package com.unbrako.e_Commerce.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.e_Commerce.account.logout.LogoutViewModel
import com.unbrako.Constants
import com.unbrako.R
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.unbrako.databinding.FragmentEcommerceAccountBinding
import com.unbrako.e_Commerce.login.LoginActivity


class AccountECommerceFragment : Fragment(), AdapterAccountEcommerce.GoToActivity {

    private lateinit var rvAccount: RecyclerView
    private lateinit var adapterAccountEcommerce: AdapterAccountEcommerce
    private lateinit var logoutViewModel: LogoutViewModel
    lateinit var mBinding: FragmentEcommerceAccountBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ecommerce_account, container, false)

        logoutViewModel = ViewModelProviders.of(this)[LogoutViewModel::class.java]
        setLayout()
        getAdapter()
        return mBinding.root
    }

    private fun setLayout() {

        val manager3 = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        mBinding.rvAccount.layoutManager = manager3
        mBinding.rvAccount.itemAnimator = DefaultItemAnimator()
        mBinding.rvAccount.isNestedScrollingEnabled = false
        mBinding.rvAccount.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))

    }

    private fun getAdapter() {
        adapterAccountEcommerce = AdapterAccountEcommerce(this.activity!!, getSearchList(), this)
        mBinding.rvAccount.adapter = adapterAccountEcommerce
    }

    private fun getSearchList(): ArrayList<ModelAccountEcommerce> {
        val arrayList: ArrayList<ModelAccountEcommerce> = ArrayList()

        var modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_profile
        modelAccountEcommerce.accountText = "Profile"
        arrayList.add(modelAccountEcommerce)

        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_order_history
        modelAccountEcommerce.accountText = "Order History"
        arrayList.add(modelAccountEcommerce)

        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_invoice_history
        modelAccountEcommerce.accountText = "Invoice History"
        arrayList.add(modelAccountEcommerce)

        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_quotation
        modelAccountEcommerce.accountText = "Enquiry History"
        arrayList.add(modelAccountEcommerce)

        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_enquiry
        modelAccountEcommerce.accountText = "New Enquiry"
        arrayList.add(modelAccountEcommerce)


        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_favorite
        modelAccountEcommerce.accountText = "Wishlist"
        arrayList.add(modelAccountEcommerce)


        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_dispute
        modelAccountEcommerce.accountText = "Dispute Center"
        arrayList.add(modelAccountEcommerce)

        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_send_us_feedback
        modelAccountEcommerce.accountText = "Send Us a feedback"
        arrayList.add(modelAccountEcommerce)

        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_share_the_app
        modelAccountEcommerce.accountText = "Share the app"
        arrayList.add(modelAccountEcommerce)

        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_terms_of_use
        modelAccountEcommerce.accountText = "Terms of use"
        arrayList.add(modelAccountEcommerce)

        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_privacy_policy_one
        modelAccountEcommerce.accountText = "Privacy Policy"
        arrayList.add(modelAccountEcommerce)

        modelAccountEcommerce = ModelAccountEcommerce()
        modelAccountEcommerce.accountNext = R.drawable.ic_keyboard_arrow
        modelAccountEcommerce.accountImage = R.drawable.ic_logout_1
        modelAccountEcommerce.accountText = "Logout"
        arrayList.add(modelAccountEcommerce)

        return arrayList
    }

    override fun CallingActivity() {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle("Alert!")
        builder.setMessage("Are you sure you want to logout?")
        builder.setPositiveButton("Ok") { dialog, which ->

            Constants.setLoggedIn(requireActivity(), false)
            val prefs = Constants.getSharedPrefs(requireActivity()).edit()
            prefs.remove("App")
            prefs.apply()
            val auth = Constants.getPrefs(requireActivity()).getString(Constants.token, "")

            logoutViewModel.getLogoutData(auth!!, mBinding.progressLogout)

            logoutViewModel.getmLogoutData().observe(this, Observer {
                Constants.deleteCart(requireActivity()).execute()
                Constants.deleteBasket(requireActivity()).execute()
                val i = Intent(activity, LoginActivity::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
                (activity as Activity).overridePendingTransition(0, 0)
                requireActivity().finish()
                dialog.dismiss()
            })
        }
        builder.setNegativeButton("Cancel") { dialog, which ->

            dialog.dismiss()
        }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()


    }
}