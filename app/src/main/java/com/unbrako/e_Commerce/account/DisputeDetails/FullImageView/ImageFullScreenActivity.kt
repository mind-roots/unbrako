package com.unbrako.e_Commerce.account.DisputeDetails.FullImageView

import android.annotation.SuppressLint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.unbrako.R
import com.unbrako.databinding.ActivityImageFullScreenBinding
import kotlinx.android.synthetic.main.activity_image_full_screen.*
import me.relex.circleindicator.Config

class ImageFullScreenActivity : AppCompatActivity() {

    private lateinit var AdapterFullScreen: FullScreenImageViewAdapter
    private lateinit var mbinding: ActivityImageFullScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_image_full_screen)
        window.setBackgroundDrawable(ColorDrawable(0))
        setToolbar()
        val setData:ArrayList<String> = intent.getStringArrayListExtra("keyImage")!!
        val position = intent.getIntExtra("position", 0)


        val indicatorWidth = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 10f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorHeight = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 4f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorMargin = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 6f,
            resources.displayMetrics
        ) + 0.5f).toInt()


        val config = Config.Builder().width(indicatorWidth)
            .height(indicatorHeight)
            .margin(indicatorMargin)
            .drawable(R.drawable.blac_radius_square)
            .drawableUnselected(R.drawable.login_background_pager)
            .build()
        mbinding.circleIndi1.initialize(config)

        AdapterFullScreen = FullScreenImageViewAdapter(this,setData)

        viewPagerFullScreenView.setPageTransformer(
            true,
            ZoomOutPageTransformer()
        )
        viewPagerFullScreenView!!.adapter = AdapterFullScreen
        mbinding.circleIndi1.setViewPager(viewPagerFullScreenView)
        viewPagerFullScreenView.currentItem = position

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mbinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mbinding.include2.toolbarTitle.text = "Images"
    }
}