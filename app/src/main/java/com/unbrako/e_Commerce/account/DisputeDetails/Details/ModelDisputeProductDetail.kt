package com.unbrako.e_Commerce.account.DisputeDetails.Details

import android.os.Parcel
import android.os.Parcelable

class ModelDisputeProductDetail() : Parcelable {

    var sku: String = ""
    var name: String = ""
    var price: String = ""
    var quantity: String = ""
    var image: String = ""
    var product_id: String = ""
    var saleable: String = ""
    var stock: String = ""
    var tax_class_id: String = ""
    var display_price: String = ""
    var uom: String = ""
    var line_price: String = ""
    var isSelected: Boolean = false

    constructor(parcel: Parcel) : this() {
        sku = parcel.readString()!!
        name = parcel.readString()!!
        price = parcel.readString()!!
        quantity = parcel.readString()!!
        image = parcel.readString()!!
        product_id = parcel.readString()!!
        saleable = parcel.readString()!!
        stock = parcel.readString()!!
        tax_class_id = parcel.readString()!!
        display_price = parcel.readString()!!
        uom = parcel.readString()!!
        line_price = parcel.readString()!!
        isSelected = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(sku)
        parcel.writeString(name)
        parcel.writeString(price)
        parcel.writeString(quantity)
        parcel.writeString(image)
        parcel.writeString(product_id)
        parcel.writeString(saleable)
        parcel.writeString(stock)
        parcel.writeString(tax_class_id)
        parcel.writeString(display_price)
        parcel.writeString(uom)
        parcel.writeString(line_price)
        parcel.writeByte(if (isSelected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelDisputeProductDetail> {
        override fun createFromParcel(parcel: Parcel): ModelDisputeProductDetail {
            return ModelDisputeProductDetail(
                parcel
            )
        }

        override fun newArray(size: Int): Array<ModelDisputeProductDetail?> {
            return arrayOfNulls(size)
        }
    }
}