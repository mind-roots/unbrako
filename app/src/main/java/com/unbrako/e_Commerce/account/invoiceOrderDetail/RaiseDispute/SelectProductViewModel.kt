package com.unbrako.e_Commerce.account.invoiceOrderDetail.RaiseDispute

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.cart.checkOut.ModelStatus
import java.io.File

class SelectProductViewModel(application: Application) : AndroidViewModel(application) {
    fun createDispute(
        invoiceId: String,
        reason: String,
        products: String,
        subtotal2: Double,
        items: Array<File>
    ) {
        repo.creatingDispute(invoiceId,reason,products,subtotal2,items)
    }

    fun getDispute():MutableLiveData<ModelStatus>{

        return repo.getStatus()
    }

    var repo: SelectProductRepo = SelectProductRepo(application)
}