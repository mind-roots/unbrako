package com.unbrako.e_Commerce.account.quatation_orderDetail

import android.annotation.SuppressLint
import android.graphics.Paint
import android.icu.util.ValueIterator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import kotlinx.android.synthetic.main.activity_quatation_order_history.view.*
import kotlinx.android.synthetic.main.order_details_adapter_layout.view.*

class QuatationOrderDetailAdapter(var context: FragmentActivity?) :
    RecyclerView.Adapter<QuatationOrderDetailAdapter.ViewHolder>() {

    private var items = ArrayList<QuatationInfoModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(context).inflate(R.layout.order_details_adapter_layout, parent, false)
        return QuatationOrderDetailAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.itemView.name.text = items[position].description
        holder.itemView.partNo.text = items[position].sku
        holder.itemView.originalPrice.text =
            "₹" + Constants.currencyFormt(items[position].price.toDouble() * (items[position].saleable).toDouble()).toString()
        holder.itemView.discountPrice.text = " per " + items[position].saleable+" "+items[position].uom
        holder.itemView.qtyPiece.text =
            ((items[position].quantity.toDouble()) / (items[position].saleable).toDouble()).toInt().toString()
        Picasso.get().load(items[position].image).placeholder(R.mipmap.ic_launcher).fit().centerCrop()
            .into(holder.itemView.iv_cart)

        val saleable = items[position].saleable.toDouble()
        val quantity = (items[position].quantity.toDouble()) / (items[position].saleable).toDouble()
        val stock = items[position].stock.toDouble()
        if (saleable * quantity > stock) {
            holder.itemView.notavailable.visibility = View.VISIBLE
        } else {
            holder.itemView.notavailable.visibility = View.GONE
        }
        if (items[position].discount_percentage.isNotEmpty()&&items[position].discount_percentage!=".00"&&items[position].discount_percentage!="0.00"){
            holder.itemView.dis.visibility=View.VISIBLE
            holder.itemView.disprice.visibility=View.VISIBLE
            holder.itemView.disprice.text=Constants.currencyFormt(items[position].discount_percentage.toDouble())+"%"
        }else{
            holder.itemView.dis.visibility=View.GONE
            holder.itemView.disprice.visibility=View.GONE
        }

    }

    fun update(it: java.util.ArrayList<QuatationInfoModel>) {
        items = it
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}
