package com.unbrako.e_Commerce.account.orderHistory

import android.app.Application
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class ViewModelOrderHistory(application: Application) :AndroidViewModel(application){
    fun getOrderHistory(progressBar: ProgressBar, noData: TextView) {
        refVal.orderHistoryRepo(progressBar,noData)
    }

    fun getAllOrder() :MutableLiveData<ArrayList<ModelOrderHistory>>{
     return refVal.getAllOrders()
    }

    var refVal:OrderHistoryRepo= OrderHistoryRepo(application)

}