package com.unbrako.e_Commerce.account.quatation_orderDetail

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class QuatationOrderDetailRepo(var application: Application) {

    //step 2
    var mData = MutableLiveData<ArrayList<QuatationInfoModel>>()
    var mDataT = MutableLiveData<ArrayList<ModelQuotationTotal>>()
    var mDatac = MutableLiveData<QuatationCommentModel>()
    var status = MutableLiveData<DeclineModel>()

    fun getQuatationDetail(progress: RelativeLayout, quote_id: String) {

        progress.visibility = View.VISIBLE
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()


        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.quoteInfo(auth!!, quote_id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progress.visibility = View.GONE
                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optBoolean("success")
                    val error = json.optString("error")


                    val list: ArrayList<QuatationInfoModel> = ArrayList()
                    val list2: ArrayList<ModelQuotationTotal> = ArrayList()

                    if (success) {
                        val data = json.optJSONObject("data")
                        val comment = data.optString("comment")
                        val shipping_cost = data.optString("shipping_cost")
                        val total = data.optString("total")
                        val inquiry_id = data.optString("inquiry_id")
                        val user_comment = data.optString("user_comment")
                        val quotation_status = data.optString("quotation_status")
                        val quotation_msg = data.optString("quotation_msg")

                        val products = data.optJSONArray("products")
                        for (i in 0 until products.length()) {
                            val obj = products.getJSONObject(i)
                            val quatationInfoModel = QuatationInfoModel()
                            quatationInfoModel.sku = obj.optString("sku")
                            quatationInfoModel.quantity = obj.optString("quantity")
                            quatationInfoModel.description = obj.optString("description")
                            quatationInfoModel.price = obj.optString("price")
                            quatationInfoModel.saleable = obj.optString("saleable")
                            quatationInfoModel.image = obj.optString("image")
                            quatationInfoModel.product_id = obj.optString("product_id")
                            quatationInfoModel.stock = obj.optString("stock")
                            quatationInfoModel.tax_class_id = obj.optString("tax_class_id")
                            quatationInfoModel.display_price = obj.optString("display_price")
                            quatationInfoModel.discount_percentage = obj.optString("discount_percentage")
                            quatationInfoModel.uom = obj.optString("uom")
                            quatationInfoModel.inquiry_id=inquiry_id
                            quatationInfoModel.comment=comment
                            quatationInfoModel.shipping_cost=shipping_cost
                            quatationInfoModel.user_comment=user_comment
                            quatationInfoModel.quotation_msg=quotation_msg
                            quatationInfoModel.quotation_status=quotation_status
                            quatationInfoModel.total=total

                            list.add(quatationInfoModel)
                        }

                        //step 1
                        mData.value = list
                    }


                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progress.visibility = View.GONE
            }

        })

    }

    fun getDeclineDetail(progress: ProgressBar, quoteId: String, comment: String) {

        progress.visibility = View.VISIBLE
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.declineQuote(auth!!, quoteId, comment)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progress.visibility = View.GONE
                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optBoolean("success")
                    val error = json.optString("error")

                    if (success) {
                        var modle: DeclineModel = DeclineModel()
                        modle.error = error
                        status.value = modle
                    }


                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progress.visibility = View.GONE
            }

        })

    }


    // step 3
    fun getMemData(): MutableLiveData<ArrayList<QuatationInfoModel>> {
        return mData
    }

    fun getTotal(): MutableLiveData<ArrayList<ModelQuotationTotal>> {
        return mDataT
    }

    fun getComment(): MutableLiveData<QuatationCommentModel> {
        return mDatac
    }

    fun getStaus(): MutableLiveData<DeclineModel> {
        return status
    }


}
