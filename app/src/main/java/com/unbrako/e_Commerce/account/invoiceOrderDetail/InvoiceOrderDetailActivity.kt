package com.unbrako.e_Commerce.account.invoiceOrderDetail

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.unbrako.R
import com.unbrako.databinding.InvoiceOrderDetailBinding
import com.unbrako.e_Commerce.account.invoiceOrderDetail.invoicedetail.InvoiceDetailFragment
import com.unbrako.e_Commerce.account.invoiceOrderDetail.invoicedownload.InvoiceDownloadFragment
import com.unbrako.e_Commerce.account.invoiceOrderDetail.invoiceproduct.InvoiceProductFragment


class InvoiceOrderDetailActivity : AppCompatActivity(), View.OnClickListener, InvoiceDetailFragment.AddressData,
    InvoiceProductFragment.ProductData, InvoiceDownloadFragment.DownLoadData {


    lateinit var mBinding: InvoiceOrderDetailBinding
    lateinit var mViewModel: ViewModelInvoiceOrderDetail
    lateinit var fragment: Fragment
    var modelOrderDetail: ArrayList<ModelProductDetail> = ArrayList<ModelProductDetail>()
    var modelOrderCertificate: ArrayList<ModelCertificate> = ArrayList<ModelCertificate>()
    var downLoadList: ArrayList<ModelDownloadDetails> = ArrayList<ModelDownloadDetails>()
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    var position = 0
    var modelInfo = ModelInvoiceHistory()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.invoice_order_detail)
        mViewModel = ViewModelProviders.of(this)[ViewModelInvoiceOrderDetail::class.java]
        mViewModel.getInvoiceResponseDetails(mBinding.progressBar, intent.getStringExtra("invoiceNo"))
        setToolbar()
        setFragment()
        mViewModel.getInvoiceData().observe(this, Observer {
            modelInfo = it
            setFragment()
        })

        mViewModel.getInvoiceProducts().observe(this, Observer {
            if (it.size > 0) {
                modelOrderDetail = it
            }
        })

        mViewModel.getInvoiceCertificate().observe(this, Observer {
            if (it.size > 0) {
                modelOrderCertificate = it
            }
        })

        mViewModel.getDownLoads().observe(this, Observer {
            if (it.size > 0) {
                downLoadList = it
            }
        })
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 21){
            mViewModel.getInvoiceResponseDetails(mBinding.progressBar, intent.getStringExtra("invoiceNo"))
            if (fragment is InvoiceDetailFragment){
                (fragment as InvoiceDetailFragment).showLayout(data!!.getStringExtra("status")!!)
            }

        }
    }
    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Invoice Details"
    }
    private fun setFragment() {
        fragment = InvoiceDetailFragment()
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.detail_container, fragment)
        fragmentTransaction.commit()
        getFragment()
    }

    private fun getFragment() {
        mBinding.details.setOnClickListener(this)
        mBinding.products.setOnClickListener(this)
        mBinding.documents.setOnClickListener(this)
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.detail_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onClick(v: View?) {

        when (v!!.id) {

            R.id.details -> {
                fragment = InvoiceDetailFragment()
                loadFragment(fragment)
                position = 0
                mBinding.details.setBackgroundResource(R.drawable.selected_order_back)
                mBinding.products.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.documents.setBackgroundResource(R.drawable.unselected_detail_bg)
                return
            }
            R.id.products -> {

                fragment = InvoiceProductFragment()
                loadFragment(fragment)
                position = 1
                mBinding.details.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.products.setBackgroundResource(R.drawable.selected_order_back)
                mBinding.documents.setBackgroundResource(R.drawable.unselected_detail_bg)
                return
            }
            R.id.documents -> {
                fragment = InvoiceDownloadFragment()
                loadFragment(fragment)
                position = 2
                mBinding.details.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.products.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.documents.setBackgroundResource(R.drawable.selected_order_back)

                return
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getInvoiceData(): ModelInvoiceHistory {
        return modelInfo
    }

    override fun getProductData(): ArrayList<ModelProductDetail> {
        return modelOrderDetail
    }

    override fun getDownLoadData(): ArrayList<ModelDownloadDetails> {
        return downLoadList
    }

    override fun getCertificateData(): ArrayList<ModelCertificate> {
        return modelOrderCertificate
    }
}