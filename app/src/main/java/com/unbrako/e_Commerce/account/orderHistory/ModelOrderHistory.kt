package com.unbrako.e_Commerce.account.orderHistory

import android.os.Parcel
import android.os.Parcelable

class ModelOrderHistory() : Parcelable {

    var order_id : String = ""
    var name : String = ""
    var status : String = ""
    var date_added : String = ""
    var total : String = ""
    var products : Int =0

    constructor(parcel: Parcel) : this() {
        order_id = parcel.readString()!!
        name = parcel.readString()!!
        status = parcel.readString()!!
        date_added = parcel.readString()!!
        total = parcel.readString()!!
        products = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(order_id)
        parcel.writeString(name)
        parcel.writeString(status)
        parcel.writeString(date_added)
        parcel.writeString(total)
        parcel.writeInt(products)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelOrderHistory> {
        override fun createFromParcel(parcel: Parcel): ModelOrderHistory {
            return ModelOrderHistory(parcel)
        }

        override fun newArray(size: Int): Array<ModelOrderHistory?> {
            return arrayOfNulls(size)
        }
    }

}
