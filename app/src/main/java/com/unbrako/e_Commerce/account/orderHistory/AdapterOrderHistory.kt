package com.unbrako.e_Commerce.account.orderHistory

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.e_Commerce.account.orderDetail.OrderDetailsActivity
import kotlinx.android.synthetic.main.order_history_layout.view.*

class AdapterOrderHistory(var context: Context, var list: ArrayList<ModelOrderHistory>) :
    RecyclerView.Adapter<AdapterOrderHistory.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.order_history_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val intent = Intent(context, OrderDetailsActivity::class.java)
            intent.putExtra("order_id", list[position].order_id)
            intent.putExtra("order_information", list[position])
            context.startActivity(intent)
        }

        holder.itemView.orderId.text = list[position].order_id
        holder.itemView.status.text = list[position].status
        holder.itemView.orderDate.text = list[position].date_added

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}