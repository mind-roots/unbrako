package com.unbrako.e_Commerce.account.quatation_orderDetail

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class ModelQuatationOrderDetail {

    @PrimaryKey
    @ColumnInfo(name = "product_id")
    lateinit var product_id: String


    @ColumnInfo(name = "sku")
    var sku:String=""

   @ColumnInfo(name = "tax_class_id")
    var tax_class_id:String=""

    @ColumnInfo(name = "name")
    var name:String=""

    @ColumnInfo(name = "quantity")
    var quantity:String=""

    @ColumnInfo(name = "stock")
    var stock:String=""

    @ColumnInfo(name = "display_price")
    var display_price:String=""

    @ColumnInfo(name = "saleable")
    var saleable:String=""

    @ColumnInfo(name = "price")
    var price:String=""

    @ColumnInfo(name = "total")
    var total:String=""

    @ColumnInfo(name = "image")
    var image:String=""
@ColumnInfo(name = "uom")
    var uom:String=""

}
