package com.unbrako.e_Commerce.account.DisputeDetails.Messages

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.unbrako.R
import com.unbrako.e_Commerce.account.DisputeDetails.FullImageView.ImageFullScreenActivity
import kotlinx.android.synthetic.main.image_item.view.*

class  ImageChatAdapter (var context: Context) :
RecyclerView.Adapter<ImageChatAdapter.ViewHolder>() {

    var items = ArrayList<String>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.image_chat_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size

    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Picasso.get().load(items[position]).placeholder(R.mipmap.ic_launcher).fit()
            .centerCrop()
            .into(holder.itemView.img_pager)

        holder.itemView.setOnClickListener {

            context.startActivity(Intent(context,ImageFullScreenActivity::class.java).putExtra("keyImage",items)
                .putExtra("position",position))
        }

    }

    fun update(items1: java.util.ArrayList<String>) {

        items=items1
        notifyDataSetChanged()
    }


}



//    (var context: FragmentActivity?) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {
//
//    private var items = ArrayList<ModelDisputeProductDetail>()
//
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val view = LayoutInflater.from(context).inflate(R.layout.invoice_product_layout, parent, false)
//        return ViewHolder(view)
//    }
//
//    override fun getItemCount(): Int {
//        return items.size
//    }
//
//    @SuppressLint("SetTextI18n")
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//
//        holder.itemView.name.text = items[position].name
//        holder.itemView.reason.text = items[position].name
//        holder.itemView.imagesRecycler.layoutManager = LinearLayoutManager(context)
//        mAdapter = DisputeProductAdapter(context)
//        holder.itemView.imagesRecycler.adapter = mAdapter
//
//        mAdapter.update(orderArray)
//
//    }
//
//    fun update(items: ArrayList<ModelDisputeProductDetail>) {
//        this.items = items
//        notifyDataSetChanged()
//    }
//
//    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
//}