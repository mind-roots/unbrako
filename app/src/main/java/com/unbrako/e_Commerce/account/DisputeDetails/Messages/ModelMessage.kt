package com.unbrako.e_Commerce.account.DisputeDetails.Messages

import android.os.Parcel
import android.os.Parcelable
import com.unbrako.e_Commerce.account.DisputeDetails.Details.ModelDisputeProductDetail

class ModelMessage: Parcelable {

    var reason: String = ""
    var created_by: String = ""
    var images: String = ""
    var created_at: String = ""



    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(reason)
        parcel.writeString(created_by)
        parcel.writeString(images)
        parcel.writeString(created_at)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelDisputeProductDetail> {
        override fun createFromParcel(parcel: Parcel): ModelDisputeProductDetail {
            return ModelDisputeProductDetail(
                parcel
            )
        }

        override fun newArray(size: Int): Array<ModelDisputeProductDetail?> {
            return arrayOfNulls(size)
        }
    }
}