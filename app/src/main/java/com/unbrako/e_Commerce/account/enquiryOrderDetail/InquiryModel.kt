package com.unbrako.e_Commerce.account.enquiryOrderDetail

class InquiryModel {

    var stk_code: String = ""
    var quantity: String = ""
    var description: String = ""
    var price: String = ""
    var image: String = ""
    var product_id: String = ""
    var saleable: String = ""
    var comment: String = ""
    var uom: String = ""
}