package com.unbrako.e_Commerce.account.orderDetail.product

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.FragmentProductBinding
import com.unbrako.e_Commerce.account.orderDetail.ModelOrderDetail
import com.unbrako.e_Commerce.account.orderDetail.ModelTotal
import com.unbrako.e_Commerce.account.orderDetail.OrderDetailAdapter
import com.unbrako.e_Commerce.catalog.ModelTax

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class   ProductFragment : Fragment() {


    lateinit var mBinding: FragmentProductBinding
    lateinit var mAdapter: OrderDetailAdapter
    lateinit var productData: ProductData


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product, container, false)

        setHasOptionsMenu(true)
        serAdapter()
        return mBinding.root
    }

    @SuppressLint("SetTextI18n")
    private fun serAdapter() {
        val orderArray: ArrayList<ModelOrderDetail> = productData.getProductData()
        val newArray: ArrayList<ModelOrderDetail> = ArrayList()
        newArray.addAll(orderArray)
        var existCredit=0
        var existCreditPrice=0.0
        for(i in orderArray){
            if (i.sku=="UBR_CREDITS"){
                existCreditPrice=i.price.toDouble()
                newArray.remove(i)
                existCredit=1
                break
            }
        }
        if (existCredit==1){
            mBinding.tvCredit.visibility=View.VISIBLE
            mBinding.tvCreditCost.visibility=View.VISIBLE

            mBinding.tvCreditCost.text="₹" + Constants.formatValues2(existCreditPrice.toString().replace("-",""))
        }else{
            mBinding.tvCredit.visibility=View.GONE
            mBinding.tvCreditCost.visibility=View.GONE
        }
        mBinding.recyclerView2.layoutManager = LinearLayoutManager(activity)
        mAdapter = OrderDetailAdapter(activity)
        mBinding.recyclerView2.adapter = mAdapter
        mBinding.recyclerView2.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        mAdapter.update(newArray)

        val productTotal: ArrayList<ModelTotal> = productData.getTotalData()
        mBinding.tvPriceCost.text = "₹" + Constants.formatValues2(findSubTotal(orderArray, "0",orderArray[0].discountPercent,orderArray[0].delivery_cost,newArray))
        mBinding.tvTaxCost.text = "₹" + Constants.formatValues2(findSubTotal(orderArray, "1",orderArray[0].discountPercent,orderArray[0].delivery_cost,newArray))
        mBinding.amount.text = "₹" + Constants.formatValues2(productTotal[0].text.toDouble().toString())
       if (orderArray[0].delivery_cost=="0"){
           mBinding.tvshippingCost.text="Free"
       }else{
          // mBinding.amount.text = "₹" + Constants.formatValues2((productTotal[0].text.toDouble()+orderArray[0].delivery_cost.toDouble()).toString())
           mBinding.tvshippingCost.text="₹" + Constants.formatValues2(orderArray[0].delivery_cost.toDouble().toString())
       }

            var discount2=0.0
            for(i in 0 until newArray.size){
                if(newArray[i].sku=="UBR_CREDITS")
                {
                   // creditPrice=orderArray[i].price.toDouble()
                }else{
                discount2 +=
                    (newArray[i].quantity.toDouble() / newArray[i].saleable.toDouble() * newArray[i].price.replace("₹", "").replace(
                        ",",
                        ""
                    ).toDouble() * newArray[i].saleable.toDouble()) * newArray[i].discountPercent.toDouble() / 100
            }
            }
            mBinding.tvDiscountCost.text = "₹" + Constants.formatValues2(discount2.toString())
        if (discount2== 0.0){
            mBinding.tvDiscountCost.visibility =View.GONE
            mBinding.tvDiscount.visibility =View.GONE
            mBinding.viewDiscount.visibility =View.GONE
        }else {
            mBinding.tvDiscountCost.visibility =View.VISIBLE
            mBinding.tvDiscount.visibility =View.VISIBLE
            mBinding.viewDiscount.visibility =View.VISIBLE
        }


    }


    @SuppressLint("SetTextI18n")
    private fun findSubTotal(
        orderArray: ArrayList<ModelOrderDetail>,
        taxOrPrice: String,
        couponDiscount: String,
        deliveryCost: String,
        newArray: ArrayList<ModelOrderDetail>
    ): String {

        var price = 0.0
        var price2 = 0.0
        var tax = 0.0
        var tax2 = 0.0
        var discount = 0.0
        var creditPrice=0.0

        for (i in 0 until orderArray.size) {
            // discount calculation started
            if(orderArray[i].sku=="UBR_CREDITS")
            {
                creditPrice=orderArray[i].price.toDouble()
            }else{
             discount =
                (orderArray[i].quantity.toDouble() / orderArray[i].saleable.toDouble() * orderArray[i].price.replace("₹", "").replace(
                    ",",
                    ""
                ).toDouble() * orderArray[i].saleable.toDouble()) * orderArray[0].discountPercent.toDouble() / 100

            price += ((orderArray[i].quantity.toDouble() / orderArray[i].saleable.toDouble()) * orderArray[i].price.replace(
                "₹",
                ""
            ).replace(
                ",",
                ""
            ).toDouble() * orderArray[i].saleable.toDouble())- discount

           // Price calculation new
            price2 += (orderArray[i].quantity.toDouble() / orderArray[i].saleable.toDouble() * orderArray[i].price.replace(
                "₹",
                ""
            ).replace(
                ",",
                ""
            ).toDouble() * orderArray[i].saleable.toDouble())

            tax += getProductTax(
                orderArray[i].tax_class_id,
                ((orderArray[i].quantity.toDouble() / orderArray[i].saleable.toDouble()) * orderArray[i].price.replace(
                    "₹",
                    ""
                ).replace(
                    ",",
                    ""
                ).toDouble() * orderArray[i].saleable.toDouble())- discount
            )

            // Tax calculation new
            tax2 += getProductTax(
                orderArray[i].tax_class_id,
                price
            )

        }
 }


        //price = (price2 + tax2)-(discount2)

        if (taxOrPrice == "1") {

            var discount2=0.0
            var creditPrice1=0.0
            for(i in 0 until orderArray.size) {
                if (orderArray[i].sku == "UBR_CREDITS") {
                    creditPrice1=orderArray[i].price.toDouble()
                } else {
                    discount2 +=
                        (orderArray[i].quantity.toDouble() / orderArray[i].saleable.toDouble() * orderArray[i].price.replace(
                            "₹",
                            ""
                        ).replace(
                            ",",
                            ""
                        ).toDouble() * orderArray[i].saleable.toDouble()) * orderArray[i].discountPercent.toDouble() / 100
                }
            }
            val taxUnbrako = Constants.getSharedPrefs(activity!!).getString("unbrako_tax", "0")!!.toDouble()
            return (((price2-discount2+deliveryCost.toDouble()+creditPrice1)*taxUnbrako/100)).toString()
        }

        return (price2).toString()


    }

    private fun getProductTax(tax_class_id: String, price: Double): Double {
        val mVariable = Gson()
        val response = Constants.getSharedPrefs(activity!!).getString("rate", "")
        val lstArrayList = mVariable.fromJson<ArrayList<ModelTax>>(
            response,
            object : TypeToken<List<ModelTax>>() {

            }.type
        )
        for (i in 0 until lstArrayList.size) {
            if (lstArrayList[i].tax_class_id == tax_class_id) {
                if (lstArrayList[i].rate.isEmpty()) {
                    lstArrayList[i].rate = "0"
                }
                return (price * lstArrayList[i].rate.toDouble()) / 100
            }
        }
        return 0.0
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        productData = context as ProductData
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        productData = activity as ProductData
    }


    interface ProductData {
        fun getProductData(): ArrayList<ModelOrderDetail>
        fun getTotalData(): ArrayList<ModelTotal>
    }
}
