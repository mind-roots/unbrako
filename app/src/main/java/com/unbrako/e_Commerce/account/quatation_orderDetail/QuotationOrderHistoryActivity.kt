package com.unbrako.e_Commerce.account.quatation_orderDetail

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityQuatationOrderHistoryBinding
import com.unbrako.e_Commerce.cart.checkOut.CheckOutActivity
import com.unbrako.e_Commerce.catalog.ModelTax
import com.unbrako.e_Commerce.dataBase.CartModel

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class QuotationOrderHistoryActivity : AppCompatActivity() {

    private var aarr: ArrayList<QuatationInfoModel> = ArrayList()
    lateinit var mBinding: ActivityQuatationOrderHistoryBinding
    lateinit var mViewModel: QuatationOrderHistoryViewModel
    lateinit var adapter: QuatationOrderDetailAdapter
    var cartArray: ArrayList<CartModel> = ArrayList()
    var totalPriceWithGst = 0.0
    var price2 = 0.0
    var shipping = "0"
    var coupon_applied = "0"
    var coupon_amount = "0"
    var modelQuotationOrderDetail: ArrayList<QuatationInfoModel> = ArrayList()
    var discount2 = 0.0
    var subTotalPrice = 0.0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_quatation_order_history)
        mViewModel = ViewModelProviders.of(this).get(QuatationOrderHistoryViewModel::class.java)
        setToolbar()
        placeOrder()
        setAdapter()
        decline()
        setObserver()


    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Quotation Details"
    }

    override fun onResume() {
        super.onResume()
        mViewModel.getQuotationResponseDetails(mBinding.progress, intent.getStringExtra("quote_id")!!)
    }

    private fun setAdapter() {
        val manager = LinearLayoutManager(this)
        adapter = QuatationOrderDetailAdapter(this)
        mBinding.rvQuatationOrderHistory.layoutManager = manager
        mBinding.rvQuatationOrderHistory.adapter = adapter
        mBinding.rvQuatationOrderHistory.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }


    @SuppressLint("SetTextI18n")
    private fun setObserver() {

        mViewModel.getAllData().observe(this, Observer {

            if (it.size > 0) {
                if (it[0].comment.isNotBlank()) {
                    mBinding.comment.text = it[0].comment
                    mBinding.totalAmount1.visibility = View.VISIBLE
                    mBinding.viewPrice2.visibility = View.VISIBLE

                } else {
                    mBinding.totalAmount1.visibility = View.GONE
                    mBinding.viewPrice2.visibility = View.GONE
                }
                aarr = it
                adapter.update(it)
                modelQuotationOrderDetail = it




                if (it[0].quotation_status == "1") {
                    mBinding.buttons.visibility = View.GONE
                    mBinding.status.visibility = View.VISIBLE
                    mBinding.status.text = it[0].quotation_msg
                } else {
                    mBinding.buttons.visibility = View.VISIBLE
                    mBinding.status.visibility = View.GONE
                }


                var price = 0.0
                 price2 = 0.0

                var tax = 0.0
                 discount2 = 0.0
                for (i in 0 until it.size) {
                    it[i].price.replace("₹", "").replace(",", "")
                    val discount =
                        (it[i].quantity.toDouble() / it[i].saleable.toDouble() * it[i].price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() * it[i].saleable.toDouble()) * it[i].discount_percentage.toDouble() / 100
                    discount2 +=
                        (it[i].quantity.toDouble() / it[i].saleable.toDouble() * it[i].price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() * it[i].saleable.toDouble()) * it[i].discount_percentage.toDouble() / 100
                    price += (it[i].quantity.toDouble() / it[i].saleable.toDouble() * it[i].price.replace(
                        "₹",
                        ""
                    ).replace(
                        ",",
                        ""
                    ).toDouble() * it[i].saleable.toDouble()) - discount

                    price2 += (it[i].quantity.toDouble() / it[i].saleable.toDouble() * it[i].price.replace(
                        "₹",
                        ""
                    ).replace(
                        ",",
                        ""
                    ).toDouble() * it[i].saleable.toDouble())

                    tax += getProductTax(
                        it[i].tax_class_id,
                        (it[i].quantity.toDouble() / it[i].saleable.toDouble() * it[i].price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() * it[i].saleable.toDouble()) - discount
                    )
                   /* tax2 += getProductTax(
                        it[i].tax_class_id,
                        price
                    )*/
                }


                mBinding.tvPriceCost.text = "₹" + Constants.currencyFormt(price2)
                val taxUnbrako = Constants.getSharedPrefs(this).getString("unbrako_tax", "0")!!.toDouble()
                if (discount2==0.0){
                    mBinding.discount.visibility=View.GONE
                    mBinding.tvdiscount.visibility=View.GONE
                    coupon_applied="0"
                }else{
                    coupon_applied="1"
                    coupon_amount=discount2.toString()
                    mBinding.discount.visibility=View.VISIBLE
                    mBinding.tvdiscount.visibility=View.VISIBLE
                }
                mBinding.tvdiscount.text = "₹" + Constants.currencyFormt(discount2)
                if (it[0].shipping_cost == "0" || it[0].shipping_cost.isEmpty()) {

                    mBinding.tvShippingCost.text = "Free"
                    mBinding.tvTaxCost.text = "₹" + Constants.currencyFormt((price2-discount2)*taxUnbrako/100)
                    mBinding.amount.text = "₹" + Constants.currencyFormt(((price2-discount2) + (price2-discount2)*taxUnbrako/100))

                    totalPriceWithGst =((price2-discount2) + (price2-discount2)*taxUnbrako/100)
                } else {
                      mBinding.tvShippingCost.text = "₹" + Constants.currencyFormt(it[0].shipping_cost.toDouble())
var hh=((price2-discount2) + it[0].shipping_cost.toDouble()) * taxUnbrako/ 100
                    mBinding.tvTaxCost.text =
                        "₹" + Constants.currencyFormt(((price2-discount2) + it[0].shipping_cost.toDouble()) * taxUnbrako/ 100)

                    mBinding.amount.text =
                        "₹" + Constants.currencyFormt(((price2-discount2) + it[0].shipping_cost.toDouble())+(((price2-discount2) + it[0].shipping_cost.toDouble()) * taxUnbrako/ 100))
//                    totalPriceWithGst =
//                        price + tax + it[0].shipping_cost.toDouble() + it[0].shipping_cost.toDouble() * 18 / 100
                    shipping = it[0].shipping_cost
                    totalPriceWithGst = (price2-discount2)+ it[0].shipping_cost.toDouble() +(((price2-discount2) + it[0].shipping_cost.toDouble()) * taxUnbrako/ 100)
                }
                subTotalPrice=((price2-discount2) + it[0].shipping_cost.toDouble())
            }

        })

        mViewModel.getTotalData().observe(this, Observer {

        })

        mViewModel.getCommentData().observe(this, Observer {

            mBinding.comment.text = it.comment
            if (it.admin_comment.isNotBlank()) {
                mBinding.totalAmount3.visibility = View.VISIBLE
                mBinding.ucomment.text = it.admin_comment
            } else {

                mBinding.totalAmount3.visibility = View.GONE
            }

        })
    }

    private fun getProductTax(tax_class_id: String, price: Double): Double {
        val gson = Gson()
        val response = Constants.getSharedPrefs(this).getString("rate", "")
        val lstArrayList = gson.fromJson<ArrayList<ModelTax>>(
            response,
            object : TypeToken<List<ModelTax>>() {

            }.type
        )
        for (i in 0 until lstArrayList.size) {
            if (lstArrayList[i].tax_class_id == tax_class_id) {
                if (lstArrayList[i].rate.isEmpty()) {
                    lstArrayList[i].rate = "0"
                }
                return (price * lstArrayList[i].rate.toDouble()) / 100
            }
        }
        return 0.0
    }

    private fun placeOrder() {
        mBinding.placeOrder.setOnClickListener {

            for (i in 0 until aarr.size) {
                val model = CartModel()
                model.sku = aarr[i].sku
                model.quantity = aarr[i].quantity
                model.selectQuant = ((aarr[i].quantity.toDouble()) / (aarr[i].saleable.toDouble()).toInt()).toString()
                model.description = aarr[i].description
                model.price = aarr[i].price
                model.saleable = aarr[i].saleable
                model.image = aarr[i].image
                model.product_id = aarr[i].product_id
                model.display_price = aarr[i].display_price
                model.tax_class_id = aarr[i].tax_class_id
                model.discount_percentage = aarr[i].discount_percentage
                cartArray.add(model)
            }
            startActivity(
                Intent(this, CheckOutActivity::class.java).putExtra(
                    "amount",
                    mBinding.amount.text.toString()
                ).putExtra(
                    "array",
                    cartArray
                ).putExtra(
                    "fullTotal",
                    totalPriceWithGst.toString()
                )
                    .putExtra(
                        "comment",
                        mBinding.comment.text.toString()
                        //mBinding.editText.text.toString()
                    ).putExtra(
                        "quotation_no",
                        intent.getStringExtra("quote_id")
                    ).putExtra(
                        "shipping_cost",
                        shipping
                    ).putExtra(
                        "coupon_applied",
                        coupon_applied
                    ).putExtra(
                        "coupon_amount",
                       coupon_amount.toString()
                    ).putExtra(
                        "couponDiscount",
                        (0.0).toString()
                    ).putExtra(
                        "quotationCheck",
                       "d"
                    ).putExtra(
                        "price",
                        subTotalPrice.toString()
//                        price2.toString()
                    )
            )
        }
    }

    private fun decline() {
        mBinding.decline.setOnClickListener {
            showDialogee()
        }

    }

    private fun showDialogee() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.cutsom_layoutfordecline)
        val body = dialog.findViewById(R.id.quotationaddComment) as EditText
        val yesBtn = dialog.findViewById(R.id.submit) as TextView
        val progress = dialog.findViewById(R.id.progress) as ProgressBar



        yesBtn.setOnClickListener {
            Constants.hideSoftKeyboard(yesBtn, this)
            if (body.text.toString().isEmpty()) {
                Toast.makeText(this, "Please add comments", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            mViewModel.getdeclineRespose(progress, intent.getStringExtra("quote_id")!!, body.text.toString())
            mViewModel.getDeclineStaus().observe(this, Observer {
                Toast.makeText(this, it.error, Toast.LENGTH_SHORT).show()
                dialog.dismiss()
                finish()
            })


        }
        dialog.show()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}