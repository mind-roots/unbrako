package com.unbrako.e_Commerce.account.wishlist

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.FragmentEcommerceWishlistBinding
import com.unbrako.e_Commerce.catalog.AdapterMainCatalog
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import com.unbrako.e_Commerce.wishlist.AdapterWishListEco
import com.unbrako.e_Commerce.wishlist.ViewModelWishList


class WishlistActivity : AppCompatActivity(), AdapterWishListEco.BlankView,
    AdapterWishListEco.WishListApiCall {

    var array: ArrayList<ModelWishList> = ArrayList<ModelWishList>()

    private lateinit var adapterWishlistEco: AdapterWishListEco
    private lateinit var mAdapterMainCatalog: AdapterMainCatalog
    lateinit var mBinding: FragmentEcommerceWishlistBinding
    lateinit var viewModel: ViewModelWishList
    lateinit var menuItem: Menu
    var edit = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.fragment_ecommerce_wishlist)
        clickEvents()
        viewModel = ViewModelProviders.of(this)[ViewModelWishList::class.java]
        val manager3 = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        mBinding.rvWishList.layoutManager = manager3
        mBinding.rvWishList.itemAnimator = DefaultItemAnimator()
        mBinding.rvWishList.isNestedScrollingEnabled = false
        mBinding.rvWishList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        adapterWishlistEco = AdapterWishListEco(this!!, this)
        mBinding.rvWishList.adapter = adapterWishlistEco

        setToolbar()
        mBinding.bottomBack.visibility = View.GONE
    }


    override fun onResume() {
        super.onResume()
        viewModel.getWishList(adapterWishlistEco, mBinding.progressBar, mBinding.noData)
        viewModel.getData().observe(this, Observer {
            array = it
            if (it.size > 0) {

                menuItem.findItem(R.id.editWishlist).isVisible = true
                mBinding.noData.visibility = View.GONE
                adapterWishlistEco.update(it)

            } else {
                menuItem.findItem(R.id.editWishlist).isVisible = false
                mBinding.noData.visibility = View.VISIBLE
            }

        })
    }

    override fun blankView() {
        mBinding.noData.visibility = View.VISIBLE
    }
    override fun addToCartLayout(model: FetchProductsModel, position: Int, spinnerDes: String) {

            val gson = Gson()
            val innerJson = gson.toJson(model)
            val obj = gson.fromJson(innerJson, CartModel::class.java)
            CheckIfExistInCartList(obj, this, spinnerDes).execute()

    }

    @SuppressLint("StaticFieldLeak")
    inner class CheckIfExistInCartList(
        var model: CartModel,
        var context: Context,
        var spinnerDes: String
    ) : AsyncTask<Void, Void, Void>() {
        var exist = 0
        override fun doInBackground(vararg params: Void?): Void? {
            val db = Constants.getDataBase(context)
            val cartItems: ArrayList<CartModel> =
                db!!.productDao().getAllCart() as ArrayList<CartModel>

            for (j in 0 until cartItems.size) {
                if (model.product_id == cartItems[j].product_id) {
                    exist = 1
                }
            }
            return null
        }
        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (exist == 0) {
                model.quantity = spinnerDes
                Constants.AddToCart(context, true, model).execute()
                showCustomAlert("Added to Cart")
                Constants.getProductsOfCart(this@WishlistActivity).execute()
            } else {
                showCustomAlert("Already Added in Cart")
            }
        }
    }

    fun showCustomAlert(msg: String) {
        val context = this
        val inflater = layoutInflater
        // Call toast.xml file for toast layout
        val toast1 = inflater.inflate(R.layout.toast, null)
        val textm: TextView = toast1.findViewById(R.id.text)
        textm.text = msg
        val toast = Toast(context)
        // Set layout to toast
        toast.view = toast1
        toast.setGravity(
            Gravity.BOTTOM,
            0, 0
        )
        toast.duration = Toast.LENGTH_LONG
        toast.show()
    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Wishlist"
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        if (item!!.itemId == R.id.editWishlist) {
            if (edit == 0) {
                edit = 1
                mBinding.bottomBack.visibility = View.VISIBLE
                adapterWishlistEco.getEditableList(true)
                menuItem.findItem(R.id.editWishlist).title = "Cancel"
            } else {
                edit = 0
                mBinding.bottomBack.visibility = View.GONE
                adapterWishlistEco.getEditableList(false)
                menuItem.findItem(R.id.editWishlist).title = "Edit"
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.wishlist_edit_delete, menu)
        menuItem = menu
        return true
    }
    override fun updateSelection(b: Boolean, position: Int) {
        if (array.size > 0) {
            array[position].isChecked = b
            adapterWishlistEco.notifyDataSetChanged()
        }
    }
    private fun clickEvents() {

        mBinding.markAll.setOnClickListener {
            mBinding.markAll.visibility = View.GONE
            mBinding.UnMarkAll.visibility = View.VISIBLE
            for (i in 0 until array.size) {
                array[i].isChecked = true
            }
            mBinding.UnMarkAll.setTextColor(resources.getColor(R.color.buttonColor))
            adapterWishlistEco.update(array)
        }
        mBinding.UnMarkAll.setOnClickListener {
            mBinding.markAll.visibility = View.VISIBLE
            mBinding.UnMarkAll.visibility = View.GONE
            mBinding.markAll.setTextColor(Color.parseColor("#000000"))

            for (i in 0 until array.size) {
                array[i].isChecked = false
            }
            adapterWishlistEco.update(array)
        }
        mBinding.remove.setOnClickListener {

            var array2: ArrayList<ModelWishList> = ArrayList<ModelWishList>()
            for (i in 0 until array.size) {
                if (array[i].isChecked) {
                    Constants.WishListApi(this, array[i])
                } else {
                    array2.add(array[i])
                }
            }
            array.clear()
            array.addAll(array2)
            if (array.size == 0) {
                mBinding.bottomBack.visibility = View.GONE
                menuItem.findItem(R.id.editWishlist).isVisible = false
                mBinding.noData.visibility = View.VISIBLE
            } else {
                mBinding.markAll.visibility = View.VISIBLE
                mBinding.UnMarkAll.visibility = View.GONE
            }
            adapterWishlistEco.update(array)

        }
    }


}