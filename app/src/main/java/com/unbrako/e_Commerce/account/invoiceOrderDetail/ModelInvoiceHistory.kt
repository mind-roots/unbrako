package com.unbrako.e_Commerce.account.invoiceOrderDetail

import android.os.Parcel
import android.os.Parcelable

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ModelInvoiceHistory() :Parcelable{
    var discountPrice: String =""
    var invoice_no: String = ""
    var created_at: String = ""
    var order_no: String = ""
    var sub_total: String = ""
    var gst: String = ""
    var total: String = ""
    var is_disputed: String = ""
    var credit: String = ""

    constructor(parcel: Parcel) : this() {
        discountPrice = parcel.readString()!!
        invoice_no = parcel.readString()!!
        created_at = parcel.readString()!!
        order_no = parcel.readString()!!
        sub_total = parcel.readString()!!
        gst = parcel.readString()!!
        total = parcel.readString()!!
        is_disputed = parcel.readString()!!
        credit = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(discountPrice)
        parcel.writeString(invoice_no)
        parcel.writeString(created_at)
        parcel.writeString(order_no)
        parcel.writeString(sub_total)
        parcel.writeString(gst)
        parcel.writeString(total)
        parcel.writeString(is_disputed)
        parcel.writeString(credit)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelInvoiceHistory> {
        override fun createFromParcel(parcel: Parcel): ModelInvoiceHistory {
            return ModelInvoiceHistory(parcel)
        }

        override fun newArray(size: Int): Array<ModelInvoiceHistory?> {
            return arrayOfNulls(size)
        }
    }


}