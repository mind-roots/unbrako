package com.unbrako.e_Commerce.account.DisputeList

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.e_Commerce.account.DisputeDetails.DisputeDetailsActivity
import kotlinx.android.synthetic.main.dispute_item.view.*


class AdapterDispute(var context: Context) : RecyclerView.Adapter<AdapterDispute.ViewHolder>() {
    var list: ArrayList<DisputeModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.dispute_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val intent = Intent(context, DisputeDetailsActivity::class.java)
            intent.putExtra("disputeNo", list[position].id)
            context.startActivity(intent)
        }

        holder.itemView.orderId.text = list[position].id
       // holder.itemView.orderId.text = list[position].trans_no
        holder.itemView.status.text = list[position].status
        holder.itemView.orderDate.text = list[position].created_at

    }

    fun update(it: java.util.ArrayList<DisputeModel>) {
        list = it
        notifyDataSetChanged()

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}

