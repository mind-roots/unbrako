package com.unbrako.e_Commerce.account.DisputeDetails

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.unbrako.R
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.unbrako.databinding.ActivityDisputeDetailsBinding
import com.unbrako.e_Commerce.account.DisputeDetails.Details.DisputeDetailFragment
import com.unbrako.e_Commerce.account.DisputeDetails.Details.ModelDisputeDetail
import com.unbrako.e_Commerce.account.DisputeDetails.Messages.DisputeMessageFragment
import com.unbrako.e_Commerce.account.DisputeDetails.Details.ModelDisputeProductDetail

class DisputeDetailsActivity : AppCompatActivity(), View.OnClickListener ,
    DisputeDetailFragment.DisputeData , DisputeMessageFragment.ProductData {


    lateinit var mBinding: ActivityDisputeDetailsBinding
    lateinit var mViewModel: DisputeDetailViewModel
    lateinit var fragment: Fragment
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    var position=0
    var modelOrderDetail= ModelDisputeDetail()
    var productDetail: ArrayList<ModelDisputeProductDetail> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dispute_details)
        mViewModel = ViewModelProviders.of(this)[DisputeDetailViewModel::class.java]

        setToolbar()
        setFragment()
        mBinding.progressBar.visibility=View.VISIBLE
        mViewModel.getAllDetails(intent.getStringExtra("disputeNo")!!)
        mViewModel.getDisputeData().observe(this, Observer {
            mBinding.progressBar.visibility=View.GONE
            modelOrderDetail = it
            setFragment()
        })

        mViewModel.getDisputeProducts().observe(this, Observer {
            if (it.size > 0) {
                productDetail = it
            }
            mBinding.progressBar.visibility=View.GONE
        })
    }

    override fun disputeData(): ModelDisputeDetail {
        return modelOrderDetail
    }

    override fun getProductData(): ArrayList<ModelDisputeProductDetail> {
        return productDetail
    }

    override fun getDisputeID(): String {
        return intent.getStringExtra("disputeNo")!!
    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Dispute Details"
    }
    private fun setFragment() {
        fragment = DisputeDetailFragment()
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.detail_container, fragment)
        fragmentTransaction.commit()
        getFragment()
    }

    private fun getFragment() {
        mBinding.details.setOnClickListener(this)
        mBinding.products.setOnClickListener(this)

    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.detail_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onClick(v: View?) {

        when (v!!.id) {

            R.id.details -> {
                fragment = DisputeDetailFragment()
                loadFragment(fragment)
                position = 0
                mBinding.details.setBackgroundResource(R.drawable.selected_order_back)
                mBinding.products.setBackgroundResource(R.drawable.unselected_detail_bg)
                return
            }
            R.id.products -> {

                fragment = DisputeMessageFragment()
                loadFragment(fragment)
                position = 1
                mBinding.details.setBackgroundResource(R.drawable.unselected_detail_bg)
                mBinding.products.setBackgroundResource(R.drawable.selected_order_back)
                return
            }

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
