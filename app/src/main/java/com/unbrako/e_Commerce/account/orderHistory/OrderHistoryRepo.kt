package com.unbrako.e_Commerce.account.orderHistory

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.account.ProfileModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class OrderHistoryRepo(var application: Application) {

    val mData = MutableLiveData<ProfileModel>()
    val mDataH = MutableLiveData<ArrayList<ModelOrderHistory>>()




    fun orderHistoryRepo(progressBar: ProgressBar, noData: TextView) {
        getOrderHistoryResponse(progressBar, noData)

    }

    private fun getOrderHistoryResponse(progressBar: ProgressBar, noData: TextView) {


        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.MyOrdersV1(auth!!)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optString("success")
                    val arayH: ArrayList<ModelOrderHistory> = ArrayList()

                    if (success == "true") {
                        val orders = json.getJSONArray("orders")
                        //val pagination = json.getJSONObject("pagination    ")


                        for (i in 0 until orders.length()) {
                            var jsonO = orders.optJSONObject(i)
                            val modelOrderHistory = ModelOrderHistory()
                            modelOrderHistory.order_id = jsonO.optString("order_id")
                            modelOrderHistory.name = jsonO.optString("name")
                            modelOrderHistory.status = jsonO.optString("status")
                            modelOrderHistory.date_added = jsonO.optString("date_added")
                            //    modelOrderHistory.products = jsonO.optInt("products")
                            modelOrderHistory.total = jsonO.optString("total")
                            arayH.add(modelOrderHistory)
                        }
                    }
                    if (arayH.size > 0) {
                        noData.visibility = View.GONE
                    } else {
                        noData.visibility = View.VISIBLE
                    }
                    mDataH.value = arayH

//                    mData.value = profileModel


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.GONE
                noData.visibility = View.VISIBLE
            }
        })
    }


    fun getAllOrders(): MutableLiveData<ArrayList<ModelOrderHistory>> {

        return mDataH
    }

}