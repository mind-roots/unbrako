package com.unbrako.e_Commerce.account.DisputeDetails.Products

import android.annotation.SuppressLint
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.e_Commerce.account.DisputeDetails.Details.ModelDisputeProductDetail
import kotlinx.android.synthetic.main.invoice_product_layout.view.*

class DisputeProductAdapter(var context : FragmentActivity?) : RecyclerView.Adapter<DisputeProductAdapter.ViewHolder>() {

    private var items = ArrayList<ModelDisputeProductDetail>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.invoice_product_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.name.text = items[position].name
        holder.itemView.partNo.text = items[position].sku
        holder.itemView.qtyPiece.text = (items[position].quantity.toDouble()).toString()
        holder.itemView.originalPrice.text = "₹" + (Constants.currencyFormt(
            items[position].price.replace(
                "₹",
                ""
            ).toDouble() * items[position].saleable.toDouble()
        ))
        holder.itemView.discountPrice.text = " per " + Constants.formatValues2(items[position].saleable)+" "+items[position].uom
        if (items[position].display_price.toDouble() > 0) {
            holder.itemView.off.text =
                "₹" + Constants.formatValues2((items[position].display_price.toDouble() * items[position].saleable.toDouble()).toString())
            holder.itemView.cutOff.visibility = View.VISIBLE
        } else {
            holder.itemView.cutOff.visibility = View.GONE
        }
        Picasso.get().load(items[position].image).placeholder(R.mipmap.ic_launcher).fit().centerCrop()
            .into(holder.itemView.iv_cart)
        holder.itemView.off.paintFlags = holder.itemView.off.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    }

    fun update(items: ArrayList<ModelDisputeProductDetail>) {
        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}