package com.unbrako.e_Commerce.account.enquiry_history

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.ArrayList

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class RepoEnquiryHistory(var application: Application) {

    val mData = MutableLiveData<ArrayList<EnquiryModel>>()


    fun quotationDetailRepo(progressBar: ProgressBar, noData: TextView) {

        getQuotationResponse(progressBar, noData)
    }

    private fun getQuotationResponse(progressBar: ProgressBar, noData: TextView) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.getInquires(auth!!)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optBoolean("success")


                    val list: ArrayList<EnquiryModel> = ArrayList()


                    if (success) {
                        val inquires = json.optJSONArray("inquires")
                        for (i in 0 until inquires.length()) {
                            val quotesModel = EnquiryModel()
                            val obj = inquires.getJSONObject(i)
                            quotesModel.inquiry_no = obj.optString("inquiry_no")
                            quotesModel.status = obj.optString("status")
                            quotesModel.created_at = obj.optString("created_at")
                            list.add(quotesModel)
                        }
                    }
                    if (list.size > 0) {
                        noData.visibility = View.GONE
                    } else {
                        noData.visibility = View.VISIBLE
                    }
                    mData.value = list

                } catch (e: Exception) {
                    noData.visibility = View.VISIBLE
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                progressBar.visibility = View.GONE
                noData.visibility = View.VISIBLE
            }
        })
    }

    fun getAllQuotes(): MutableLiveData<ArrayList<EnquiryModel>> {
        return mData
    }


}
