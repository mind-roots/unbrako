package com.unbrako.e_Commerce.account

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.signup.ModelStatus
import com.unbrako.e_Commerce.signup.SignUpModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ProfileRepo(var application: Application) {

    private val mUpdate = MutableLiveData<Boolean>()
    val mData = MutableLiveData<ProfileModel>()
    val mDataS = MutableLiveData<ModelStatus>()


    fun mDataProfile(): LiveData<ProfileModel> {

        return mData
    }

    fun getResponseData() {

        return getProfileResponse()

    }

    fun readUpdate(): LiveData<Boolean> {
        return mUpdate
    }

    private fun getProfileResponse() {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.getProfile(auth!!)
        //service.getProfile(auth)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
//
                        if (success) {


                            val signUpObj = json.getJSONObject("profile")
                            val editorJson = Constants.getPrefs(application).edit()
                            editorJson.putString("signUpObj", signUpObj.toString())
                            editorJson.apply()
                            val loginModel = SignUpModel()
                            loginModel.customer_id = signUpObj.optString("customer_id")

                            loginModel.customer_group_id = signUpObj.optString("customer_group_id")
                            loginModel.store_id = signUpObj.optString("store_id")
                            loginModel.language_id = signUpObj.optString("language_id")
                            loginModel.firstname = signUpObj.optString("firstname")
                            loginModel.lastname = signUpObj.optString("lastname")
                            loginModel.email = signUpObj.optString("email")
                            loginModel.telephone = signUpObj.optString("telephone")
                            loginModel.fax = signUpObj.optString("fax")
                            loginModel.password = signUpObj.optString("password")
                            loginModel.salt = signUpObj.optString("salt")
                            loginModel.cart = signUpObj.optString("cart")
                            loginModel.wishlist = signUpObj.optString("wishlist")
                            loginModel.newsletter = signUpObj.optString("newsletter")
                            loginModel.address_id = signUpObj.optString("address_id")
                            loginModel.custom_field = signUpObj.optString("custom_field")
                            loginModel.ip = signUpObj.optString("ip")
                            loginModel.status = signUpObj.optString("status")
                            loginModel.safe = signUpObj.optString("safe")
                            loginModel.token = signUpObj.optString("token")
                            loginModel.code = signUpObj.optString("code")
                            loginModel.date_added = signUpObj.optString("date_added")
                            loginModel.company_name = signUpObj.optString("company_name")
                            loginModel.gst = signUpObj.optString("gst")
                            loginModel.landline = signUpObj.optString("landline")
                            loginModel.customer_type = signUpObj.optString("customer_type")
                            val gson = Gson()
                            val json1 = gson.toJson(loginModel)
                            Constants.getPrefs(application).edit().putString("profile", json1).apply()
                        }
                    } catch (e: Exception) {
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })
    }

    fun updateProfile(progressBar: ProgressBar, profileModel: ProfileModel) {
        updateProfileResponse(progressBar, profileModel)
    }

    private fun updateProfileResponse(progressBar: ProgressBar, profileModel: ProfileModel) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.updateProfile(
            auth!!,
            profileModel.company_name,
            profileModel.gst,
            profileModel.firstname,
            profileModel.lastname,
            profileModel.email,
            profileModel.telephone,
            profileModel.password,
            profileModel.landline
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optString("success")
                    mUpdate.value = success == "true"

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.VISIBLE
            }
        })
    }

    fun status(): MutableLiveData<Boolean> {

        return mUpdate
    }
}