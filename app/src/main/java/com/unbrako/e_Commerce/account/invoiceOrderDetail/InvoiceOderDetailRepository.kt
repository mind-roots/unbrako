package com.unbrako.e_Commerce.account.invoiceOrderDetail

import android.app.Application
import android.view.View
import android.widget.RelativeLayout
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class InvoiceOderDetailRepository(var application: Application) {
    val mData = MutableLiveData<ArrayList<ModelProductDetail>>()
    val downLoad = MutableLiveData<ArrayList<ModelDownloadDetails>>()
    val certificateM = MutableLiveData<ArrayList<ModelCertificate>>()
    val mDataInfo = MutableLiveData<ModelInvoiceHistory>()
    fun getInvoiceDetails(invoiceNo: String?, progressBar: RelativeLayout) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.getInvoiceDetails(auth!!, invoiceNo!!)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE

                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optBoolean("success")
                    val list: ArrayList<ModelProductDetail> = ArrayList()
                    val listDownload: ArrayList<ModelDownloadDetails> = ArrayList()
                    val listCertificate: ArrayList<ModelCertificate> = ArrayList()

                    val modelInfo = ModelInvoiceHistory()

                    if (success) {
                        val data = json.optJSONObject("data")
                        val invoiceInfo = data.optJSONObject("invoice_info")
                        modelInfo.created_at = invoiceInfo.optString("created_at")
                        modelInfo.sub_total = invoiceInfo.optString("sub_total")
                        modelInfo.gst = invoiceInfo.optString("gst")
                        modelInfo.order_no = invoiceInfo.optString("order_no")
                        modelInfo.invoice_no = invoiceInfo.optString("invoice_no")
                        modelInfo.total = invoiceInfo.optString("total")
                        modelInfo.discountPrice = invoiceInfo.optString("discount_price")
                        modelInfo.is_disputed = invoiceInfo.optString("is_disputed")
                        modelInfo.credit = invoiceInfo.optString("credit")
                        mDataInfo.value = modelInfo
                        val invoiceProduct = data.optJSONArray("invoice_product")
                        val invoice_documents = data.optJSONArray("invoice_documents")
                        val certificate = data.optJSONArray("certificate")

                        for (j in 0 until invoice_documents.length()) {
                            val modelDownloadDetails = ModelDownloadDetails()
                            val obj = invoice_documents.getJSONObject(j)
                            modelDownloadDetails.pdf = obj.optString("pdf")
                            modelDownloadDetails.title = obj.optString("title")
                            listDownload.add(modelDownloadDetails)
                        }
                        downLoad.value = listDownload

                        for (i in 0 until invoiceProduct.length()) {
                            val invoiceModel = ModelProductDetail()
                            val obj = invoiceProduct.getJSONObject(i)

                            invoiceModel.sku = obj.optString("sku")
                            invoiceModel.name = obj.optString("name")
                            invoiceModel.price = obj.optString("price")
                            invoiceModel.quantity = obj.optString("quantity")
                            invoiceModel.image = obj.optString("image")
                            invoiceModel.product_id = obj.optString("product_id")
                            invoiceModel.saleable = obj.optString("saleable")
                            invoiceModel.stock = obj.optString("stock")
                            invoiceModel.tax_class_id = obj.optString("tax_class_id")
                            invoiceModel.display_price = obj.optString("display_price")
                            invoiceModel.uom = obj.optString("uom")
                            invoiceModel.discount_percent = obj.optString("discount_percent")
                            list.add(invoiceModel)
                        }
                        for (i in 0 until certificate.length()) {
                            val invoiceModelq = ModelCertificate()
                            val objq = certificate.getJSONObject(i)

                            invoiceModelq.title = objq.optString("title")
                            invoiceModelq.pdf = objq.optString("pdf")

                            listCertificate.add(invoiceModelq)
                        }
                        certificateM.value = listCertificate
                    }

                    mData.value = list

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.GONE
                // noData.visibility = View.VISIBLE
            }
        })
    }

    fun getInvoiceData(): MutableLiveData<ModelInvoiceHistory> {
        return mDataInfo
    }
 fun getInvoiceCertificate(): MutableLiveData<java.util.ArrayList<ModelCertificate>> {
        return certificateM
    }

    fun getInvoiceProduct(): MutableLiveData<java.util.ArrayList<ModelProductDetail>> {
        return mData
    }

    fun getDownLoadData(): MutableLiveData<java.util.ArrayList<ModelDownloadDetails>> {
        return downLoad
    }

}
