package com.unbrako.e_Commerce.account.invoiceOrderDetail

import android.app.Application
import android.widget.RelativeLayout
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class ViewModelInvoiceOrderDetail(application: Application):AndroidViewModel(application) {
    fun getInvoiceResponseDetails(progressBar: RelativeLayout, invoiceNo: String?) {

        repo.getInvoiceDetails(invoiceNo,progressBar)
    }

    fun getInvoiceData(): MutableLiveData<ModelInvoiceHistory> {
        return  repo.getInvoiceData()

    }
 fun getInvoiceCertificate(): MutableLiveData<ArrayList<ModelCertificate>> {
        return  repo.getInvoiceCertificate()

    }

    fun getInvoiceProducts(): MutableLiveData<ArrayList<ModelProductDetail>> {

        return  repo.getInvoiceProduct()
    }

    fun getDownLoads(): MutableLiveData<ArrayList<ModelDownloadDetails>> {
        return  repo.getDownLoadData()
    }

    var repo : InvoiceOderDetailRepository = InvoiceOderDetailRepository(application)
}
