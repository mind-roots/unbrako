package com.unbrako.e_Commerce.account.invoiveDetail

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class InvoiceDetailRepo(var application: Application) {

    val mData = MutableLiveData<ArrayList<InvoiceModel>>()

    fun inVoiceDetailRepo(progressBar: ProgressBar, noData: TextView) {
        getInvoiceResponse(progressBar, noData)
    }

    private fun getInvoiceResponse(progressBar: ProgressBar, noData: TextView) {
        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        progressBar.visibility = View.VISIBLE

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.myInvoice(auth!!)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressBar.visibility = View.GONE
                val list: ArrayList<InvoiceModel> = ArrayList()
                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optBoolean("success")

                    if (success) {
                        val data = json.optJSONArray("data")
                        for (i in 0 until data.length()) {
                            val invoiceModel = InvoiceModel()
                            val obj = data.getJSONObject(i)
                            invoiceModel.invoice_no = obj.optString("invoice_no")
                            invoiceModel.order_no = obj.optString("order_no")
                            invoiceModel.created_at = obj.optString("created_at")
                            invoiceModel.total = obj.optString("total")
                            list.add(invoiceModel)
                        }
                    }
                    if (list.size > 0) {
                        noData.visibility = View.GONE
                    } else {
                        noData.visibility = View.VISIBLE
                    }
                    mData.value = list

                } catch (e:Exception) {
                    if (list.size > 0) {
                        noData.visibility = View.GONE
                    } else {
                        noData.visibility = View.VISIBLE
                    }
                    e.printStackTrace()
                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressBar.visibility = View.GONE
                noData.visibility = View.VISIBLE
            }

        })
    }

        fun getInvoice(): MutableLiveData<ArrayList<InvoiceModel>> {
            return mData
        }


    }



