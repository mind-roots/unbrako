package com.unbrako.e_Commerce.account.enquiryOrderDetail.product

import android.os.Parcel
import android.os.Parcelable

class ProductQuoteModel() :Parcelable {
    var quote_id: String = ""
    var status: String = ""
    var date_added: String = ""

    constructor(parcel: Parcel) : this() {
        quote_id = parcel.readString()!!
        status = parcel.readString()!!
        date_added = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(quote_id)
        parcel.writeString(status)
        parcel.writeString(date_added)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductQuoteModel> {
        override fun createFromParcel(parcel: Parcel): ProductQuoteModel {
            return ProductQuoteModel(parcel)
        }

        override fun newArray(size: Int): Array<ProductQuoteModel?> {
            return arrayOfNulls(size)
        }
    }
}