package com.unbrako.e_Commerce.account.invoiveDetail

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.e_Commerce.account.invoiceOrderDetail.InvoiceOrderDetailActivity
import kotlinx.android.synthetic.main.invoice_detail_adapter_layout.view.*

class AdapterInvoiceDetail(var context: Context) : RecyclerView.Adapter<AdapterInvoiceDetail.ViewHolder>() {
    var list: ArrayList<InvoiceModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.invoice_detail_adapter_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val intent = Intent(context, InvoiceOrderDetailActivity::class.java)
            intent.putExtra("invoiceNo", list[position].invoice_no)
            context.startActivity(intent)
        }

        holder.itemView.orderId.text = list[position].invoice_no
        holder.itemView.status.text = list[position].order_no
        holder.itemView.orderDate.text = list[position].created_at

    }

    fun update(it: java.util.ArrayList<InvoiceModel>) {
        list = it
        notifyDataSetChanged()

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}

