package com.unbrako.e_Commerce.account.orderDetail

import android.app.Application
import android.view.View
import android.widget.RelativeLayout
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RepoOderDetails(var application: Application) {


    var productsArray: ArrayList<ModelOrderDetail> = ArrayList<ModelOrderDetail>()
    var mData = MutableLiveData<ArrayList<ModelOrderDetail>>()
    var mDataDocument = MutableLiveData<ArrayList<ModalDocument>>()
    var mDataAddress = MutableLiveData<ShippingAddressModel>()
    var mDataT = MutableLiveData<ArrayList<ModelTotal>>()


    fun getItems() {

        mData.value = productsArray

    }

//    fun getMData() : MutableLiveData<ArrayList<ModelOrderDetail>> {
//
//        return mData
//    }


    fun getOrderDetail(progress: RelativeLayout, order_id: String) {

        progress.visibility = View.VISIBLE
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.orderInfo(auth!!, order_id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progress.visibility = View.GONE
                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optBoolean("success")
                    val order_info = json.optJSONObject("order_info")

//                    val order_id = order_info.optString("order_id")
//                    val status = order_info.optString("status")
//                    val date_added = order_info.optString("date_added")
                    val total = order_info.optString("total")
                    val shipping_address = order_info.optString("shipping_address")
                    val coupon_discount = order_info.optString("coupon_discount")
                    val delivery_cost = order_info.optString("delivery_cost")


                    val array: ArrayList<ModelOrderDetail> = ArrayList()
                    if (success) {

                        val products = json.getJSONArray("products")
                        for (i in 0 until products.length()) {
                            val jsonO = products.optJSONObject(i)
                            val modelOrderDetail = ModelOrderDetail()
                            modelOrderDetail.sku = jsonO.optString("sku")
                            modelOrderDetail.name = jsonO.optString("name")
                            modelOrderDetail.price = jsonO.optString("price")
                            modelOrderDetail.quantity = jsonO.optString("quantity")
                            modelOrderDetail.image = jsonO.optString("image")
                            modelOrderDetail.saleable = jsonO.optString("saleable")
                            modelOrderDetail.stock = jsonO.optString("stock")
                            modelOrderDetail.display_price = jsonO.optString("display_price")
                            modelOrderDetail.product_id = jsonO.optString("product_id")
                            modelOrderDetail.tax_class_id = jsonO.optString("tax_class_id")
                            modelOrderDetail.discountPercent = jsonO.optString("discount_percentage")
                            modelOrderDetail.uom = jsonO.optString("uom")
                            modelOrderDetail.coupon_discount = coupon_discount
                            modelOrderDetail.delivery_cost = delivery_cost

                            array.add(modelOrderDetail)
                            
                        }

                        //  val documnet: ArrayList<ModalDocument> = ArrayList()

                        //  val products = json.optJSONArray("products")
                        //  val totals = json.optJSONArray("totals")
                        // val documents = json.optJSONArray("documents")
                        //  val shipping_address = json.optJSONObject("shipping_address")
//                        for (i in 0 until products.length()) {
//
//                            val modelOrderDetail = ModelOrderDetail()
//                            val jj = products.optJSONObject(i)
//                            modelOrderDetail.name = jj.optString("name")
//                            modelOrderDetail.model = jj.optString("model")
//                            modelOrderDetail.option = jj.optString("option")
//                            modelOrderDetail.quantity = jj.optString("quantity")
//                            modelOrderDetail.stock = jj.optString("stock")
//                            modelOrderDetail.display_price = jj.optString("display_price")
//                            modelOrderDetail.saleable = jj.optString("saleable")
//                            modelOrderDetail.price = jj.optString("price")
//                            modelOrderDetail.total = jj.optString("total")
//                            modelOrderDetail.image = jj.optString("image")
//                            modelOrderDetail.sku = jj.optString("sku")
//
//                            array.add(modelOrderDetail)
//
//                        }
//                        for( p in 0 until totals.length()){
//                            val tt=totals.optJSONObject(p)
//                            val model=ModelTotal()
//                            model.text=tt.optString("text")
//                            model.title=tt.optString("title")
//                            subtot.add(model)
                    }
//                        for (j in 0 until documents.length()){
//                            val documentData = documents.optJSONObject(j)
//                            val model = ModalDocument()
//                            model.id = documentData.optString("id")
//                            model.order_id = documentData.optString("order_id")
//                            model.name = documentData.optString("name")
//                            model.image = documentData.optString("image")
//                            model.date_added = documentData.optString("date_added")
//                            documnet.add(model)
//                        }

                    val model = ShippingAddressModel()
                    model.firstname = shipping_address

//                            model.lastname = shipping_address.optString("lastname")
//                            model.company = shipping_address.optString("company")
//                            model.address_1 = shipping_address.optString("address_1")
//                            model.address_2 = shipping_address.optString("address_2")
//                            model.city = shipping_address.optString("city")
//                            model.postcode = shipping_address.optString("postcode")
//                            model.zone = shipping_address.optString("zone")
//                            model.zone_code = shipping_address.optString("zone_code")
//                            model.country = shipping_address.optString("country")
                    val subtot: ArrayList<ModelTotal> = ArrayList()
                    val model2 = ModelTotal()
                    model2.text = total
                    //model.title=tt.optString("title")
                    subtot.add(model2)

                    mDataT.value = subtot
                    mData.value = array
                    // mDataDocument.value = documnet
                    mDataAddress.value = model
                } else {

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progress.visibility = View.GONE
            }


        })
    }


    fun getMemData(): MutableLiveData<ArrayList<ModelOrderDetail>> {
        return mData
    }

    fun getTotal(): MutableLiveData<ArrayList<ModelTotal>> {

        return mDataT
    }


//    fun getDocument(): MutableLiveData<ArrayList<ModalDocument>> {
//
//        return mDataDocument
//    }


    fun getAddress(): MutableLiveData<ShippingAddressModel> {

        return mDataAddress
    }


}