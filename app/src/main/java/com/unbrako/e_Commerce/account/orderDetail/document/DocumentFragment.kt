package com.unbrako.e_Commerce.account.orderDetail.document

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.R
import com.unbrako.databinding.FragmentDocumnetBinding
import com.unbrako.e_Commerce.account.orderDetail.ModalDocument
import kotlin.collections.ArrayList

class DocumentFragment : Fragment() {

    lateinit var mAdapter: DocumentsAdapter
    lateinit var mBinding: FragmentDocumnetBinding
    private lateinit var documentData: DocumentData


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_documnet, container, false)

        setHasOptionsMenu(true)
        serAdapter()

        return mBinding.root
    }

    private fun serAdapter() {

        val documentArray: ArrayList<ModalDocument> = documentData.getDocumentData()

        mBinding.recyclerView2.layoutManager = LinearLayoutManager(activity)
        mAdapter = DocumentsAdapter(activity)
        mBinding.recyclerView2.adapter = mAdapter
        mBinding.recyclerView2.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        mAdapter.update(documentArray)

    }

    override fun onAttach(context: android.content.Context) {
        super.onAttach(context)
        documentData = context as DocumentData

    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        documentData = activity as DocumentData
    }


    interface DocumentData{

    fun getDocumentData( ) :ArrayList<ModalDocument>

    }
}