package com.unbrako.e_Commerce.account.enquiryOrderDetail.product

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.R
import com.unbrako.databinding.FragProdBinding
import com.unbrako.e_Commerce.account.enquiryOrderDetail.QuotModel

@Suppress("DEPRECATION")
class ProdFragment : Fragment() {

    lateinit var mBinding: FragProdBinding
    lateinit var adapter: AdapterProduct
    lateinit var productData: ProductData


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        mBinding = DataBindingUtil.inflate(inflater, R.layout.frag_prod, container, false)
        setAdapter()
        return mBinding.root
    }

    private fun setAdapter() {
        val orderArray: ArrayList<QuotModel> = productData.getProductData()
        if (orderArray.size > 0) {
            val manager = LinearLayoutManager(activity)
            adapter = AdapterProduct(activity)
            mBinding.rvQuatationDetail.layoutManager = manager
            mBinding.rvQuatationDetail.adapter = adapter
            mBinding.rvQuatationDetail.addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter.update(orderArray)
            mBinding.noData.visibility = View.GONE
        } else {
            //no data view
            mBinding.noData.visibility = View.VISIBLE
        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        productData = context as ProductData
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        productData = activity as ProductData
    }

    interface ProductData {
        fun getProductData(): ArrayList<QuotModel>

    }

}