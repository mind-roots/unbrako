package com.unbrako.e_Commerce.account.enquiryOrderDetail.detail

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.R
import com.unbrako.databinding.FragmentDetailBinding
import com.unbrako.e_Commerce.account.enquiryOrderDetail.InquiryModel

@Suppress("DEPRECATION")
class DetailFragment : Fragment() {

    lateinit var mBinding: FragmentDetailBinding
    private lateinit var addressData: AddressData
    lateinit var adapter: AdapterDetail

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)

        setHasOptionsMenu(true)
        setAdapter()
        return mBinding.root
    }

    private fun setAdapter() {
        val orderArray: ArrayList<InquiryModel> = addressData.getAddressData()

        if (orderArray.size > 0) {
            val manager = LinearLayoutManager(activity)
            adapter = AdapterDetail(activity)
            mBinding.rvQuatationOrderHistory.layoutManager = manager
            mBinding.rvQuatationOrderHistory.adapter = adapter
            adapter.update(orderArray)
            mBinding.noData.visibility = View.GONE
            if (orderArray[0].comment.isNotBlank()) {
                mBinding.comment.text = orderArray[0].comment
                mBinding.totalAmount1.visibility = View.VISIBLE
            } else {
                mBinding.totalAmount1.visibility = View.GONE
            }
            mBinding.rvQuatationOrderHistory.addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )

        } else {
            mBinding.noData.visibility = View.VISIBLE
        }

    }

    override fun onAttach(context: android.content.Context) {
        super.onAttach(context)
        addressData = context as AddressData

    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        addressData = activity as AddressData
    }


    interface AddressData {
        fun getAddressData(): ArrayList<InquiryModel>
    }
}