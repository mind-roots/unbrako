package com.unbrako.e_Commerce.account.orderDetail

import android.app.Application
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class ViewModelOrderDetail (application: Application) : AndroidViewModel(application)  {

    var catalogRepository : RepoOderDetails = RepoOderDetails(application)


    fun getOrderResponseDetails(progress: RelativeLayout, order_id: String
    ) {
        catalogRepository.getOrderDetail(progress,order_id)
    }



fun getAllData() : MutableLiveData<ArrayList<ModelOrderDetail>>{
        return catalogRepository.getMemData()
    }

    fun getTotalData(): MutableLiveData<ArrayList<ModelTotal>> {
        return catalogRepository.getTotal()

    }
//
//    fun getDocumentData(): MutableLiveData<ArrayList<ModalDocument>> {
//        return catalogRepository.getDocument()
//
//    }
//
    fun getAddressData(): MutableLiveData<ShippingAddressModel> {
        return catalogRepository.getAddress()

    }

}