package com.unbrako.e_Commerce.account.DisputeDetails

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.account.DisputeDetails.Details.ModelDisputeDetail
import com.unbrako.e_Commerce.account.DisputeDetails.Details.ModelDisputeProductDetail

class DisputeDetailViewModel(application: Application) : AndroidViewModel(application) {
    fun getDisputeData(): MutableLiveData<ModelDisputeDetail> {
return repo.disputeData()
    }

    fun getAllDetails(stringExtra: String) {
        repo.getAllDisputeDetails(stringExtra)
    }

    fun getDisputeProducts(): MutableLiveData<ArrayList<ModelDisputeProductDetail>> {
        return  repo.getProducts()
    }

    var repo: DisputeDetailRepo = DisputeDetailRepo(application)
}