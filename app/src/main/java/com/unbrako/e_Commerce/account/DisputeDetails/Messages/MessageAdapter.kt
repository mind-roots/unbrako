package com.unbrako.e_Commerce.account.DisputeDetails.Messages

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.e_Commerce.signup.SignUpModel
import kotlinx.android.synthetic.main.dispute_product_layout.view.*
import java.util.*
import kotlin.collections.ArrayList

class MessageAdapter(var context: Context) : RecyclerView.Adapter<MessageAdapter.ViewHolder>() {

    private var items = ArrayList<ModelMessage>()

    lateinit var mAdapter : ImageChatAdapter
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.dispute_product_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (items[position].created_by=="0"){
            holder.itemView.name.text = "Unbrako Team"

        }else{
            val mVariable = Gson()
            val json = Constants.getPrefs(context).getString("profile", "")
            val profileData: SignUpModel = mVariable.fromJson(json, SignUpModel::class.java)
            holder.itemView.name.text = profileData.firstname
        }

        holder.itemView.reason.text = items[position].reason
        holder.itemView.date.text = "Created at: "+items[position].created_at
        holder.itemView.imagesRecycler.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        mAdapter = ImageChatAdapter(context)
        holder.itemView.imagesRecycler.adapter = mAdapter


        val gson = Gson()
        val arrayData: ArrayList<String> =
            gson.fromJson(
                items[position].images,
                object : TypeToken<ArrayList<String>>() {}.type
            )
       // arrayData.reverse()
        mAdapter.update(arrayData)

    }

    fun update(items: ArrayList<ModelMessage>) {
        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
