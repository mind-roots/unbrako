package com.unbrako.e_Commerce.account.DisputeDetails.Messages

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.unbrako.e_Commerce.cart.checkOut.ModelStatus
import java.io.File

class ViewModelMessage(application: Application) : AndroidViewModel(application) {
    fun getMessages(disputeID:String) {

        repo.getMessage(disputeID)
    }

    fun getData():MutableLiveData<ArrayList<ModelMessage>> {

        return  repo.getData()
    }

    fun addDisputeMessage(
        disputeID: String,
        text: String,
        items: Array<File>
    ) {

        repo.addMessage(disputeID,text,items)
    }

    fun addDisputeMsg(): MutableLiveData<ModelStatus> {
    return repo.messageSent()
    }
    var repo: MessageRepo = MessageRepo(application)
}