package com.unbrako.e_Commerce.account.enquiryOrderDetail

import android.app.Application
import android.view.View
import android.widget.RelativeLayout
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.ArrayList

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class QuadOrderRepo(var application: Application) {

    var nData = MutableLiveData<ArrayList<InquiryModel>>()
    var nData1 = MutableLiveData<ArrayList<QuotModel>>()

    fun inquiryInfo(progress: RelativeLayout, inquiryNo: String) {
        inquiryInfoResponse(progress, inquiryNo)
    }

    private fun inquiryInfoResponse(progress: RelativeLayout, inquiryNo: String) {

        progress.visibility = View.VISIBLE
        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()


        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.getInquiryInfo(auth!!, inquiryNo)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progress.visibility = View.GONE
                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optBoolean("success")

                    val list: ArrayList<InquiryModel> = ArrayList()
                    val list1: ArrayList<QuotModel> = ArrayList()

                    if (status) {

                        val data = json.optJSONObject("data")
                        val comment = data.optString("comment")
                        val inquiry = data.optJSONArray("inquiry")
                        for (i in 0 until inquiry.length()) {

                            val obj = inquiry.getJSONObject(i)
                            val inquiryModel = InquiryModel()
                            inquiryModel.stk_code = obj.optString("stk_code")
                            inquiryModel.quantity = obj.optString("quantity")
                            inquiryModel.description = obj.optString("description")
                            inquiryModel.price = obj.optString("price")
                            inquiryModel.image = obj.optString("image")
                            inquiryModel.product_id = obj.optString("product_id")
                            inquiryModel.saleable = obj.optString("saleable")
                            inquiryModel.uom = obj.optString("uom")
                            inquiryModel.comment = comment
                            list.add(inquiryModel)

                        }
                        try {
                            val quotation = data.optJSONArray("quotation")

                            for (i in 0 until quotation.length()) {
                                val obj = quotation.getJSONObject(i)
                                val quotModel = QuotModel()
                                quotModel.quotation_no = obj.optString("quotation_no")
                                quotModel.status = obj.optString("status")
                                quotModel.created_at = obj.optString("created_at")
                                list1.add(quotModel)
                            }
                        } catch (e: Exception) {

                        }
                        nData.value = list
                        nData1.value = list1

                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progress.visibility = View.GONE
            }
        })

    }


    fun getInquiryData(): MutableLiveData<ArrayList<InquiryModel>> {
        return nData
    }

    fun getQuotData(): MutableLiveData<ArrayList<QuotModel>> {
        return nData1
    }


}
