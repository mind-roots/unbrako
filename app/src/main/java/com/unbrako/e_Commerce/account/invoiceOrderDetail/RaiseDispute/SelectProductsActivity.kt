package com.unbrako.e_Commerce.account.invoiceOrderDetail.RaiseDispute

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivitySelectProductsBinding
import com.unbrako.databinding.OpenFromCameraPermissionBinding
import com.unbrako.e_Commerce.account.invoiceOrderDetail.ModelProductDetail
import com.unbrako.e_Commerce.catalog.ModelTax
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@Suppress(
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class SelectProductsActivity : AppCompatActivity() {

    lateinit var mBinding: ActivitySelectProductsBinding
    lateinit var mAdapter: ProductsArrayAdapter
    lateinit var mAdapter1: ImageAdapter
    lateinit var mViewModel: SelectProductViewModel
    var items = ArrayList<ModelImages>()
    private var photoFile: File? = null
    var check: Int = -1
    private var listFile = arrayOf<String>()
    private var listFilefie = arrayOf<File>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_products)

        mViewModel = ViewModelProviders.of(this)[SelectProductViewModel::class.java]
        setToolbar()
        deleteFromSdcard()
        val orderArray: ArrayList<ModelProductDetail> =
            intent.getParcelableArrayListExtra("productArray")!!
        mBinding.recyclerView2.layoutManager = LinearLayoutManager(this)
        mAdapter = ProductsArrayAdapter(this)
        mBinding.recyclerView2.adapter = mAdapter

        mBinding.recyclerView2.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )
        mAdapter.update(orderArray)
        clicks()
        mBinding.imageRecycler.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true)
        mAdapter1 = ImageAdapter(this)
        mBinding.imageRecycler.adapter = mAdapter1
        mViewModel.getDispute().observe(this, Observer {
            mBinding.progressBar.visibility = View.GONE
            if (it.msg == "Dispute raised") {
                deleteFromSdcard()
                Toast.makeText(this, "Dispute raised Successfully.", Toast.LENGTH_LONG).show()
                val intent = Intent()
                intent.putExtra("status", "On Progress")
                setResult(21, intent)
                finish()
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun deleteFromSdcard() {
        try {
            val file = File(Environment.getExternalStorageDirectory(), "req_images")

            if (file.isDirectory) {
                listFile = file.list()
                for (element in listFile) {
                    File(file, element).delete()
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getFromSdcard() {

        try {
            val file = File(Environment.getExternalStorageDirectory(), "req_images")

            if (file.isDirectory) {
                listFilefie = file.listFiles()

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            photoFile = createImageFile()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        if (photoFile != null) {
            val uri = FileProvider.getUriForFile(
                this,
                "com.unbrako.app" + ".provider",
                photoFile!!
            )
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(intent, 29)
        }

    }

    @SuppressLint("IntentReset")
    private fun openGallery() {

        val intent = Intent()
        intent.action = Intent.ACTION_PICK
        intent.type = "image/*"
        startActivityForResult(intent, 1)
    }

    @SuppressLint("SimpleDateFormat")
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES
        )
        return File.createTempFile(
            imageFileName,  // prefix
            ".jpg",         // suffix
            storageDir      // directory
        )
    }

    private fun showDialog() {

        val builder: androidx.appcompat.app.AlertDialog.Builder =
            androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setTitle("Select")
        val mBinding: OpenFromCameraPermissionBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(this),
                R.layout.open_from_camera_permission,
                null,
                false
            )

        if (check == 0) {
            mBinding.camera.isChecked = true
            mBinding.gallery.isChecked = false
        }
        if (check == 1) {
            mBinding.gallery.isChecked = false
            mBinding.camera.isChecked = true
        }
        builder.setView(mBinding.root)
        builder.setCancelable(false)

        builder.setPositiveButton("Ok") { dialog, which ->

            if (mBinding.camera.isChecked) {
                openCamera()
                check = 0


            } else if (mBinding.gallery.isChecked) {
                openGallery()
                check = 1

            }
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialog, which ->

        }
        builder.create()
        builder.show()

    }

    private fun getImageUri(inImage: Bitmap): File {
        val root: String = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/req_images")
        myDir.mkdirs()
        val fName = "Image_profile" + System.currentTimeMillis() + ".jpg"
        val file = File(myDir, fName)
        if (file.exists())
            file.delete()

        try {
            val out = FileOutputStream(file)
            inImage.compress(Bitmap.CompressFormat.JPEG, 60, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return file
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        try {
            if (requestCode == 29) {
                //   val contentURI: Bitmap = data.extras.get("data") as Bitmap
                try {

                    val resultBitmap = getRotateImage(photoFile)
                    val uri = getImageUri(resultBitmap)
//..................................Api FOr Update Image....................................................................//


                    val model = ModelImages()
                    model.image = uri
                    items.add(model)
                    mAdapter1.update(items)


                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
//2nd code
            else if (requestCode == 1 && data != null) {
                try {

                    val selectedPicture: Uri = data.data!!

                    val file: File =
                        uploadImage2(getBitmapImage(selectedPicture, this))
                    val model = ModelImages()
                    model.image = file
                    items.add(model)
                    mAdapter1.update(items)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if (requestCode == 2 && data != null) {
                try {
                    Toast.makeText(this, "lll!", Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    private fun uploadImage2(
        bitmap: Bitmap
    ): File {
        return getImageUri35(bitmap)
    }


    private fun getImageUri35(inImage: Bitmap): File {

        val root: String = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/req_images")

        val fName = "Image_profile" + System.currentTimeMillis() + ".jpg"
        val file = File(myDir, fName)
        if (file.exists())
            file.delete()
        myDir.mkdirs()
        try {

            val out = FileOutputStream(file)

            inImage.compress(Bitmap.CompressFormat.JPEG, 60, out)

            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return file
    }


    private fun checkPermissionForCameraGallery(context: Activity): Boolean {
        when {
            ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED -> {
                ActivityCompat.requestPermissions(
                    context, arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), 102
                )
                return false
            }
            ActivityCompat.checkSelfPermission(
                context, Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED -> {
                ActivityCompat.requestPermissions(
                    context, arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA

                    ), 102
                )
                return false
            }
            else -> {
                return true
            }
        }
    }

    private fun rotateBitmap(bitmap: Bitmap, degrees: Int): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degrees.toFloat())
        return Bitmap.createBitmap(
            bitmap,
            0,
            0,
            bitmap.width,
            bitmap.height,
            matrix,
            true
        )
    }

    private fun getRotateImage(photoFile: File?): Bitmap {

        val ei = ExifInterface(photoFile!!.path)
        val orientation = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        val bitmap = BitmapFactory.decodeFile(photoFile.path)

        var rotatedBitmap: Bitmap? = bitmap
        when (orientation) {

            ExifInterface.ORIENTATION_ROTATE_90 ->
                rotatedBitmap = TransformationUtils.rotateImage(bitmap, 90)


            ExifInterface.ORIENTATION_ROTATE_180 ->
                rotatedBitmap = TransformationUtils.rotateImage(bitmap, 180)


            ExifInterface.ORIENTATION_ROTATE_270 ->
                rotatedBitmap = TransformationUtils.rotateImage(bitmap, 270)


            ExifInterface.ORIENTATION_NORMAL ->
                rotatedBitmap = bitmap
        }

        return rotatedBitmap!!
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 102) {
            if (!grantResults.contains(-1)) {
                deleteFromSdcard()
                showDialog()
            }
        }
    }

    private fun getBitmapImage(selectedPicture: Uri, context: Context): Bitmap {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = context.contentResolver.query(
            selectedPicture,
            filePathColumn,
            null,
            null,
            null
        )
        cursor!!.moveToFirst()

        val columnIndex = cursor.getColumnIndex(filePathColumn[0])
        val picturePath = cursor.getString(columnIndex)
        cursor.close()
        var exif: ExifInterface? = null
        var loadedBitmap = BitmapFactory.decodeFile(picturePath)


        try {
            val pictureFile = File(picturePath)
            exif = ExifInterface(pictureFile.absolutePath)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        var orientation = ExifInterface.ORIENTATION_NORMAL

        if (exif != null)
            orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )

        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 ->
                loadedBitmap = rotateBitmap(loadedBitmap, 90)

            ExifInterface.ORIENTATION_ROTATE_180 ->
                loadedBitmap = rotateBitmap(loadedBitmap, 180)


            ExifInterface.ORIENTATION_ROTATE_270 ->
                loadedBitmap = rotateBitmap(loadedBitmap, 270)

        }

        return loadedBitmap
    }


    private fun clicks() {
        mBinding.pickImage.setOnClickListener {
            if (checkPermissionForCameraGallery(this)) {
                try {
                    val file = File(Environment.getExternalStorageDirectory(), "req_images")

                    if (file.isDirectory) {
                        listFilefie = file.listFiles()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (listFilefie.size < 5) {
                    showDialog()
                } else {
                    Toast.makeText(this, "You cannot upload more than 5 images.", Toast.LENGTH_LONG)
                        .show()
                }
            }

        }
        mBinding.disputeButton.setOnClickListener {
            askUserAction()

        }
    }

    private fun askUserAction() {

        val orders: ArrayList<ModelProductDetail> = mAdapter.getSelectedItems()
        val invoiceId: String = intent.getStringExtra("invoiceId")!!
        if (orders.size > 0) {
            var id = ""
            for (i in orders) {
                if (id.isEmpty()) {
                    id = i.product_id
                } else {
                    id += "," + i.product_id
                }
            }
            if (mBinding.disputeReason.text.toString().isEmpty()) {
                Toast.makeText(this, "Please give reason to raise dispute.", Toast.LENGTH_LONG)
                    .show()
            } else {
                val reason = mBinding.disputeReason.text.toString()
                val jsonArray2 = JSONArray()
                var subtotal2 = 0.0
                var tax = 0.0
                for (i in 0 until orders.size) {
                    val jsonObject = JSONObject()

                    jsonObject.put("id", orders[i].product_id)
                    jsonObject.put("qty", (orders[i].quantity.toDouble())).toString()
                    jsonObject.put("price", orders[i].price)
                    jsonObject.put(
                        "tax",
                        getProductTax(
                            orders[i].tax_class_id,
                            orders[i].price.toDouble() * (orders[i].quantity.toDouble()) -
                                    orders[i].price.toDouble() * (orders[i].quantity.toDouble()) * ((orders[i].discount_percent.toDouble() * 100) / 100)
                        ).toString()
                    )
                    jsonObject.put(
                        "total",
                        ((orders[i].price.toDouble() * (orders[i].quantity.toDouble()) -
                                orders[i].price.toDouble() * (orders[i].quantity.toDouble()) * ((orders[i].discount_percent.toDouble() * 100) / 100)) + getProductTax(
                            orders[i].tax_class_id,
                            orders[i].price.toDouble() * (orders[i].quantity.toDouble()) -
                                    orders[i].price.toDouble() * (orders[i].quantity.toDouble()) * ((orders[i].discount_percent.toDouble() * 100) / 100)
                        )).toString()
                    )
                    jsonArray2.put(jsonObject)

                    tax += getProductTax(
                        orders[i].tax_class_id,
                        orders[i].price.toDouble() * ((orders[i].quantity.toDouble()))
                    )

                    subtotal2 += ((orders[i].price.toDouble() * (orders[i].quantity.toDouble()) -
                            orders[i].price.toDouble() * (orders[i].quantity.toDouble()) * ((orders[i].discount_percent.toDouble() * 100) / 100)) + getProductTax(
                        orders[i].tax_class_id,
                        orders[i].price.toDouble() * (orders[i].quantity.toDouble()) -
                                orders[i].price.toDouble() * (orders[i].quantity.toDouble()) * ((orders[i].discount_percent.toDouble() * 100) / 100)
                    ))

                }
                getFromSdcard()
                val products = jsonArray2.toString()
                mBinding.progressBar.visibility = View.VISIBLE
                var listFileList = arrayOf<File>()
                try {
                    val file = File(Environment.getExternalStorageDirectory(), "req_images")

                    if (file.isDirectory) {
                        listFileList = file.listFiles()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                mViewModel.createDispute(invoiceId, reason, products, subtotal2, listFileList)

                Constants.hideSoftKeyboard(mBinding.progressBar, this)
            }

        } else {
            Toast.makeText(
                this,
                "Please select product(s) to raise dispute.",
                Toast.LENGTH_LONG
            ).show()

        }

    }

    private fun getProductTax(tax_class_id: String, price: Double): Double {
        val mVariable = Gson()
        val response = Constants.getSharedPrefs(this).getString("rate", "")
        val lstArrayList = mVariable.fromJson<ArrayList<ModelTax>>(
            response,
            object : TypeToken<List<ModelTax>>() {

            }.type
        )
        for (i in 0 until lstArrayList.size) {
            if (lstArrayList[i].tax_class_id == tax_class_id) {
                if (lstArrayList[i].rate.isEmpty()) {
                    lstArrayList[i].rate = "0"
                }

                return (price * lstArrayList[i].rate.toDouble()) / 100

            }
        }
        return 0.0
    }


    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Raise Disputes"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)

    }
}
