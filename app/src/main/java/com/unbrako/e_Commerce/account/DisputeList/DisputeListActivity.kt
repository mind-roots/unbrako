package com.unbrako.e_Commerce.account.DisputeList

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.R
import com.unbrako.databinding.ActivityDisputeListBinding

class DisputeListActivity : AppCompatActivity() {


    lateinit var mBinding: ActivityDisputeListBinding
    lateinit var mViewModel: DisputeViewModel
    lateinit var adapter: AdapterDispute


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dispute_list)
        mViewModel = ViewModelProviders.of(this).get(DisputeViewModel::class.java)
        mBinding.progressBar.visibility = View.VISIBLE
        setToolbar()
        setAdapter()
        mViewModel.getDisputeData()

        mViewModel.getStatus().observe(this, Observer {
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
            mBinding.noData.visibility = View.VISIBLE
            mBinding.progressBar.visibility = View.GONE
        })


        mViewModel.getDisputeDataResponse().observe(this, Observer {
            mBinding.progressBar.visibility = View.GONE
            if (it.size > 0) {
                it.reverse()
                adapter.update(it)
                mBinding.noData.visibility=View.GONE
            }else{
                mBinding.noData.visibility=View.VISIBLE
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Dispute Center"
    }
    private fun setAdapter() {
        val manager = LinearLayoutManager(this)
        adapter = AdapterDispute(this)
        mBinding.rvInvoiceDetail.layoutManager = manager
        mBinding.rvInvoiceDetail.adapter = adapter
        mBinding.rvInvoiceDetail.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )
    }
}
