package com.unbrako.e_Commerce.account.send_enquiry

import android.app.AlertDialog
import android.content.Context
import android.graphics.Paint
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import com.unbrako.e_Commerce.search.SearchModelEcommerce
import kotlinx.android.synthetic.main.search_recycler_layout.view.circular_img
import kotlinx.android.synthetic.main.search_recycler_layout.view.cutOff
import kotlinx.android.synthetic.main.search_recycler_layout.view.iv_fav
import kotlinx.android.synthetic.main.search_recycler_layout.view.partNo
import kotlinx.android.synthetic.main.search_recycler_layout.view.spinner_des
import kotlinx.android.synthetic.main.search_recycler_layout.view.tvThree
import kotlinx.android.synthetic.main.search_recycler_layout.view.tvTwo
import kotlinx.android.synthetic.main.search_recycler_layout.view.tv_one
import kotlinx.android.synthetic.main.search_recycler_layout.view.tvfive
import kotlinx.android.synthetic.main.search_recycler_layout.view.tvfour
import kotlinx.android.synthetic.main.search_recycler_layout_new.view.*
import kotlinx.android.synthetic.main.sort_filter_layout.view.*

class AdapterNewArray(var mContext: Context, sendEnquiryActivity: SendEnquiryActivity) :
    RecyclerView.Adapter<AdapterNewArray.MyViewHolder>() {
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val modelSearchEcommerce = list[position]
        val model = FetchProductsModel()
        model.product_id = modelSearchEcommerce.product_id
        model.name = modelSearchEcommerce.name
        model.description = modelSearchEcommerce.description
        model.sku = modelSearchEcommerce.sku
        model.quantity = modelSearchEcommerce.quantity
        model.image = modelSearchEcommerce.image
        model.price = modelSearchEcommerce.price
        model.status = modelSearchEcommerce.status
        model.display_price = modelSearchEcommerce.display_price
        model.category_id = modelSearchEcommerce.category_id
        model.saleable = modelSearchEcommerce.saleable
        model.tax_class_id = modelSearchEcommerce.tax_class_id
        model.wish = modelSearchEcommerce.wish
        model.selectQuant = modelSearchEcommerce.selectQuant
        model.stock = modelSearchEcommerce.stock
        model.uom = modelSearchEcommerce.uom
        model.flash_discount = modelSearchEcommerce.flash_discount
        model.isexempted = modelSearchEcommerce.isexempted
        if (modelSearchEcommerce.rating == null || modelSearchEcommerce.rating == "null") {
            model.rating = 0.0
        } else {
            model.rating = modelSearchEcommerce.rating.toDouble()
        }
        val gson = Gson()
        val json = gson.toJson(model)
        val fetchProductsModel: CartModel = gson.fromJson(json, CartModel::class.java)

        holder.itemView.tvThree.text = Constants.getPriceText(fetchProductsModel, mContext, 2)
        holder.itemView.tvfour.text =Constants.getPriceText2(fetchProductsModel,mContext,2)

//        holder.itemView.tvThree.text =
//            "₹" + Constants.currencyFormt((list[position].price.toDouble() * list[position].saleable.toDouble()))
//        holder.itemView.tvfour.text = " per " + (list[position].saleable)+" "+list[position].uom
        if (list[position].display_price.toDouble() > 0) {
            holder.itemView.cutOff.visibility = View.VISIBLE
            holder.itemView.tvfive.text =
                "₹" + Constants.currencyFormt((list[position].display_price.toDouble() * list[position].saleable.toDouble()))
        } else {
            holder.itemView.cutOff.visibility = View.GONE
        }
        holder.itemView.tvfive.paintFlags = holder.itemView.tvfive.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG


        if (list[position].image.isNotEmpty()) {
            Picasso.get().load(list[position].image).fit().centerCrop().placeholder(R.drawable.pro_img_new)
                .into(holder.itemView.circular_img)
        }

        if (modelSearchEcommerce.name.contains("®")) {
            val vv = modelSearchEcommerce.name.split("®")
            holder.itemView.tv_one.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
        } else {
            holder.itemView.tv_one.text = modelSearchEcommerce.name
        }

        holder.itemView.tvTwo.text = list[position].price
        holder.itemView.partNo.text = list[position].sku
        //holder.itemView.tvTwo.paintFlags = holder.itemView.tvTwo.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        if (modelSearchEcommerce.wish) {
            holder.itemView.iv_fav.setImageResource(R.drawable.ic_favorite)
        } else {
            holder.itemView.iv_fav.setImageResource(R.drawable.ic_favorite_not)
        }
        holder.itemView.iv_fav.setOnClickListener {
            if (modelSearchEcommerce.wish) {
                holder.itemView.iv_fav.setImageResource(R.drawable.ic_favorite_not)
            } else {
                holder.itemView.iv_fav.setImageResource(R.drawable.ic_favorite)
            }
            val mm = ModelWishList()
            mm.product_id = model.product_id

            if (modelSearchEcommerce.wish) {
                list[position].wish = false
                Constants.WishListApiAddRemove(mContext, mm, "2")
            } else {
                list[position].wish = true
                Constants.WishListApiAddRemove(mContext, mm, "1")
            }
            notifyDataSetChanged()
        }
        holder.itemView.spinner_des.text = modelSearchEcommerce.selectQuant
        holder.itemView.spinner_des.setOnClickListener {
            openDialogQuantity(mContext, position, model)

        }

        var saleable = modelSearchEcommerce.saleable.toDouble()
        var quantity = modelSearchEcommerce.selectQuant.toDouble()
        var stock = modelSearchEcommerce.stock.toDouble()
        if (stock>0) {
            holder.itemView.notavailable.text = "The available stock for this product is ${modelSearchEcommerce.stock}"
        }
        if (saleable * quantity > stock) {
            holder.itemView.notavailable.visibility = View.VISIBLE
        } else {
            holder.itemView.notavailable.visibility = View.GONE
        }

    }

    var list: ArrayList<SearchModelEcommerce> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(mContext).inflate(R.layout.search_recycler_layout_new, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun openDialogQuantity(mContext: Context, position: Int, quantity: FetchProductsModel) {

        var color = "1"
        var dd = quantity.selectQuant.toInt()
        lateinit var dialog: AlertDialog

        // Initialize an array of colors
        val array =
            arrayOf(
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24",
                "25",
                "26",
                "27",
                "28",
                "29",
                "30",
                "31",
                "32",
                "33",
                "34",
                "35",
                "36",
                "37",
                "38",
                "39",
                "40",
                "41",
                "42",
                "43",
                "44",
                "45",
                "46",
                "47",
                "48",
                "49",
                "50"
            )

        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(mContext)

        // Set a title for alert dialog
        builder.setTitle("Select Quantity")

        builder.setSingleChoiceItems(array, dd - 1) { _, which ->
            color = array[which]
            var model=FetchProductsModel()
            model=quantity
            model.selectQuant=color
          //  if(Constants.IsStockAvailable(model)) {
                list[position].selectQuant = color
                Constants.UpdateToCarts(mContext, quantity).execute()
                notifyDataSetChanged()
//            }else{
//                quantity.selectQuant=dd.toString()
//                Toast.makeText(mContext,"Stock Unavailable",Toast.LENGTH_LONG).show()
//            }


            // Dismiss the dialog
            dialog.dismiss()
        }

        dialog = builder.create()
        dialog.show()
    }

    fun update(it: ArrayList<SearchModelEcommerce>) {

        this.list = it
        notifyDataSetChanged()


    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}


