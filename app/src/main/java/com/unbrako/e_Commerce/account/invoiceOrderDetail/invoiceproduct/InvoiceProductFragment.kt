package com.unbrako.e_Commerce.account.invoiceOrderDetail.invoiceproduct

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.R
import com.unbrako.databinding.InvoiceFragmentProductBinding
import com.unbrako.e_Commerce.account.invoiceOrderDetail.InvoiceDetailAdapter
import com.unbrako.e_Commerce.account.invoiceOrderDetail.ModelProductDetail

class InvoiceProductFragment : Fragment() {

    lateinit var mBinding: InvoiceFragmentProductBinding
    lateinit var productData: ProductData
    lateinit var mAdapter: InvoiceDetailAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.invoice_fragment_product, container, false)
        setHasOptionsMenu(true)
        setAdapter()
        return mBinding.root
    }

    private fun setAdapter() {
        val orderArray: ArrayList<ModelProductDetail> = productData.getProductData()
        val newArray: ArrayList<ModelProductDetail> = ArrayList()
        newArray.addAll(orderArray)
        for(i in orderArray){
            if (i.sku=="UBR_CREDITS"){
                newArray.remove(i)
            }
        }
        mBinding.recyclerView2.layoutManager = LinearLayoutManager(activity)
        mAdapter = InvoiceDetailAdapter(activity)
        mBinding.recyclerView2.adapter = mAdapter
        mBinding.recyclerView2.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        mAdapter.update(newArray)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        productData = context as ProductData
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        productData = activity as ProductData
    }


    interface ProductData {
        fun getProductData(): ArrayList<ModelProductDetail>
    }

}