package com.unbrako.e_Commerce.account.enquiry_history

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.ActivityQuatationDetailBinding

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class EnquiryHistoryActivity : AppCompatActivity() {


    lateinit var mBinding: ActivityQuatationDetailBinding
    lateinit var mViewModel: EnquiryHistoryViewModel
    lateinit var adapter: AdapterEnquiryHistory


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_quatation_detail)
        mViewModel = ViewModelProviders.of(this).get(EnquiryHistoryViewModel::class.java)
        setToolbar()
        setAdapter()


        mViewModel.getQuotationDetail(mBinding.progressBar, mBinding.noData)

        mViewModel.getQuotes().observe(this, Observer {
            if (it.size > 0) {
                it.reverse()
                adapter.update(it)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        val vv = Constants.getPrefs(this).getString("gotoCart", "")
        if (vv!!.isNotBlank()) {
            finish()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {

        setSupportActionBar(mBinding.include2.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.include2.toolbarTitle.text = "Enquiry History"
    }

    private fun setAdapter() {
        val manager = LinearLayoutManager(this)
        adapter = AdapterEnquiryHistory(this)
        mBinding.rvQuatationDetail.layoutManager = manager
        mBinding.rvQuatationDetail.adapter = adapter
        mBinding.rvQuatationDetail.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}