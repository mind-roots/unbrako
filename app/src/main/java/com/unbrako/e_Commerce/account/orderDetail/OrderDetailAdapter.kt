package com.unbrako.e_Commerce.account.orderDetail

import android.annotation.SuppressLint
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.unbrako.Constants
import com.unbrako.R
import kotlinx.android.synthetic.main.order_details_adapter_layout.view.*

class OrderDetailAdapter(var context: FragmentActivity?) : RecyclerView.Adapter<OrderDetailAdapter.ViewHolder>() {

    private var items = ArrayList<ModelOrderDetail>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.order_details_adapter_layout, parent, false)
        return OrderDetailAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //  holder.itemView.originalPrice.paintFlags = holder.itemView.originalPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        if(items[position].sku=="UBR_CREDITS")
        {
           // creditPrice=orderArray[i].price.toDouble()
        }else{

        holder.itemView.name.text = items[position].name
        holder.itemView.partNo.text = items[position].sku
        holder.itemView.qtyPiece.text =
            (items[position].quantity.toDouble() / items[position].saleable.toDouble()).toInt().toString()
        holder.itemView.originalPrice.text = "₹" + (Constants.currencyFormt(
            items[position].price.replace(
                "₹",
                ""
            ).toDouble() * items[position].saleable.toDouble()
        ))
        holder.itemView.discountPrice.text = " per " + Constants.formatValues2(items[position].saleable)+" "+items[position].uom
        if (items[position].display_price.toDouble() > 0) {
            holder.itemView.off.text =
                "₹" + Constants.formatValues2((items[position].display_price.toDouble() * items[position].saleable.toDouble()).toString())
            holder.itemView.cutOff.visibility = View.VISIBLE
        } else {
            holder.itemView.cutOff.visibility = View.GONE
        }

        Picasso.get().load(items[position].image).placeholder(R.mipmap.ic_launcher).fit().centerCrop()
            .into(holder.itemView.iv_cart)

        holder.itemView.off.paintFlags = holder.itemView.off.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG


        val saleable = items[position].saleable.toDouble()
        val quantity = items[position].quantity.toDouble() / items[position].saleable.toDouble()
        val stock = items[position].stock.toDouble()
        if (saleable * quantity > stock) {
        //    holder.itemView.notavailable.visibility = View.VISIBLE
        } else {
            holder.itemView.notavailable.visibility = View.GONE
        }
 }

    }

    fun update(items: ArrayList<ModelOrderDetail>) {
        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}