package com.unbrako.e_Commerce.account.enquiryOrderDetail.detail

import android.os.Parcel
import android.os.Parcelable

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ModelDetail() :Parcelable {

    var product_id: String=""
    var name: String=""
    var description: String=""
    var sku: String=""
    var quantity: String=""
    var image: String=""
    var price: String=""
    var rating: Double = 0.0
    var status: String=""
    var display_price: String=""
    var category_id: String=""
    var selectQuant: String=""
    var saleable: String=""
    var tax_class_id: String=""
    var wish: Boolean=false

    constructor(parcel: Parcel) : this() {
        product_id = parcel.readString()!!
        name = parcel.readString()!!
        description = parcel.readString()!!
        sku = parcel.readString()!!
        quantity = parcel.readString()!!
        image = parcel.readString()!!
        price = parcel.readString()!!
        rating = parcel.readDouble()!!
        status = parcel.readString()!!
        display_price = parcel.readString()!!
        category_id = parcel.readString()!!
        selectQuant = parcel.readString()!!
        saleable = parcel.readString()!!
        tax_class_id = parcel.readString()!!
        wish = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(product_id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(sku)
        parcel.writeString(quantity)
        parcel.writeString(image)
        parcel.writeString(price)
        parcel.writeDouble(rating)
        parcel.writeString(status)
        parcel.writeString(display_price)
        parcel.writeString(category_id)
        parcel.writeString(selectQuant)
        parcel.writeString(saleable)
        parcel.writeString(tax_class_id)
        parcel.writeByte(if (wish) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelDetail> {
        override fun createFromParcel(parcel: Parcel): ModelDetail {
            return ModelDetail(parcel)
        }

        override fun newArray(size: Int): Array<ModelDetail?> {
            return arrayOfNulls(size)
        }
    }
}