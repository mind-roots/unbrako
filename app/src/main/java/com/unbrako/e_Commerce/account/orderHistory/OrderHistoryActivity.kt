package com.unbrako.e_Commerce.account.orderHistory

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.unbrako.R
import com.unbrako.databinding.ActivityOrderHistoryBinding

class OrderHistoryActivity : AppCompatActivity() {


    lateinit var adapter:AdapterOrderHistory
    lateinit var viewModel:ViewModelOrderHistory
    lateinit var mBinding:ActivityOrderHistoryBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(this,R.layout.activity_order_history)

        setSupportActionBar(mBinding.includeOrderHistory.toolbarCommon)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        mBinding.includeOrderHistory.toolbarTitle.text = "Order History"
        viewModel=ViewModelProviders.of(this)[ViewModelOrderHistory::class.java]

        viewModel.getOrderHistory(mBinding.progressBar,mBinding.noData)

        viewModel.getAllOrder().observe(this, Observer {

            if(it.size>0) {
                it.reverse()
                mBinding.rvOrderHistory.layoutManager = LinearLayoutManager(this)
                mBinding.rvOrderHistory.adapter = AdapterOrderHistory(this, it)
                mBinding.rvOrderHistory.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId==android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
