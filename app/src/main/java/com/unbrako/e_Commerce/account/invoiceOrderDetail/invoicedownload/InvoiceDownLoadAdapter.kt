package com.unbrako.e_Commerce.account.invoiceOrderDetail.invoicedownload

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import kotlinx.android.synthetic.main.invoice_download_item.view.*
import android.content.Intent
import android.net.Uri
import com.unbrako.e_Commerce.account.invoiceOrderDetail.ModelDownloadDetails
import kotlin.collections.ArrayList


class InvoiceDownLoadAdapter(var context: Context) : RecyclerView.Adapter<InvoiceDownLoadAdapter.ViewHolder>() {

    var list = ArrayList<ModelDownloadDetails>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.invoice_download_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.link.text = list[position].title
        holder.itemView.link.setOnClickListener {
            if(list[position].pdf.isNotEmpty()) {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(list[position].pdf))
                context.startActivity(browserIntent)
            }
        }
    }

    fun update(orderArray: ArrayList<ModelDownloadDetails>) {
        this.list = orderArray
        notifyDataSetChanged()
    }
}