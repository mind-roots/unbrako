package com.unbrako.e_Commerce.account.quatation_orderDetail

import android.app.Application
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class QuatationOrderHistoryViewModel(application: Application):AndroidViewModel(application) {

    fun getQuotationResponseDetails(progress: RelativeLayout, quote_id: String) {
        repo.getQuatationDetail(progress,quote_id)
    }

    fun getAllData() : MutableLiveData<ArrayList<QuatationInfoModel>>{
        return repo.getMemData()
    }

    fun getTotalData(): MutableLiveData<ArrayList<ModelQuotationTotal>> {
        return repo.getTotal()

    }
    fun getCommentData(): MutableLiveData<QuatationCommentModel> {
        return repo.getComment()

    }

    fun getdeclineRespose(progress: ProgressBar, quote_id: String, comment: String) {
        return repo.getDeclineDetail(progress,quote_id,comment)

    }

    fun getDeclineStaus() :MutableLiveData<DeclineModel>{

        return repo.getStaus()

    }

    var repo:QuatationOrderDetailRepo= QuatationOrderDetailRepo(application)

}
