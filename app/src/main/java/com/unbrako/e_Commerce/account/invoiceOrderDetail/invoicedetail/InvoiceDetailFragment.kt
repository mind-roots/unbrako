package com.unbrako.e_Commerce.account.invoiceOrderDetail.invoicedetail

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.databinding.InvoiceFragmentDetailBinding
import com.unbrako.e_Commerce.account.DisputeDetails.DisputeDetailsActivity
import com.unbrako.e_Commerce.account.invoiceOrderDetail.RaiseDispute.SelectProductsActivity
import com.unbrako.e_Commerce.account.invoiceOrderDetail.ModelInvoiceHistory
import com.unbrako.e_Commerce.account.invoiceOrderDetail.ModelProductDetail
import java.lang.Exception

class InvoiceDetailFragment : Fragment() {

    lateinit var mBinding: InvoiceFragmentDetailBinding
    lateinit var addressData: AddressData

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.invoice_fragment_detail, container, false)

        setHasOptionsMenu(true)
        setData()
        setClicks()
        return mBinding.root
    }

    private fun setClicks() {
        mBinding.disputeButton.setOnClickListener {
            val orderArray: ArrayList<ModelProductDetail> = addressData.getProductData()
            val modelInvoiceHistory: ModelInvoiceHistory = addressData.getInvoiceData()
            if (mBinding.disputeButton.text.toString() == "Raise Dispute") {
                activity!!.startActivityForResult(
                    Intent(activity, SelectProductsActivity::class.java)
                        .putExtra("productArray", orderArray)
                        .putExtra("orderId", modelInvoiceHistory.order_no)
                        .putExtra("invoiceId", modelInvoiceHistory.invoice_no), 101
                )
            }else{
                val intent = Intent(context, DisputeDetailsActivity::class.java)
                intent.putExtra("disputeNo", modelInvoiceHistory.is_disputed)
               startActivity(intent)
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private fun setData() {

        val modelInvoiceHistory: ModelInvoiceHistory = addressData.getInvoiceData()

        if (modelInvoiceHistory.is_disputed=="0"){

            mBinding.disputeButton.text="Raise Dispute"
        }else{

            mBinding.disputeButton.text="View Dispute"
        }
        if(modelInvoiceHistory.invoice_no.isNotEmpty()) {
            mBinding.orderId.text = modelInvoiceHistory.invoice_no
            mBinding.orderDate.text = modelInvoiceHistory.created_at
            mBinding.subtotal.text =  "₹" +Constants.formatValues2(modelInvoiceHistory.sub_total)
            mBinding.gst.text =  "₹" +Constants.formatValues2(modelInvoiceHistory.gst)
            mBinding.orderStatus.text = modelInvoiceHistory.order_no
            mBinding.totalAmount.text = "₹" + (modelInvoiceHistory.total)
            mBinding.creditAmount.text = "₹" + (modelInvoiceHistory.credit)
            try {
                if (modelInvoiceHistory.credit.toDouble() > 0) {
                    mBinding.creditDiv.visibility = View.VISIBLE
                    mBinding.creditLay.visibility = View.VISIBLE
                } else {
                    mBinding.creditDiv.visibility = View.GONE
                    mBinding.creditLay.visibility = View.GONE
                }
            }catch (e:Exception){
                e.printStackTrace()
            }
            mBinding.noData.visibility=View.GONE

            val result = Constants.currencyFormt(modelInvoiceHistory.discountPrice.toDouble())
            mBinding.discount.text = "₹ $result"

            if (result == ".00"){
                mBinding.viewDiscount.visibility = View.GONE
                mBinding.dividerDiscount.visibility = View.GONE
            }else{
                mBinding.viewDiscount.visibility = View.VISIBLE
                mBinding.dividerDiscount.visibility = View.VISIBLE
            }

        }else{
            mBinding.noData.visibility=View.VISIBLE
        }

    }

    override fun onAttach(context: android.content.Context) {
        super.onAttach(context)
        addressData = context as AddressData

    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        addressData = activity as AddressData
    }

    @SuppressLint("SetTextI18n")
    fun showLayout(stringExtra: String) {
        mBinding.disputeButton.text="View Dispute"

    }

    interface AddressData {

        fun getInvoiceData(): ModelInvoiceHistory
        fun getProductData(): java.util.ArrayList<ModelProductDetail>

    }

}