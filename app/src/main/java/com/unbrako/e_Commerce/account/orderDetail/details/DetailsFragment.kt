package com.unbrako.e_Commerce.account.orderDetail.details

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.unbrako.R
import com.unbrako.databinding.FragmentDetailsBinding
import com.unbrako.e_Commerce.account.orderDetail.ShippingAddressModel
import com.unbrako.e_Commerce.account.orderHistory.ModelOrderHistory

class DetailsFragment : Fragment() {

    lateinit var addressData: AddressData
    lateinit var mBinding: FragmentDetailsBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false)
        setHasOptionsMenu(true)
        setData()

        return mBinding.root
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        val documentArray: ShippingAddressModel = addressData.getAddressData()
        val modelOrderHistory: ModelOrderHistory = addressData.getOrderData()

        mBinding.fullAddress.text = documentArray.firstname + " " + documentArray.lastname + ", " + documentArray.company + ", " + documentArray.city + ", " + documentArray.postcode + ", " + documentArray.zone + ", " + documentArray.country
        mBinding.orderId.text = modelOrderHistory.order_id
        mBinding.orderDate.text = modelOrderHistory.date_added
        mBinding.orderStatus.text = modelOrderHistory.status
        mBinding.totalAmount.text = "₹" + (modelOrderHistory.total)

    }


    override fun onAttach(context: android.content.Context) {
        super.onAttach(context)
        addressData = context as AddressData
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        addressData = activity as AddressData
    }


    interface AddressData {

        fun getAddressData(): ShippingAddressModel
        fun getOrderData(): ModelOrderHistory

    }

}