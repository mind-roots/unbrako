package com.unbrako.e_Commerce.account.send_enquiry

import android.app.Application
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.unbrako.e_Commerce.cart.quotaion.QuotationModel
import com.unbrako.e_Commerce.search.SearchModelEcommerce

class SearchEnquiryViewModel(application: Application) : AndroidViewModel(application) {
    fun getRepoData(query: String, progressBar: ProgressBar) {

        return repo.getItems(query, progressBar)
    }

    fun getmData(): LiveData<ArrayList<SearchModelEcommerce>> {

        return repo.getmDataValue()

    }

    fun addComment(
        authCode: String,
        products: String,
        subtotal: String,
        fulltotal: String,
        comment: String,
        tax: String,
        progressBar: ProgressBar
    ) {
        repo.addingCOmment(authCode, products, subtotal, fulltotal, comment, tax, progressBar)
    }

    fun getAddData(): LiveData<QuotationModel> {
        return repo.getmDataComment()
    }

    var repo: SearchEnquiryRepository = SearchEnquiryRepository(application)

}
