package com.unbrako.e_Commerce.account.invoiceOrderDetail

import android.os.Parcel
import android.os.Parcelable

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ModelProductDetail() :Parcelable {

    var sku: String = ""
    var name: String = ""
    var price: String = ""
    var quantity: String = ""
    var image: String = ""
    var product_id: String = ""
    var saleable: String = ""
    var stock: String = ""
    var tax_class_id: String = ""
    var display_price: String = ""
    var uom: String = ""
    var discount_percent: String = ""
    var isSelected: Boolean = false

    constructor(parcel: Parcel) : this() {
        sku = parcel.readString()!!
        name = parcel.readString()!!
        price = parcel.readString()!!
        quantity = parcel.readString()!!
        image = parcel.readString()!!
        product_id = parcel.readString()!!
        saleable = parcel.readString()!!
        stock = parcel.readString()!!
        tax_class_id = parcel.readString()!!
        display_price = parcel.readString()!!
        uom = parcel.readString()!!
        discount_percent = parcel.readString()!!
        isSelected = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(sku)
        parcel.writeString(name)
        parcel.writeString(price)
        parcel.writeString(quantity)
        parcel.writeString(image)
        parcel.writeString(product_id)
        parcel.writeString(saleable)
        parcel.writeString(stock)
        parcel.writeString(tax_class_id)
        parcel.writeString(display_price)
        parcel.writeString(uom)
        parcel.writeString(discount_percent)
        parcel.writeByte(if (isSelected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelProductDetail> {
        override fun createFromParcel(parcel: Parcel): ModelProductDetail {
            return ModelProductDetail(parcel)
        }

        override fun newArray(size: Int): Array<ModelProductDetail?> {
            return arrayOfNulls(size)
        }
    }


}