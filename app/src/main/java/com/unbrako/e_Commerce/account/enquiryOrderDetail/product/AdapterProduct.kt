package com.unbrako.e_Commerce.account.enquiryOrderDetail.product

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.e_Commerce.account.enquiryOrderDetail.QuotModel
import com.unbrako.e_Commerce.account.quatation_orderDetail.QuotationOrderHistoryActivity
import kotlinx.android.synthetic.main.order_history_layout.view.*

class AdapterProduct(var context: FragmentActivity?) : RecyclerView.Adapter<AdapterProduct.ViewHolder>() {
    var list: ArrayList<QuotModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.adapter_product_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val intent = Intent(context, QuotationOrderHistoryActivity::class.java)
            intent.putExtra("quote_id", list[position].quotation_no)
            context?.startActivity(intent)
        }
        holder.itemView.orderId.text = list[position].quotation_no
        holder.itemView.status.text = list[position].status
        holder.itemView.orderDate.text = list[position].created_at
    }

    fun update(it: ArrayList<QuotModel>) {
        this.list = it
        notifyDataSetChanged()

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}

