package com.unbrako.e_Commerce.account.orderDetail

class ShippingAddressModel {

    var firstname : String = ""
    var lastname : String = ""
    var company : String = ""
    var address_1 : String = ""
    var address_2 : String = ""
    var city : String = ""
    var postcode : String = ""
    var zone : String = ""
    var zone_code : String = ""
    var country : String = ""
    var coupon_discount : String = ""

}
