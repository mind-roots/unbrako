package com.unbrako.e_Commerce.account.invoiceOrderDetail.invoicedownload

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.unbrako.R
import com.unbrako.e_Commerce.account.invoiceOrderDetail.ModelCertificate
import kotlinx.android.synthetic.main.invoice_download_item.view.*

class InvoiceCertificateAdapter(var context: Context) : RecyclerView.Adapter<InvoiceCertificateAdapter.ViewHolder>() {

    var list = ArrayList<ModelCertificate>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.invoice_download_item_certificate, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.link.text = list[position].title
        holder.itemView.setOnClickListener {
            if(list[position].pdf.isNotEmpty()) {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(list[position].pdf))
                context.startActivity(browserIntent)
            }
        }
    }

    fun update(orderArray: ArrayList<ModelCertificate>) {
        this.list = orderArray
        notifyDataSetChanged()
    }
}