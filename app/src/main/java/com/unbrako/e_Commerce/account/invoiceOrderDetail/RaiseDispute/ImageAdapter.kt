package com.unbrako.e_Commerce.account.invoiceOrderDetail.RaiseDispute

import android.annotation.SuppressLint
import android.content.Context
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.unbrako.R
import kotlinx.android.synthetic.main.image_item.view.*
import java.io.File

class ImageAdapter (var context: Context) :
    RecyclerView.Adapter<ImageAdapter.ViewHolder>() {

    var items = ArrayList<ModelImages>()
    var  listFile= arrayOf<String>()
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.image_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size

    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Picasso.get().load(items[position].image).placeholder(R.mipmap.ic_launcher).fit()
            .centerCrop()
            .into(holder.itemView.img_pager)

        holder.itemView.delete.setOnClickListener {
            deleteFromSdcard(items[position].image)
            items.removeAt(position)
          notifyDataSetChanged()
        }


    }
    private fun deleteFromSdcard(image: File)
    {
        try {
            val file = File(Environment.getExternalStorageDirectory(), "req_images")

            if (file.isDirectory) {
                listFile = file.list()


                for (ff in listFile.indices) {
                    if (image.name==listFile[ff])
                    File(file, listFile[ff]).delete();
                    // f.add(listFile[ff].getAbsolutePath());

                }

            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }
    fun update(items1: java.util.ArrayList<ModelImages>) {

        items=items1
        notifyDataSetChanged()
    }


}