package com.unbrako.e_Commerce.account.DisputeDetails.Messages


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import com.unbrako.R
import com.unbrako.databinding.FragmentDisputeProductBinding
import com.unbrako.databinding.OpenFromCameraPermissionBinding
import com.unbrako.e_Commerce.account.DisputeDetails.Details.ModelDisputeProductDetail
import com.unbrako.e_Commerce.account.invoiceOrderDetail.RaiseDispute.ImageAdapter
import com.unbrako.e_Commerce.account.invoiceOrderDetail.RaiseDispute.ModelImages
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class DisputeMessageFragment : Fragment() {
    lateinit var mBinding: FragmentDisputeProductBinding
    private lateinit var productData: ProductData
    lateinit var mAdapter: MessageAdapter
    lateinit var mViewModel: ViewModelMessage

    private lateinit var mAdapter1: ImageAdapter
    var items = ArrayList<ModelImages>()
    private var photoFile: File? = null
    var check: Int = -1
     var  listFile= arrayOf<String>()
     var  listFilefie= arrayOf<File>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_dispute_product, container, false)
        mViewModel = ViewModelProviders.of(this)[ViewModelMessage::class.java]
        mViewModel.getMessages(productData.getDisputeID())
        mBinding.progress.visibility = View.VISIBLE
        setHasOptionsMenu(true)
        setAdapter()
        deleteFromSdcard()
        clicks()
        mViewModel.getData().observe(activity!!, Observer {
            mAdapter.update(it)
            mBinding.progress.visibility = View.GONE
        })
        mViewModel.addDisputeMsg().observe(activity!!, Observer {
            if (it.msg == "success") {
                //deleteFromSdcard()
                mViewModel.getMessages(productData.getDisputeID())
                items.clear()
                deleteFromSdcard()
                mAdapter1.update(items)
                mBinding.response.setText("")
            } else {
                Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
            }

            mBinding.progress.visibility = View.GONE
        })
        return mBinding.root
    }

    private fun clicks() {
        mBinding.addClick.setOnClickListener {
            if (checkPermissionForCameraGallery(activity!!))
                try{
                    val file=  File(Environment.getExternalStorageDirectory(),"req_images")

                    if (file.isDirectory)
                    {
                        listFilefie = file.listFiles()
                    }
                }catch (e:Exception){
                    e.printStackTrace()
                }
                if (listFilefie.size<5){
                    showDialog()
                }else{
                    Toast.makeText(activity,"You cannot upload more than 5 images.",Toast.LENGTH_LONG).show()

                }
    }
        mBinding.done.setOnClickListener {
            val text = mBinding.response.text.toString()
            if (text.isEmpty() && items.size == 0) {
                Toast.makeText(activity, "Please add either comment or Image.", Toast.LENGTH_SHORT)
                    .show()
            } else {
                var  listFilefieL= arrayOf<File>()
                try{
                    val file=  File(Environment.getExternalStorageDirectory(),"req_images")

                    if (file.isDirectory)
                    {
                        listFilefieL = file.listFiles()
                    }
                }catch (e:Exception){
                    e.printStackTrace()
                }
                mBinding.progress.visibility = View.VISIBLE
                mViewModel.addDisputeMessage(productData.getDisputeID(), text, listFilefieL)
            }
        }
    }



 fun deleteFromSdcard()
{

    try{
    val file=  File(Environment.getExternalStorageDirectory(),"req_images")

        if (file.isDirectory)
        {
            listFile = file.list()



            for (element in listFile)
            {
                File(file, element).delete()
               // f.add(listFile[ff].getAbsolutePath())

            }

        }
    }catch (e:Exception){
        e.printStackTrace()
    }
}

    private fun setAdapter() {
        mBinding.recyclerView2.layoutManager = LinearLayoutManager(activity!!)
        mAdapter = MessageAdapter(activity!!)
        mBinding.recyclerView2.adapter = mAdapter
        mBinding.recyclerView2.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )
        mBinding.imageRecycler.layoutManager =
            LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, true)
        mAdapter1 = ImageAdapter(activity!!)
        mBinding.imageRecycler.adapter = mAdapter1
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        productData = context as ProductData
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        productData = activity as ProductData
    }


    interface ProductData {
        fun getProductData(): ArrayList<ModelDisputeProductDetail>
        fun getDisputeID(): String
    }

    private fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            photoFile = createImageFile()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        if (photoFile != null) {
            val uri = FileProvider.getUriForFile(
                activity!!,
                "com.unbrako.app" + ".provider",
                photoFile!!
            )
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(intent, 29)
        }

    }

    @SuppressLint("IntentReset")
    private fun openGallery() {

        val intent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        intent.type = "image/*"
        startActivityForResult(intent, 1)
    }

    @SuppressLint("SimpleDateFormat")
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES
        )
        return File.createTempFile(
            imageFileName,  // prefix
            ".jpg",         // suffix
            storageDir      // directory
        )
    }

    private fun showDialog() {

        val builder: androidx.appcompat.app.AlertDialog.Builder =
            androidx.appcompat.app.AlertDialog.Builder(activity!!)
        builder.setTitle("Select")
        val mBinding: OpenFromCameraPermissionBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(activity),
                R.layout.open_from_camera_permission,
                null,
                false
            )

        if (check == 0) {
            mBinding.camera.isChecked = true
            mBinding.gallery.isChecked = false
        }
        if (check == 1) {
            mBinding.gallery.isChecked = false
            mBinding.camera.isChecked = true
        }
        builder.setView(mBinding.root)
        builder.setCancelable(false)

        builder.setPositiveButton("Ok") { dialog, which ->

            if (mBinding.camera.isChecked) {
                openCamera()
                check = 0


            } else if (mBinding.gallery.isChecked) {
                openGallery()
                check = 1

            }
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialog, which ->

        }
        builder.create()
        builder.show()

    }

    private fun getImageUri(inImage: Bitmap): File {
        val root: String = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/req_images")
        myDir.mkdirs()
        val fName = "Image_profile"+System.currentTimeMillis()+".jpg"
        val file = File(myDir, fName)
        if (file.exists())
            file.delete()

        try {
            val out = FileOutputStream(file)
            inImage.compress(Bitmap.CompressFormat.JPEG, 60, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return file
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        try {
            if (requestCode == 29) {
                //   val contentURI: Bitmap = data.extras.get("data") as Bitmap
                try {

                    val resultBitmap = getRotateImage(photoFile)
                    val uri = getImageUri(resultBitmap)
//..................................Api FOr Update Image....................................................................//


                    val model = ModelImages()
                    model.image =uri
                    items.add(model)
                    mAdapter1.update(items)


                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
//2nd code
            else if (requestCode == 1 && data != null) {
                try {

                    val selectedPicture: Uri = data.data!!
                   val file: File =
                        uploadImage2(getBitmapImage(selectedPicture, activity!!))
                    val model = ModelImages()
                    model.image = file
                    items.add(model)
                    mAdapter1.update(items)
                   // mViewModel.addDisputeMessage2(productData.getDisputeID(), "fffff", File(selectedPicture.path))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if (requestCode == 2 && data != null) {
                try {
                    Toast.makeText(activity, "lll!", Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    private fun uploadImage2(
        bitmap: Bitmap
    ): File {

        return getImageUri35(bitmap)
    }



    private fun getImageUri35(inImage: Bitmap): File {

        val root: String = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/req_images")

        val fName = "Image_profile"+System.currentTimeMillis()+".jpg"
        val file = File(myDir, fName)
        if (file.exists())
            file.delete()
        myDir.mkdirs()
        try {

            val out = FileOutputStream(file)

            inImage.compress(Bitmap.CompressFormat.JPEG, 60, out)

            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return file
    }


    private fun checkPermissionForCameraGallery(context: Activity): Boolean {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                context, arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 102
            )
            return false
        } else if (ActivityCompat.checkSelfPermission(
                context, Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                context, arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA

                ), 102
            )
            return false
        } else {
            return true
        }
    }

    private fun rotateBitmap(bitmap: Bitmap, degrees: Int): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degrees.toFloat())
        return Bitmap.createBitmap(
            bitmap,
            0,
            0,
            bitmap.width,
            bitmap.height,
            matrix,
            true
        )
    }

    private fun getRotateImage(photoFile: File?): Bitmap {

        val ei = ExifInterface(photoFile!!.path)
        val orientation = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        val bitmap = BitmapFactory.decodeFile(photoFile.path)

        var rotatedBitmap: Bitmap? = bitmap
        when (orientation) {

            ExifInterface.ORIENTATION_ROTATE_90 ->
                rotatedBitmap = TransformationUtils.rotateImage(bitmap, 90)


            ExifInterface.ORIENTATION_ROTATE_180 ->
                rotatedBitmap = TransformationUtils.rotateImage(bitmap, 180)


            ExifInterface.ORIENTATION_ROTATE_270 ->
                rotatedBitmap = TransformationUtils.rotateImage(bitmap, 270)


            ExifInterface.ORIENTATION_NORMAL ->
                rotatedBitmap = bitmap
        }

        return rotatedBitmap!!
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 102) {
            if (grantResults.contains(-1)) {

            } else {
                deleteFromSdcard()
                showDialog()
            }
        }
    }

    private fun getBitmapImage(selectedPicture: Uri, context: Context): Bitmap {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = context.contentResolver.query(
            selectedPicture,
            filePathColumn,
            null,
            null,
            null
        )
        cursor!!.moveToFirst()

        val columnIndex = cursor.getColumnIndex(filePathColumn[0])
        val picturePath = cursor.getString(columnIndex)
        cursor!!.close()
        var exif: ExifInterface? = null
        var loadedBitmap = BitmapFactory.decodeFile(picturePath)


        try {
            val pictureFile = File(picturePath)
            exif = ExifInterface(pictureFile.absolutePath)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        var orientation = ExifInterface.ORIENTATION_NORMAL

        if (exif != null)
            orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )

        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 ->
                loadedBitmap = rotateBitmap(loadedBitmap, 90)

            ExifInterface.ORIENTATION_ROTATE_180 ->
                loadedBitmap = rotateBitmap(loadedBitmap, 180)


            ExifInterface.ORIENTATION_ROTATE_270 ->
                loadedBitmap = rotateBitmap(loadedBitmap, 270)

        }

        return loadedBitmap
    }


}
