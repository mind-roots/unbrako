package com.unbrako.e_Commerce.account.DisputeDetails.Messages

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.cart.checkOut.ModelStatus
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File
import java.util.concurrent.TimeUnit

@Suppress("NAME_SHADOWING", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MessageRepo(var application: Application) {

    var mData= MutableLiveData<ArrayList<ModelMessage>>()
    var statusData = MutableLiveData<ModelStatus>()

    fun getMessage(disputeID: String) {

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()

        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.getMessage(auth!!,disputeID)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val list: ArrayList<ModelMessage> = ArrayList()
                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optBoolean("success")

                    if (success) {
                        val dd=json.optJSONObject("data")
                        val data = dd.optJSONArray("msg")
                        for (i in 0 until data.length()) {
                            val invoiceModel = ModelMessage()
                            val obj = data.getJSONObject(i)
                            invoiceModel.reason = obj.optString("reason")
                            invoiceModel.created_at = obj.optString("created_at")
                            invoiceModel.created_by = obj.optString("created_by")
                            invoiceModel.images = obj.optJSONArray("images").toString()
                            list.add(invoiceModel)
                        }
                        mData.value = list
                    }



                } catch (e:Exception) {

                    e.printStackTrace()
                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

            }

        })
    }

    fun getData(): MutableLiveData<java.util.ArrayList<ModelMessage>> {
        return mData
    }

    fun addMessage(
        disputeID: String,
        text: String,
        imageModelArrayList: Array<File>
    ) {

        val multipartTypedOutput =
            arrayOfNulls<MultipartBody.Part>(imageModelArrayList.size)
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        for (index in imageModelArrayList.indices) {
            val surveyBody: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imageModelArrayList[index])
            multipartTypedOutput[index] =
                MultipartBody.Part.createFormData("imageFiles[]",
                    imageModelArrayList[index].name, surveyBody)
        }

        val text1: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(), text)
        val disputeID: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(), disputeID)
         val auth1: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(),auth!!)


//        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageModelArrayList.get(0).image)
//        val body = MultipartBody.Part.createFormData("image", imageModelArrayList.get(0).image.name, requestFile)




        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS).build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .client(httpClient.build()).build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.addDisputeMsg(
            auth1,
            disputeID,
            text1,
            multipartTypedOutput
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
                        if (success) {
                            val m = ModelStatus()
//                            m.status = success
//                            m.order_id = json.optString("order_id")
//                            m.total = json.optString("total")
                            m.msg ="success"
                            statusData.value = m
                        }else{
                            val m = ModelStatus()
//                            m.status = success
//                            m.order_id = json.optString("order_id")
//                            m.total = json.optString("total")
                            m.msg =json.optString("error")
                            statusData.value = m
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                        val m = ModelStatus()
                        m.msg ="Server Error"
                        statusData.value = m
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val m = ModelStatus()
                m.msg ="Network connection failed."
                statusData.value = m
            }
        })
    }

    fun messageSent(): MutableLiveData<ModelStatus> {
        return statusData
    }

    fun test(disputeID: String, s: String, selectedPicture: File) {
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val text1: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(), s)
        val disputeID: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(), disputeID)
        val auth1: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(),auth!!)


//        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"),  selectedPicture.path)
//        val body = MultipartBody.Part.createFormData("image", selectedPicture.path, requestFile)


        val multipartTypedOutput =
            ArrayList<MultipartBody.Part>()

            val surveyBody: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), selectedPicture.path)
            multipartTypedOutput.add( MultipartBody.Part.createFormData("images",
                selectedPicture.name, surveyBody))



        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS).build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .client(httpClient.build()).build()

        val service = retrofit.create(EcommerceService::class.java)
        val call: Call<ResponseBody> = service.addDisputeMsg2(
            auth1,
            disputeID,
            text1,
            multipartTypedOutput
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val success = json.optBoolean("success")
                        if (success) {
                            val m = ModelStatus()
//                            m.status = success
//                            m.order_id = json.optString("order_id")
//                            m.total = json.optString("total")
                            m.msg ="success"
                            statusData.value = m
                        }else{
                            val m = ModelStatus()
//                            m.status = success
//                            m.order_id = json.optString("order_id")
//                            m.total = json.optString("total")
                            m.msg =json.optString("error")
                            statusData.value = m
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                        val m = ModelStatus()
                        m.msg ="Server Error"
                        statusData.value = m
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val m = ModelStatus()
                m.msg ="Network connection failed."
                statusData.value = m
            }
        })    }
}