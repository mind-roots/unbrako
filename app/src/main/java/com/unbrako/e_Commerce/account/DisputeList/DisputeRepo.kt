package com.unbrako.e_Commerce.account.DisputeList

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import com.unbrako.e_Commerce.cart.quotaion.Webservice
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.ArrayList

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DisputeRepo (var application: Application) {

    var data = MutableLiveData<ArrayList<DisputeModel>>()
    var error = MutableLiveData<ModelStatus>()
    fun getDispute(){
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_Ecommerce)
            .build()
        val service = retrofit.create(Webservice::class.java)
        val call: Call<ResponseBody> = service.getDisputes(auth!!)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val list: ArrayList<DisputeModel> = ArrayList()
                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val success = json.optBoolean("success")

                    if (success) {
                        val data = json.optJSONArray("dispute")
                        for (i in 0 until data.length()) {
                            val invoiceModel = DisputeModel()
                            val obj = data.getJSONObject(i)
                            invoiceModel.id = obj.optString("id")
                            invoiceModel.trans_no = obj.optString("trans_no")
                            invoiceModel.reason = obj.optString("reason")
                            invoiceModel.amount = obj.optString("amount")
                            invoiceModel.created_at = obj.optString("created_at")
                            invoiceModel.status = obj.optString("status")
                            list.add(invoiceModel)
                        }
                    }

                    data.value = list

                } catch (e:Exception) {
                    val model= ModelStatus()
                    model.msg="Server Error"
                    error.value=model
                    e.printStackTrace()
                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val model= ModelStatus()
                model.msg="Check Internet connection."
                error.value=model
            }

        })

    }

    fun getStatus(): MutableLiveData<ModelStatus> {
        return error
    }

    fun getResponseData(): MutableLiveData<ArrayList<DisputeModel>> {

        return data
    }
}