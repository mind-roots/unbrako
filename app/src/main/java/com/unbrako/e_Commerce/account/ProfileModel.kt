package com.unbrako.e_Commerce.account

class ProfileModel {

    var customer_id : String = ""
    var customer_group_id : String = ""
    var store_id : String = ""
    var language_id : String = ""
    var firstname : String = ""
    var lastname : String = ""
    var email : String = ""
    var telephone : String = ""
    var fax : String = ""
    var password : String = ""
    var salt : String = ""
    var cart : String = ""
    var wishlist : String = ""
    var newsletter : String = ""
    var address_id : String = ""
    var custom_field : String = ""
    var ip : String = ""
    var status : String = ""
    var safe : String = ""
    var token : String = ""
    var code : String = ""
    var date_added : String = ""
    var company_name : String = ""
    var gst : String = ""
    var landline : String = ""
    var credit : String = ""

}
