package com.unbrako.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
class ProductMetric {

    @PrimaryKey
    lateinit var pro_det_id: String
    @ColumnInfo(name = "ProductID")
    lateinit var ProductID: String
    @ColumnInfo(name = "Diameter")
    lateinit var Diameter: String
    @ColumnInfo(name = "AFNom")
    lateinit var AFNom: String
    @ColumnInfo(name = "HeadODMax")
    lateinit var HeadODMax: String
    @ColumnInfo(name = "HeadHeightMax")
    lateinit var HeadHeightMax: String
    @ColumnInfo(name = "ThreadLengthRef")
    lateinit var ThreadLengthRef: String
    @ColumnInfo(name = "Grade")
    lateinit var Grade: String
    @ColumnInfo(name = "Thread")
    lateinit var Thread: String
    @ColumnInfo(name = "Pitch")
    lateinit var Pitch: String
    @ColumnInfo(name = "TensileStressArea")
    lateinit var TensileStressArea: String
    @ColumnInfo(name = "NominalShankArea")
    lateinit var NominalShankArea: String
    @ColumnInfo(name = "SeatingTorqueNm")
    lateinit var SeatingTorqueNm: String
    @ColumnInfo(name = "SeatingTorqueInlbs")
    lateinit var SeatingTorqueInlbs: String
    @ColumnInfo(name = "TensileStrengthkN")
    lateinit var TensileStrengthkN: String
    @ColumnInfo(name = "TensileStrengthlbs")
    lateinit var TensileStrengthlbs: String
    @ColumnInfo(name = "CtrBore")
    lateinit var CtrBore: String
    @ColumnInfo(name = "BodyDrill")
    lateinit var BodyDrill: String
    @ColumnInfo(name = "TapDrill")
    lateinit var TapDrill: String
    @ColumnInfo(name = "BodyODMax")
    lateinit var BodyODMax: String
    @ColumnInfo(name = "BodyODMin")
    lateinit var BodyODMin: String
    @ColumnInfo(name = "ShearStrengthOfBody")
    lateinit var ShearStrengthOfBody: String
    @ColumnInfo(name = "ScrewLengthMin")
    lateinit var ScrewLengthMin: String
    @ColumnInfo(name = "teeth")
    lateinit var Teeth: String
    @ColumnInfo(name = "last_modified_time")
    lateinit var last_modified_time: String
    @ColumnInfo(name = "status")
    lateinit var status: String
    @ColumnInfo(name = "webStatus")
    lateinit var webStatus: String
    @ColumnInfo(name = "HeadOdMin")
    lateinit var HeadOdMin: String
    @ColumnInfo(name = "SocketDepth")
    lateinit var SocketDepth: String
    @ColumnInfo(name = "D1MIN")
    lateinit var D1MIN: String
    @ColumnInfo(name = "D2MAX")
    lateinit var D2MAX: String
    @ColumnInfo(name = "HMAX")
    lateinit var HMAX: String
    @ColumnInfo(name = "HMIN")
    lateinit var HMIN: String
    @ColumnInfo(name = "hardness")
    lateinit var hardness: String
}