package com.unbrako.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
class Products {

    @PrimaryKey
    lateinit var ProductID : String
    @ColumnInfo(name = "name")
    lateinit var name : String
    @ColumnInfo(name = "prod_image")
    lateinit var prod_image : String
}