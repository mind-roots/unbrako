package com.unbrako.dataBase

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity

@Entity
class HomeServiceModel() :Parcelable{


    lateinit var category_id: String


    lateinit var image: String

    lateinit var parent_id: String

    lateinit var top: String
    lateinit var column: String

    lateinit var sort_order: String
    lateinit var status: String

    lateinit var date_added: String
    lateinit var date_modified: String

    lateinit var language_id: String
    lateinit var name: String

    lateinit var description: String

    lateinit var meta_title: String

    lateinit var meta_description: String

    lateinit var meta_keyword: String

    lateinit var store_id: String
    var hasData: Boolean=false

    constructor(parcel: Parcel) : this() {
        category_id = parcel.readString()!!
        image = parcel.readString()!!
        parent_id = parcel.readString()!!
        top = parcel.readString()!!
        column = parcel.readString()!!
        sort_order = parcel.readString()!!
        status = parcel.readString()!!
        date_added = parcel.readString()!!
        date_modified = parcel.readString()!!
        language_id = parcel.readString()!!
        name = parcel.readString()!!
        description = parcel.readString()!!
        meta_title = parcel.readString()!!
        meta_description = parcel.readString()!!
        meta_keyword = parcel.readString()!!
        store_id = parcel.readString()!!
        hasData = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(category_id)
        parcel.writeString(image)
        parcel.writeString(parent_id)
        parcel.writeString(top)
        parcel.writeString(column)
        parcel.writeString(sort_order)
        parcel.writeString(status)
        parcel.writeString(date_added)
        parcel.writeString(date_modified)
        parcel.writeString(language_id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(meta_title)
        parcel.writeString(meta_description)
        parcel.writeString(meta_keyword)
        parcel.writeString(store_id)
        parcel.writeByte(if (hasData) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HomeServiceModel> {
        override fun createFromParcel(parcel: Parcel): HomeServiceModel {
            return HomeServiceModel(parcel)
        }

        override fun newArray(size: Int): Array<HomeServiceModel?> {
            return arrayOfNulls(size)
        }
    }


}