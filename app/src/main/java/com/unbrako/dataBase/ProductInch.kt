package com.unbrako.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
class ProductInch {
    @PrimaryKey
    lateinit var inch_det_id: String
    @ColumnInfo(name = "ProductID")
    lateinit var ProductID: String
    @ColumnInfo(name = "Diameter")
    lateinit var Diameter: String
    @ColumnInfo(name = "DiamDecimal")
    lateinit var DiamDecimal: String
    @ColumnInfo(name = "DiamMM")
    lateinit var DiamMM: String
    @ColumnInfo(name = "AFNom")
    lateinit var AFNom: String
    @ColumnInfo(name = "HeadODMax")
    lateinit var HeadODMax: String
    @ColumnInfo(name = "HeadHeightMax")
    lateinit var HeadHeightMax: String
    @ColumnInfo(name = "ThreadLengthRef")
    lateinit var ThreadLengthRef: String
    @ColumnInfo(name = "Grade")
    lateinit var Grade: String
    @ColumnInfo(name = "Thread")
    lateinit var Thread: String
    @ColumnInfo(name = "PitchUNC")
    lateinit var PitchUNC: String
    @ColumnInfo(name = "PitchUNF")
    lateinit var PitchUNF: String
    @ColumnInfo(name = "TensileStressAreaUNC")
    lateinit var TensileStressAreaUNC: String
    @ColumnInfo(name = "TensileStressAreaUNF")
    lateinit var TensileStressAreaUNF: String
    @ColumnInfo(name = "NominalShankAreaUNC")
    lateinit var NominalShankAreaUNC: String
    @ColumnInfo(name = "NominalShankAreaUNF")
    lateinit var NominalShankAreaUNF: String
    @ColumnInfo(name = "SeatingTorqueInlbsUNC")
    lateinit var SeatingTorqueInlbsUNC: String
    @ColumnInfo(name = "SeatingTorqueInlbsUNF")
    lateinit var SeatingTorqueInlbsUNF: String
    @ColumnInfo(name = "TensileStrengthlbsUNC")
    lateinit var TensileStrengthlbsUNC: String
    @ColumnInfo(name = "TensileStrengthlbsUNF")
    lateinit var TensileStrengthlbsUNF: String
    @ColumnInfo(name = "CtrBore")
    lateinit var CtrBore: String
    @ColumnInfo(name = "BodyDrill")
    lateinit var BodyDrill: String
    @ColumnInfo(name = "TapDrillUNC")
    lateinit var TapDrillUNC: String
    @ColumnInfo(name = "TapDrillUNF")
    lateinit var TapDrillUNF: String
    @ColumnInfo(name = "BodyODMax")
    lateinit var BodyODMax: String
    @ColumnInfo(name = "BodyODMin")
    lateinit var BodyODMin: String
    @ColumnInfo(name = "ShearStrengthOfBody")
    lateinit var ShearStrengthOfBody: String
    @ColumnInfo(name = "ScrewLengthMin")
    lateinit var ScrewLengthMin: String
    @ColumnInfo(name = "last_modified_time")
    lateinit var last_modified_time: String
    @ColumnInfo(name = "status")
    lateinit var status: String
    @ColumnInfo(name = "webStatus")
    lateinit var webStatus: String

    @ColumnInfo(name = "hardness")
    lateinit var hardness: String

    @ColumnInfo(name = "SocketDepth")
    lateinit var SocketDepth: String

    @ColumnInfo(name = "Pitch_TPI")
    lateinit var Pitch_TPI: String


}