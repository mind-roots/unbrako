package com.unbrako.dataBase

import androidx.room.*
import com.unbrako.e_Commerce.account.quatation_orderDetail.ModelQuatationOrderDetail
import com.unbrako.e_Commerce.catalog.filter.FilterModel
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FavouriteModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel


@Dao
interface ProductDao {

    @Query("SELECT * FROM InchBox")
    abstract fun getAllInchBoxArray(): List<InchBox>

    @Query("SELECT * FROM BasketModel")
    abstract fun getAllBasketModelArray(): List<BasketModel>

    @Query("SELECT * FROM Products")
    abstract fun getAllProducts(): List<Products>

    @Query("SELECT * FROM MetricBox")
    abstract fun getAllMetricBoxArray(): List<MetricBox>


    @Query("SELECT * FROM BookMark")
    abstract fun getAllBookMarkArray(): List<BookMark>


    @Query("SELECT * FROM Products WHERE ProductID = :name LIMIT 1")
    abstract fun getProductId(name: String): List<Products>

    @Query("SELECT * FROM ProductInch WHERE ProductID = :id AND webStatus = :status")
    abstract fun getAllInchProducts(id: String, status: String): List<ProductInch>

    @Query("SELECT * FROM ProductMetric WHERE ProductID = :id AND webStatus = :status")
    abstract fun getAllMetricProducts(id: String, status: String): List<ProductMetric>

    @Query("SELECT * FROM ProductMetric")
    abstract fun getAllMetricProductsDb(): List<ProductMetric>

    @Query("SELECT * FROM InchBox WHERE ProductID = :id AND Diameter = :diameter AND webStatus = :status")
    abstract fun getAllInchBox(id: String, diameter: String, status: String): List<InchBox>

    @Query("SELECT * FROM MetricBox WHERE ProductID = :id AND Diameter = :diameter AND webStatus = :status")
    abstract fun getAllMetricBox(id: String, diameter: String, status: String): List<MetricBox>

    @Query("SELECT * FROM BookMark WHERE PartNumber = :id AND Diameter = :diameter")
    abstract fun getAllBookMark(id: String, diameter: String): List<BookMark>


    @Delete
    abstract fun delete(task: InchBox)

    @Update
    abstract fun update(task: InchBox)

    @Update
    abstract fun updateProductMetric(task: ProductMetric)

    @Update
    abstract fun updateProductInch(task: ProductInch)

    @Update
    abstract fun updateMetricBox(task: MetricBox)

    @Update
    abstract fun updateInchBox(task: InchBox)

    @Update
    abstract fun updateCart(task: CartModel)

   @Update
    abstract fun updateBasket(task: BasketModel)

    @Update
    abstract fun updateCarts(task: CartModel)


//----------------------------------Inserting Data into Tables---------------

    @Insert
    abstract fun insertInchBox(task: InchBox)

    @Insert
    abstract fun insertMetricBox(task: MetricBox)

    @Insert
    abstract fun insertProducts(task: Products)

    @Insert
    abstract fun insertProductMetric(task: ProductMetric)

    @Insert
    abstract fun insertProductInch(task: ProductInch)

    @Insert
    abstract fun insertBookMark(task: BookMark)

    @Insert
    abstract fun insertBasket(task: BasketModel)


    //------------------Deleting Tables--------------------------------------

    @Query("DELETE FROM InchBox")
    abstract fun deleteInchBoxTable()

    @Query("DELETE FROM MetricBox")
    abstract fun deleteMetricBoxTable()

    @Query("DELETE FROM ProductMetric")
    abstract fun deleteProductMetricTable()

    @Query("DELETE FROM ProductInch")
    abstract fun deleteProductInchTable()

    @Query("DELETE FROM Products")
    abstract fun deleteProductsTable()

    @Query("DELETE FROM CartModel")
    abstract fun deleteCart()
@Query("DELETE FROM BasketModel")
    abstract fun deleteBasket()

    @Query("DELETE FROM BookMark WHERE PartNumber = :id AND Diameter = :diameter")
    abstract fun deleteBookMarkTable(id: String, diameter: String)

 @Query("DELETE FROM BasketModel WHERE ProductId = :id")
    abstract fun deleteBasketTable(id: String)


    //----------------------------------------------------------------------------
    @Query("SELECT * FROM ProductMetric WHERE ProductID = :productID AND Diameter = :diameter")
    abstract fun CheckIfExistProductMetric(diameter: String, productID: String): List<ProductMetric>

    @Query("SELECT * FROM ProductInch WHERE ProductID = :productID AND Diameter = :diameter")
    abstract fun CheckIfExistProductInch(diameter: String, productID: String): List<ProductInch>

    @Query("SELECT * FROM MetricBox WHERE PartNumber = :partNumber AND Diameter = :diameter")
    abstract fun CheckIfExistMetricBox(diameter: String, partNumber: String): List<MetricBox>

    @Query("SELECT * FROM InchBox WHERE PartNumber = :partNumber AND Diameter = :diameter")
    abstract fun CheckIfExistInchBoxArray(diameter: String, partNumber: String): List<InchBox>


    @Insert
    abstract fun insertFetchProductsModel(task: FavouriteModel)

    @Query("SELECT * FROM FavouriteModel")
    abstract fun getAllFavourite(): List<FavouriteModel>

    @Query("DELETE FROM FavouriteModel WHERE ProductId = :product_id ")
    abstract fun deleteFetchProductsModelTable(product_id: String)

    @Insert
    abstract fun insertCartProductsModel(task: CartModel)

    @Insert
    abstract fun insertCartProductsModel1(task: ModelQuatationOrderDetail)

    @Query("SELECT * FROM CartModel")
    abstract fun getAllCart(): List<CartModel>

    @Query("SELECT * FROM ModelQuatationOrderDetail")
    abstract fun getAllCart1(): List<ModelQuatationOrderDetail>

    @Query("DELETE FROM CartModel WHERE ProductId = :product_id ")
    abstract fun deleteCartProductsModelTable(product_id: String)


    @Insert
    abstract fun insertFilterModel(task: FilterModel)

    @Query("SELECT * FROM FilterModel")
    abstract fun getFilterData(): List<FilterModel>


}