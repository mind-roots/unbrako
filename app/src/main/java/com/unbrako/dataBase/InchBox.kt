package com.unbrako.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class InchBox {
    @PrimaryKey
    lateinit var inch_box_id :String

    @ColumnInfo(name = "ShanonPartNumber")
    lateinit var ShanonPartNumber :String

    @ColumnInfo(name = "PartNumber")
    lateinit var PartNumber :String


    @ColumnInfo(name = "ProductID")
    lateinit var ProductID :String

    @ColumnInfo(name = "DescriptionOne")
    lateinit var DescriptionOne :String

    @ColumnInfo(name = "DescriptionTwo")
    lateinit var DescriptionTwo :String

    @ColumnInfo(name = "BoxQuantity")
    lateinit var BoxQuantity :String

    @ColumnInfo(name = "Diameter")
    lateinit var Diameter :String

    @ColumnInfo(name = "Length")
    lateinit var Length :String

    @ColumnInfo(name = "countinchbox")
    lateinit var countinchbox :String

    @ColumnInfo(name = "last_modified_time")
    lateinit var last_modified_time :String

    @ColumnInfo(name = "thread_type")
    lateinit var Thread_type :String

     @ColumnInfo(name = "bookmark_desc")
    lateinit var bookmark_desc :String

    @ColumnInfo(name = "webStatus")
    lateinit var webStatus: String
}
