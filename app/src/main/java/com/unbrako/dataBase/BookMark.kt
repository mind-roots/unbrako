package com.unbrako.dataBase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.unbrako.R


@Entity
class BookMark {
    @PrimaryKey
    lateinit var PartNumber: String
    @ColumnInfo(name = "id")
    lateinit var id: String
    @ColumnInfo(name = "Description")
    lateinit var Description: String
    @ColumnInfo(name = "ProductID")
    lateinit var ProductID: String
    @ColumnInfo(name = "Diameter")
    lateinit var Diameter: String
    @ColumnInfo(name = "selection")
    var selection: Boolean = false
    @ColumnInfo(name = "type")
    lateinit var type: String
    @ColumnInfo(name = "BoxQuantity")
    lateinit var BoxQuantity: String

    @ColumnInfo(name = "ProductImage")
    var ProductImage: Int = R.mipmap.catalog_one

}