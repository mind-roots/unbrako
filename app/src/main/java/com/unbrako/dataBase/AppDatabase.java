package com.unbrako.dataBase;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.unbrako.e_Commerce.account.quatation_orderDetail.ModelQuatationOrderDetail;
import com.unbrako.e_Commerce.catalog.filter.FilterModel;
import com.unbrako.e_Commerce.dataBase.BasketModel;
import com.unbrako.e_Commerce.dataBase.CartModel;
import com.unbrako.e_Commerce.dataBase.FavouriteModel;

@Database(entities = {BookMark.class,Products.class,InchBox.class,MetricBox.class,ProductInch.class,ProductMetric.class, FavouriteModel.class, CartModel.class, FilterModel.class, ModelQuatationOrderDetail.class, BasketModel.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ProductDao productDao();



}