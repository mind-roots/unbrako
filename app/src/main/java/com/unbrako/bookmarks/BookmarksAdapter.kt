package com.unbrako.bookmarks

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.catalog.RecyclerModel
import com.unbrako.dataBase.BookMark
import kotlinx.android.synthetic.main.bookmark_listdata.view.*

class BookmarksAdapter(
    val mContext: Context, private val bookModel:
    ArrayList<BookMark>
) : androidx.recyclerview.widget.RecyclerView.Adapter<BookmarksAdapter.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.bookmark_listdata, p0, false)
        return ViewHolder(v)

    }

    override fun getItemCount(): Int {
        return bookModel.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        val model = bookModel[p1]

        p0.itemView.img_description.text = model.Description
        p0.itemView.circular_img.setImageResource(model.ProductImage)
        p0.itemView.img_code.text = model.PartNumber
        p0.itemView.checklistt.isChecked = model.selection
        p0.itemView.setOnClickListener{
            val modelP= RecyclerModel()
            modelP.imageName= ""
            modelP.id=bookModel[p1].ProductID
            modelP.image=bookModel[p1].ProductImage

            modelP.Diameter=bookModel[p1].Diameter
            Constants.navigate(mContext,it,modelP,"bookmark",model.type)
        }

    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        private var imgDescription: TextView
        private var circularImg: ImageView
        private var imgCode: TextView
        var checklistt: CheckBox

        init {
            imgDescription = itemView.findViewById(R.id.img_description)
            circularImg = itemView.findViewById(R.id.circular_img)
            imgCode = itemView.findViewById(R.id.img_code)
            checklistt = itemView.findViewById(R.id.checklistt)
        }

    }
}