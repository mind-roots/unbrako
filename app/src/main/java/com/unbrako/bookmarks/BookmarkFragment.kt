package com.unbrako.bookmarks

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.bookmarkEdit.BookmarkEditActivity
import com.unbrako.dataBase.BookMark
import kotlin.collections.ArrayList


class BookmarkFragment : androidx.fragment.app.Fragment() {

    lateinit var recyclerBookmarks:RecyclerView
    lateinit var editText: TextView
    lateinit var adapter: BookmarksAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_bookmark, container, false)
        recyclerBookmarks = view.findViewById(R.id.recylerViewEdit)
        editText = view.findViewById(R.id.editText)

        setHasOptionsMenu(true)
        clickEvents()


        return view
    }

    override fun onResume() {
        super.onResume()
        setAdapter()
    }
    private fun clickEvents() {
        editText.setOnClickListener {
            startActivity(Intent(activity!!, BookmarkEditActivity::class.java))
        }
    }

    private fun setAdapter() {

        getData(activity!!)

    }
    private fun getData(context: Context) {


        class GetProduct : AsyncTask<Void, Void, Void>() {
            lateinit var array:ArrayList<BookMark>
            override fun doInBackground(vararg params: Void?): Void? {

                val db = Constants.getDataBase(context)

                 array= db!!.productDao().getAllBookMarkArray() as ArrayList<BookMark>


                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)
                if (array.size<=0){
                    recyclerBookmarks.visibility=View.GONE
                    Toast.makeText(context,"No BookMarks", Toast.LENGTH_LONG).show()
                }else {
                    array.reverse()
                    recyclerBookmarks.visibility=View.VISIBLE
                    recyclerBookmarks.layoutManager = LinearLayoutManager(context)
                    adapter = BookmarksAdapter(activity!!, array )
                    recyclerBookmarks.adapter = adapter
                }
            }


        }
        GetProduct().execute()

    }



}

