package com.unbrako.bookmarkEdit

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.unbrako.R
import com.unbrako.dataBase.BookMark
import kotlinx.android.synthetic.main.bookmarkedit_listdata.view.*


class BookmarkEditAdapter(
    val context: Context, val bookmarkEditModel: ArrayList<BookMark>)
    : RecyclerView.Adapter<BookmarkEditAdapter.ViewHolder>() {

    var update = context as UpdateBookmark
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(context)
            .inflate(R.layout.bookmarkedit_listdata, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return bookmarkEditModel.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val model: BookMark = bookmarkEditModel[p1]
        p0.itemView.text.text = model.Description
        p0.itemView.image.setImageResource( model.ProductImage)
        p0.itemView.img_code1.text = model.PartNumber
        p0.itemView.checklist1.isChecked = model.selection
        p0.itemView.checklist1.setOnClickListener {
            if (model.selection) {
                bookmarkEditModel[p1].selection = false
                notifyDataSetChanged()
                update.updateSelection(false, p1)
            } else {
                bookmarkEditModel[p1].selection = true
                notifyDataSetChanged()
                update.updateSelection(true, p1)
            }
        }
    }

    fun getSelectedList(): ArrayList<BookMark> {

        val list: ArrayList<BookMark> = arrayListOf()
        for (i in 0 until bookmarkEditModel.size) {
            if (bookmarkEditModel[i].selection) {
                list.add(bookmarkEditModel[i])
            }
        }
        return list
    }

    interface UpdateBookmark {
        fun updateSelection(b: Boolean, p1: Int)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}