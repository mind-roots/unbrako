package com.unbrako.bookmarkEdit

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.widget.NestedScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.dataBase.BookMark
import kotlinx.android.synthetic.main.activity_bookmark_edit.*
import java.util.*

@Suppress("DEPRECATION")
class BookmarkEditActivity : AppCompatActivity(), BookmarkEditAdapter.UpdateBookmark {

    private lateinit var recyclerViewEdit: RecyclerView
    private lateinit var toolbar: Toolbar
    private lateinit var markALl: TextView
    private lateinit var unmarkALl: TextView
    private lateinit var requestForQuote: TextView
    private lateinit var remove: TextView
    private lateinit var nestedScrollView: NestedScrollView
    private lateinit var adapter: BookmarkEditAdapter
    lateinit var fragment: Fragment
    lateinit var array:ArrayList<BookMark>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bookmark_edit)

        recyclerViewEdit = findViewById(R.id.recylerViewEdit)
        nestedScrollView = findViewById(R.id.nestedScrollView)
        toolbar = findViewById(R.id.toolbar_edit)
        markALl = findViewById(R.id.markALl)
        unmarkALl = findViewById(R.id.unmarkALl)
        requestForQuote = findViewById(R.id.request)
        remove = findViewById(R.id.remove)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //title = "Bookmarks"

        setAdapter()
        clickEvents()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun clickEvents() {
        markALl.setOnClickListener {
            if (array.size>0) {
                unmarkALl.visibility = View.VISIBLE
                markALl.visibility = View.GONE
                unmarkALl.setBackgroundColor(resources.getColor(R.color.white))
                unmarkALl.setTextColor(resources.getColor(R.color.buttonColor))
                requestForQuote.setTextColor(resources.getColor(R.color.grey))
                remove.setTextColor(resources.getColor(R.color.grey))
                for (i in 0 until array.size) {
                    array[i].selection = true

                }
                adapter.notifyDataSetChanged()
            }else{
                Toast.makeText(this@BookmarkEditActivity, "No Bookmark", Toast.LENGTH_LONG).show()

            }
        }




        unmarkALl.setOnClickListener{
            markALl.visibility = View.VISIBLE
            unmarkALl.visibility = View.GONE
            markALl.setBackgroundColor(resources.getColor(R.color.white))
            //                markALl.setBackgroundResource(R.drawable.text_select)
            markALl.setTextColor(resources.getColor(R.color.grey))
            requestForQuote.setTextColor(resources.getColor(R.color.grey))
            remove.setTextColor(resources.getColor(R.color.grey))
            for (i in 0 until array.size) {
                array[i].selection = false
            }
            adapter.notifyDataSetChanged()

        }

        requestForQuote.setOnClickListener {

            if (array.size>0) {

                var exist = 0
                for (i in 0 until array.size) {
                    if (array[i].selection) {
                        exist = 1
                        break
                    }
                }
                if (exist == 1) {
                    var text=""
                    for (i in 0 until array.size) {
                        if (array[i].selection) {
                             text=text+ "\n\n# PartNumber - "+array[i].PartNumber+"\n #Description - "+array[i].Description+"\n #Order quantity - "+array[i].BoxQuantity
                        }
                    }

                    Constants.showComposer(this@BookmarkEditActivity,text)
                } else {
                    Toast.makeText(this@BookmarkEditActivity, "Please mark  Bookmark", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }

            }else{
                Toast.makeText(this@BookmarkEditActivity, "No item in Bookmark", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            requestForQuote.setBackgroundColor(Color.parseColor("#FFFFFF"))
            //                markALl.setBackgroundResource(R.drawable.text_select)
            requestForQuote.setTextColor(Color.parseColor("#F37836"))
            remove.setTextColor(Color.parseColor("#464646"))
            markALl.setTextColor(Color.parseColor("#464646"))
            unmarkALl.setTextColor(Color.parseColor("#464646"))
        }
        remove.setOnClickListener {
            remove.setBackgroundColor(Color.parseColor("#FFFFFF"))
            //                markALl.setBackgroundResource(R.drawable.text_select)
            remove.setTextColor(Color.parseColor("#F37836"))
            requestForQuote.setTextColor(Color.parseColor("#464646"))
            markALl.setTextColor(Color.parseColor("#464646"))
            unmarkALl.setTextColor(Color.parseColor("#464646"))
            RemoveFromDb()

        }



    }

    private fun RemoveFromDb() {
        val selectedList: ArrayList<BookMark> = adapter.getSelectedList()


        class GetProduct : AsyncTask<Void, Void, Void>() {

            override fun doInBackground(vararg params: Void?): Void? {

                val db = Constants.getDataBase(this@BookmarkEditActivity)

                for (i in 0 until selectedList.size) {
                    abcloop@ for (k in 0 until array.size) {
                        if (selectedList[i].id == array[k].id) {
                            db!!.productDao().deleteBookMarkTable(array[k].PartNumber,array[k].Diameter)
                            array.removeAt(k)

                            break@abcloop
                        }
                    }
                }




                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)
                if (array.size == 0) {
                    markALl.visibility=View.VISIBLE
                    unmarkALl.visibility=View.GONE
                    nestedScrollView.visibility = View.GONE
                    no_Data.visibility = View.VISIBLE
                } else {
                    nestedScrollView.visibility = View.VISIBLE
                    no_Data.visibility = View.GONE
                }
                adapter.notifyDataSetChanged()
            }


        }
        GetProduct().execute()

    }

    private fun setAdapter() {
        getData(this@BookmarkEditActivity)
    }

    private fun getData(context: Context) {


        class GetProduct : AsyncTask<Void, Void, Void>() {

            override fun doInBackground(vararg params: Void?): Void? {

                val db = Constants.getDataBase(context)

                array= db!!.productDao().getAllBookMarkArray() as ArrayList<BookMark>


                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)
                if (array.size<=0){
                    recyclerViewEdit.visibility=View.GONE
                    Toast.makeText(context,"NO BookMarks", Toast.LENGTH_LONG).show()
                }else {
                    array.reverse()

                    recyclerViewEdit.visibility=View.VISIBLE

                    recyclerViewEdit.layoutManager =
                        LinearLayoutManager(this@BookmarkEditActivity)
                    adapter = BookmarkEditAdapter(this@BookmarkEditActivity,array)
                    recyclerViewEdit.adapter = adapter
                }
            }


        }
        GetProduct().execute()

    }


    override fun updateSelection(b: Boolean, p1: Int) {

        array[p1].selection = b
        adapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.bookmark_cancel, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == R.id.cancel_bookmark) {
           finish()
        }
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}