package com.unbrako.inch

import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.unbrako.R
import com.unbrako.dataBase.ProductInch
import kotlinx.android.synthetic.main.socket_reccycler_circle.view.*

class InchAdapter(
    val context: Context,
    val inchModel: ArrayList<ProductInch>,
    var row_index: Int
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<InchAdapter.MyViewHolder>() {

   var update = context as setLayout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(context).inflate(
            R.layout.socket_reccycler_circle,
            parent, false
        )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return inchModel.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = inchModel[position]

        holder.itemView.tv_socket.text = model.Diameter

        holder.itemView.setOnClickListener {
            row_index = position

            for (i in 0 until inchModel.size) {

                inchModel[i].status = "false"
            }
            inchModel[position].status = "true"
            holder.itemView.refreshDrawableState()

//            listener.onItemClicked(position)

            holder.itemView.scrollTo(0, 0)

            notifyDataSetChanged()
        }



        if (model.status == "true") {
           update.settingValues(model)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tv_socket.background = context.getDrawable(R.drawable.circle_text)
                holder.tv_socket.setTextColor(Color.parseColor("#F37836"))
            }


        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tv_socket.background = context.getDrawable(R.drawable.circular_textview)
                holder.tv_socket.setTextColor(Color.parseColor("#ffffff"))
            }
        }


    }

    class MyViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        var tv_socket: TextView

        init {
            tv_socket = itemView.findViewById(R.id.tv_socket)
        }


    }

    interface setLayout {
        fun settingValues(model: ProductInch)
    }

    interface RecyclerClickListener {
        fun onItemClicked(position: Int)
    }
}
