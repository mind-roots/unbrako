package com.unbrako.inch

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.dataBase.BookMark
import com.unbrako.dataBase.InchBox
import com.unbrako.dataBase.ProductInch
import kotlinx.android.synthetic.main.socket_swipe_recycler.view.*
import kotlin.collections.ArrayList

class InchSwipeAdapter(
    val context: Context,
    val socketSwipeModel:
    ArrayList<InchBox>,
    var positionS: Int,
    var modelt: ProductInch,
   var  id: String
) : RecyclerSwipeAdapter<InchSwipeAdapter.ViewHolder>() {
    var hide=context as HideSwipeInch
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(context).inflate(R.layout.socket_swipe_recycler, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {

        return socketSwipeModel.size
    }


    override fun onBindViewHolder(holder: ViewHolder, positionT: Int) {
        var model=InchBox()
        if ( positionS==0){
             model = socketSwipeModel[positionT]
        }else{
             model = socketSwipeModel[positionS]
            positionS=0
        }
        //holder.itemView.tv_length.text = model.Length
//        if (!model.Length.isEmpty()){
//            holder.itemView.tv_length.text = model.Diameter+" - " +model.Length
//        } else if (model.Length.isEmpty()){
//            holder.itemView.tv_length.text = model.Diameter
//        }
        if (model.Thread_type == "UNC") {
            if (!modelt.PitchUNC.isEmpty() && !model.Length.isEmpty()) {
                if (id=="23"){
                    holder.itemView.tv_length.text = model.Diameter + "  " + "UNC" + " X " + model.Length
                }else{
                    holder.itemView.tv_length.text = model.Diameter + " - " + modelt.PitchUNC + " X " + model.Length

                }
            } else if (modelt.PitchUNC.isEmpty() && !model.Length.isEmpty()) {
                holder.itemView.tv_length.text = model.Diameter + " - " + model.Length
            } else if (!modelt.PitchUNC.isEmpty() && model.Length.isEmpty()) {
                holder.itemView.tv_length.text = model.Diameter + " - " + modelt.PitchUNC
            } else if (modelt.PitchUNC.isEmpty() && model.Length.isEmpty()) {
                holder.itemView.tv_length.text = model.Diameter
            }
        }else{
            if (!modelt.PitchUNF.isEmpty() && !model.Length.isEmpty()) {
                if (id=="23"){
                    holder.itemView.tv_length.text = model.Diameter + "  " + "UNF" + " X " + model.Length
                }else{
                    holder.itemView.tv_length.text = model.Diameter + " - " + modelt.PitchUNF + " X " + model.Length

                }

            } else if (modelt.PitchUNF.isEmpty() && !model.Length.isEmpty()) {
                holder.itemView.tv_length.text = model.Diameter + " - " + model.Length
            } else if (!modelt.PitchUNF.isEmpty() && model.Length.isEmpty()) {
                holder.itemView.tv_length.text = model.Diameter + " - " + modelt.PitchUNF
            } else if (modelt.PitchUNF.isEmpty() && model.Length.isEmpty()) {
                holder.itemView.tv_length.text = model.Diameter
            }
        }
        if (model.Thread_type == "UNC"){
        holder.itemView.tv_tpi.text = modelt.PitchUNC
        }else{
            holder.itemView.tv_tpi.text = modelt.PitchUNF

        }
//        if ((!model.DescriptionTwo.isEmpty()) && (modelt.ProductID == "24")){
//        holder.itemView.tv_part_description.visibility = View.VISIBLE
//            holder.itemView.tv_part_description.text = model.DescriptionTwo
//        }
//        else{
//            holder.itemView.tv_part_description.visibility = View.GONE
//        }
        if ((model.DescriptionTwo.isEmpty())||model.DescriptionTwo.equals("N/A")||model.DescriptionTwo.equals("n/a")){
            holder.itemView.tv_part_description.visibility = View.GONE
        }else{
            holder.itemView.tv_part_description.visibility = View.VISIBLE
            holder.itemView.tv_part_description.text = model.DescriptionTwo
        }

        holder.itemView.tv_part.text = model.PartNumber
        holder.itemView.tv_box_qty.text = model.BoxQuantity
//        if (model.isOpen
        holder.itemView.swipe.showMode = SwipeLayout.ShowMode.PullOut

        holder.itemView.swipe.addDrag(
            SwipeLayout.DragEdge.Right,
            holder.itemView.swipe
                .findViewById(R.id.bottom_wrapper)
        )

        holder.itemView.swipe.addSwipeListener(object : SwipeLayout.SwipeListener{

            override fun onOpen(layout: SwipeLayout?) {

            }

            override fun onUpdate(layout: SwipeLayout?, leftOffset: Int, topOffset: Int) {

            }

            override fun onStartOpen(layout: SwipeLayout?) {
                hide.hideSwipingArrowInch()
            }

            override fun onStartClose(layout: SwipeLayout?) {
            }

            override fun onHandRelease(layout: SwipeLayout?, xvel: Float, yvel: Float) {
            }

            override fun onClose(layout: SwipeLayout?) {
            }

        })
        holder.itemView.bookmark.setOnClickListener{
           // saveToDatabase(model,context)
            var modelBookMark=BookMark()
            modelBookMark.id=model.inch_box_id
            modelBookMark.PartNumber=model.PartNumber
            modelBookMark.ProductID=model.ProductID
            modelBookMark.Description=model.bookmark_desc
            modelBookMark.Diameter=model.Diameter
            modelBookMark.type="1"
            modelBookMark.BoxQuantity=model.BoxQuantity
            modelBookMark.ProductImage = Constants.getProductImageForBookmark(model.ProductID)
            Constants.BookMarkItem(context,modelBookMark)
            holder.itemView.swipe.toggle(true)
        }
        holder.itemView.email.setOnClickListener{
            var text="\n\n# PartNumber - "+model.PartNumber+"\n #Description - "+model.bookmark_desc+"\n #Order quantity - "+model.BoxQuantity
            Constants.showComposer(context,text)
            holder.itemView.swipe.toggle(true)

        }
        mItemManger.bindView(holder.itemView, positionT)

    }

   // private fun saveToDatabase(model: InchBox, context: Context) {





   // }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe
    }


    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

    interface HideSwipeInch{
        fun hideSwipingArrowInch()
    }
}