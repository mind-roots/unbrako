package com.unbrako.inch

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Rect
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.annotation.RequiresApi
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.catalog.RecyclerModel
import com.unbrako.dataBase.InchBox
import com.unbrako.dataBase.ProductInch
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView


class InchActivity : AppCompatActivity(), InchAdapter.setLayout,InchSwipeAdapter.HideSwipeInch {
//    , InchAdapter.RecyclerClickListener


    private lateinit var toolbar9: Toolbar
    lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    lateinit var recyclerViewSocket: androidx.recyclerview.widget.RecyclerView
    lateinit var recyclerModel: RecyclerModel
    lateinit var tv_inch_grade: TextView
    lateinit var cl_inch_grade: LinearLayout
    lateinit var pitchLay2: LinearLayout
    lateinit var stpfLay: LinearLayout
    lateinit var tsLay: LinearLayout
    lateinit var tapdrillLay: LinearLayout
    lateinit var tensileLay2: LinearLayout
    lateinit var tv_inch_thread: TextView
    lateinit var cl_inch_thread: LinearLayout
    lateinit var tv_inch_pitch_one: TextView
    lateinit var tv_inch_pitch_two: TextView
    lateinit var cl_inch_pitch: LinearLayout
    lateinit var tv_inch_tsa_one: TextView
    lateinit var tv_inch_tsa_two: TextView
    lateinit var cl_inch_tsa: LinearLayout
    lateinit var tv_inch_nsa_one: TextView
    lateinit var tv_inch_nsa_two: TextView
    lateinit var cl_inch_nsa: LinearLayout
    lateinit var stpfoneLay: LinearLayout
    lateinit var tv_inch_st_one: TextView
    lateinit var tv_inch_st_two: TextView
    lateinit var cl_inch_st: LinearLayout
    lateinit var tv_inch_ts_one: TextView
    lateinit var tv_inch_ts_two: TextView
    lateinit var cl_inch_ts: LinearLayout
    lateinit var tv_inch_ctr: TextView
    lateinit var cl_inch_ctr: LinearLayout
    lateinit var tv_inch_body: TextView
    lateinit var cl_inch_body: LinearLayout
    lateinit var tv_inch_tap_one: TextView
    lateinit var tv_inch_tap_two: TextView
    lateinit var cl_inch_tap: LinearLayout
    lateinit var tv_inch_od_max: TextView
    lateinit var cl_inch_od_max: LinearLayout

    lateinit var tv_inch_od_min: TextView
    lateinit var cl_inch_od_min: LinearLayout
    lateinit var arrow: ImageView
    lateinit var tv_inch_shear: TextView
    lateinit var cl_inch_shear: LinearLayout

    lateinit var tv_inch_screw: TextView
    lateinit var cl_inch_screw: LinearLayout

    lateinit var tv_inch_diam_one: TextView
    lateinit var tv_inch_diam_two: TextView
    lateinit var cl_body_diam: LinearLayout

    lateinit var cl_inch_hardness: LinearLayout
    lateinit var tv_inch_hardness: TextView
    lateinit var cl_inch_socket_depth: LinearLayout
    lateinit var cl_inch_pitchTpi: LinearLayout
    lateinit var tv_inch_pitchTpi: TextView
    lateinit var tv_inch_socket_depth: TextView

    lateinit var text_view_catalog: TextView
    lateinit var tsa_ts: TextView
    lateinit var tvUnf: TextView

    lateinit var partLay: RelativeLayout
    lateinit var max: TextView
    lateinit var max_1: TextView
    lateinit var max_15: TextView

    lateinit var stpf_lbs_inch: TextView
    lateinit var stpf_ll: LinearLayout

    lateinit var tapdrill_ll: LinearLayout

    lateinit var pitch_ll: LinearLayout

 lateinit var tsa_lbs: TextView
    lateinit var tsa_ll: LinearLayout

    lateinit var stpf_ll_one: LinearLayout

    lateinit var scrollViewInch: ScrollView
    lateinit var rotate: Animation
    lateinit var textView2: TextView

    lateinit var tvbmax: TextView
    lateinit var tvbmin: TextView
    lateinit var ssb: TextView
    lateinit var tvStrength: TextView

    private var isShown = false

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setLayout()
        scrollViewInch.setOnScrollChangeListener(
            (View.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                showCase()
                return@OnScrollChangeListener

            })
        )

        arrow.visibility = View.INVISIBLE

    }

    private fun showCase(){
        var value = Constants.getSharedPrefs(this).getString("showOnce", "one")

        if (value == "one"){
            val view = recyclerViewSocket.getChildAt(0)
            if (getVisibility(view) && !isShown) {
                isShown = true
                scrollViewInch.fling(0)
                MaterialShowcaseView.Builder(this)
                    .setTarget(view)
                    .withRectangleShape()
                    .setDismissText("GOT IT")
                    .singleUse("1")

                    .setMaskColour(Color.parseColor("#CBF37836"))
                    .setContentText("Swipe left on a row to Bookmark the item or get a quote.")
                    //  .singleUse(SHOWCASE_ID) // provide a unique ID used to ensure it is only shown once
                    .show()
                Constants.getSharedPrefs(this).edit().putString("showOnce", "dcd").apply()
            }
        }


    }

    private fun getVisibility(view: View): Boolean {
        val rect = Rect()
        if (view.getGlobalVisibleRect(rect)
            && view.height == rect.height()
            && view.width == rect.width()
        ) {
            return true// view is fully visible on screen
        }

        return false
    }

    private fun init() {
        recyclerView = findViewById(R.id.recycler_view)
        recyclerViewSocket = findViewById(R.id.recycler_view_socket)
        recyclerViewSocket.isNestedScrollingEnabled = false
        stpfoneLay = findViewById(R.id.stpfoneLay)
        tapdrillLay = findViewById(R.id.tapdrillLay)
        tsLay = findViewById(R.id.tsLay)
        stpfLay = findViewById(R.id.stpfLay)
        pitchLay2 = findViewById(R.id.pitchLay2)
        tensileLay2 = findViewById(R.id.tensileLay2)
        arrow = findViewById(R.id.arrow)
        partLay = findViewById(R.id.partLay)
        tv_inch_grade = findViewById(R.id.tv_inch_grade)
        cl_inch_grade = findViewById(R.id.cl_inch_grade)

        tv_inch_thread = findViewById(R.id.tv_inch_thread)
        cl_inch_thread = findViewById(R.id.cl_inch_thread)

        tv_inch_pitch_one = findViewById(R.id.tv_inch_pitch_one)
        tv_inch_pitch_two = findViewById(R.id.tv_inch_pitch_two)
        cl_inch_pitch = findViewById(R.id.cl_inch_pitch)

        tv_inch_tsa_one = findViewById(R.id.tv_inch_tsa_one)
        tv_inch_tsa_two = findViewById(R.id.tv_inch_tsa_two)
        cl_inch_tsa = findViewById(R.id.cl_inch_tsa)

        tv_inch_nsa_one = findViewById(R.id.tv_inch_nsa_one)
        tv_inch_nsa_two = findViewById(R.id.tv_inch_nsa_two)
        cl_inch_nsa = findViewById(R.id.cl_inch_nsa)

        tv_inch_st_one = findViewById(R.id.tv_inch_st_one)
        tv_inch_st_two = findViewById(R.id.tv_inch_st_two)
        cl_inch_st = findViewById(R.id.cl_inch_st)

        tv_inch_ts_one = findViewById(R.id.tv_inch_ts_one)
        tv_inch_ts_two = findViewById(R.id.tv_inch_ts_two)
        cl_inch_ts = findViewById(R.id.cl_inch_ts)

        tv_inch_ctr = findViewById(R.id.tv_inch_ctr)
        cl_inch_ctr = findViewById(R.id.cl_inch_ctr)

        tv_inch_body = findViewById(R.id.tv_inch_body)
        cl_inch_body = findViewById(R.id.cl_inch_body)

        tv_inch_tap_one = findViewById(R.id.tv_inch_tap_one)
        tv_inch_tap_two = findViewById(R.id.tv_inch_tap_two)
        cl_inch_tap = findViewById(R.id.cl_inch_tap)

        tv_inch_od_max = findViewById(R.id.tv_inch_od_max)
        cl_inch_od_max = findViewById(R.id.cl_inch_od_max)

        tv_inch_od_min = findViewById(R.id.tv_inch_od_min)
        cl_inch_od_min = findViewById(R.id.cl_inch_od_min)

        tv_inch_shear = findViewById(R.id.tv_inch_shear)
        cl_inch_shear = findViewById(R.id.cl_inch_shear)

        tv_inch_screw = findViewById(R.id.tv_inch_screw)
        cl_inch_screw = findViewById(R.id.cl_inch_screw)

        tv_inch_diam_one = findViewById(R.id.tv_inch_diam_one)
        tv_inch_diam_two = findViewById(R.id.tv_inch_diam_two)
        cl_body_diam = findViewById(R.id.cl_body_diam)

        pitch_ll = findViewById(R.id.pitch_ll)
        tsa_ll = findViewById(R.id.tsa_ll)
        tsa_lbs = findViewById(R.id.tsa_lbs)
        tapdrill_ll = findViewById(R.id.tapdrill_ll)

        stpf_ll = findViewById(R.id.stpf_ll)
        stpf_lbs_inch = findViewById(R.id.stpf_lbs_inch)

        text_view_catalog = findViewById(R.id.text_view_catalog)

        max_1 = findViewById(R.id.max_1)
        scrollViewInch = findViewById(R.id.scrollViewInch)

        cl_inch_hardness = findViewById(R.id.cl_inch_hardness)
        tv_inch_hardness = findViewById(R.id.tv_inch_hardness)
        cl_inch_socket_depth = findViewById(R.id.cl_inch_socket_depth)
        tv_inch_socket_depth = findViewById(R.id.tv_inch_socket_depth)
        tv_inch_pitchTpi = findViewById(R.id.tv_inch_pitchTpi)
        cl_inch_pitchTpi = findViewById(R.id.cl_inch_pitchTpi)

        textView2 = findViewById(R.id.textView2)
        stpf_ll_one = findViewById(R.id.stpf_ll_one)


    }

    private fun setLayout() {
        recyclerModel = intent.getParcelableExtra("parcel_value")!!

        if (recyclerModel.id == "14") {
            setContentView(R.layout.activity_inch)
            max_15 = findViewById(R.id.max_15)
            max = findViewById(R.id.max)
            tvStrength = findViewById(R.id.tvStrength)


        } else if (recyclerModel.id == "27") {
            setContentView(R.layout.inch_low_socket)
            max_15 = findViewById(R.id.max_15)
            max = findViewById(R.id.max)
            tvStrength = findViewById(R.id.tvStrength)



        } else if (recyclerModel.id == "23") {
            setContentView(R.layout.inch_socket_head_shoulder)
            max_15 = findViewById(R.id.max_15)
            max = findViewById(R.id.max)
            tvUnf = findViewById(R.id.tvUnf)
            tsa_ts = findViewById(R.id.tsa_ts)
            tvbmax = findViewById(R.id.tvbmax)
            tvbmin = findViewById(R.id.tvbmin)
            tvStrength = findViewById(R.id.tvStrength)
            ssb = findViewById(R.id.ssb)

        } else if (recyclerModel.id == "19") {
            setContentView(R.layout.inch_flat_socket_head)
            max_15 = findViewById(R.id.max_15)
            max = findViewById(R.id.max)
            tvStrength = findViewById(R.id.tvStrength)

        } else if (recyclerModel.id == "20") {
            setContentView(R.layout.inch_button_socket_head)
            max = findViewById(R.id.max)
            max_15 = findViewById(R.id.max_15)
            tvStrength = findViewById(R.id.tvStrength)

        } else if (recyclerModel.id == "21") {
            setContentView(R.layout.inch_flange_button_socket)
            max = findViewById(R.id.max)
            max_1 = findViewById(R.id.max_1)
            max_15 = findViewById(R.id.max_15)
            tvStrength = findViewById(R.id.tvStrength)

        } else if (recyclerModel.id == "24") {
            setContentView(R.layout.inch_socket_set_screw)
            tvStrength = findViewById(R.id.tvStrength)


        } else if (recyclerModel.id == "28") {
            setContentView(R.layout.inch_tapper_pressure_plug)
            tvStrength = findViewById(R.id.tvStrength)

        }

//        3/4 taper pressure
        else if (recyclerModel.id == "40") {
            setContentView(R.layout.inch_tapper_pressure_plug)
        } else if (recyclerModel.id == "41") {
            setContentView(R.layout.inch_tapper_pressure_plug)

        } else if (recyclerModel.id == "42") {
            setContentView(R.layout.inch_tapper_pressure_plug)

        } else if (recyclerModel.id == "43") {
            setContentView(R.layout.inch_tapper_pressure_plug)

        } else if (recyclerModel.id == "38") {

            setContentView(R.layout.socket_set_screw_kurnled_inch)
            tvStrength = findViewById(R.id.tvStrength)

        } else if (recyclerModel.id == "39") {
            setContentView(R.layout.socket_set_screw_plaincup_inch)
            tvStrength = findViewById(R.id.tvStrength)

        }
//        7/8 taper pressure
        else if (recyclerModel.id == "44") {
            setContentView(R.layout.inch_7_8_tapper)
        } else if (recyclerModel.id == "45") {
            setContentView(R.layout.inch_7_8_tapper)
        } else if (recyclerModel.id == "46") {
            setContentView(R.layout.inch_7_8_tapper)
        } else if (recyclerModel.id == "47") {
            setContentView(R.layout.inch_7_8_tapper)
        }


        toolbar9 = findViewById(R.id.toolbar_edit9)
        setSupportActionBar(toolbar9)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
      //  title = "Catalog"

        init()

        layoutManager()
        setAdapter()
        setBannerName()

//        recyclerView.setOnClickListener {
//           scrollViewInch.scrollTo(0,0)
//
//        }


//        refresh()

    }

    private fun showHideLayout(model: ProductInch) {

        if (model.Grade.trim().isEmpty()) {
            cl_inch_grade.visibility = View.GONE
        } else {
            cl_inch_grade.visibility = View.VISIBLE
        }
        if (model.Pitch_TPI.trim().isEmpty()) {
            cl_inch_pitchTpi.visibility = View.GONE
        } else {
            cl_inch_pitchTpi.visibility = View.VISIBLE
        }

        if (model.Thread.trim().isEmpty()) {
            cl_inch_thread.visibility = View.GONE
        } else {
            cl_inch_thread.visibility = View.VISIBLE
        }

        if ((model.PitchUNC.trim().isEmpty()) && (model.PitchUNF.trim().isEmpty())) {
            cl_inch_pitch.visibility = View.GONE
        } else {
            cl_inch_pitch.visibility = View.VISIBLE
        }
        if ((model.TensileStressAreaUNC.trim().isEmpty()) && (model.TensileStressAreaUNF.isEmpty())) {

            cl_inch_tsa.visibility = View.GONE
        } else {
            cl_inch_tsa.visibility = View.VISIBLE
        }
        if ((model.NominalShankAreaUNC.trim().isEmpty()) && (model.NominalShankAreaUNF.trim().isEmpty())) {
            cl_inch_nsa.visibility = View.GONE
        } else {
            cl_inch_nsa.visibility = View.VISIBLE
        }
        if ((model.SeatingTorqueInlbsUNC.trim().isEmpty()) && (model.SeatingTorqueInlbsUNF.trim().isEmpty())) {
            cl_inch_st.visibility = View.GONE
        } else {
            cl_inch_st.visibility = View.VISIBLE
        }
        if ((model.TensileStrengthlbsUNC.trim().isEmpty()) && (model.TensileStrengthlbsUNF.trim().isEmpty())) {
            cl_inch_ts.visibility = View.GONE
        } else {
            cl_inch_ts.visibility = View.VISIBLE
        }
        if (model.CtrBore.trim().isEmpty()) {
            cl_inch_ctr.visibility = View.GONE
        } else {
            cl_inch_ctr.visibility = View.VISIBLE
        }
        if (model.BodyDrill.trim().isEmpty()) {
            cl_inch_body.visibility = View.GONE
        } else {
            cl_inch_body.visibility = View.VISIBLE
        }
        if ((model.TapDrillUNC.trim().isEmpty()) && (model.TapDrillUNF.trim().isEmpty())) {
            cl_inch_tap.visibility = View.GONE
        } else {
            cl_inch_tap.visibility = View.VISIBLE
        }
        if (model.BodyODMax.trim().isEmpty()) {
            cl_inch_od_max.visibility = View.GONE
        } else {
            cl_inch_od_max.visibility = View.VISIBLE
        }
        if (model.BodyODMin.trim().isEmpty()) {
            cl_inch_od_min.visibility = View.GONE
        } else {
            cl_inch_od_min.visibility = View.VISIBLE
        }
        if (model.ShearStrengthOfBody.trim().isEmpty()) {
            cl_inch_shear.visibility = View.GONE
        } else {
            cl_inch_shear.visibility = View.VISIBLE
        }
        if (model.ScrewLengthMin.trim().isEmpty()) {
            cl_inch_screw.visibility = View.GONE
        } else {
            cl_inch_screw.visibility = View.VISIBLE
        }
        if ((model.DiamDecimal.trim().isEmpty()) && (model.DiamMM.trim().isEmpty())) {
            cl_body_diam.visibility = View.GONE
        } else {
            cl_body_diam.visibility = View.VISIBLE
        }

        if (recyclerModel.id == "24") {
            cl_inch_screw.visibility = View.GONE
        }

        if (recyclerModel.id == "40"||recyclerModel.id == "41"
            ||recyclerModel.id == "42"||recyclerModel.id == "43"
            ||recyclerModel.id == "44"||recyclerModel.id == "45"||recyclerModel.id == "46"
            ||recyclerModel.id == "47") {
            cl_inch_hardness.visibility = View.VISIBLE
            cl_inch_socket_depth.visibility = View.VISIBLE
        }


    }

    @SuppressLint("WrongConstant")
    private fun layoutManager() {

        val linearmanager1 = androidx.recyclerview.widget.LinearLayoutManager(
            this,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerView.layoutManager = linearmanager1
        recyclerView.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()

        val linearManagaer2 = androidx.recyclerview.widget.LinearLayoutManager(
            this,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            false
        )
        recyclerViewSocket.layoutManager = linearManagaer2

        val dividerItemDecoration = androidx.recyclerview.widget.DividerItemDecoration(
            recyclerView.context,
            linearManagaer2.orientation
        )
        recyclerViewSocket.addItemDecoration(dividerItemDecoration)

    }


    private fun setAdapter() {
        //  getValues(this@InchActivity)
        GetProduct(this@InchActivity, recyclerModel, recyclerView).execute()
//            , this!!.listener!!

    }


    //  private fun getValues(context: Context) {

    inner class GetProduct(
        var context: InchActivity,
        var recyclerModel: RecyclerModel,
        var recyclerView: androidx.recyclerview.widget.RecyclerView
//        var listener : Constants.ClickListener
    ) : AsyncTask<Void, Void, Void>() {
        var arrayList: ArrayList<ProductInch> = arrayListOf()
        var arrayList2: ArrayList<ProductInch> = arrayListOf()
        var arrayListInchBox: ArrayList<InchBox> = arrayListOf()
        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(context)

            arrayList = db!!.productDao().getAllInchProducts(recyclerModel.id, "1") as ArrayList<ProductInch>
            for (i in 0 until arrayList.size) {
                arrayListInchBox =
                    db!!.productDao().getAllInchBox(recyclerModel.id, arrayList[i].Diameter, "1") as ArrayList<InchBox>
                if (arrayListInchBox.size > 0) {
                    arrayList2.add(arrayList[i])
                }
            }

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            var position = 0

            if (arrayList2.size > 0) {
                arrayList2 = Constants.sortInchArray(arrayList2, this@InchActivity)
                if (recyclerModel.Diameter.equals("0")) {
                    arrayList2[0].status = "true"
                } else {
                    for (i in 0 until arrayList2.size) {
                        if (recyclerModel.Diameter.equals(arrayList2[i].Diameter)) {
                            arrayList2[i].status = "true"
                            position = i
                            break
                        }
                    }
                }

                recyclerView.adapter = InchAdapter(context, arrayList2, -1)
//                    , listener
                recyclerView.getLayoutManager()!!.scrollToPosition(position)
            } else {


            }
        }
        // }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun settingValues(model: ProductInch) {
        setSwipe(model)

        tv_inch_pitchTpi.text = (model.Pitch_TPI)
        tv_inch_grade.text = (model.Grade)
        tv_inch_thread.text = (model.Thread)
        tv_inch_pitch_one.text = (model.PitchUNC)

//        pitch
        tv_inch_pitch_two.text = (model.PitchUNF)
        if (model.PitchUNF.isEmpty()){
            pitchLay2.visibility = View.GONE

        }
        else{
            pitchLay2.visibility = View.VISIBLE
        }

        tv_inch_tsa_one.text = (model.TensileStressAreaUNC)

//        tsa
        tv_inch_tsa_two.text = (model.TensileStressAreaUNF)

        if (model.TensileStressAreaUNF.isEmpty()){
            tensileLay2.visibility = View.GONE

        }
        else{
            tensileLay2.visibility = View.VISIBLE

        }



        tv_inch_nsa_one.text = (model.NominalShankAreaUNC)
        tv_inch_nsa_two.text = (model.NominalShankAreaUNF)

//        stpf onee
        tv_inch_st_one.text = (model.SeatingTorqueInlbsUNC)
        if (model.SeatingTorqueInlbsUNC.isEmpty()){
            stpfoneLay.visibility = View.GONE

        }
        else{
            stpfoneLay.visibility = View.VISIBLE

        }



        tv_inch_st_two.text = (model.SeatingTorqueInlbsUNF)
        if (model.SeatingTorqueInlbsUNF.isEmpty()){
            stpfLay.visibility = View.GONE

        }
        else{
            stpfLay.visibility = View.VISIBLE

        }

        tv_inch_ts_one.text = (model.TensileStrengthlbsUNC)
        tv_inch_ts_two.text = (model.TensileStrengthlbsUNF)
        tv_inch_ctr.text = (model.CtrBore)
        tv_inch_body.text = (model.BodyDrill)
        tv_inch_tap_one.text = (model.TapDrillUNC)

//        tap drill
        tv_inch_tap_two.text = (model.TapDrillUNF)
        if (model.TapDrillUNF.isEmpty()){
            tapdrillLay.visibility = View.GONE
        }
        else{
            tapdrillLay.visibility = View.VISIBLE
        }

        tv_inch_od_max.text = (model.BodyODMax)
        tv_inch_od_min.text = (model.BodyODMin)
        tv_inch_screw.text = (model.ScrewLengthMin)
        tv_inch_shear.text = (model.ShearStrengthOfBody)
        text_view_catalog.text = (model.AFNom)
        if (recyclerModel.id != "24" && recyclerModel.id != "38" && recyclerModel.id != "39" && recyclerModel.id != "40" && recyclerModel.id != "41"
            && recyclerModel.id != "42" && recyclerModel.id != "43" && recyclerModel.id != "44" && recyclerModel.id != "45" && recyclerModel.id != "46" && recyclerModel.id != "47"
        ) {
            max.text = (model.HeadODMax)
            max_15.text = (model.ThreadLengthRef)
        }
        if (recyclerModel.id=="21"){
            max_1.text = (model.HeadODMax)
            max.text = (model.HeadHeightMax)
        }else{
            max_1.text = (model.HeadHeightMax)
        }
        tv_inch_diam_one.text = (model.DiamDecimal)
        tv_inch_diam_two.text = (model.DiamMM)
        tv_inch_shear.text = (model.ShearStrengthOfBody)

        if (recyclerModel.id == "24"||recyclerModel.id == "39") {
            max_1.text = (model.ScrewLengthMin)
        }
        if (recyclerModel.id == "40"||recyclerModel.id == "44"||recyclerModel.id == "45"||recyclerModel.id == "46"||recyclerModel.id == "47") {
            max_1.text = (model.ScrewLengthMin)
            tv_inch_hardness.text = (model.hardness)
            tv_inch_socket_depth.text = (model.SocketDepth)
        }
        if (recyclerModel.id == "41") {
            max_1.text = (model.ScrewLengthMin)
            tv_inch_hardness.text = (model.hardness)
            tv_inch_socket_depth.text = (model.SocketDepth)
        }
        if (recyclerModel.id == "42") {
            max_1.text = (model.ScrewLengthMin)
            tv_inch_hardness.text = (model.hardness)
            tv_inch_socket_depth.text = (model.SocketDepth)
        }
        if (recyclerModel.id == "43") {
            max_1.text = (model.ScrewLengthMin)
            tv_inch_hardness.text = (model.hardness)
            tv_inch_socket_depth.text = (model.SocketDepth)
        }

        if (recyclerModel.id == "23") {
            tv_inch_thread.text = (model.Thread)+ " - "+ (model.PitchUNC)

            tvbmax.text = (model.BodyODMax)
            tvbmin.text = (model.BodyODMin)
        }
        scrollViewInch.scrollTo(0, 0)
        showHideLayout(model)
        if (recyclerModel.id == "38") {
            max_1.text = (model.ScrewLengthMin)
        }
        if (recyclerModel.id == "38" || recyclerModel.id == "39") {
            cl_inch_screw.visibility = View.GONE
            tvStrength.text = "Hardness:"
        }
        if (recyclerModel.id == "40" || recyclerModel.id == "41"
            || recyclerModel.id == "42" || recyclerModel.id == "43"||
            recyclerModel.id == "44"|| recyclerModel.id == "45"||
            recyclerModel.id == "46"|| recyclerModel.id == "47"
        ) {
            cl_inch_thread.visibility = View.GONE
            cl_inch_screw.visibility = View.GONE
            cl_inch_socket_depth.visibility = View.GONE
            max_1.text = model.ScrewLengthMin

        }
        if (recyclerModel.id == "39"){
            max_1.text = model.ScrewLengthMin
            max_1.visibility = View.VISIBLE
        }
        if (recyclerModel.id == "23"){
            tsa_ts.text = "Tensile Strength"
            ssb.text = "Single Shear Strength of Body:"
            tsa_lbs.text = "lbs"

            tvUnf.visibility = View.GONE
        }
        if (recyclerModel.id == "19" || recyclerModel.id == "20" || recyclerModel.id == "21"){
            cl_inch_ctr.visibility = View.GONE
        }
//        if (recyclerModel.id == "40" || recyclerModel.id == "41"
//            || recyclerModel.id == "42"|| recyclerModel.id == "43"
//            || recyclerModel.id == "44" || recyclerModel.id == "45"
//            || recyclerModel.id == "46"|| recyclerModel.id == "47"){
//        }

        if (recyclerModel.id == "23"){
            cl_inch_pitch.visibility = View.GONE
            cl_inch_od_max.visibility = View.GONE
            cl_inch_od_min.visibility = View.GONE
            cl_body_diam.visibility = View.GONE
            cl_inch_screw.visibility = View.GONE
        }

    }

    private fun setSwipe(model: ProductInch) {

        var arrayList: ArrayList<InchBox> = arrayListOf()

        class GetProduct : AsyncTask<Void, Void, Void>() {

            override fun doInBackground(vararg params: Void?): Void? {

                var db = Constants.getDataBase(this@InchActivity)

                arrayList = db!!.productDao().getAllInchBox(recyclerModel.id, model.Diameter, "1") as ArrayList<InchBox>

                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)
                var ll = 0
                if (arrayList.size > 0) {
                    arrayList = Constants.sortInchBox(arrayList, this@InchActivity)
                    partLay.visibility = View.VISIBLE
                    for (i in 0 until arrayList.size) {
                        if (recyclerModel.Diameter == "0") {

                        } else {
                            for (i in 0 until arrayList.size) {
                                if (recyclerModel.Diameter.equals(arrayList[i].Diameter)) {

                                    ll = i
                                    break
                                }
                            }
                        }
                    }
                    recyclerViewSocket.adapter = InchSwipeAdapter(this@InchActivity, arrayList, ll, model,recyclerModel.id)
                    rotate = AnimationUtils.loadAnimation(this@InchActivity, R.anim.swipe)
                    rotate.repeatCount = Animation.INFINITE
                  //  arrow.startAnimation(rotate)
                } else {
                    partLay.visibility = View.GONE
                    // Toast.makeText(this@InchActivity,"No Data", Toast.LENGTH_LONG).show()
                }
            }
        }
        GetProduct().execute()
    }

    private fun setBannerName() {
        if (recyclerModel.id == "40") {
            textView2.text = "BSPT Thread Black Oxide"
        }
        if (recyclerModel.id == "41") {
            textView2.text = "NPTF Dryseal Thread Black Oxide"
        }
        if (recyclerModel.id == "42") {
            textView2.text = "NPTF Dryseal Thread Brass"
        }
        if (recyclerModel.id == "43") {
            textView2.text = "NPTF Dryseal Thread Stainless Steel"
        }
        if (recyclerModel.id == "44") {
            textView2.text = "PTF LEVL-SEAL Thread Black Oxide"
        }
        if (recyclerModel.id == "45") {
            textView2.text = "PTF LEVL-SEAL Thread Teflon Coated"
        }
        if (recyclerModel.id == "46") {
            textView2.text = "PTF LEVL-SEAL Thread Brass"
        }
        if (recyclerModel.id == "47") {
            textView2.text = "PTF LEVL-SEAL Stainless Steel"
        }


    }

    override fun hideSwipingArrowInch() {
        arrow.clearAnimation()
        arrow.visibility = View.GONE
    }

}