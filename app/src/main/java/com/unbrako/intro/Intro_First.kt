package com.unbrako.intro

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.unbrako.Constants
import com.unbrako.R


class Intro_First : androidx.fragment.app.Fragment() {

    lateinit var btnContinue: Button
    lateinit var moveToNext: swapToNextFragment
    lateinit var skip: TextView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.activity_intro, container, false)
        init(view)
        clickEvents()
        return view
    }

    private fun init(view: View) {
        btnContinue = view.findViewById(R.id.btnContinue)
        skip = view.findViewById(R.id.skip)
    }

    private fun clickEvents() {
        btnContinue.setOnClickListener {
            moveToNext.callSecondFragment()


        }

        skip.setOnClickListener {
//            val prefs = Constants.getSharedPrefs(activity!!).edit()
//            prefs.putString("LoggedIn", "1")
//            prefs.apply()
            val prefs = Constants.getSharedPrefs(activity!!).edit()
            prefs.putString("SelectApp", "1")
            prefs.apply()
            val intent = Intent(activity, SelectApp::class.java)
            startActivity(intent)
            activity!!.finish()
        }
    }

    interface swapToNextFragment {
        fun callSecondFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        moveToNext = context as swapToNextFragment
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        moveToNext = activity as swapToNextFragment
    }


}