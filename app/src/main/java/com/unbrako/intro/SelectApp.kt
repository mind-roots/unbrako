package com.unbrako.intro


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.dashBoard.DashboardActivity
import com.unbrako.e_Commerce.login.LoginActivity
import java.util.*


class SelectApp : AppCompatActivity() {

    lateinit var technicalSelector: RelativeLayout
    lateinit var estoreSelect: RelativeLayout
    lateinit var loginViewModel: SelectEcommViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_select_app)
        loginViewModel = ViewModelProviders.of(this)[SelectEcommViewModel::class.java]

        init()
        clickEvents()
loginViewModel.getCountryDetails().observe(this, androidx.lifecycle.Observer {


    if (it.countryCode == "IN"){

        val prefs = Constants.getSharedPrefs(this).edit()
        prefs.putString("LoggedIn", "1")
        prefs.putString("from", "Emain")
        prefs.apply()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()

    }else{
        Toast.makeText(this, "Sorry this app is not available for your country", Toast.LENGTH_SHORT).show()
    }
})
    }

    private fun clickEvents() {

        technicalSelector.setOnClickListener {
            val prefs = Constants.getSharedPrefs(this).edit()
            prefs.putString("LoggedIn", "1")
            prefs.putString("App", "main")
            prefs.putString("from", "main")
            prefs.apply()
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
            finish()

        }


        estoreSelect.setOnClickListener {
            loginViewModel.getCountryCode()

        }
    }

    private fun init() {
        estoreSelect=findViewById(R.id.estoreSelect)
        technicalSelector=findViewById(R.id.technicalSelector)

    }


}
