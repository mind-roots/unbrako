package com.unbrako.intro

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.unbrako.Constants
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class SelectEcommRepo(var application: Application) {

    val mData = MutableLiveData<ModelCountryCode>()
    fun getCurrentCountryCode(){

        val retrofit = Retrofit.Builder()
            .baseUrl("http://ip-api.com/")
            .build()

        val service = retrofit.create(SelectEcommService::class.java)
        val call: Call<ResponseBody> = service.getCountryCode()
        call.enqueue(object : Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        Log.i("POST RES", res)
                        val json = JSONObject(res)
                        var modelCountryCode=ModelCountryCode()
                        modelCountryCode.countryCode=json.optString("countryCode")
                        mData.value=modelCountryCode
                        Constants.countryCode = json.optString("countryCode")
                    Constants.getPrefs(application).edit().putString("countryCode",json.optString("countryCode")).apply()

                    }catch (e : Exception){
                        e.printStackTrace()
                    }
                }


            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "No internet connection", Toast.LENGTH_SHORT).show()
            }


        })

    }

    fun getCountryDetails(): MutableLiveData<ModelCountryCode> {

        return mData
    }


}