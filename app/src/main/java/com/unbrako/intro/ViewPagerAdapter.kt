package com.unbrako.intro

import androidx.fragment.app.Fragment

class ViewPagerAdapter internal constructor(fm: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment = Intro_First()

        when (position) {
            0 -> fragment = Intro_First()
            1 -> fragment = Intro_Second()
           // 2 -> fragment = SelectApp()

        }

        return fragment
    }

    override fun getCount(): Int {
        return 2
    }

}
