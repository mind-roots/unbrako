package com.unbrako.intro

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class SelectEcommViewModel(application: Application) : AndroidViewModel(application){
    var refVar: SelectEcommRepo = SelectEcommRepo(application)

    fun getCountryCode(){
        refVar.getCurrentCountryCode()
    }

    fun getCountryDetails():MutableLiveData<ModelCountryCode> {
        return  refVar.getCountryDetails()

    }

}