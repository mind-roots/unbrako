package com.unbrako.intro

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.unbrako.Constants
import com.unbrako.R

class Intro_Second : androidx.fragment.app.Fragment() {

    lateinit var skip2: TextView
    lateinit var ready: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.intro_two, container, false)
        init(view)
        clickEvents()
        return view

    }

    private fun init(view: View) {
        skip2 = view.findViewById(R.id.skip2)
        ready = view.findViewById(R.id.ready)

    }

    private fun clickEvents() {

        skip2.setOnClickListener {
//            val prefs = Constants.getSharedPrefs(activity!!).edit()
//            prefs.putString("LoggedIn", "1")
//            prefs.apply()
            val prefs = Constants.getSharedPrefs(activity!!).edit()
            prefs.putString("SelectApp", "1")
            prefs.apply()

            val intent = Intent(activity, SelectApp::class.java)
            startActivity(intent)
            activity!!.finish()
        }

        ready.setOnClickListener {
            val prefs = Constants.getSharedPrefs(activity!!).edit()
            prefs.putString("SelectApp", "1")
            prefs.apply()
            val intent = Intent(activity, SelectApp::class.java)
            startActivity(intent)
            activity!!.finish()
        }
    }
}