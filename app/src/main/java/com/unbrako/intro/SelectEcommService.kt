package com.unbrako.intro

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface SelectEcommService {

    @GET("json")
    fun getCountryCode(): Call<ResponseBody>
}