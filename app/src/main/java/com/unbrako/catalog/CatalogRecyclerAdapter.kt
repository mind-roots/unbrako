package com.unbrako.catalog

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.dataBase.Products
import com.unbrako.inch.InchActivity
import com.unbrako.metric.MetricActivity
import kotlinx.android.synthetic.main.catalog_recycler_card.view.*
import kotlin.collections.ArrayList

class CatalogRecyclerAdapter(
    val mContext: Context,
    var recyclerModels: ArrayList<RecyclerModel>,
    var catalogFragment: CatalogFragment
) : androidx.recyclerview.widget.RecyclerView.Adapter<CatalogRecyclerAdapter.ViewHolder>() {

        var showSub=catalogFragment as ShowSubCategories

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.catalog_recycler_card, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return recyclerModels.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = recyclerModels[position]

        if (model.imageName.contains("®")) {
            var vv = model.imageName.split("®")
            holder.itemView.textview_card_title.text = Html.fromHtml(vv[0] + "<sup>®</sup>" + vv[1])
        }else{
            holder.itemView.textview_card_title.text = model.imageName

        }

        holder.itemView.imageview_card.setImageResource(model.image)


        holder.itemView.setOnClickListener { view ->
            if(model.id.equals("24")||model.id.equals("30")||model.id.equals("29")){
               showSub.ShowChildCategories(model.id,model.imageName)
            }else{
                if (Constants.getSharedPrefs(mContext).getString("Selected", "1").equals("1")) {

                    val intent = Intent(view.context, MetricActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    intent.putExtra("parcel_value", model)
                    mContext.startActivity(intent)
                } else {
                    val intent = Intent(view.context, InchActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    intent.putExtra("parcel_value", model)
                    mContext.startActivity(intent)
                }
                //navigate(mContext,view,model)
            }

        }
    }

    fun updateArray(finalArray: ArrayList<RecyclerModel>) {
        recyclerModels = finalArray
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)


    private fun navigate(
        mContext: Context,
        view: View,
        model: RecyclerModel
    ) {


        GetProduct(mContext, model).execute()
    }

    interface ShowSubCategories{
        fun ShowChildCategories(id: String, imageName: String)
    }

    class GetProduct(var mContext: Context, var model: RecyclerModel) : AsyncTask<Void, Void, Void>() {
        lateinit var productIdArr: ArrayList<Products>
        lateinit var productId: String
        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(mContext)
            productIdArr = db!!.productDao().getProductId(model.id) as ArrayList<Products>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (productIdArr.size>0) {
                productId = productIdArr[0].ProductID
                model.id = productId

            }else{
                Toast.makeText(mContext,"No Data",Toast.LENGTH_LONG).show()
            }
        }
    }
}
