package com.unbrako.catalog

import android.os.Parcel
import android.os.Parcelable

class RecyclerModel() : Parcelable {

    var image: Int = 0
    lateinit var imageName: String
    lateinit var id: String
     var Diameter: String="0"

    constructor(parcel: Parcel) : this() {
        image = parcel.readInt()
        imageName = parcel.readString().toString()
        id = parcel.readString()!!
        Diameter = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(image)
        parcel.writeString(imageName)
        parcel.writeString(id)
        parcel.writeString(Diameter)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RecyclerModel> {
        override fun createFromParcel(parcel: Parcel): RecyclerModel {
            return RecyclerModel(parcel)
        }

        override fun newArray(size: Int): Array<RecyclerModel?> {
            return arrayOfNulls(size)
        }
    }
}