package com.unbrako.catalog

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.RadioButton
import android.widget.RadioGroup
import com.unbrako.Constants
import com.unbrako.R
import kotlin.collections.ArrayList
import androidx.appcompat.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import android.view.*
import android.widget.TextView
import android.widget.ImageView
import androidx.core.widget.NestedScrollView
import com.unbrako.intro.SelectApp


class CatalogFragment : androidx.fragment.app.Fragment(), RadioGroup.OnCheckedChangeListener, CatalogRecyclerAdapter.ShowSubCategories {

    lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    lateinit var  scroll: NestedScrollView
    private lateinit var radioButtonMetric: RadioButton
    private lateinit var radioButtonInch: RadioButton
    private lateinit var radioGroup: RadioGroup
    private var finalArray = ArrayList<RecyclerModel>()
    private lateinit var mAdapter: CatalogRecyclerAdapter
    private lateinit var toolbar: Toolbar
    private lateinit var tvCatalog: TextView
    private lateinit var iv_cart: ImageView
    private lateinit var manager: androidx.recyclerview.widget.GridLayoutManager



    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.fragment_catalog, container, false)
        scroll = view.findViewById(R.id.scroll)
        SetToolBar(view)
        setHasOptionsMenu(true)


        recyclerView = view.findViewById(R.id.recycler_view)
        radioGroup = view.findViewById(R.id.radio)
        radioButtonMetric = view.findViewById(R.id.rb_metric)
        radioButtonInch = view.findViewById(R.id.rb_inch)
        iv_cart = view.findViewById(R.id.iv_cart)
        iv_cart.visibility = View.VISIBLE
         manager = androidx.recyclerview.widget.GridLayoutManager(

             context,
             2,
             androidx.recyclerview.widget.GridLayoutManager.VERTICAL,
             false

         )
        recyclerView.layoutManager = manager
        recyclerView.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
        radioGroup.setOnCheckedChangeListener(this)
        if (Constants.getSharedPrefs(activity!!).getString("Selected","1")=="1"){
            finalArray = getList()
            radioGroup.check(radioButtonMetric.id)
        }else{
            finalArray = getInchList()
            radioGroup.check(radioButtonInch.id)
        }

        setAdapter()
        goToCart()

        return view
    }

    private fun SetToolBar(view: View) {

        toolbar = view.findViewById(R.id.toolbar_edit1) as Toolbar
        tvCatalog = toolbar.findViewById(R.id.tvCatalog) as TextView
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowHomeEnabled(false)
    }
    private fun goToCart(){
        iv_cart.setOnClickListener {
            val prefs = Constants.getSharedPrefs(activity!!).edit()
            prefs.putString("App", "Emain")
            prefs.apply()
            val intent = Intent(activity!!, SelectApp::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            activity!!.finish()

        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

        if (radioButtonMetric.isChecked) {
            val prefs = Constants.getSharedPrefs(requireActivity()).edit()
            prefs.putString("Selected", "1")
            prefs.apply()
            radioButtonMetric.setTextColor(Color.parseColor("#000000"))
            radioButtonInch.setTextColor(Color.GRAY)
           // if (mAdapter != null && mAdapter.itemCount > 0) {
                finalArray = getList()
                setAdapter()
            scroll.smoothScrollTo(0, 0)
            //}
        }

        if (radioButtonInch.isChecked) {
            val prefs = Constants.getSharedPrefs(activity!!).edit()
            prefs.putString("Selected", "0")
            prefs.apply()
            radioButtonInch.setTextColor(Color.parseColor("#000000"))
            radioButtonMetric.setTextColor(Color.GRAY)
           // if (mAdapter != null && mAdapter.itemCount > 0) {
                finalArray = getInchList()
                setAdapter()
            scroll.smoothScrollTo(0, 0)
           // }

        }
    }


    private fun setAdapter() {
        mAdapter = CatalogRecyclerAdapter(activity!!, finalArray, this)
        recyclerView.adapter = mAdapter
//        manager.scrollToPosition(8)
//        manager.scrollToPositionWithOffset(8, 9)
    }

    private fun getList(): ArrayList<RecyclerModel> {

        val arrayList: java.util.ArrayList<RecyclerModel> = arrayListOf()

        val recyclerModel1 = RecyclerModel()
        recyclerModel1.image = R.mipmap.catalog_one
        recyclerModel1.imageName = "Socket Head Cap Screws"
        recyclerModel1.id = "14"
        arrayList.add(recyclerModel1)

        val recyclerModel2 = RecyclerModel()
        recyclerModel2.image = R.mipmap.catalog_2
        recyclerModel2.imageName = "Socket Low Head Cap Screws"
        recyclerModel2.id = "27"

        arrayList.add(recyclerModel2)

        val recyclerModel3 = RecyclerModel()
        recyclerModel3.image = R.mipmap.catalog_three
        recyclerModel3.imageName = "Socket Head Shoulder Screws"
        recyclerModel3.id = "23"

        arrayList.add(recyclerModel3)


        val recyclerModel4 = RecyclerModel()
        recyclerModel4.image = R.mipmap.catalog_four
        recyclerModel4.imageName = "Countersunk Socket Cap Screws"
        recyclerModel4.id = "19"

        arrayList.add(recyclerModel4)


        val recyclerModel5 = RecyclerModel()
        recyclerModel5.image = R.mipmap.catalog_five
        recyclerModel5.imageName = "Button Head Socket Screws"
        recyclerModel5.id = "20"

        arrayList.add(recyclerModel5)


        val recyclerModel6 = RecyclerModel()
        recyclerModel6.image = R.mipmap.catalog_six
        recyclerModel6.imageName = "Flange Button Head Socket Screws"
        recyclerModel6.id = "21"

        arrayList.add(recyclerModel6)


        val recyclerModel7 = RecyclerModel()
        recyclerModel7.image = R.mipmap.catalog_seven
        recyclerModel7.imageName = "Socket Set Screws"
        recyclerModel7.id = "24"

        arrayList.add(recyclerModel7)

        val recyclerModel8 = RecyclerModel()
        recyclerModel8.image = R.mipmap.catalog_eight
        recyclerModel8.imageName = "Taper Pressure Plugs"
        recyclerModel8.id = "28"

        arrayList.add(recyclerModel8)

//durlok
        val recyclerModel9 = RecyclerModel()
        recyclerModel9.image = R.mipmap.catalog_nine
        recyclerModel9.imageName = getString(R.string.Socket_nine_string)
        recyclerModel9.id = "22"
        arrayList.add(recyclerModel9)

        val recyclerModel10 = RecyclerModel()
        recyclerModel10.image = R.mipmap.durlok_nut
        recyclerModel10.imageName =getString(R.string.durloknut_string)
        recyclerModel10.id = "31"
        arrayList.add(recyclerModel10)

        val recyclerModel11 = RecyclerModel()
        recyclerModel11.image = R.mipmap.durlok_washer
        recyclerModel11.imageName =getString(R.string.Socket_ten_string)
        recyclerModel11.id = "32"
        arrayList.add(recyclerModel11)

        return arrayList

    }

    private fun getListForSocketHeadCapScrewForMetric(): ArrayList<RecyclerModel> {

        val arrayList: java.util.ArrayList<RecyclerModel> = arrayListOf()
        val recyclerModel3 = RecyclerModel()
        recyclerModel3.image = R.mipmap.cone_point_socket_set_screw
        recyclerModel3.imageName = "Cone Point Socket Set Screw"
        recyclerModel3.id = "33"
        arrayList.add(recyclerModel3)


        val recyclerModel4 = RecyclerModel()
        recyclerModel4.image = R.mipmap.dog_point_socketse_screw
        recyclerModel4.imageName = "Dog Point Socket Set Screw"
        recyclerModel4.id = "34"

        arrayList.add(recyclerModel4)


        val recyclerModel5 = RecyclerModel()
        recyclerModel5.image = R.mipmap.flat_point_socketset_screw
        recyclerModel5.imageName = "Flat Point Socket Set Screw"
        recyclerModel5.id = "35"
        arrayList.add(recyclerModel5)

        val recyclerModel6 = RecyclerModel()
        recyclerModel6.image = R.mipmap.knurled_cup
        recyclerModel6.imageName = "Knurled Cup Point Socket Set Screw"
        recyclerModel6.id = "36"

        arrayList.add(recyclerModel6)

        val recyclerModel2 = RecyclerModel()
        recyclerModel2.image = R.mipmap.plain_cup
        recyclerModel2.imageName = "Plain Cup Socket Set Screw"
        recyclerModel2.id = "37"

        arrayList.add(recyclerModel2)


        return arrayList

    }


    private fun getInchList(): ArrayList<RecyclerModel> {
        val arrayList: java.util.ArrayList<RecyclerModel> = arrayListOf()

        val recyclerModel1 = RecyclerModel()
        recyclerModel1.image = R.mipmap.catalog_one
        recyclerModel1.imageName = "Socket Head Cap Screws"
        recyclerModel1.id = "14"
        arrayList.add(recyclerModel1)

        val recyclerModel2 = RecyclerModel()
        recyclerModel2.image = R.mipmap.catalog_2
        recyclerModel2.imageName = "Socket Low Head Cap Screws"
        recyclerModel2.id = "27"

        arrayList.add(recyclerModel2)

        val recyclerModel3 = RecyclerModel()
        recyclerModel3.image = R.mipmap.catalog_three
        recyclerModel3.imageName = "Socket Head Shoulder Screws"
        recyclerModel3.id = "23"

        arrayList.add(recyclerModel3)


        val recyclerModel4 = RecyclerModel()
        recyclerModel4.image = R.mipmap.catalog_four
        recyclerModel4.imageName = "Countersunk Socket Cap Screws"
        recyclerModel4.id = "19"

        arrayList.add(recyclerModel4)


        val recyclerModel5 = RecyclerModel()
        recyclerModel5.image = R.mipmap.catalog_five
        recyclerModel5.imageName = "Button Head Socket Screws"
        recyclerModel5.id = "20"

        arrayList.add(recyclerModel5)


        val recyclerModel6 = RecyclerModel()
        recyclerModel6.image = R.mipmap.catalog_six
        recyclerModel6.imageName = "Flange Button Head Socket Screws"
        recyclerModel6.id = "21"

        arrayList.add(recyclerModel6)


        val recyclerModel7 = RecyclerModel()
        recyclerModel7.image = R.mipmap.catalog_seven
        recyclerModel7.imageName = "Socket Set Screws"
        recyclerModel7.id = "24"

        arrayList.add(recyclerModel7)

        val recyclerModel8 = RecyclerModel()
        recyclerModel8.image = R.mipmap.catalog_eight
        recyclerModel8.imageName = "3/4\"Taper Pressure Plugs"
        recyclerModel8.id = "30"
        arrayList.add(recyclerModel8)

        val recyclerModel9 = RecyclerModel()
        recyclerModel9.image = R.mipmap.catalog_eight
        recyclerModel9.imageName = "7/8\"Taper Pressure Plugs"
        recyclerModel9.id = "29"

        arrayList.add(recyclerModel9)

        return arrayList

    }

    private fun getListForSocketHeadCapScrewForInch(): ArrayList<RecyclerModel> {
        val arrayList: java.util.ArrayList<RecyclerModel> = arrayListOf()

        val recyclerModel1 = RecyclerModel()
        recyclerModel1.image = R.mipmap.knurled_cup
        recyclerModel1.imageName = "Knurled Cup Point Socket Set Screw"
        recyclerModel1.id = "38"
        arrayList.add(recyclerModel1)

        val recyclerModel2 = RecyclerModel()
        recyclerModel2.image = R.mipmap.plain_cup
        recyclerModel2.imageName = "Plain Cup Socket Set Screw"
        recyclerModel2.id = "39"

        arrayList.add(recyclerModel2)



        return arrayList

    }

    private fun getListForTaperPressure7ForInch(): ArrayList<RecyclerModel> {
        val arrayList: java.util.ArrayList<RecyclerModel> = arrayListOf()

        val recyclerModel1 = RecyclerModel()
        recyclerModel1.image = R.mipmap.catalog_eight
        recyclerModel1.imageName = "PTF LEVL-SEAL Thread Black Oxide"
        recyclerModel1.id = "44"
        arrayList.add(recyclerModel1)

        val recyclerModel2 = RecyclerModel()
        recyclerModel2.image = R.mipmap.catalog_eight
        recyclerModel2.imageName = "PTF LEVL-SEAL Thread Teflon Coated"
        recyclerModel2.id = "45"
        arrayList.add(recyclerModel2)

        val recyclerModel3 = RecyclerModel()
        recyclerModel3.image = R.mipmap.catalog_eight
        recyclerModel3.imageName = "PTF LEVL-SEAL Thread Brass"
        recyclerModel3.id = "46"
        arrayList.add(recyclerModel3)

        val recyclerModel4 = RecyclerModel()
        recyclerModel4.image = R.mipmap.catalog_eight
        recyclerModel4.imageName = "PTF LEVL-SEAL Stainless Steel"
        recyclerModel4.id = "47"
        arrayList.add(recyclerModel4)




        return arrayList

    }

    private fun getListForTaperPressure3ForInch(): ArrayList<RecyclerModel> {
        val arrayList: java.util.ArrayList<RecyclerModel> = arrayListOf()

        val recyclerModel1 = RecyclerModel()
        recyclerModel1.image = R.mipmap.catalog_eight
        recyclerModel1.imageName = "BSPT Thread Black Oxide"
        recyclerModel1.id = "40"
        arrayList.add(recyclerModel1)

        val recyclerModel2 = RecyclerModel()
        recyclerModel2.image = R.mipmap.catalog_eight
        recyclerModel2.imageName = "NPTF Dryseal Thread Black Oxide"
        recyclerModel2.id = "41"
        arrayList.add(recyclerModel2)

        val recyclerModel3 = RecyclerModel()
        recyclerModel3.image = R.mipmap.catalog_eight
        recyclerModel3.imageName = "NPTF Dryseal Thread Brass"
        recyclerModel3.id = "42"
        arrayList.add(recyclerModel3)

        val recyclerModel4 = RecyclerModel()
        recyclerModel4.image = R.mipmap.catalog_eight
        recyclerModel4.imageName = "NPTF Dryseal Thread Stainless Steel"
        recyclerModel4.id = "43"
        arrayList.add(recyclerModel4)





        return arrayList

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> {
                Constants.getSharedPrefs(this.activity!!).edit().putString("child","no").apply()
                (activity as AppCompatActivity).setSupportActionBar(toolbar)
                (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(false)
                (activity as AppCompatActivity).supportActionBar!!.setDisplayShowHomeEnabled(false)
                finalArray = getList()
                tvCatalog.text = "Catalog"
                radioGroup.visibility = View.VISIBLE
                if (Constants.getSharedPrefs(activity!!).getString("Selected","1")=="1"){
                    finalArray = getList()
                    radioGroup.check(radioButtonMetric.id)
                }else{
                    finalArray = getInchList()
                    radioGroup.check(radioButtonInch.id)
                }
                setAdapter()

//                // Save state
//                recyclerViewState = recyclerView.layoutManager!!.onSaveInstanceState()!!
//
//                // Restore state
//                recyclerView.layoutManager!!.onRestoreInstanceState(recyclerViewState)


                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun ShowChildCategories(id: String, imageName: String) {
        Constants.getSharedPrefs(this.activity!!).edit().putString("child","yes").apply()
        radioGroup.visibility = View.GONE
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowHomeEnabled(true)
        tvCatalog.text = imageName
        //finalArray = getListForSocketHeadCapScrew()
        if (Constants.getSharedPrefs(activity!!).getString("Selected", "1") == "1") {
            mAdapter = CatalogRecyclerAdapter(activity!!, getListForSocketHeadCapScrewForMetric(), this)
            recyclerView.adapter = mAdapter
        } else {
            if (id == "30") {
                mAdapter = CatalogRecyclerAdapter(activity!!, getListForTaperPressure3ForInch(), this)
                recyclerView.adapter = mAdapter
            } else if (id == "29") {
                mAdapter = CatalogRecyclerAdapter(activity!!, getListForTaperPressure7ForInch(), this)
                recyclerView.adapter = mAdapter
            } else if (id == "24") {
                mAdapter = CatalogRecyclerAdapter(activity!!, getListForSocketHeadCapScrewForInch(), this)
                recyclerView.adapter = mAdapter
            }
        }
        scroll.scrollTo(0, 0)
    }


}
