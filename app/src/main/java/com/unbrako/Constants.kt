package com.unbrako

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StrikethroughSpan
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.unbrako.catalog.RecyclerModel
import com.unbrako.dataBase.*
import com.unbrako.e_Commerce.EcommerceService
import com.unbrako.e_Commerce.catalog.ModelWishList
import com.unbrako.e_Commerce.dataBase.BasketModel
import com.unbrako.e_Commerce.dataBase.CartModel
import com.unbrako.e_Commerce.dataBase.FetchProductsModel
import com.unbrako.e_Commerce.forgotPassword.ModelForgotPassword
import com.unbrako.e_Commerce.signup.SignUpModel
import com.unbrako.inch.InchActivity
import com.unbrako.metric.MetricActivity
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList


class Constants {

    companion object {
        //        const val BASE_URL = "http://cloudart.com.au/projects/unbrako/api/"
//        const val BASE_URL = "http://www.unbrako.com/unbrakoapp/api/"
        const val BASE_URL = "https://unbrakoonline.com/unbrakoapp/api/"
        const val BASE_URL_Ecommerce = "https://unbrakoonline.com/"

        //                const val BASE_URL_Ecommerce = "http://www.unbrako.com/unbrako_ecommerce/?route=mobileapi/catalog/"



//         const val BASE_URL_Ecommerce = "http://www.unbrako.com/unbrako_ecommerce/"
        private var retrofit: Retrofit? = null
        private var retrofit2: Retrofit? = null

        var cartSize = 0
        var basketSize = 0
        var db: AppDatabase? = null
        val token: String = "token"
        var countryCode: String = ""

        //----------------------------------------------shared Prefrence--------------------------

        fun getPrefs(context: Context): SharedPreferences {
            return context.getSharedPreferences("RegisterLogin", Context.MODE_PRIVATE)
        }


        fun setLoggedIn(context: Context, loggedIn: Boolean) {
            val editor = getPrefs(context).edit()
            editor.putBoolean(LOGGED_IN_PREF, loggedIn)
            editor.apply()
        }

        fun getLoggedStatus(context: Context): Boolean {
            return getPrefs(context).getBoolean(LOGGED_IN_PREF, false)
        }

        // Values for Shared Prefrences
        val LOGGED_IN_PREF = "logged_in_status"


        fun hideSoftKeyboard(view: View, context: Context) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN)
        }

        var prefs: SharedPreferences? = null

        fun getSharedPrefs(context: Context): SharedPreferences {
            return if (prefs == null) {
                context.getSharedPreferences("Unbrako", 0)
            } else {
                prefs!!
            }
        }


        fun getWebClient(): Retrofit? {
            val builder = OkHttpClient.Builder()
            builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
            var client = OkHttpClient()
            client = builder.build()
            //if (retrofit == null) {
            retrofit = Retrofit.Builder().client(client).baseUrl(Constants.BASE_URL).build()
            //}
            return retrofit
        }


        fun getWebClient3(): Retrofit? {
            val builder = OkHttpClient.Builder()
            builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
            var client = OkHttpClient()
            client = builder.build()
            //if (retrofit == null) {
            retrofit =
                Retrofit.Builder().client(client).baseUrl(Constants.BASE_URL_Ecommerce).build()
            // }
            return retrofit
        }


        fun getWebClient2(): Retrofit? {
            val builder = OkHttpClient.Builder()
            builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
            var client = OkHttpClient()
            client = builder.build()
            // if (retrofit2 == null) {
            retrofit2 =
                Retrofit.Builder().client(client)
                    .baseUrl("http://cloudart.com.au/projects/unbrako_eco/index.php")
                    .build()
            //}
            return retrofit2
        }

        //----------------------------------------------DataBaseHandling--------------------------

        fun getDataBase(applicationContext: Context): AppDatabase? {
            if (db == null) {
                return Room.databaseBuilder(
                    applicationContext,
                    AppDatabase::class.java, "unbrako_db"
                ).build()
            } else {
                return db
            }
            return db
        }


        //------------------------------------------------------------------------------------------


        fun BookMarkItem(context: Context, modelBookMark: BookMark) {
            class GetProduct() : AsyncTask<Void, Void, Void>() {
                lateinit var productIdArr: ArrayList<ProductInch>
                var array = ArrayList<BookMark>()
                var exist = 0
                override fun doInBackground(vararg params: Void?): Void? {

                    val db = Constants.getDataBase(context)

                    array = db!!.productDao().getAllBookMark(
                        modelBookMark.PartNumber,
                        modelBookMark.Diameter
                    ) as ArrayList<BookMark>

                    if (array.size > 0) {
                        exist = 1
                    } else {

                        db!!.productDao().insertBookMark(modelBookMark)
                    }
                    return null
                }

                override fun onPostExecute(result: Void?) {
                    super.onPostExecute(result)
                    if (exist == 1) {
                        Toast.makeText(
                            context,
                            "This part number has already Bookmarked",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        Toast.makeText(context, "Added to Bookmark", Toast.LENGTH_LONG).show()

                    }
                }
            }

            GetProduct().execute()
        }

        fun navigate(
            mContext: Context,
            view: View,
            model: RecyclerModel,
            s: String,
            type: String
        ) {

            class GetProduct : AsyncTask<Void, Void, Void>() {
                lateinit var productIdArr: ArrayList<Products>
                lateinit var productId: String
                override fun doInBackground(vararg params: Void?): Void? {

                    val db = Constants.getDataBase(mContext)
                    productIdArr = db!!.productDao().getProductId(model.id) as ArrayList<Products>

                    return null
                }

                override fun onPostExecute(result: Void?) {
                    super.onPostExecute(result)
                    if (productIdArr.size > 0) {
                        productId = productIdArr[0].ProductID
                        model.id = productId
//                        if (Constants.getSharedPrefs(mContext).getString("Selected", "1").equals("1")) {

                        if (s == "search") {
                            if (Constants.getSharedPrefs(mContext).getString(
                                    "Selected",
                                    "1"
                                ) == "1"
                            ) {
                                val intent = Intent(view.context, InchActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                intent.putExtra("parcel_value", model)
                                mContext.startActivity(intent)
                            } else {
                                val intent = Intent(view.context, MetricActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                intent.putExtra("parcel_value", model)
                                mContext.startActivity(intent)

                            }
                        } else {
                            if (type == "1") {
                                val intent = Intent(view.context, InchActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                intent.putExtra("parcel_value", model)
                                mContext.startActivity(intent)
                            } else {
                                val intent = Intent(view.context, MetricActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                                intent.putExtra("parcel_value", model)
                                mContext.startActivity(intent)

                            }
                        }
                        if (s == "search") {
                            (mContext as Activity).finish()
                        }
                    } else {
                        //Toast.makeText(mContext,"No Data", Toast.LENGTH_LONG).show()

                    }
                }
            }
            GetProduct().execute()
        }

        fun showComposer(context: Context, text: String) {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "sales@unbrakoonline.com", null
                )
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback about Unbrako App")
            emailIntent.putExtra(
                Intent.EXTRA_TEXT,
                ""
            )
            context.startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }

        fun showComposer2(context: Context, text: String) {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "unbrakosales@unbrako.com", null
                )
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Send query")
            emailIntent.putExtra(
                Intent.EXTRA_TEXT,
                text
            )
            context.startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }


        fun formatValuesTwo(value: Double): Double {
            var bd = BigDecimal(value)
            bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP)
            return bd.toDouble()
        }

        fun formatValues(value: String): String {
            var comingValue: String = value
            try {
                if (value.contains(".")) {
                    var splitString = value.split(".")
                    var lengthOfDecimal: Int = splitString[1].length

                    if (lengthOfDecimal > 3) {
                        val doubleValue = value.toDouble()
                        var bd = BigDecimal(doubleValue)
                        bd = bd.setScale(3, BigDecimal.ROUND_HALF_UP)
                        comingValue = bd.toString()
                        return comingValue
                    } else {
                        return value
                    }


                } else {
                    return comingValue
                }
            } catch (e: Exception) {
                return comingValue
            }
            return comingValue
        }

        fun formatValues2(value: String): String {
            var comingValue: String = value
            try {
                if (value.contains(".")) {
                    var splitString = value.split(".")
                    var lengthOfDecimal: Int = splitString[1].length

                    if (lengthOfDecimal > 0) {
                        val doubleValue = value.toDouble()
                        var bd = BigDecimal(doubleValue)
                        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP)
                        comingValue = bd.toString()
                        return comingValue
                    } else {
                        return value
                    }


                } else {
                    return comingValue

                }
            } catch (e: Exception) {
                return comingValue
            }
            return comingValue
        }

        fun formatValuesForGrades(value: String): String {
            var comingValue: String = value
            try {
                if (value.contains(".")) {

                    var splitString = value.split(".")
                    var lengthOfDecimal: Int = splitString[1].length

                    if (lengthOfDecimal > 3) {
                        val doubleValue = value.toDouble()
                        var bd = BigDecimal(doubleValue)
                        bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP)
                        comingValue = bd.toString()
                        return comingValue
                    } else {
                        return value
                    }

                } else {
                    return comingValue
                }
            } catch (e: Exception) {
                return comingValue
            }
            return comingValue
        }

        fun getProductImageForBookmark(productID: String): Int {
            if (productID == "14") {
                return R.mipmap.catalog_one
            } else if (productID == "27") {
                return R.mipmap.catalog_2
            } else if (productID == "23") {
                return R.mipmap.catalog_three
            } else if (productID == "19") {
                return R.mipmap.catalog_four
            } else if (productID == "20") {
                return R.mipmap.catalog_five
            } else if (productID == "21") {
                return R.mipmap.catalog_six
            } else if (productID == "24") {
                return R.mipmap.catalog_seven
            } else if (productID == "28") {
                return R.mipmap.catalog_eight
            } else if (productID == "29") {
                return R.mipmap.catalog_eight
            } else if (productID == "30") {
                return R.mipmap.catalog_eight
            } else if (productID == "22") {
                return R.mipmap.catalog_nine
            } else if (productID == "31") {
                return R.mipmap.durlok_nut
            } else if (productID == "32") {
                return R.mipmap.durlok_washer
            } else if (productID == "33") {
                return R.mipmap.cone_point_socket_set_screw
            } else if (productID == "34") {
                return R.mipmap.dog_point_socketse_screw
            } else if (productID == "35") {
                return R.mipmap.flat_point_socketset_screw
            } else if (productID == "36") {
                return R.mipmap.knurled_cup
            } else if (productID == "37") {
                return R.mipmap.plain_cup
            } else if (productID == "38") {
                return R.mipmap.knurled_cup
            } else if (productID == "39") {
                return R.mipmap.plain_cup
            } else if (productID == "40") {
                return R.mipmap.catalog_eight
            } else if (productID == "41") {
                return R.mipmap.catalog_eight
            } else if (productID == "42") {
                return R.mipmap.catalog_eight
            } else if (productID == "43") {
                return R.mipmap.catalog_eight
            } else if (productID == "44") {
                return R.mipmap.catalog_eight
            } else if (productID == "45") {
                return R.mipmap.catalog_eight
            } else if (productID == "46") {
                return R.mipmap.catalog_eight
            } else if (productID == "47") {
                return R.mipmap.catalog_eight
            }
            return R.mipmap.catalog_one
        }

        fun sortArray(arrayList: ArrayList<ProductMetric>): ArrayList<ProductMetric> {
            var ff = ""
            var newList = ArrayList<ProductMetric>()
            newList.addAll(arrayList)
            var newList2 = ArrayList<ProductMetric>()
            for (i in 0 until arrayList.size) {
                var vv = newList[i].Diameter
                if (vv.contains("M")) {
                    ff = "M"
                    vv = vv.replace("M", "")
                } else if (vv.contains("mm")) {
                    vv = vv.replace("mm", "")
                    ff = "mm"
                }
                newList[i].Diameter = vv
            }

            Collections.sort(newList, object : Comparator<ProductMetric> {
                override fun compare(lhs: ProductMetric, rhs: ProductMetric): Int {
                    return lhs.Diameter.toDouble().compareTo(rhs.Diameter.toDouble())
                }
            })
            for (j in 0 until newList.size) {
                for (k in 0 until arrayList.size) {
                    if (newList[j].pro_det_id == arrayList[k].pro_det_id) {
                        newList2.add(arrayList[k])
                    }
                }
            }
            for (j in 0 until newList2.size) {
                if (ff == "mm") {
                    newList2[j].Diameter = newList2[j].Diameter + ff
                } else {
                    newList2[j].Diameter = ff + newList2[j].Diameter
                }

            }
            return newList2
        }

        fun sortInchArray(
            arrayList: ArrayList<ProductInch>,
            context: Context
        ): ArrayList<ProductInch> {

            var editor = Constants.getSharedPrefs(context).edit()
            val gson = Gson()
            val json = gson.toJson(arrayList)
            editor.remove("inch").commit()
            editor.putString("inch", json)
            editor.commit()

            var hashArray = ArrayList<ProductInch>()
            var newArray = ArrayList<ProductInch>()
            var newArray3 = ArrayList<ProductInch>()
            for (i in 0 until arrayList.size) {
                if (arrayList[i].Diameter.contains("#")) {
                    hashArray.add(arrayList[i])
                } else {

                    newArray.add(arrayList[i])
                }
            }
            for (j in 0 until hashArray.size) {
                hashArray[j].Diameter = hashArray[j].Diameter.replace("#", "")
            }
            for (k in 0 until newArray.size) {
                if (newArray[k].Diameter.contains(" ")) {
                    val parts = newArray[k].Diameter.split(" ")
                    var one = parts[0].toDouble()
                    val parts2 = parts[1].split("/")
                    var dd = parts2[0].toDouble() / parts2[1].toDouble()

                    newArray[k].Diameter = (one + dd).toString()
                } else {
                    if (newArray[k].Diameter.contains("/")) {
                        val parts2 = newArray[k].Diameter.split("/")
                        var dd = parts2[0].toDouble() / parts2[1].toDouble()
                        newArray[k].Diameter = (dd).toString()
                    } else {
                        newArray[k].Diameter = newArray[k].Diameter.toDouble().toString()
                    }
                }

            }
            Collections.sort(hashArray, object : Comparator<ProductInch> {
                override fun compare(lhs: ProductInch, rhs: ProductInch): Int {
                    return lhs.Diameter.toDouble().compareTo(rhs.Diameter.toDouble())
                }
            })
            Collections.sort(newArray, object : Comparator<ProductInch> {
                override fun compare(lhs: ProductInch, rhs: ProductInch): Int {
                    return lhs.Diameter.toDouble().compareTo(rhs.Diameter.toDouble())
                }
            })
            for (u in 0 until hashArray.size) {
                hashArray[u].Diameter = "#" + hashArray[u].Diameter
            }

            var newArray6 = ArrayList<ProductInch>()

            val response = Constants.getSharedPrefs(context).getString("inch", "")
            val lstArrayList = gson.fromJson<ArrayList<ProductInch>>(
                response,
                object : TypeToken<List<ProductInch>>() {

                }.type
            )

            for (p in 0 until hashArray.size) {
                for (k in 0 until lstArrayList.size) {
                    if (hashArray[p].inch_det_id == lstArrayList[k].inch_det_id) {
                        newArray3.add(lstArrayList[k])
                    }
                }
            }
            for (p in 0 until newArray.size) {
                for (k in 0 until lstArrayList.size) {
                    if (newArray[p].inch_det_id == lstArrayList[k].inch_det_id) {
                        newArray3.add(lstArrayList[k])
                    }
                }
            }
            return newArray3
        }

        fun sortMetrixBox(
            arrayList: ArrayList<MetricBox>,
            context: Context
        ): ArrayList<MetricBox> {

            var editor = Constants.getSharedPrefs(context).edit()
            val gson = Gson()
            val json = gson.toJson(arrayList)
            editor.remove("metricBox").commit()
            editor.putString("metricBox", json)
            editor.commit()
            var newArray = ArrayList<MetricBox>()
            var newArray3 = ArrayList<MetricBox>()
            newArray.addAll(arrayList)
            for (i in 0 until newArray.size) {
                if (newArray[i].Length.isEmpty()) {
                    newArray[i].Length = "0"
                }
                if (newArray[i].Length.contains(" ")) {
                    val parts = newArray[i].Length.split(" ")
                    var one = parts[0].toDouble()
                    val parts2 = parts[1].split("/")
                    var dd = parts2[0].toDouble() / parts2[1].toDouble()

                    newArray[i].Length = (one + dd).toString()
                } else {
                    if (newArray[i].Length.contains("/")) {
                        val parts2 = newArray[i].Length.split("/")
                        var dd = parts2[0].toDouble() / parts2[1].toDouble()
                        newArray[i].Length = (dd).toString()
                    } else {

                        newArray[i].Length = newArray[i].Length.toDouble().toString()
                    }
                }
            }
            Collections.sort(newArray, object : Comparator<MetricBox> {
                override fun compare(lhs: MetricBox, rhs: MetricBox): Int {
                    return lhs.Length.toDouble().compareTo(rhs.Length.toDouble())
                }
            })

            var newArray6 = ArrayList<InchBox>()

            val response = Constants.getSharedPrefs(context).getString("metricBox", "")
            val lstArrayList = gson.fromJson<ArrayList<MetricBox>>(
                response,
                object : TypeToken<List<MetricBox>>() {

                }.type
            )
            for (p in 0 until newArray.size) {
                for (k in 0 until lstArrayList.size) {
                    if (newArray[p].met_box_id == lstArrayList[k].met_box_id) {
                        newArray3.add(lstArrayList[k])
                    }
                }
            }
            return newArray3
        }


        fun sortInchBox(
            arrayList: ArrayList<InchBox>,
            context: Context
        ): ArrayList<InchBox> {
            var editor = Constants.getSharedPrefs(context).edit()
            val gson = Gson()
            val json = gson.toJson(arrayList)
            editor.remove("inchBox").commit()
            editor.putString("inchBox", json)
            editor.commit()
            var newArray = ArrayList<InchBox>()
            var newArray3 = ArrayList<InchBox>()
            newArray.addAll(arrayList)
            for (i in 0 until newArray.size) {
                if (newArray[i].Length.isEmpty()) {
                    newArray[i].Length = "0"
                }
                if (newArray[i].Length.contains(" ")) {
                    val parts = newArray[i].Length.split(" ")
                    var one = parts[0].toDouble()
                    val parts2 = parts[1].split("/")
                    var dd = parts2[0].toDouble() / parts2[1].toDouble()

                    newArray[i].Length = (one + dd).toString()
                } else {
                    if (newArray[i].Length.contains("/")) {
                        val parts2 = newArray[i].Length.split("/")
                        var dd = parts2[0].toDouble() / parts2[1].toDouble()
                        newArray[i].Length = (dd).toString()
                    } else {
                        newArray[i].Length = newArray[i].Length.toDouble().toString()
                    }
                }
            }
            Collections.sort(newArray, object : Comparator<InchBox> {
                override fun compare(lhs: InchBox, rhs: InchBox): Int {
                    return lhs.Length.toDouble().compareTo(rhs.Length.toDouble())
                }
            })

            var newArray6 = ArrayList<InchBox>()

            val response = Constants.getSharedPrefs(context).getString("inchBox", "")
            val lstArrayList = gson.fromJson<ArrayList<InchBox>>(
                response,
                object : TypeToken<List<InchBox>>() {

                }.type
            )
            for (p in 0 until newArray.size) {
                for (k in 0 until lstArrayList.size) {
                    if (newArray[p].inch_box_id == lstArrayList[k].inch_box_id) {
                        newArray3.add(lstArrayList[k])
                    }
                }
            }
            return newArray3
        }


        //----------------------------------------------check email pattern-------------------------

        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }


        //----------------------------------------------show dialog--------------------------
        fun isGstValid(text: String): Boolean {
            var pattern = Pattern.compile("^\\d{2}\\D{5}\\d{4}\\D\\dZ\\w")
            var macther: Matcher = pattern.matcher(text)
            return macther.matches()
        }

        fun showLoginDialog(context: Context) {
            android.app.AlertDialog.Builder(context)
                .setTitle("Thank you!")
                .setMessage("Your account has been sent to the admin for verification. Once verified, you will be notified via email and will be able to login into the eCommerce app.")
                .setPositiveButton("Ok") { dialog, which -> dialog.dismiss() }
                .show()
        }

        fun showLoginDialogError(context: Context, msg: String) {
            android.app.AlertDialog.Builder(context)
                .setTitle("Alert!")
                .setMessage(msg)
                .setPositiveButton("Ok") { dialog, which -> dialog.dismiss() }
                .show()
        }


        fun showForgotDialog(
            context: Context,
            it: ModelForgotPassword
        ) {
            android.app.AlertDialog.Builder(context)
                .setMessage(it.error)
                .setPositiveButton("Ok") { dialog, which ->
                    dialog.dismiss()
                }

                .show()
        }


        fun WishListApi(mContext: Context, model: ModelWishList) {

            val authCode = Constants.getPrefs(mContext).getString(Constants.token, "")
            val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL_Ecommerce)
                .build()
            val service = retrofit.create(EcommerceService::class.java)
            val call: Call<ResponseBody> = service.addWishList(authCode!!, model.product_id, "2")
            call.enqueue(object : Callback<ResponseBody> {

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {

                    if (response.isSuccessful) {
                        try {
                            val res = response.body()!!.string()
                            val json = JSONObject(res)

                        } catch (e: Exception) {
                            e.printStackTrace()

                        }
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                }
            })
        }

        fun WishListApiAddRemove(mContext: Context, model: ModelWishList, value: String) {

            val authCode = Constants.getPrefs(mContext).getString(Constants.token, "")
            val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL_Ecommerce)
                .build()
            val service = retrofit.create(EcommerceService::class.java)
            val call: Call<ResponseBody> = service.addWishList(authCode!!, model.product_id, value)
            call.enqueue(object : Callback<ResponseBody> {

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {

                    if (response.isSuccessful) {
                        try {
                            val res = response.body()!!.string()
                            val json = JSONObject(res)
                            val success = json.optString("success")


                        } catch (e: Exception) {
                            e.printStackTrace()

                        }
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                }
            })
        }

        fun openDialog(mContext: Context): String {
            var color = "1"
            lateinit var dialog: AlertDialog

            // Initialize an array of colors
            val array =
                arrayOf(
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21",
                    "22",
                    "23",
                    "24",
                    "25",
                    "26",
                    "27",
                    "28",
                    "29",
                    "30",
                    "31",
                    "32",
                    "33",
                    "34",
                    "35",
                    "36",
                    "37",
                    "38",
                    "39",
                    "40",
                    "41",
                    "42",
                    "43",
                    "44",
                    "45",
                    "46",
                    "47",
                    "48",
                    "49",
                    "50"
                )

            // Initialize a new instance of alert dialog builder object
            val builder = AlertDialog.Builder(mContext)
            builder.setTitle("Choose a color.")
            builder.setSingleChoiceItems(array, -1) { _, which ->
                color = array[which]
                dialog.dismiss()
            }

            dialog = builder.create()
            dialog.show()
            return color
        }

        fun currencyFormt(value: Double): String {
            val formatter = DecimalFormat("#,###,###.00")
            val yourFormattedString = formatter.format(value)
            return yourFormattedString
        }

        fun IsStockAvailable(model: FetchProductsModel): Boolean {
            try {
                var saleable = model.saleable.toDouble()
                var quantity = model.selectQuant.toDouble()
                var stock = model.stock.toDouble()
                if (saleable * quantity < stock) {
                    return true
                }
            } catch (e: Exception) {

            }

            return false
        }

        fun getDiscountedPrice(
            fetchProductsModel: FetchProductsModel,
            mContext: Context
        ): Double {
            var priceAfterFlatDiscount = 0.0
            val gson = Gson()
            val json1 = getPrefs(mContext).getString("profile", "")
            val flash_sale = getSharedPrefs(mContext).getString("flash_sale", "")
            val flash_sale_msg = getSharedPrefs(mContext).getString("flash_sale_msg", "")
            val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
            if (fetchProductsModel.flash_discount.isEmpty()) {
                fetchProductsModel.flash_discount = "0"
            }
            if (fetchProductsModel.isexempted == "1") {
                if (flash_sale == "off") {
                    val discount =
                        (fetchProductsModel.price.toDouble() * profileData.discount.toDouble()) / 100
                    priceAfterFlatDiscount = fetchProductsModel.price.toDouble() - discount


                } else {
                    val discount =
                        (fetchProductsModel.price.toDouble() * profileData.discount.toDouble()) / 100
                    val price = fetchProductsModel.price.toDouble() - discount

                    val flatDiscount = (price * fetchProductsModel.flash_discount.toDouble()) / 100
                    priceAfterFlatDiscount = price - flatDiscount

                }
            } else {
                if (flash_sale == "off") {
                    priceAfterFlatDiscount = fetchProductsModel.price.toDouble()
                } else {
                    val flatDiscount =
                        (fetchProductsModel.price.toDouble() * fetchProductsModel.flash_discount.toDouble()) / 100
                    priceAfterFlatDiscount = fetchProductsModel.price.toDouble() - flatDiscount
                }
            }
            return priceAfterFlatDiscount
        }

        fun getDiscountApplies(
            fetchProductsModel: FetchProductsModel,
            mContext: Context

        ): String {
            val gson = Gson()
            val json1 = getPrefs(mContext).getString("profile", "")
            val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
            val flash_sale = getSharedPrefs(mContext).getString("flash_sale", "")
            val flash_sale_msg = getSharedPrefs(mContext).getString("flash_sale_msg", "")
            if (fetchProductsModel.isexempted == "1") {
                if (fetchProductsModel.flash_discount.isEmpty()) {
                    fetchProductsModel.flash_discount = "0"
                }
                if (fetchProductsModel.flash_discount.toDouble() > 0) {
                    if (flash_sale == "off") {
                        return " (" + formatValues2( profileData.discount) + "%)"
                    } else {
                        return " (" + formatValues2(profileData.discount) + "% +" +
                                formatValues2(fetchProductsModel.flash_discount).toString() + "%)"
                    }
                } else {
                    return " (" + formatValues2(profileData.discount) + "%)"
                }


            } else {
                if (flash_sale == "off") {
                    return ""
                } else {
                    return " (" +
                            formatValues2(fetchProductsModel.flash_discount) + "%)"
                }
            }


        }

        fun getDiscountedPriceCart(
            fetchProductsModel: CartModel,
            mContext: Context
        ): Double {
            var priceAfterFlatDiscount = 0.0
            val gson = Gson()
            val json1 = Constants.getPrefs(mContext).getString("profile", "")
            val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
//            if (fetchProductsModel.flash_discount.replace("₹", "").replace(
//                    ",",
//                    ""
//                ).isEmpty()
//            ) {
//                fetchProductsModel.flash_discount = "0"
//            }
//            if (fetchProductsModel.isexempted == "1") {
//                val discount =
//                    (fetchProductsModel.price.replace("₹", "").replace(
//                        ",",
//                        ""
//                    ).toDouble() * profileData.discount.toDouble()) / 100
//                val price = fetchProductsModel.price.replace("₹", "").replace(
//                    ",",
//                    ""
//                ).toDouble() - discount
//
//                val flatDiscount =
//                    (price * fetchProductsModel.flash_discount.replace("₹", "").replace(
//                        ",",
//                        ""
//                    ).toDouble()) / 100
//                priceAfterFlatDiscount = price - flatDiscount
//
//
//            } else {
//
//                val flatDiscount =
//                    (fetchProductsModel.price.replace("₹", "").replace(
//                        ",",
//                        ""
//                    ).toDouble() * fetchProductsModel.flash_discount.replace("₹", "").replace(
//                        ",",
//                        ""
//                    ).toDouble()) / 100
//                priceAfterFlatDiscount = fetchProductsModel.price.replace("₹", "").replace(
//                    ",",
//                    ""
//                ).toDouble() - flatDiscount
//            }
//            return priceAfterFlatDiscount
            val flash_sale = getSharedPrefs(mContext).getString("flash_sale", "")
            val flash_sale_msg = getSharedPrefs(mContext).getString("flash_sale_msg", "")
            if (fetchProductsModel.flash_discount.replace("₹", "").replace(
                    ",",
                    ""
                ).isEmpty()
            ) {
                fetchProductsModel.flash_discount = "0"
            }
            if (fetchProductsModel.isexempted == "1") {
                if (flash_sale == "off") {
                    val discount =
                        (fetchProductsModel.price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() * profileData.discount.toDouble()) / 100
                    priceAfterFlatDiscount = fetchProductsModel.price.replace("₹", "").replace(
                        ",",
                        ""
                    ).toDouble() - discount


                } else {
                    val discount =
                        (fetchProductsModel.price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() * profileData.discount.toDouble()) / 100
                    val price = fetchProductsModel.price.replace("₹", "").replace(
                        ",",
                        ""
                    ).toDouble() - discount

                    val flatDiscount =
                        (price * fetchProductsModel.flash_discount.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble()) / 100
                    priceAfterFlatDiscount = price - flatDiscount

                }
            } else {
                if (flash_sale == "off") {
                    priceAfterFlatDiscount = fetchProductsModel.price.replace("₹", "").replace(
                        ",",
                        ""
                    ).toDouble()
                } else {
                    val flatDiscount =
                        (fetchProductsModel.price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() * fetchProductsModel.flash_discount.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble()) / 100

                    priceAfterFlatDiscount =
                        fetchProductsModel.price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() - flatDiscount


                }
            }
            return priceAfterFlatDiscount
        }

        fun getDiscountAppliesCart(
            fetchProductsModel: CartModel,
            mContext: Context

        ): String {
            val gson = Gson()
            val json1 = getPrefs(mContext).getString("profile", "")
            val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
//            if (fetchProductsModel.isexempted=="1"){
//                if (fetchProductsModel.flash_discount.replace("₹", "").replace(
//                        ",",
//                        ""
//                    ).isEmpty()){
//                    fetchProductsModel.flash_discount="0"
//                }
//                if (fetchProductsModel.flash_discount.replace("₹", "").replace(
//                        ",",
//                        ""
//                    ).toDouble()>0){
//                    return " ("+currencyFormt(profileData.discount.toDouble())+"% +" +currencyFormt(fetchProductsModel.flash_discount.replace("₹", "").replace(
//                        ",",
//                        ""
//                    ).toDouble())+"%)"
//                }else{
//                    return " ("+currencyFormt(profileData.discount.toDouble())+"%)"
//                }
//
//
//            }
//                return ""
//
//        }

            val flash_sale = getSharedPrefs(mContext).getString("flash_sale", "")
            val flash_sale_msg = getSharedPrefs(mContext).getString("flash_sale_msg", "")
            if (fetchProductsModel.isexempted == "1") {
                if (fetchProductsModel.flash_discount.replace("₹", "").replace(
                        ",",
                        ""
                    ).isEmpty()
                ) {
                    fetchProductsModel.flash_discount = "0"
                }
                if (fetchProductsModel.flash_discount.replace("₹", "").replace(
                        ",",
                        ""
                    ).toDouble() > 0
                ) {
                    if (flash_sale == "off") {
                        if (profileData.discount.toDouble()==0.0){
                            return ""
                        }
                        return " (Less " + formatValues2(profileData.discount) + "%)"
                    } else {
                        if (profileData.discount.toDouble()==0.0){
                            if (fetchProductsModel.flash_discount.replace("₹", "").replace(
                                    ",",
                                    ""
                                ).toDouble()==0.0){
                                return ""

                            }
                            return " (" + formatValues2(fetchProductsModel.flash_discount.replace("₹", "").replace(
                                ",",
                                ""
                            )) + "%)"
                        }
                       if (fetchProductsModel.flash_discount.replace("₹", "").replace(
                               ",",
                               ""
                           ).toDouble()==0.0){
                            return ""

                        }
                        return " (Less " + formatValues2(profileData.discount) + "% +" +
                                formatValues2(fetchProductsModel.flash_discount.replace("₹", "").replace(
                                    ",",
                                    ""
                                )) + "%)"
                    }
                } else {
                    if (profileData.discount.toDouble()==0.0){
                        return ""
                    }
                    return " (Less " + formatValues2(profileData.discount) + "%)"
                }


            } else {
                if (flash_sale == "off") {
                    return ""
                } else {
                    if (fetchProductsModel.flash_discount.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble()==0.0){
                        return ""

                    }
                    return " (" +
                            formatValues2(fetchProductsModel.flash_discount.replace("₹", "").replace(
                                ",",
                                ""
                            )) + "%)"
                }
            }

        }

        fun CuttOfPrice(fetchProductsModel11: CartModel, mContext: Context): Double {
            var priceAfterFlatDiscount = 0.0
            val gson = Gson()
            val json1 = getPrefs(mContext).getString("profile", "")
            val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)

            val json = gson.toJson(fetchProductsModel11)
            val fetchProductsModel: CartModel = gson.fromJson(json, CartModel::class.java)

            val flash_sale = getSharedPrefs(mContext).getString("flash_sale", "")
            val flash_sale_msg = getSharedPrefs(mContext).getString("flash_sale_msg", "")
            if (fetchProductsModel.flash_discount.isEmpty()) {
                fetchProductsModel.flash_discount = "0"
            }


            if (fetchProductsModel.flash_discount.replace("₹", "").replace(
                    ",",
                    ""
                ).isEmpty()
            ) {
                fetchProductsModel.flash_discount = "0"
            }
            if (fetchProductsModel.isexempted == "1") {
                if (flash_sale == "off") {
                    return 0.0


                } else {
                    val discount =
                        (fetchProductsModel.price.replace("₹", "").replace(
                            ",",
                            ""
                        ).toDouble() * profileData.discount.toDouble()) / 100
                    val price = fetchProductsModel.price.replace("₹", "").replace(
                        ",",
                        ""
                    ).toDouble() - discount


                    priceAfterFlatDiscount = price

                }
            } else {
                if (flash_sale == "off") {
                    return 0.0
                } else {

                    priceAfterFlatDiscount = fetchProductsModel.price.replace("₹", "").replace(
                        ",",
                        ""
                    ).toDouble()
                }
            }
            return priceAfterFlatDiscount * fetchProductsModel.saleable.toDouble()
        }

        @RequiresApi(Build.VERSION_CODES.M)
        fun getPriceText(
            fetchProductsModel: CartModel,
            mContext: Context,
            nextLine: Int
        ): SpannableString {
            val flash_sale = getSharedPrefs(mContext).getString("flash_sale", "")
            val flash_sale_msg = getSharedPrefs(mContext).getString("flash_sale_msg", "")
            val price = (getDiscountedPriceCart(
                fetchProductsModel,
                mContext
            ) * fetchProductsModel.saleable.toDouble())

            var stringFull = ""
            var start = 0
            var end = 0
            if (flash_sale == "off") {
                stringFull = ("₹" + currencyFormt((price)))
                if (nextLine == 0) {
                    stringFull = ("₹" + currencyFormt((price))+ " ")
                }else{
                    stringFull = ("₹" + currencyFormt((price)))
                }
                return SpannableString(stringFull)

            } else {
                var cutPrice = getDiscountAppliesww(fetchProductsModel, mContext)
//                var orginalPrice=currencyFormt(fetchProductsModel.price.toDouble() * fetchProductsModel.saleable.toDouble())
//                if (cutPrice == orginalPrice) {
//                    stringFull = ("₹" + currencyFormt((price)))
//                    return SpannableString(stringFull + "")
//                }
                cutPrice = cutPrice.replace(",","")
                if (cutPrice.toDouble() == price) {
                    if (nextLine == 0) {
                        stringFull = ("₹" + currencyFormt((price))+ " ")
                    }else{
                        stringFull = ("₹" + currencyFormt((price)))
                    }
                    return SpannableString(stringFull)
                } else {
                    val cut2 = "₹$cutPrice"
                    if (currencyFormt(cutPrice.toDouble()) == currencyFormt((price))) {
                        if (nextLine == 0) {
                            stringFull = ("₹" + currencyFormt((price))+ " ")
                        }else{
                            stringFull = ("₹" + currencyFormt((price)))
                        }
                        return SpannableString(stringFull + "")
                    }
//                    if ( currencyFormt((price)).trim() == currencyFormt(cut2.replace("₹", "").replace(
//                            ",",
//                            ""
//                        ).toDouble())) {
//                        stringFull = ("₹" + currencyFormt((price)))
//                        if (nextLine == 0) {
//                            stringFull = ("₹" + currencyFormt((price))+ " ")
//                        }else{
//                            stringFull = ("₹" + currencyFormt((price)))
//                        }
//                        return SpannableString(stringFull + "")
//                    }
                    if (nextLine == 1) {
                        stringFull = ("₹" + currencyFormt((price)) + " " + cut2 )
                        start = ("₹" + currencyFormt((price))).length+1
                        end = ("₹" + currencyFormt((price))).length + cut2.length+1
                        val string = SpannableString(stringFull )
                        string.setSpan(StrikethroughSpan(), start, end, 0)
                        string.setSpan(RelativeSizeSpan(.8f), start, end, 0)
                        return string
                    } else {

                        stringFull = ("₹" + currencyFormt((price)).trim() +"\n"+cut2)
                        start = ("₹" + currencyFormt((price))).length
                        end = ("₹" + currencyFormt((price))).length + cut2.length+1
                        val string = SpannableString(stringFull)
                        string.setSpan(StrikethroughSpan(), start, end, 0)
                        string.setSpan(RelativeSizeSpan(.8f), start, end, 0)
                        return string
                    }

                }
            }

        }

        private fun getDiscountAppliesww(
            fetchProductsModel: CartModel,
            mContext: Context
        ): String {
            val gson = Gson()
            val json1 = getPrefs(mContext).getString("profile", "")
            val profileData: SignUpModel = gson.fromJson(json1, SignUpModel::class.java)
            if (fetchProductsModel.isexempted == "1") {
                val discount =
                    ((fetchProductsModel.price.toDouble() * fetchProductsModel.saleable.toDouble() * profileData.discount.toDouble()) / 100)
                return currencyFormt(fetchProductsModel.price.toDouble() * fetchProductsModel.saleable.toDouble() - discount)
            } else {
                val discount =
                    ((fetchProductsModel.price.toDouble() * fetchProductsModel.saleable.toDouble() * fetchProductsModel.flash_discount.toDouble()) / 100)

                return currencyFormt((fetchProductsModel.price.toDouble() * fetchProductsModel.saleable.toDouble()))

                //   return currencyFormt((fetchProductsModel.price.toDouble() * fetchProductsModel.saleable.toDouble()) - discount)

            }
        }

        @RequiresApi(Build.VERSION_CODES.M)
        fun getPriceText2(
            fetchProductsModel: CartModel,
            mContext: Context,
            nextLine: Int
        ): SpannableString {
            val price = (getDiscountedPriceCart(
                fetchProductsModel,
                mContext
            ) * fetchProductsModel.saleable.toDouble())
            val cut = CuttOfPrice(fetchProductsModel, mContext)
            val cut2 = "₹" + (getDiscountAppliesCart(fetchProductsModel, mContext))

            val stringFull2 =
                ("per " + formatValues2(fetchProductsModel.saleable) + " " + fetchProductsModel.uom) + (getDiscountAppliesCart(
                    fetchProductsModel,
                    mContext
                ))

            var string2 = SpannableString(stringFull2)

            val discountStart =
                ("per " + formatValues2(fetchProductsModel.saleable) + " " + fetchProductsModel.uom).length
            val discountEnd =
                ("per " + formatValues2(fetchProductsModel.saleable) + " " + fetchProductsModel.uom).length + (getDiscountAppliesCart(
                    fetchProductsModel,
                    mContext
                )).length

            if (cut != 0.0 && cut != price) {

                string2.setSpan(
                    ForegroundColorSpan(mContext.getColor(R.color.colorAccent)),
                    discountStart + 2,
                    discountEnd - 1,
                    0
                )

                return string2
            } else {
                if (fetchProductsModel.isexempted == "1") {
                    try {
                        if (discountStart+2>discountEnd-1){

                            return  SpannableString(string2)
                        }else {
                            string2.setSpan(
                                ForegroundColorSpan(mContext.getColor(R.color.colorAccent)),
                                discountStart + 2,
                                discountEnd - 1,
                                0
                            )
                        }
                    }catch (e:Exception){

                      return  SpannableString(string2)
                    }

                    return string2
                }
                string2 =
                    SpannableString(("per " + formatValues2(fetchProductsModel.saleable) + " " + fetchProductsModel.uom))

                return string2
            }

        }
    }

    class deleteCart(
        val context: Context
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            //Done due to crash in DB having null value from api
            val db = getDataBase(context)

            db!!.productDao().deleteCart()



            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

        }
    }

    class deleteBasket(
        val context: Context
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            //Done due to crash in DB having null value from api
            val db = getDataBase(context)

            db!!.productDao().deleteBasket()

            var list = db!!.productDao().getAllBasketModelArray()
            basketSize = list.size

            return null
        }


        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

        }
    }


    //    get the data from database to filter
    class AddToCart(
        val context: Context,
        var b: Boolean,
        var model: CartModel
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            //Done due to crash in DB having null value from api
            val db = getDataBase(context)
            if (b) {

                db!!.productDao().insertCartProductsModel(model)

            } else {
                db!!.productDao().deleteCartProductsModelTable(model.product_id)
                //remove
            }

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

        }
    }

    class AddToBasket(
        val context: Context,
        var b: Boolean,
        var model: BasketModel
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            //Done due to crash in DB having null value from api
            val db = getDataBase(context)
            if (b) {

                db!!.productDao().insertBasket(model)

            } else {
                db!!.productDao().deleteBasketTable(model.product_id)
                //remove
            }
            var list = db!!.productDao().getAllBasketModelArray()
            basketSize = list.size
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

        }
    }


    class UpdateToCart(
        val context: Context,
        var model: CartModel
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            //Done due to crash in DB having null value from api
            val db = getDataBase(context)


            db!!.productDao().updateCart(model)



            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

        }
    }

    class UpdateToBasket(
        val context: Context,
        var model: BasketModel
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            //Done due to crash in DB having null value from api
            val db = getDataBase(context)


            db!!.productDao().updateBasket(model)



            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

        }
    }


    class getProductsOfCart(val context: Context) : AsyncTask<Void, Void, Void>() {
        var array = ArrayList<CartModel>()
        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(context)

            array = db!!.productDao().getAllCart() as ArrayList<CartModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            cartSize = array.size
        }
    }

    class getProductsOfBasket(val context: Context) : AsyncTask<Void, Void, Void>() {
        var array = ArrayList<BasketModel>()
        override fun doInBackground(vararg params: Void?): Void? {

            val db = Constants.getDataBase(context)

            array = db!!.productDao().getAllBasketModelArray() as ArrayList<BasketModel>

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            basketSize = array.size
        }
    }


    class UpdateToCarts(
        val context: Context,
        var model: FetchProductsModel
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            val gson = Gson()
            val innerJson = gson.toJson(model)
            val obj = gson.fromJson(innerJson, CartModel::class.java)
            val db = getDataBase(context)


            db!!.productDao().updateCarts(obj)

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

        }
    }


}
