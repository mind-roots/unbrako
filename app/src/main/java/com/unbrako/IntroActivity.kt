package com.unbrako

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.unbrako.intro.Intro_First
import com.unbrako.intro.SelectApp
import com.unbrako.intro.SelectEcommViewModel
import com.unbrako.intro.ViewPagerAdapter

class IntroActivity : AppCompatActivity(), Intro_First.swapToNextFragment {
    lateinit var pager: androidx.viewpager.widget.ViewPager
    lateinit var loginViewModel: SelectEcommViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
                loginViewModel = ViewModelProviders.of(this)[SelectEcommViewModel::class.java]
        loginViewModel.getCountryCode()

        init()

        val prefs = Constants.getSharedPrefs(this)

        if (prefs.getString("Selected", "1") != null){

        }

        if (prefs.getString("LoggedIn", "0") != null && !prefs.getString("LoggedIn", "0").equals("0")) {
            val value = Constants.getSharedPrefs(this).getString("App", "Emain")

            if (value == "Emain"){
                startActivity(Intent(this, SelectApp::class.java))
                finish()
            }else {
                val intent = Intent(this, SelectApp::class.java)
                startActivity(intent)
                finish()
            }
        }else if(prefs.getString("SelectApp", "0")=="1"){

            startActivity(Intent(this, SelectApp::class.java))
            finish()
        }
    }

    private fun init() {
        pager = findViewById(R.id.viewPager)
        val adapter = ViewPagerAdapter(supportFragmentManager)
        pager.adapter = adapter

    }


    override fun callSecondFragment() {
        pager.currentItem = 1

    }

}