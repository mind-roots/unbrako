package com.unbrako.search


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.unbrako.Constants
import com.unbrako.R
import android.os.AsyncTask
import com.unbrako.dataBase.*
import kotlinx.android.synthetic.main.activity_search.*


class SearchActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    lateinit var searchView: SearchView
    lateinit var searchCancel: TextView
    var recyclerView: androidx.recyclerview.widget.RecyclerView? = null
    var layout: LinearLayout? = null
    var productName: ArrayList<Products> = arrayListOf()
    var inchArray: ArrayList<InchBox> = arrayListOf()
    var metrixArray: java.util.ArrayList<MetricBox> = arrayListOf()
    private val allParts: ArrayList<SearchModel> = ArrayList()
//    private val animals2: ArrayList<SeazrchModel> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        recyclerView = findViewById(R.id.recyclerView)
        searchCancel = findViewById(R.id.searchCancel)
        layout = findViewById(R.id.layout)
        searchView = findViewById(R.id.searchView)
        val closeBtn = searchView.findViewById(R.id.search_close_btn)
                as ImageView
        closeBtn.isEnabled = false
        closeBtn.setImageDrawable(null)
        searchView.isIconified = false
        searchView.setOnQueryTextListener(this)

        InchArray()
        MetrixArray()

        searchCancel.setOnClickListener {
            searchView.setQuery("", false)

            Constants.hideSoftKeyboard(searchView, this)
            finish()

        }
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {

        if (p0?.toString()!!.isNotEmpty()) {
            Constants.hideSoftKeyboard(searchCancel, this)
            layout!!.visibility = View.GONE
            recyclerView!!.visibility = View.VISIBLE
            val animals3: ArrayList<SearchModel> = ArrayList()
            val animals33: ArrayList<SearchModel> = ArrayList()
            for (i in 0 until allParts.size) {
                if ((allParts[i].PartNumber.contains(p0)&& allParts[i].status!="0")||(allParts[i].ProductName.toLowerCase().contains(p0.toLowerCase()))) {
                    animals3.add(allParts[i])
                }
            }
            for ( j in 0 until animals3.size){
                if (animals3[j].ProductId != "24"){
                    animals33.add(animals3[j])
                }
            }
            recyclerView!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
            recyclerView!!.adapter = SearchAdapter(allParts,animals33, this)


        } else {
            layout!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
        }
        return false
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        if (p0?.toString()!!.isNotEmpty()) {
            layout!!.visibility = View.GONE
            recyclerView!!.visibility = View.VISIBLE
            val animals3: ArrayList<SearchModel> = ArrayList()
            val animals33: ArrayList<SearchModel> = ArrayList()
            for (i in 0 until allParts.size) {
                if ((allParts[i].PartNumber.contains(p0)&& allParts[i].status!="0")||(allParts[i].ProductName.toLowerCase().contains(p0.toLowerCase()))) {
                    animals3.add(allParts[i])
                }
            }
            for ( j in 0 until animals3.size){
                if (animals3[j].ProductId != "24"){
                    animals33.add(animals3[j])
                }
            }
            recyclerView!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
            recyclerView!!.adapter = SearchAdapter(allParts,animals33, this)

        } else {
            layout!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
        }
        return false
    }


    private fun InchArray() {


        class GetProduct : AsyncTask<Void, Void, Void>() {

            override fun doInBackground(vararg params: Void?): Void? {

                var db = Constants.getDataBase(this@SearchActivity)

                inchArray = db!!.productDao().getAllInchBoxArray() as ArrayList<InchBox>
                productName = db!!.productDao().getAllProducts() as ArrayList<Products>

                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)

            }
        }
        GetProduct().execute()

    }

    private fun MetrixArray() {

        class GetProduct : AsyncTask<Void, Void, Void>() {
            override fun onPreExecute() {
                super.onPreExecute()
                progress.visibility=View.VISIBLE
            }
            override fun doInBackground(vararg params: Void?): Void? {

                var db = Constants.getDataBase(this@SearchActivity)

                metrixArray = db!!.productDao().getAllMetricBoxArray() as java.util.ArrayList<MetricBox>
                for (i in 0 until inchArray.size) {
                    val model = SearchModel()
                    model.PartNumber = inchArray[i].PartNumber
                    model.Diameter = inchArray[i].Diameter
                    model.ProductId = inchArray[i].ProductID
                    model.status = inchArray[i].webStatus
                    model.ProductName =""

                    model.type = "1"
                    allParts.add(model)
                }
                for (i in 0 until metrixArray.size) {
                    val model = SearchModel()
                    model.PartNumber = metrixArray[i].PartNumber
                    model.Diameter = metrixArray[i].Diameter
                    model.ProductId = metrixArray[i].ProductID
                    model.status = metrixArray[i].webStatus
                    model.ProductName = ""
                    model.type = "0"
                    allParts.add(model)
                }
                for (i in 0 until productName.size){
                    val model = SearchModel()
                    model.PartNumber = ""
                    model.Diameter = ""
                    model.ProductId = productName[i].ProductID
                    model.ProductName = productName[i].name
                    model.status = "0"
                    model.type = "2"
                    allParts.add(model)
                }
                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)
                progress.visibility=View.GONE
            }
        }
        GetProduct().execute()

    }

}