package com.unbrako.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.catalog.RecyclerModel
import android.app.Activity

import android.app.AlertDialog
import android.widget.RadioButton
import android.widget.RadioGroup


/**
 * Created by amrit on 12/1/19.
 */
class SearchAdapter(
    var arrayMain: ArrayList<SearchModel>, var array: ArrayList<SearchModel>,
    var context: Context
) : androidx.recyclerview.widget.RecyclerView.Adapter<SearchAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return array.size
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        var textViewName: TextView

        init {
            textViewName = itemView.findViewById(R.id.textViewName)

        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {

        val v = LayoutInflater.from(context)
            .inflate(R.layout.search_lauout, p0, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        if (array[p1].type == "1") {
            p0.textViewName.text = array[p1].PartNumber
        } else if (array[p1].type == "0") {
            p0.textViewName.text = array[p1].PartNumber

        } else if (array[p1].type == "2") {
            p0.textViewName.text = array[p1].ProductName
        }
        p0.itemView.setOnClickListener {

            if (array[p1].type == "1") {
                Constants.getSharedPrefs(context).edit().putString("Selected", "1").apply()
                var modelP = RecyclerModel()
                modelP.image = 0
                modelP.id = array[p1].ProductId
                modelP.imageName = ""
                modelP.Diameter = array[p1].Diameter
                Constants.navigate(context, it, modelP, "search", "")
            } else if (array[p1].type == "0") {
                Constants.getSharedPrefs(context).edit().putString("Selected", "0").apply()
                var modelP = RecyclerModel()
                modelP.image = 0
                modelP.id = array[p1].ProductId
                modelP.imageName = ""
                modelP.Diameter = array[p1].Diameter
                Constants.navigate(context, it, modelP, "search", "")
            }
            else if (array[p1].type == "2") {

                if (array[p1].ProductId == "22" || array[p1].ProductId == "31" || array[p1].ProductId == "32" ) {
                    Constants.getSharedPrefs(context).edit().putString("Selected", "0").apply()
                    val modelP = RecyclerModel()
                    modelP.image = 0
                    modelP.id = array[p1].ProductId
                    modelP.imageName = ""
                    modelP.Diameter = "0"
                    Constants.navigate(context, it, modelP, "search","")
                }else if (array[p1].ProductId == "33" || array[p1].ProductId == "34" || array[p1].ProductId == "35" || array[p1].ProductId == "36" || array[p1].ProductId == "37"){
                    Constants.getSharedPrefs(context).edit().putString("Selected", "0").apply()
                    val modelP = RecyclerModel()
                    modelP.image = 0
                    modelP.id = array[p1].ProductId
                    modelP.imageName = ""
                    modelP.Diameter = "0"
                    Constants.navigate(context, it, modelP, "search", "")
                }
                else if (array[p1].ProductId == "38" || array[p1].ProductId == "39" || array[p1].ProductId == "40" || array[p1].ProductId == "41" ||array[p1].ProductId == "42" || array[p1].ProductId == "43" || array[p1].ProductId == "44" || array[p1].ProductId == "45" ||array[p1].ProductId == "46" || array[p1].ProductId == "47" ){
                    Constants.getSharedPrefs(context).edit().putString("Selected", "1").apply()
                    val modelP = RecyclerModel()
                    modelP.image = 0
                    modelP.id = array[p1].ProductId
                    modelP.imageName = ""
                    modelP.Diameter ="0"
                    Constants.navigate(context, it, modelP, "search", "")
                }
                else {
                    showDialog(p1, it)
                }
            }
        }
    }

    private fun showDialog(p1: Int, it: View) {
        val basic_reg: AlertDialog
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setTitle("Select category")
        val inflater = (context as Activity).layoutInflater
        val v = inflater.inflate(R.layout.custom_aler_search, null)
        builder.setView(v)
        builder.setCancelable(false)
        val group = v.findViewById<RadioGroup>(R.id.group)
        val radioButton = v.findViewById<RadioButton>(R.id.metric)
        val radioButton2 = v.findViewById<RadioButton>(R.id.inch)

        builder.setPositiveButton("Ok")
        { _, _ ->
            if (radioButton.isChecked) {

                Constants.getSharedPrefs(context).edit().putString("Selected", "0").apply()
                val modelP = RecyclerModel()
                modelP.image = 0
                modelP.id = array[p1].ProductId
                modelP.imageName = ""
                modelP.Diameter = "0"
                Constants.navigate(context, it, modelP, "search", "")
            }
            else {

                Constants.getSharedPrefs(context).edit().putString("Selected", "1").apply()
                var modelP = RecyclerModel()
                modelP.image = 0
                modelP.id = array[p1].ProductId
                modelP.imageName = ""
                modelP.Diameter = "0"
                Constants.navigate(context, it, modelP, "search", "")
            }
        }
        builder.setNegativeButton("Cancel") { dialog, which ->
        }
        builder.create()
        basic_reg = builder.show()
    }


}