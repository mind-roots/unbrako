package com.unbrako.metric

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.catalog.RecyclerModel
import com.unbrako.dataBase.MetricBox
import com.unbrako.dataBase.ProductMetric
import kotlinx.android.synthetic.main.metric_fields.*
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView
import java.util.*


class MetricActivity : AppCompatActivity(), MetricAdapter.setLayoutMetric, MetricSwipeAdapter.HideSwipe {

    lateinit var toolbar_edit_catalog: Toolbar
    lateinit var rVMetricSwipe: androidx.recyclerview.widget.RecyclerView
    //lateinit var rotate: Animation
    private lateinit var mAdapter: MetricSwipeAdapter
    lateinit var rVMetricDia: androidx.recyclerview.widget.RecyclerView
    lateinit var recyclerModel: RecyclerModel
    lateinit var cl_grade: LinearLayout
    lateinit var partLay: RelativeLayout
    lateinit var tv_grade: TextView
    lateinit var cl_thread: LinearLayout
    lateinit var hardnessLay: LinearLayout
    lateinit var tv_thread: TextView
    lateinit var cl_pitch: LinearLayout
    lateinit var tv_pitch: TextView
    lateinit var cl_tsa: LinearLayout
    lateinit var tv_tsa: TextView
    lateinit var cl_nsa: LinearLayout
    lateinit var tv_nsa: TextView
    lateinit var max_1: TextView
    lateinit var hardnessValue: TextView
    lateinit var tv_metric_5: TextView

    lateinit var cl_stpf: LinearLayout
    lateinit var tv_stpf_one: TextView
    lateinit var tv_stpf_two: TextView
    lateinit var teeth_value: TextView

    lateinit var cl_ts: LinearLayout
    lateinit var tv_ts_one: TextView
    lateinit var tv_ts_two: TextView
    lateinit var cl_ctr_body: LinearLayout
    lateinit var tv_ctr_body: TextView
    lateinit var cl_body_drill: LinearLayout
    lateinit var tv_body_drill: TextView
    lateinit var cl_tap_drill: LinearLayout
    lateinit var tv_tap_drill: TextView
    lateinit var cl_od_max: LinearLayout
    lateinit var tv_od_max: TextView
    lateinit var cl_od_min: LinearLayout
    lateinit var tv_od_min: TextView
    lateinit var cl_ssb: LinearLayout
    lateinit var tv_ssbh: TextView
    lateinit var cl_slm: LinearLayout
    lateinit var tv_slm: TextView
    lateinit var cl_teeth: LinearLayout
    lateinit var tv_teeth: TextView
    lateinit var arrow: ImageView

    lateinit var tv_metric_1: TextView
    lateinit var tv_metric_3: TextView
    lateinit var textView18: TextView
    lateinit var dMin: TextView
    lateinit var teeth_value2: TextView
    lateinit var dMax: TextView
    lateinit var textView11: TextView


    lateinit var tvDwMin: TextView
    lateinit var tvDwMaxb: TextView
    lateinit var tvDwMinb: TextView
    lateinit var tvthickness: TextView

    lateinit var stpf_lbs: TextView
    lateinit var stpf_ll: RelativeLayout

    lateinit var cl_thickness: LinearLayout
    lateinit var cl_finish: LinearLayout
    lateinit var linear_one: LinearLayout

    lateinit var rl_ts: RelativeLayout
    lateinit var tv_ts: TextView

    lateinit var tvTsa: TextView
    lateinit var tvNsa: TextView

    lateinit var tvStpfNm: TextView
    lateinit var tvbmax: TextView
    lateinit var tvbmin: TextView


    lateinit var scrollViewMetric: ScrollView

    var position = 0

    private var isShown = false

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setLayout()


        /* Handler().postDelayed({
             val displayMetrics = DisplayMetrics()
             windowManager.defaultDisplay.getMetrics(displayMetrics)
             var height = displayMetrics . heightPixels
             var width = displayMetrics . widthPixels
             scrollViewMetric.smoothScrollTo(0, (height/2))
            // showCase()

         }, 1000)*/

        scrollViewMetric.setOnScrollChangeListener(
            (View.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->

                showCase()
                return@OnScrollChangeListener

            })
        )
        arrow.visibility = View.INVISIBLE

    }


    private fun showCase() {


        var value = Constants.getSharedPrefs(this).getString("showOnce", "one")


        if (value == "one") {

            val view = rVMetricSwipe.getChildAt(0)

            try {
                if (getVisibility(view) && !isShown) {

                    isShown = true
                    scrollViewMetric.fling(0)
                    MaterialShowcaseView.Builder(this)
                        .setTarget(view)
                        .withRectangleShape()
                        .setDismissText("GOT IT")
                        .singleUse("1")
                        .setMaskColour(Color.parseColor("#CBF37836"))
                        .setContentText("Swipe left on a row to Bookmark the item or get a quote.")
                        //  .singleUse(SHOWCASE_ID) // provide a unique ID used to ensure it is only shown once
                        .show()
                    Constants.getSharedPrefs(this).edit().putString("showOnce", "dcd").apply()
                }
            } catch (e: java.lang.Exception) {

            }
        }


    }


    private fun getVisibility(view: View): Boolean {
        val rect = Rect()
        if (view.getGlobalVisibleRect(rect)
            && view.height == rect.height()
            && view.width == rect.width()
        ) {
            return true// view is fully visible on screen
        }

        return false
    }

    private fun showHideLayout() {
        if (tv_grade.text.toString().trim() == "") {
            cl_grade.visibility = View.GONE
        } else {
            cl_grade.visibility = View.VISIBLE
        }
        if (tv_thread.text.toString().trim() == "") {
            cl_thread.visibility = View.GONE
        } else {
            cl_thread.visibility = View.VISIBLE
        }
        if (tv_pitch.text.toString().trim() == "") {
            cl_pitch.visibility = View.GONE
        } else {
            cl_pitch.visibility = View.VISIBLE
        }
        if (tv_tsa.text.toString().trim() == "") {
            cl_tsa.visibility = View.GONE
        } else {
            cl_tsa.visibility = View.VISIBLE
        }
        if (tv_nsa.text.toString().trim() == "") {
            cl_nsa.visibility = View.GONE
        } else {
            cl_nsa.visibility = View.VISIBLE
        }
        if ((tv_stpf_one.text.toString().trim() == "") && (tv_stpf_two.text.toString().trim() == "")) {
            cl_stpf.visibility = View.GONE
        } else {
            cl_stpf.visibility = View.VISIBLE
        }
        if ((tv_ts_one.text.toString().trim() == "") && (tv_ts_two.text.toString().trim() == "")) {
            cl_ts.visibility = View.GONE
        } else {
            cl_ts.visibility = View.VISIBLE
        }

        if (tv_ctr_body.text.toString().trim() == "") {
            cl_ctr_body.visibility = View.GONE
        } else {
            cl_ctr_body.visibility = View.VISIBLE
        }
        if (tv_body_drill.text.toString().trim() == "") {
            cl_body_drill.visibility = View.GONE
        } else {
            cl_body_drill.visibility = View.VISIBLE
        }
        if (tv_tap_drill.text.toString().trim() == "") {
            cl_tap_drill.visibility = View.GONE
        } else {
            cl_tap_drill.visibility = View.VISIBLE
        }
        if (tv_od_max.text.toString().trim() == "") {
            cl_od_max.visibility = View.GONE
        } else {
            cl_od_max.visibility = View.VISIBLE
        }
        if (tv_od_min.text.toString().trim() == "") {
            cl_od_min.visibility = View.GONE
        } else {
            cl_od_min.visibility = View.VISIBLE
        }
        if (tv_ssbh.text.toString().trim() == "") {
            cl_ssb.visibility = View.GONE
        } else {
            cl_ssb.visibility = View.VISIBLE
        }
        if (tv_slm.text.toString().trim() == "") {
            cl_slm.visibility = View.GONE
        } else {
            cl_slm.visibility = View.VISIBLE
        }
        if (tv_teeth.text.toString().trim() == "") {
            cl_teeth.visibility = View.GONE
        } else {
            cl_teeth.visibility = View.VISIBLE
        }
        if (hardnessValue.text.toString().trim() == "") {
            hardnessLay.visibility = View.GONE
        } else {
            hardnessLay.visibility = View.VISIBLE
        }
        if (recyclerModel.id == "24") {
            cl_slm.visibility = View.GONE
        }
        if (recyclerModel.id == "23") {
            cl_pitch.visibility = View.GONE
            tv_ts.visibility = View.GONE
            rl_ts.visibility = View.GONE
            tv_ts_two.visibility = View.GONE
        }

    }


    private fun init() {
        rVMetricDia = findViewById(R.id.rv_metric_diameter)
        rVMetricSwipe = findViewById(R.id.rv_metric_swipe)
        rVMetricSwipe.isNestedScrollingEnabled = false

        hardnessLay = findViewById(R.id.hardnessLay)
        hardnessValue = findViewById(R.id.hardnessValue)
        partLay = findViewById(R.id.partLay)
        cl_grade = findViewById(R.id.cl_grade)
        tv_grade = findViewById(R.id.tv_grade)
        cl_thread = findViewById(R.id.cl_thread)
        tv_thread = findViewById(R.id.tv_thread)
        cl_pitch = findViewById(R.id.cl_pitch)
        tv_pitch = findViewById(R.id.tv_pitch)
        cl_tsa = findViewById(R.id.cl_tsa)
        tv_tsa = findViewById(R.id.tv_tsa)
        cl_nsa = findViewById(R.id.cl_nsa)
        tv_nsa = findViewById(R.id.tv_nsa)
        cl_stpf = findViewById(R.id.cl_stpf)
        tv_stpf_one = findViewById(R.id.tv_stpf_one)
        tv_stpf_two = findViewById(R.id.tv_stpf_two)
        cl_ts = findViewById(R.id.cl_ts)
        tv_ts_one = findViewById(R.id.tv_ts_one)
        tv_ts_two = findViewById(R.id.tv_ts_two)
        cl_ctr_body = findViewById(R.id.cl_ctr_body)
        tv_ctr_body = findViewById(R.id.tv_ctr_body)
        cl_body_drill = findViewById(R.id.cl_body_drill)
        tv_body_drill = findViewById(R.id.tv_body_drill)
        cl_tap_drill = findViewById(R.id.cl_tap_drill)
        tv_tap_drill = findViewById(R.id.tv_tap_drill)
        cl_od_max = findViewById(R.id.cl_od_max)
        tv_od_max = findViewById(R.id.tv_od_max)
        cl_od_min = findViewById(R.id.cl_od_min)
        tv_od_min = findViewById(R.id.tv_od_min)
        cl_ssb = findViewById(R.id.cl_ssb)
        tv_ssbh = findViewById(R.id.tv_ssbh)
        cl_slm = findViewById(R.id.cl_slm)
        tv_slm = findViewById(R.id.tv_slm)
        cl_teeth = findViewById(R.id.cl_teeth)
        tv_teeth = findViewById(R.id.tv_teeth)

        stpf_ll = findViewById(R.id.stpf_ll)
        stpf_lbs = findViewById(R.id.stpf_lbs)

        tvthickness = findViewById(R.id.tvthickness)

        scrollViewMetric = findViewById(R.id.scrollViewMetric)
        arrow = findViewById(R.id.arrow)
        tv_metric_1 = findViewById(R.id.tv_metric_1)
        linear_one = findViewById(R.id.linear_one)


//        image_view_top = findViewById(R.id.iv_metric_1)
//        image_view_bottom = findViewById(R.id.imageView6)
    }

//    set different layout for metric activity

    private var startTime: Long = 0

    private fun setLayout() {
        recyclerModel = intent.getParcelableExtra("parcel_value")!!
        if (recyclerModel.id == "14") {
            setContentView(R.layout.metric_activity)
            tv_metric_3 = findViewById(R.id.tv_metric_3)
            textView18 = findViewById(R.id.textView18)
            textView11 = findViewById(R.id.textView11)

        } else if (recyclerModel.id == "27") {
            setContentView(R.layout.socket_low_head)
            tv_metric_3 = findViewById(R.id.tv_metric_3)
            textView18 = findViewById(R.id.textView18)
            textView11 = findViewById(R.id.textView11)
            tvTsa = findViewById(R.id.tvTsa)
            tvNsa = findViewById(R.id.tvNsa)
            //arrow = findViewById(R.id.arrow)
        } else if (recyclerModel.id == "23") {
            setContentView(R.layout.socket_head_shoulder)
            tv_metric_3 = findViewById(R.id.tv_metric_3)
            textView18 = findViewById(R.id.textView18)
            textView11 = findViewById(R.id.textView11)

            tvbmax = findViewById(R.id.tvbmax)
            tvbmin = findViewById(R.id.tvbmin)

            tv_ts = findViewById(R.id.tv_ts)
            rl_ts = findViewById(R.id.rl_ts)
            tv_ts_two = findViewById(R.id.tv_ts_two)


        } else if (recyclerModel.id == "19") {
            setContentView(R.layout.socket_flat_head)
            tv_metric_3 = findViewById(R.id.tv_metric_3)
            textView18 = findViewById(R.id.textView18)
            textView11 = findViewById(R.id.textView11)
        } else if (recyclerModel.id == "20") {

            setContentView(R.layout.socket_button_head)
            tv_metric_3 = findViewById(R.id.tv_metric_3)
            textView18 = findViewById(R.id.textView18)
            textView11 = findViewById(R.id.textView11)
        } else if (recyclerModel.id == "21") {

            setContentView(R.layout.socket_flang_button)
            tv_metric_3 = findViewById(R.id.tv_metric_3)
            textView18 = findViewById(R.id.textView18)
            textView11 = findViewById(R.id.textView11)

        } else if (recyclerModel.id == "24") {
            setContentView(R.layout.socket_set_screw)
            max_1 = findViewById(R.id.max_1)
        } else if (recyclerModel.id == "28") {
            setContentView(R.layout.taper_pressure_plug)
            tv_metric_5 = findViewById(R.id.tv_metric_5)
            tvStpfNm = findViewById(R.id.tvStpfNm)
        } else if (recyclerModel.id == "22") {
            setContentView(R.layout.durlok_bolt)
            tv_metric_3 = findViewById(R.id.tv_metric_3)
            textView18 = findViewById(R.id.textView18)
            //   textView11 = findViewById(R.id.textView11)
            teeth_value = findViewById(R.id.teeth_value)
        } else if (recyclerModel.id == "31") {
            setContentView(R.layout.durlok_nut)
            tv_metric_3 = findViewById(R.id.tv_metric_3)
            textView18 = findViewById(R.id.textView18)
            dMin = findViewById(R.id.dMin)
            dMax = findViewById(R.id.dMax)
            // textView11 = findViewById(R.id.textView11)

            teeth_value2 = findViewById(R.id.teeth_value2)
            //teeth_value = findViewById(R.id.teeth_value)
        } else if (recyclerModel.id == "32") {
            setContentView(R.layout.durlok_washer)
            tv_metric_1 = findViewById(R.id.tv_metric_1)
            tvDwMin = findViewById(R.id.tvDwMin)
            tvDwMaxb = findViewById(R.id.tvDwMaxb)
            tvDwMinb = findViewById(R.id.tvDwMinb)
            cl_thickness = findViewById(R.id.cl_thickness)
            cl_finish = findViewById(R.id.cl_finish)
        } else if (recyclerModel.id == "33") {
            setContentView(R.layout.socket_set_screw_conepoint)
            max_1 = findViewById(R.id.max_1)
        } else if (recyclerModel.id == "34") {
            setContentView(R.layout.socket_set_screw_dogpoint)
            max_1 = findViewById(R.id.max_1)
            // teeth_value = findViewById(R.id.teeth_value)
        } else if (recyclerModel.id == "35") {
            setContentView(R.layout.socket_set_screw_flatpoint)
            max_1 = findViewById(R.id.max_1)
        } else if (recyclerModel.id == "36") {
            setContentView(R.layout.socket_set_screw_kurnled)
            max_1 = findViewById(R.id.max_1)
        } else if (recyclerModel.id == "37") {
            setContentView(R.layout.socket_set_screw_plaincup)
            max_1 = findViewById(R.id.max_1)
        }

        toolbar_edit_catalog = findViewById(R.id.toolbar_edit_catalog)
        setSupportActionBar(toolbar_edit_catalog)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
       // title = "Catalog"
        startTime = System.currentTimeMillis()
        init()
        setLayoutManager()
        setAdapter()
        setAdapterSwipe()


    }

//    set layoutManager for diameter and swipe view

    @SuppressLint("WrongConstant")
    private fun setLayoutManager() {


        val linearManagaer2 = androidx.recyclerview.widget.LinearLayoutManager(
            this,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            false
        )
        rVMetricSwipe.layoutManager = linearManagaer2

        val dividerItemDecoration = androidx.recyclerview.widget.DividerItemDecoration(
            rVMetricSwipe.context,
            linearManagaer2.orientation
        )
        rVMetricSwipe.addItemDecoration(dividerItemDecoration)
        var endTime = System.currentTimeMillis()
        Log.e("time: ", "" + (endTime - startTime))
    }

    //    set adapter for metric swipe
    private fun setAdapterSwipe() {

    }

    //    set adapter for diameter
    private fun setAdapter() {
        getValues(this@MetricActivity)

    }

    private fun getValues(applicationContext: Context?) {

        var arrayList: ArrayList<ProductMetric> = arrayListOf()
        var arrayList2: ArrayList<ProductMetric> = arrayListOf()
        var arrayListMetrix: ArrayList<MetricBox> = arrayListOf()

        class GetProduct : AsyncTask<Void, Void, Void>() {

            override fun doInBackground(vararg params: Void?): Void? {

                var db = Constants.getDataBase(this@MetricActivity)

                arrayList = db!!.productDao().getAllMetricProducts(recyclerModel.id, "1")
                        as ArrayList<ProductMetric>
                for (i in 0 until arrayList.size) {
                    arrayListMetrix =
                        db!!.productDao().getAllMetricBox(
                            recyclerModel.id,
                            arrayList[i].Diameter,
                            "1"
                        ) as ArrayList<MetricBox>
                    if (arrayListMetrix.size > 0) {
                        arrayList2.add(arrayList[i])
                    }
                }
                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)

                if (arrayList2.size > 0) {
                    arrayList2 = Constants.sortArray(arrayList2)
                    if (recyclerModel.Diameter == "0") {
                        arrayList2[0].status = "true"
                    } else {
                        for (i in 0 until arrayList2.size) {
                            if (recyclerModel.Diameter == arrayList2[i].Diameter) {
                                arrayList2[i].status = "true"
                                position = i
                                break
                            }
                        }
                    }

                    val linearmanager1 = androidx.recyclerview.widget.LinearLayoutManager(
                        this@MetricActivity,
                        androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
                        false
                    )
                    val adapter = MetricAdapter(this@MetricActivity, arrayList2, -1)
                    rVMetricDia.layoutManager = linearmanager1
                    rVMetricDia.adapter = adapter
                    rVMetricDia.layoutManager!!.scrollToPosition(position)
                } else {

                    //Toast.makeText(this@MetricActivity, "No Data", Toast.LENGTH_LONG).show()
                }
            }
        }
        GetProduct().execute()
    }

    //    setting the data for swipe recycler data
    private fun setSwipe(model: ProductMetric) {

        var arrayList: ArrayList<MetricBox> = arrayListOf()

        class GetProduct : AsyncTask<Void, Void, Void>() {

            override fun doInBackground(vararg params: Void?): Void? {

                var db = Constants.getDataBase(this@MetricActivity)

                arrayList =
                    db!!.productDao().getAllMetricBox(recyclerModel.id, model.Diameter, "1") as ArrayList<MetricBox>

                return null
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)
                var ll = 0
                if (arrayList.size > 0) {
                    arrayList = Constants.sortMetrixBox(arrayList, this@MetricActivity)
                    partLay.visibility = View.VISIBLE
                    for (i in 0 until arrayList.size) {
                        if (recyclerModel.Diameter.equals("0")) {

                        } else {
                            for (i in 0 until arrayList.size) {
                                if (recyclerModel.Diameter.trim().equals(arrayList[i].Diameter.trim())) {

                                    ll = i
                                    break
                                }
                            }
                        }
                    }
                    mAdapter = MetricSwipeAdapter(this@MetricActivity, arrayList, ll, model,recyclerModel.id)
                    rVMetricSwipe.adapter = mAdapter
//                    rotate = AnimationUtils.loadAnimation(this@MetricActivity, R.anim.swipe)
//                    rotate.repeatCount = Animation.INFINITE
                    // arrow.startAnimation(rotate)
                } else {
                    partLay.visibility = View.GONE
                    // Toast.makeText(this@MetricActivity, "No Data", Toast.LENGTH_LONG).show()
                }
            }
        }
        GetProduct().execute()

    }

    override fun hideSwipingArrow() {
//        arrow.clearAnimation()
//        arrow.visibility = View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun settingValuesMetric(model: ProductMetric) {

        setSwipe(model)
        try {

            tv_grade.text = model.Grade
            tv_thread.text = (model.Thread)
            tv_pitch.text = (model.Pitch)
            tv_tsa.text = (model.TensileStressArea)
            tv_nsa.text = (model.NominalShankArea)
            tv_stpf_one.text = (model.SeatingTorqueNm)
            tv_stpf_two.text = (model.SeatingTorqueInlbs)
            if (model.SeatingTorqueInlbs.isEmpty()) {
                tv_stpf_two.visibility = View.GONE
                stpf_ll.visibility = View.GONE
                stpf_lbs.visibility = View.GONE
            } else {
                tv_stpf_two.visibility = View.VISIBLE
                stpf_ll.visibility = View.VISIBLE
                stpf_lbs.visibility = View.VISIBLE
            }
            tv_ts_one.text = (model.TensileStrengthkN)
            tv_ts_two.text = (model.TensileStrengthlbs)
            tv_ctr_body.text = (model.CtrBore)
            tv_body_drill.text = (model.BodyDrill)
            tv_tap_drill.text = (model.TapDrill)
            tv_od_max.text = (model.BodyODMax)
            tv_od_min.text = (model.BodyODMin)
            tv_ssbh.text = (model.ShearStrengthOfBody)
            tv_slm.text = (model.ScrewLengthMin)
            tv_teeth.text = (model.Teeth)
            tv_metric_1.text = (model.AFNom)
            hardnessValue.text = (model.hardness)

            if (recyclerModel.id == "31") {

                dMin.text = (model.D1MIN)
                dMax.text = (model.D2MAX)
            }

            if (recyclerModel.id != "22" && recyclerModel.id != "24" && recyclerModel.id != "31") {
                textView11.text = (model.ThreadLengthRef)
            }


            if (recyclerModel.id == "24") {
                max_1.text = (model.ScrewLengthMin)
            }


            if (recyclerModel.id != "24") {
                textView18.text = (model.HeadHeightMax)
                tv_metric_3.text = (model.HeadODMax)
                teeth_value2.text = (model.Teeth)
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }
        showHideLayout()
        if (recyclerModel.id == "33" || recyclerModel.id == "34" || recyclerModel.id == "35" || recyclerModel.id == "36" || recyclerModel.id == "37") {
            max_1.text = (model.ScrewLengthMin)
            cl_slm.visibility = View.GONE
        }
        if (recyclerModel.id == "28") {
            tv_metric_5.text = (model.ScrewLengthMin)
            sdepthValue.text = (model.SocketDepth)
            cl_slm.visibility = View.GONE
            cl_thread.visibility = View.GONE
            socketDepthLay.visibility = View.VISIBLE
        }
        if (recyclerModel.id == "31" || recyclerModel.id == "32" || recyclerModel.id == "28") {
            hardnessLay.visibility = View.VISIBLE

        }
        if (recyclerModel.id == "32") {
            cl_finish.visibility = View.VISIBLE
            cl_thickness.visibility = View.VISIBLE
            tvthickness.text = (model.HMAX)
        }
        scrollViewMetric.scrollTo(0, 0)

        if (recyclerModel.id == "32") {
            tv_metric_1.text = (model.D2MAX)
            tvDwMin.text = (model.D1MIN)
            tvDwMaxb.text = (model.HMAX)
            tvDwMinb.text = (model.HMIN)
        }
        if (recyclerModel.id == "22") {
            teeth_value.text = (model.Teeth)
            cl_teeth.visibility = View.GONE
            cl_nsa.visibility = View.GONE
            cl_ctr_body.visibility = View.GONE
        }
        if (recyclerModel.id == "27") {
            tvTsa.visibility = View.VISIBLE
            tvNsa.visibility = View.VISIBLE
        }
        if (recyclerModel.id == "28") {
            tvStpfNm.visibility = View.GONE
        }
        if (recyclerModel.id == "19") {
            cl_ctr_body.visibility = View.GONE
        }
        if (recyclerModel.id == "20" || recyclerModel.id == "21") {
            cl_nsa.visibility = View.GONE
            cl_ctr_body.visibility = View.GONE
        }
        if (recyclerModel.id == "23") {
            cl_od_max.visibility = View.GONE
            cl_od_min.visibility = View.GONE
        }
        if (recyclerModel.id == "23") {

            tvbmax.text = model.BodyODMax
            tvbmin.text = model.BodyODMin

        }
    }
}