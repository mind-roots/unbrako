package com.unbrako.metric

import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.unbrako.R
import com.unbrako.dataBase.ProductMetric
import kotlinx.android.synthetic.main.metric_layout.view.*

class MetricAdapter(
    val context: Context,
    val metricModel: ArrayList<ProductMetric>,
    var row_index: Int
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<MetricAdapter.MyViewHolder>() {
    var updateMetric = context as setLayoutMetric


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(context).inflate(
            R.layout.metric_layout, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return metricModel.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = metricModel[position]

        holder.itemView.tv_socket1.text = model.Diameter
        holder.itemView.setOnClickListener {
            row_index = position

            for (i in 0 until metricModel.size){
                metricModel[i].status = "false"
            }
            metricModel[position].status = "true"
            notifyDataSetChanged()
        }

        if (model.status == "true") {
            updateMetric.settingValuesMetric(model)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tv_socket1.background = context.getDrawable(R.drawable.circle_text)
                holder.tv_socket1.setTextColor(Color.parseColor("#F37836"))
            }


        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tv_socket1.background = context.getDrawable(R.drawable.circular_textview)
                holder.tv_socket1.setTextColor(Color.parseColor("#ffffff"))
            }

        }

    }


    class MyViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var tv_socket1: TextView

        init {
            tv_socket1 = itemView.findViewById(R.id.tv_socket1)
        }
    }


    interface setLayoutMetric {
        fun settingValuesMetric(model: ProductMetric)
    }
}
