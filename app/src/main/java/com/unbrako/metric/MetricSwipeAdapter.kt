package com.unbrako.metric

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.unbrako.Constants
import com.unbrako.R
import com.unbrako.dataBase.BookMark
import com.unbrako.dataBase.MetricBox
import com.unbrako.dataBase.ProductMetric
import kotlinx.android.synthetic.main.socket_swipe_recycler.view.*
import java.util.*

class MetricSwipeAdapter(
    val context: Context,
    private val metricSwipeModel:
    ArrayList<MetricBox>,
    var positionS: Int,
    var model1: ProductMetric,
    var id: String


) : RecyclerSwipeAdapter<MetricSwipeAdapter.ViewHolder>() {
    var viewed = 0
    var pos = -90
    var hide=context as HideSwipe
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.socket_swipe_recycler, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return metricSwipeModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, positionT: Int) {
        var model = MetricBox()
        if (positionS == 0) {

            model = metricSwipeModel[positionT]
        } else {

            model = metricSwipeModel[positionS]
            positionS = 0
        }

       // holder.itemView.arrow.startAnimation(rotate);
        if (!model1.Pitch.isEmpty()&&!model.Length.isEmpty()){
            holder.itemView.tv_length.text = model.Diameter+" - " +model1.Pitch+" X "+model.Length
        }else if (model1.Pitch.isEmpty()&&!model.Length.isEmpty()){
            if (id=="23"){
                holder.itemView.tv_length.text = model.Diameter+" X " +model.Length
            }else{
                holder.itemView.tv_length.text = model.Diameter+" - " +model.Length

            }
        }else if (!model1.Pitch.isEmpty()&&model.Length.isEmpty()){
            if (id=="23"){
                holder.itemView.tv_length.text = model.Diameter+" X " +model1.Pitch
            }else{
                holder.itemView.tv_length.text = model.Diameter+" - " +model1.Pitch

            }

        } else if (model1.Pitch.isEmpty()&&model.Length.isEmpty()){
            holder.itemView.tv_length.text = model.Diameter
        }
        holder.itemView.tv_tpi.text = model1.Pitch
        holder.itemView.tv_part.text = model.PartNumber
        holder.itemView.tv_box_qty.text = model.BoxQuantity
//
//        if ((!model.DescriptionTwo.isEmpty()) && (model1.ProductID == "33")
//            ||(!model.DescriptionTwo.isEmpty()) && (model1.ProductID == "34")
//            ||(!model.DescriptionTwo.isEmpty()) && (model1.ProductID == "35")
//            ||(!model.DescriptionTwo.isEmpty()) && (model1.ProductID == "36")
//            ||(!model.DescriptionTwo.isEmpty()) && (model1.ProductID == "37")) {
//            holder.itemView.tv_part_description.visibility = View.VISIBLE
//            holder.itemView.tv_part_description.text = model.DescriptionTwo
//        } else {
//            holder.itemView.tv_part_description.visibility = View.GONE
//        }
        if(model.DescriptionTwo.isEmpty()||model.DescriptionTwo.equals("N/A")
            ||model.DescriptionTwo.equals("n/a")){
            holder.itemView.tv_part_description.visibility = View.GONE
        }else {
            holder.itemView.tv_part_description.visibility = View.VISIBLE
            holder.itemView.tv_part_description.text = model.DescriptionTwo
            holder.itemView.swipe.showMode = SwipeLayout.ShowMode.PullOut
        }

        holder.itemView.swipe.addDrag(

            SwipeLayout.DragEdge.Right,
            holder.itemView.swipe
                .findViewById(R.id.bottom_wrapper)

        )


        holder.itemView.swipe.addSwipeListener(object : SwipeLayout.SwipeListener {

            override fun onOpen(layout: SwipeLayout?) {


            }

            override fun onUpdate(layout: SwipeLayout?, leftOffset: Int, topOffset: Int) {

            }

            override fun onStartOpen(layout: SwipeLayout?) {
             //   hide.hideSwipingArrow()
            }

            override fun onStartClose(layout: SwipeLayout?) {
            }

            override fun onHandRelease(layout: SwipeLayout?, xvel: Float, yvel: Float) {

            }

            override fun onClose(layout: SwipeLayout?) {

            }

        })
        holder.itemView.bookmark.setOnClickListener {
            // saveToDatabase(model,context)
            var modelBookMark = BookMark()
            modelBookMark.id = model.met_box_id
            modelBookMark.PartNumber = model.PartNumber
            modelBookMark.ProductID = model.ProductID
            modelBookMark.Diameter = model.Diameter
            modelBookMark.type = "0"
            modelBookMark.Description = model.bookmark_desc
            modelBookMark.BoxQuantity = model.BoxQuantity
            modelBookMark.ProductImage = Constants.getProductImageForBookmark(model.ProductID)
            Constants.BookMarkItem(context, modelBookMark)
            holder.itemView.swipe.toggle(true)

            // GetProduct(context, modelBookMark).execute()
        }

        holder.itemView.email.setOnClickListener {
            val text =
                "\n\n# PartNumber - " + model.PartNumber + "\n #Description - " + model.bookmark_desc + "\n #Order quantity - " + model.BoxQuantity
            Constants.showComposer(context, text)
            holder.itemView.swipe.toggle(true)
        }
        mItemManger.bindView(holder.itemView, positionT)




    }

    override fun getSwipeLayoutResourceId(position: Int): Int {

        return R.id.swipe
    }


    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

    interface HideSwipe{
        fun hideSwipingArrow()
    }






}
